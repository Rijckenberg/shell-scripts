# This bash script currently only works in Fedora, not in Debian/Ubuntu


# install wget in Fedora
sudo dnf -y install wget
# install Teamviewer in Fedora
# switch from sys-whonix-15 NetVM to sys-firewall NetVM in order to download teamviewer package
read -p "Workaround: switch from sys-whonix-15 NetVM to sys-firewall NetVM in order to download teamviewer package - press ENTER to continue..."
wget https://download.teamviewer.com/download/linux/teamviewer.x86_64.rpm
sudo dnf -y install teamviewer.x86_64.rpm
sudo rpm -qi teamviewer
# install Google Chrome in Fedora
sudo dnf install fedora-workstation-repositories
sudo dnf config-manager --set-enabled google-chrome
sudo dnf install google-chrome-stable
sudo rpm -qi google-chrome-stable
# install gpg in Fedora
sudo dnf install gnupg2
sudo rpm -qi  gnupg2


###########################################################################################################################################################
# Enable/Disable DNS-over-TLS on Fedora
# Source:  https://gist.github.com/frafra/a1ca3d5eff81e6c6aebd8fab12b00695
# mkdir -p $HOME/bin && cd $_ && wget https://gist.githubusercontent.com/frafra/a1ca3d5eff81e6c6aebd8fab12b00695/raw/stubby-onoff.sh -O stubby-onoff && chmod +x $_ && cd -

# Ubuntu specific
sudo apt purge unbound
#sudo apt purge unbound avahi-daemon
#sudo apt purge xinetd nis yp-tools tftpd atftpd tftpd-hpa telnetd rsh-server rsh-redone-server


sudo apt install unattended-upgrades
sudo apt install stubby python-pip
sudo apt install crudini

# Fedora specific
sudo dnf remove unbound
sudo dnf install -y getdns-stubby crudini

# Fedora + Debian
LogTime=$(date '+%Y-%m-%d_%Hh%Mm%Ss')

sudo chattr -i /etc/resolv.conf
sudo chattr -i /etc/systemd/resolved.conf

cp /etc/resolv.conf $HOME/resolv.conf_$LogTime
cp /etc/nsswitch.conf $HOME/nsswitch.conf_$LogTime
cp /etc/systemd/resolved.conf $HOME/resolved.conf_$LogTime
cp /etc/network/interfaces $HOME/interfaces_$LogTime

# security harden the /etc/sysctl.conf file which is used to configure kernel parameters at runtime. 
# Linux reads and applies settings from /etc/sysctl.conf at boot time.
touch /tmp/sysctl.conf
echo '# Last config changes to file on 20191107' >> /tmp/sysctl.conf
echo '# Based on following site: http://bookofzeus.com/harden-ubuntu/hardening/sysctl-conf/' >> /tmp/sysctl.conf
echo '# Controls IP packet forwarding' >> /tmp/sysctl.conf
echo 'net.ipv4.ip_forward = 0' >> /tmp/sysctl.conf
echo '# IP Spoofing protection' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.rp_filter = 1' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.rp_filter = 1' >> /tmp/sysctl.conf
echo '# Ignore ICMP broadcast requests' >> /tmp/sysctl.conf
echo 'net.ipv4.icmp_echo_ignore_broadcasts = 1' >> /tmp/sysctl.conf
echo '# Disable source packet routing' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.accept_source_route = 0' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.all.accept_source_route = 0' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.accept_source_route = 0' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_source_route = 0' >> /tmp/sysctl.conf
echo '# Ignore send redirects' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.send_redirects = 0' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.send_redirects = 0' >> /tmp/sysctl.conf
echo '# Block SYN attacks' >> /tmp/sysctl.conf
echo 'net.ipv4.tcp_syncookies = 1' >> /tmp/sysctl.conf
echo 'net.ipv4.tcp_max_syn_backlog = 2048' >> /tmp/sysctl.conf
echo 'net.ipv4.tcp_synack_retries = 2' >> /tmp/sysctl.conf
echo 'net.ipv4.tcp_syn_retries = 5' >> /tmp/sysctl.conf
echo '# Log Martians' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.log_martians = 1' >> /tmp/sysctl.conf
echo 'net.ipv4.icmp_ignore_bogus_error_responses = 1' >> /tmp/sysctl.conf
echo '# Ignore ICMP redirects' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.accept_redirects = 0' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.all.accept_redirects = 0' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.accept_redirects = 0' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_redirects = 0' >> /tmp/sysctl.conf
echo '# Ignore Directed pings' >> /tmp/sysctl.conf
echo 'net.ipv4.icmp_echo_ignore_all = 1' >> /tmp/sysctl.conf
echo '# Accept Redirects? No, this is not router' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.secure_redirects = 0' >> /tmp/sysctl.conf
echo '# Log packets with impossible addresses to kernel log? yes' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.secure_redirects = 0' >> /tmp/sysctl.conf
echo '#Enable ExecShield protection' >> /tmp/sysctl.conf
echo 'kernel.exec-shield = 1' >> /tmp/sysctl.conf
echo 'kernel.randomize_va_space = 1' >> /tmp/sysctl.conf
echo '########## IPv6 networking start ##############' >> /tmp/sysctl.conf
echo '# Number of Router Solicitations to send until assuming no routers are present.' >> /tmp/sysctl.conf
echo '# This is host and not router' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.router_solicitations = 0' >> /tmp/sysctl.conf
echo '# Accept Router Preference in RA?' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_ra_rtr_pref = 0' >> /tmp/sysctl.conf
echo '# Learn Prefix Information in Router Advertisement' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_ra_pinfo = 0' >> /tmp/sysctl.conf
echo '# Setting controls whether the system will accept Hop Limit settings from a router advertisement' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_ra_defrtr = 0' >> /tmp/sysctl.conf
echo '#router advertisements can cause the system to assign a global unicast address to an interface' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.autoconf = 0' >> /tmp/sysctl.conf
echo '#how many neighbor solicitations to send out per address?' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.dad_transmits = 0' >> /tmp/sysctl.conf
echo '# How many global unicast IPv6 addresses can be assigned to each interface?' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.max_addresses = 1' >> /tmp/sysctl.conf
echo '########## IPv6 networking ends ##############' >> /tmp/sysctl.conf

sudo cp /tmp/sysctl.conf /etc/sysctl.conf
# Apply new settings
sudo sysctl -p

# configure DNS server on Ubuntu 20.04 LTS:
touch /tmp/interfaces
cp /etc/network/interfaces /tmp/interfaces
grep -v dns /tmp/interfaces > /tmp/interfaces.0
grep -v nameservers  /tmp/interfaces.0 > /tmp/interfaces.1
grep -v search  /tmp/interfaces.1 > /tmp/interfaces.2
grep -v options  /tmp/interfaces.2 > /tmp/interfaces.3
#echo 'dns-nameservers 9.9.9.9 2620:fe::fe' >> /tmp/interfaces.3
echo 'dns-nameservers 127.0.0.1' >> /tmp/interfaces.3
echo 'dns-search dnsknowledge.com' >> /tmp/interfaces.3
echo 'dns-options rotate' >> /tmp/interfaces.3
sudo cp /tmp/interfaces.3  /etc/network/interfaces

# enable systemd caching DNS resolver
rm /tmp/nsswitch.conf
rm /tmp/nsswitch.conf.1
cp /etc/nsswitch.conf /tmp/nsswitch.conf
grep -v hosts  /tmp/nsswitch.conf > /tmp/nsswitch.conf.1
# dns must be mentioned in next line, or else wget does not work
echo 'hosts: files mdns4_minimal [NOTFOUND=return] resolv dns myhostname mymachines' >> /tmp/nsswitch.conf.1
sudo cp /tmp/nsswitch.conf.1 /etc/nsswitch.conf

# set DNS server to 127.0.0.1
rm /tmp/resolved.conf
rm /tmp/resolved.conf.1
cp /etc/systemd/resolved.conf /tmp/resolved.conf
grep -v DNS  /tmp/resolved.conf > /tmp/resolved.conf.1
#echo 'DNS=9.9.9.9' >> /tmp/resolved.conf.1
echo 'DNS=127.0.0.1' >> /tmp/resolved.conf.1
echo 'DNSSEC=yes' >> /tmp/resolved.conf.1
sudo cp /tmp/resolved.conf.1 /etc/systemd/resolved.conf
sudo systemd-resolve --flush-caches
sudo systemctl restart systemd-resolved
sudo systemd-resolve --flush-caches
sudo systemd-resolve  --status

cp /etc/resolv.conf /tmp/resolv.conf
grep -v nameserver  /tmp/resolv.conf > /tmp/resolv.conf.1
grep -v domain  /tmp/resolv.conf.1 > /tmp/resolv.conf.2
grep -v options  /tmp/resolv.conf.2 > /tmp/resolv.conf.3
echo 'nameserver 127.0.0.1' >> /tmp/resolv.conf.3
# echo 'nameserver 2620:fe::fe' >> /tmp/resolv.conf.3
echo 'domain dnsknowledge.com' >> /tmp/resolv.conf.3
echo 'options rotate' >> /tmp/resolv.conf.3
sudo cp /tmp/resolv.conf.3  /etc/resolv.conf
sudo chattr +i /etc/resolv.conf
sudo chattr +i /etc/systemd/resolved.conf

conf=/etc/NetworkManager/NetworkManager.conf
sudo cp $conf $conf.bak
sudo systemctl stop NetworkManager
echo "Disable stubby"
sudo crudini --del $conf main dns
sudo systemctl stop stubby
sudo systemctl disable stubby
echo "Enable stubby"
sudo crudini --set $conf main dns none
sudo systemctl start stubby
sudo systemctl enable stubby

sudo service resolvconf start
sudo systemctl start NetworkManager

systemctl status stubby
sudo netstat -lnptu | grep stubby
sudo netstat -lnptu | grep systemd-resolve

# It is probably also necessary to manually set
# the DNS server to 127.0.0.1 in the router's configuration
# and in the NetworkManager GUI

# Then reboot your PC to enable new DNS over TLSv1.2 encrypted communications
# Use wireshark application and capture encrypted DNS packages on port 853 
# There should be no more DNS handshakes on port 53 and only encrypted DNS handshakes on port 853

# Test DNSSEC validation using dig command-line tool
# See: https://docs.menandmice.com/display/MM/How+to+test+DNSSEC+validation
dig pir.org +dnssec +multi
host dnsknowledge.com


# show most recently installed Fedora packages
rpm -qa --last | head
###########################################################################################################################################################