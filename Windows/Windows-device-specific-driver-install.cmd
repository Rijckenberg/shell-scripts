REM Authors: Mark Rijckenberg and ChatGPT

REM Last modification date: 2023/3/3

REM Use this script in conjunction with following other tools:

REM Virus cleaner -> https://bmrf.org/repos/tron/ 

REM Software installer -> https://ninite.com

REM HP Support assistant -> https://support.hp.com/us-en/help/hp-support-assistant 

REM Driver pack installer -> http://driveroff.net/sam/ 

REM Snappy driver installer -> https://sdi-tool.org/

REM Check if the system manufacturer is Dell
wmic computersystem get manufacturer | find "Dell" > nul
if %errorlevel% equ 0 (
    echo Dell laptop detected. Installing Dell utilities...
    REM Install Dell utilities using chocolatey
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dell-update"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dell-system-update"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dellcommandupdate"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dellcommandupdate-uwp"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y supportassist"
) else (
    echo This script is only meant for Dell laptops.
    echo Exiting script.
)

REM Check if the system manufacturer is Hewlett-Packard
wmic computersystem get manufacturer | find "Hewlett-Packard" > nul
if %errorlevel% equ 0 (
    echo HP laptop detected. Installing hpsupportassistant...
    REM Install hpsupportassistant using chocolatey
 @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hpsupportassistant"
) else (
    echo This script is only meant for HP laptops.
    echo Exiting script.
)

REM Check if the system manufacturer is Hewlett-Packard
wmic computersystem get manufacturer | find "HP " > nul
if %errorlevel% equ 0 (
    echo HP laptop detected. Installing hpsupportassistant...
    REM Install hpsupportassistant using chocolatey
 @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hpsupportassistant"
) else (
    echo This script is only meant for HP laptops.
    echo Exiting script.
)


REM Check if an Intel network card is connected
wmic path win32_PnPEntity get name | findstr "Intel" | findstr "Ethernet" > nul
if %errorlevel% equ 0 (
    echo Intel network card is detected.
    REM Install the Intel network card chocolatey package
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-dsa"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-network-drivers-win10"
) else (
    echo No Intel network card detected.
    echo Exiting script.
)


REM Check if an Intel graphics card is connected
wmic path win32_PnPEntity get name | findstr "Intel" | findstr "HD graphics"  > nul
if %errorlevel% equ 0 (
    echo Intel graphics card detected.
    REM Install the Intel graphics card chocolatey package
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-dsa"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-graphics-driver"
) else (
    echo No Intel graphics card detected.
    echo Exiting script.
)

REM Check if Intel Management Engine is installed
wmic path win32_PnPEntity get name | findstr "Intel" | findstr "Management Engine"  > nul
if %errorlevel% equ 0 (
    echo Intel Management Engine is detected.
    REM Install the Intel Management Engine chocolatey package
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-me-drivers"
) else (
    echo No Intel Management Engine detected.
    echo Exiting script.
)

REM Check if Intel Dual Band Wireless is installed
wmic path win32_PnPEntity get name | findstr "Intel" | findstr "Band Wireless" > nul
if %errorlevel% equ 0 (
    echo Intel Dual Band Wireless is detected.
    REM Install the IntelDual Band Wireless chocolatey package
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-dsa"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-proset-drivers"
) else (
    echo No Intel Dual Band Wireless detected.
    echo Exiting script.
)


REM Check if IntelPROSet is installed
wmic path win32_PnPEntity get name | findstr "Intel" | findstr "PROSet" > nul
if %errorlevel% equ 0 (
    echo Intel PROSet is detected.
    REM Install the Intel PROSet chocolatey package
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-dsa"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-proset-drivers"
) else (
    echo No Intel PROSet detected.
    echo Exiting script.
)

REM Check if the system manufacturer is Lenovo
wmic computersystem get manufacturer | find "LENOVO" > nul
if %errorlevel% equ 0 (
    echo Lenovo laptop detected. Installing lenovo-thinkvantage-system-update...
    REM Install lenovo-thinkvantage-system-update using chocolatey
 @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y lenovo-thinkvantage-system-update"
) else (
    echo This script is only meant for Lenovo laptops.
    echo Exiting script.
)

REM Check if an NVIDIA Geforce graphics card is installed
wmic path win32_VideoController get name | find "NVIDIA GeForce" > nul
if %errorlevel% equ 0 (
    echo NVIDIA Geforce graphics card detected.
 @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y geforce-game-ready-driver"
    )
) else (
    echo No NVIDIA Geforce graphics card detected.
    echo Exiting script.
)

REM Check if a Plantronics audio device is connected
wmic path win32_PnPEntity get name | find "Plantronics" > nul
if %errorlevel% equ 0 (
    echo Plantronics audio device is detected.
    REM Install the Plantronics Hub chocolatey package
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y plantronicshub"
) else (
    echo No Plantronics audio device detected.
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco uninstall -y plantronicshub"
    echo Exiting script.
)

REM Check if Realtek High Definition Audio is detected
wmic path win32_PnPEntity get name | find "Realtek High Definition Audio" > nul
if %errorlevel% equ 0 (
    echo Realtek High Definition Audio detected.
    REM Install the Realtek High Definition Audio chocolatey package
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y realtek-hd-audio-driver"
) else (
    echo No Realtek High Definition Audio detected.
    echo Exiting script.
)

pause

REM Printer section:

REM Check if any HP printers are installed
wmic printer where "DriverName like '%HP%'" get Name /value > nul
if %errorlevel% equ 0 (
    
    REM Install hp-universal-print-driver-pcl package using Chocolatey
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hp-universal-print-driver-pcl"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hppark"
)

REM Check if any HP printers are installed
wmic printer where "Name like '%HP%'" get Name /value > nul
if %errorlevel% equ 0 (
    
    REM Install hp-universal-print-driver-pcl package using Chocolatey
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hp-universal-print-driver-pcl"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hppark"
)

REM Check if any KONICA MINOLTA printers are installed
wmic printer where "Name like '%KONICA%'" get Name /value > nul
if %errorlevel% equ 0 (
    
    REM Install kmupd package using Chocolatey
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y kmupd"
)

REM Check if any KONICA MINOLTA printers are installed
wmic printer where "Name like '%MINOLTA%'" get Name /value > nul
if %errorlevel% equ 0 (
    
    REM Install kmupd package using Chocolatey
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y kmupd"
)

REM Check if any Samsung printers are installed
wmic printer where "DriverName like '%Samsung%'" get Name /value > nul
if %errorlevel% equ 0 (
    
    REM Install supd2 package using Chocolatey
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y supd2"
)

REM Check if any Samsung printers are installed
wmic printer where "Name like '%Samsung%'" get Name /value > nul
if %errorlevel% equ 0 (
    
    REM Install supd2 package using Chocolatey
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y supd2"
)

REM Check if any Xerox printers are installed
wmic printer where "DriverName like '%Xerox%'" get Name /value > nul
if %errorlevel% equ 0 (
    
    REM Install xeroxupd package using Chocolatey
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y xeroxupd"
)

REM Check if any Xerox printers are installed
wmic printer where "Name like '%Xerox%'" get Name /value > nul
if %errorlevel% equ 0 (
    
    REM Install xeroxupd package using Chocolatey
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y xeroxupd"
)

pause

REM ************************************************************************************************************************
echo "Update all currently installed chocolatey packages:"
REM ************************************************************************************************************************
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ChocolateyPackageUpdater"
cup -y all
pause

REM *********************************************************************************************
echo "List all currently installed packages using winget package manager:"
REM *********************************************************************************************
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "winget list"
pause

REM *********************************************************************************************
echo "List all currently installed packages using chocolatey package manager:"
REM *********************************************************************************************
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco list --local-only"
pause

***************************************************************************************************
REM Use sysinternals utility called "psinfo -s" to show list of installed applications:
REM Combine with grep command to find a specific application
REM *********************************************************************************************
psinfo -s
pause 

REM *********************************************************************************************
REM Show list of installed applications using Get-AppxPackage:
REM *********************************************************************************************
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "Get-AppxPackage | Format-Table -property Name,Version,InstallLocation,Status -autoSize"
pause 

