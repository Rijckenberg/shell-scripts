# Authors: Mark Rijckenberg, Kugane and ChatGPT 3.5 Turbo

# Last modification date: 2024/2/16

# MIT License

# Copyright (c) 2022 Kugane

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

# *********************************************************************************************
echo "Before continuing, please close all currently running applications, including those running in the background"
echo "However, for security purposes, please keep your antivirus/firewall/security suite software running"
echo "You can search for winget packages here: https://winget.run/ "
# *********************************************************************************************
pause

Get-AppxPackage Microsoft.Winget.Source | Remove-AppxPackage
Get-AppxPackage Microsoft.DesktopAppInstaller | Remove-AppxPackage

### Install WinGet ###
# Based on this gist: https://gist.github.com/crutkas/6c2096eae387e544bd05cde246f23901
$hasPackageManager = Get-AppxPackage -Name 'Microsoft.Winget.Source' | Select Name, Version
$hasVCLibs = Get-AppxPackage -Name 'Microsoft.VCLibs.140.00.UWPDesktop' | Select Name, Version
$hasXAML = Get-AppxPackage -Name 'Microsoft.UI.Xaml.2.7*' | Select Name, Version
$hasAppInstaller = Get-AppxPackage -Name 'Microsoft.DesktopAppInstaller' | Select Name, Version
$DesktopPath = [System.Environment]::GetFolderPath([System.Environment+SpecialFolder]::Desktop)
$errorlog = "winget_error.log"


function install_winget {
    Clear-Host
    Write-Host -ForegroundColor Yellow "Checking if WinGet is installed"
    if (!$hasPackageManager) {
            if ($hasVCLibs.Version -lt "14.0.30035.0") {
                Write-Host -ForegroundColor Yellow "Installing VCLibs dependencies..."
                Add-AppxPackage -Path "https://aka.ms/Microsoft.VCLibs.x64.14.00.Desktop.appx"
                Write-Host -ForegroundColor Green "VCLibs dependencies successfully installed."
            }
            else {
                Write-Host -ForegroundColor Green "VCLibs is already installed. Skip..."
            }
            if ($hasXAML.Version -lt "7.2203.17001.0") {
                Write-Host -ForegroundColor Yellow "Installing XAML dependencies..."
                Add-AppxPackage -Path "https://github.com/Kugane/winget/raw/main/Microsoft.UI.Xaml.2.7_7.2203.17001.0_x64__8wekyb3d8bbwe.Appx"
                Write-Host -ForegroundColor Green "XAML dependencies successfully installed."
            }
            else {
                Write-Host -ForegroundColor Green "XAML is already installed. Skip..."
            }
            if ($hasAppInstaller.Version -lt "1.16.12653.0") {
                Write-Host -ForegroundColor Yellow "Installing WinGet..."
    	        $releases_url = "https://api.github.com/repos/microsoft/winget-cli/releases/latest"
    		    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    		    $releases = Invoke-RestMethod -Uri "$($releases_url)"
    		    $latestRelease = $releases.assets | Where-Object { $_.browser_download_url.EndsWith("msixbundle") } | Select-Object -First 1
    		    Add-AppxPackage -Path $latestRelease.browser_download_url
                Write-Host -ForegroundColor Green "WinGet successfully installed."
            }
        # Fix for hangup on first start
        winget search clamav --accept-source-agreements
    }
    else {
        Write-Host -ForegroundColor Green "WinGet is already installed. Skip..."
        # Fix for hangup on first start
        winget search clamav --accept-source-agreements
        }
    Pause
    Clear-Host
}

install_winget
winget install winget

winget install fastfetch
winget install --id Microsoft.Powershell --source winget
# winget install WolframEngine
# winget install -e --id WolframResearch.WolframEngine
# install ludusavi - save game extractor tool
# https://github.com/mtkennerly/ludusavi
winget install -e --id mtkennerly.ludusavi

# **************************************************************************************************************************
echo "Update all the apps to their latest versions using winget package manager"
# ****************************************************************************************************************************
# Query application updates
winget upgrade  --include-unknown
# Upgrade applications
winget upgrade --all --include-unknown
pause


# *********************************************************************************************
echo "List all currently installed packages using winget package manager:"
# *********************************************************************************************
winget list
pause

# *********************************************************************************************
echo "List all currently installed packages using chocolatey package manager:"
# *********************************************************************************************
choco list --local-only
pause

# ***************************************************************************************************
echo "Use sysinternals utility called psinfo -s to show list of installed applications:"
# Combine with grep command to find a specific application
# *********************************************************************************************
psinfo -s
pause 

# *********************************************************************************************
echo "Show list of installed applications using Get-AppxPackage:"
# *********************************************************************************************
Get-AppxPackage | Format-Table -property Name,Version,InstallLocation,Status -autoSize
pause

# *********************************************************************************************
echo "Running winget install tool from  https://christitus.com/one-tool-for-everything/ "
# *********************************************************************************************
irm christitus.com/win | iex
# alternative command:  [Net.ServicePointManager]::SecurityProtocol=[Net.SecurityProtocolType]::Tls12;iex(New-Object Net.WebClient).DownloadString('https://raw.githubusercontent.com/ChrisTitusTech/winutil/main/winutil.ps1')
pause
