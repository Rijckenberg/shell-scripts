REM Authors: Mark Rijckenberg and ChatGPT 3.5 Turbo

REM Last modification date: 2023/3/31

REM Use this script in conjunction with following other tools:

REM Virus cleaner -> https://bmrf.org/repos/tron/ 

REM Software installer -> https://ninite.com

REM HP Support assistant -> https://support.hp.com/us-en/help/hp-support-assistant 

REM Driver pack installer -> http://driveroff.net/sam/ 

REM Snappy driver installer -> https://sdi-tool.org/

@echo off

del index.html*

wget https://www.collaboraoffice.com/downloads/Collabora-Office-23-Snapshot/Windows/

for /f "tokens=6 delims=>< " %%a in ('findstr x86-64 index.html') do set URLLONG=%%a

echo %URLLONG%

FOR /F "tokens=2 delims==%%" %%i IN ("%URLLONG%") DO set "URLSHORT=%%i"

echo %URLSHORT%

set URLSHORT=%URLSHORT:"=%

echo %URLSHORT%

REM Set the URL of the latest snapshot
set URL=https://www.collaboraoffice.com/downloads/Collabora-Office-23-Snapshot/Windows/%URLSHORT%

echo %URL%

REM Download the latest snapshot to a temporary file
echo Downloading latest snapshot...
powershell -command "(New-Object Net.WebClient).DownloadFile('%URL%', '%TEMP%\%URLSHORT%')"

REM Set the path to the temporary file
set INSTALLER_FILE=%TEMP%\%URLSHORT%

REM Check if the temporary file exists
if not exist %INSTALLER_FILE% (
    echo Installer file not found!
    goto end
)

REM Run the installer in interactive mode
echo Installing Collabora Office...
msiexec /i %INSTALLER_FILE%

REM Check the installation status
if %errorlevel% neq 0 (
    echo Installation failed!
) else (
    echo Installation successful!
)

:end
pause


