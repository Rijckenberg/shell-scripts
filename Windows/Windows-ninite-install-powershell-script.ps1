# Author: Mark Rijckenberg and ChatGPT

# Last modification date: 2023/3/5

# Use this script in conjunction with following other tools:

# Virus cleaner -> https://bmrf.org/repos/tron/ 

# Software installer -> https://ninite.com

# HP Support assistant -> https://support.hp.com/us-en/help/hp-support-assistant 

# Driver pack installer -> http://driveroff.net/sam/ 

# Snappy driver installer -> https://sdi-tool.org/

# Following .cmd Windows command script has been tested and works in Windows HVM in Qubes 

# OS 3.2 or in normal install of Windows 7/8.1/10 Pro 64-bit without Qubes OS.

# In Windows 10 Pro 64-bit, this script is compatible with NordVPN, but only if all

# network interfaces are configured to use DNS server 9.9.9.9.

# This script is not compatible with DNS over HTTPS.

# In Qubes OS, connections to TCP ports 80 and 443 need to be open in Windows HVM

# Furthermore, Windows HVM needs to use sys-firewall as NetVM, not whonix

# Recommended to run this script on SSDs, not on conventional hard disks....

#
# This script is designed to use Ninite's app URL paths to specify which apps are downloaded when the script runs ninite.exe.
#
#############
# HOW TO USE#
#############
#
# Edit the $url variable using the app URL path of each application you wish to install. 
# https://ninite.com/urlpath1-urlpath2-urlpath3/ninite.exe
# The order in which the URL paths are placed in the URL does not matter.
# I have included a list of all applications Ninite offers and their URL path, see below.
 
################################
# APPLICATION     APP URL PATH #
################################
 
#7-Zip -7zip
#AIMP -aimp
#Air -air
#Audacity -audacity
#Avast -avast
#AVG -avg
#Blender -blender
#CDBurnerXP -cdburnerxp
#CCCP -cccp
#Classic Start -classicstart
#Discord -discord
#Dropbox -dropbox
#Eclipse -eclipse
#Everything -everything
#Evernote -evernote
#FastStone -faststone
#FileZilla -filezilla
#Firefox -firefox
#Foobar2000 -foobar
#Foxit Reader -foxit
#GIMP -gimp
#Glary -glary
#GOM -gom
#Google Backup -googlebackupandsync
#Google Chrome -chrome
#Google Earth -googleearth
#HandBrake -handbrake
#Inkscape -inkscape
#InfraRecorder -infrarecorder
#IrfanView -irfanview
#Itunes -itunes
#JDK 8 -jdk8
#JDK x64 8 -jdkx8
#Java 8 -java8
#KeePass 2 -keepass2
#K-Lite Codecs -klitecodecs
#Krita -krita
#Launchy -launchy
#LibreOffice -libreoffice
#Malwarebytes -malwarebytes
#MediaMonkey -mediamonkey
#Mozy -mozy
#MS Essentials -essentials
#Notepad++ -notepadplusplus
#NV Access -nvda
#OneDrive -onedrive
#Opera -operaChromium
#OpenOffice -openoffice
#Paint.NET -paint.net
#PeaZip -peazip
#Pidgin -pidgin
#PDFCreator -pdfcreator
#PuTTy -putty
#qBitTorrent -qbittorrent
#RealVNC -realvnc
#Revo -revo
#ShareX -sharex
#Shockwave -shockwave
#Silverlight -silverlight
#Skype -skype
#SPERAntiSpyWare -super
#Spotify -spotify
#SugarSync -sugarsync
#SumatraPDF -sumatrapdf
#Teamviewer 15 -teamviewer15
#TeraCopy -teracopy
#Thunderbird -thunderbird
#Trillian -trillian
#VLC -vlc
#Vis Studio
 
 
###################### Script Starts Here #############################
# Edit the URL using the URL paths listed above. 
# EX: to download Firefox, Chrome, and 7-Zip change the URL to "https://ninite.com/chrome-firefox-7zip/ninite.exe"
$url = "https://ninite.com/7zip-classicstart-edge-firefox-foxit-chrome-googleearth-greenshot-irfanview-itunes-klitecodecs-libreoffice-openoffice-opera-pdfcreator-qbittorrent-teamviewer15-vlc/ninite.exe"
$output = "C:\Scripts\ninite.exe"
 
# Creates Scripts directory in the root of C:
New-Item C:\Scripts\ -ItemType Directory
 
# Calls upon Ninite URL to grab .exe
Invoke-WebRequest -Uri $url -OutFile $output
 
# Starts Ninite.exe
Start-Process -FilePath "C:\Scripts\ninite.exe"
