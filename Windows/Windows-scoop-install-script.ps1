# Creation date: 2024/5/4
# See https://bjansen.github.io/scoop-apps/main/neofetch/
# scoop package repositories do not have Belgium eid-mw software (yet)
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression
scoop bucket add extras
scoop bucket add games
scoop bucket add main
scoop bucket add nerd-fonts
scoop bucket add nirsoft
scoop bucket add nonportable
scoop bucket add versions
scoop install main/7zip
scoop install main/bind
scoop install extras/bitwarden
scoop install extras/bleachbit
scoop install main/bottom
scoop install main/btop
scoop install extras/bulk-crap-uninstaller
scoop install versions/chromium-dev
scoop install main/coreutils
scoop install extras/cpu-z
scoop install main/curl
scoop install main/czkawka
scoop install games/epic-games-launcher
scoop install main/fastfetch
scoop install versions/firefox-nightly
scoop install main/git
scoop install main/git-branchless
scoop install extras/gitkraken
scoop install games/goggalaxy
scoop install extras/googlechrome
scoop install extras/gpu-z
scoop install main/grep
scoop install extras/grepwin
scoop install main/gsudo
scoop install extras/hashcheck
scoop install games/heroic-games-launcher
scoop install extras/hostsman
scoop install games/legendary
scoop install extras/librewolf
scoop install extras/ludusavi
scoop install main/neofetch
scoop install nonportable/nvidia-display-driver-dch-np
scoop install extras/nvcleanstall
scoop install nonportable/open-shell-np
scoop install extras/optimizer
scoop install main/osquery
scoop install extras/patchcleaner
scoop install main/pipx
scoop install extras/playnite
scoop install nonportable/portmaster-np
scoop install games/rare
scoop install main/rga
scoop install nirsoft/securitysoftview
scoop install extras/snappy-driver-installer-origin
scoop install extras/speccy
scoop install games/steam
scoop install versions/systeminformer-nightly
scoop install extras/thorium
scoop install extras/tinynvidiaupdatechecker
scoop install main/ugrep
scoop install main/unzip
scoop install extras/vcredist-aio
scoop install main/wget
scoop install main/wget2
scoop install main/winfetch
scoop install main/zoxide