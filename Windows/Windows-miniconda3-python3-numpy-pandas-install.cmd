REM Authors: Mark Rijckenberg and ChatGPT 3.5 Turbo

REM Last modification date: 2023/4/5

REM Prerequisite: Windows 10 64-bit or newer

REM Prerequisite: https://chocolatey.org/install

REM run the following commands using cmd.exe with Administrator privileges:

@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python get-pip.py

@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "Invoke-WebRequest https://bootstrap.pypa.io/get-pip.py -OutFile get-pip.py"

python get-pip.py

REM choco feature enable -n useFipsCompliantChecksums

REM choco install miniconda3 

REM run the following commands with Administrator privileges

REM via Start menu/Programs/Anaconda3/Anaconda command line prompt:

REM #########################################################################
REM Never use pip to install spyder application.
REM never use pip to upgrade python packages to avoid corrupting spyder install
REM Instead, use conda to install spyder application.
REM Only use conda in combination with spyder and python3.6+
REM ##########################################################################
REM following package requires at least 700 MB of free disk space:
REM conda config --add channels conda-forge

conda install -y astroML
conda install -y astropy
conda install -y beautifulsoup4
conda install -y bcolz
conda install -y boto3
conda install -y configobj
conda install -y cython
conda install -y dvc
conda install -y elasticsearch
conda install -y filelock
conda install -y fire

pip install gfpgan

conda install -y ipython
conda install -y ipywidgets
conda install -y instantmusic
conda install -y jupyter
conda install -y jsonschema
REM conda install -y keras

pip install lama-cleaner

conda install -y matplotlib
conda install -y mistune
conda install -y mock
conda install -y networkx
conda install -y nibabel
conda install -y numpy

pip install openplayground

conda install -y pandas
REM do not install pattern package, because it causes severe downgrade of anaconda
REM and python package versions: conda install -y pattern 
REM conda install -y pattern
conda install -y Pillow-SIMD
conda install -y pip

pip install --upgrade pip

conda install -y py4j
conda install -y pytest
conda install -y pytest-mock
conda install -y qgrid

pip install realesrgan
pip install rembg

conda install -y runipy
conda install -y scikit-image
conda install -y scikit-learn
conda install -y scikit-learn-intelex
conda install -y scipy
conda install -y scipy-data_fitting
conda install -y seaborn
conda install -y setuptools
conda install -y socli
conda install -y spectral
conda install -y spyder
conda install -y statsmodels
conda install -y streamlink
conda install -y sympy
REM conda install -y tensorfx
REM conda install -y tensorflow
REM conda install -y tensorflow-gpu
conda install -y tornado
conda install -y tqdm
conda install -y traitlets
conda install -y visdom
conda install -y spyder

conda update -y conda
conda update -y cython
conda update -y matplotlib
conda update -y numpy
conda update -y pandas
conda update -y pip
conda update -y scipy
conda update -y spyder
conda update -y sympy

REM Check if all dependencies are installed by inspecting

REM Spyder/Help/Dependencies window in Spyder application
