REM Authors: Mark Rijckenberg and ChatGPT 3.5 Turbo

REM Last modification date: 2025/2/18

REM Use this script in conjunction with following other tools:

REM Virus cleaner -> https://bmrf.org/repos/tron/ 

REM Software installer -> https://ninite.com

REM HP Support assistant -> https://support.hp.com/us-en/help/hp-support-assistant 

REM Driver pack installer -> http://driveroff.net/sam/ 

REM Snappy driver installer -> https://sdi-tool.org/

REM Following .cmd Windows command script has been tested and works in Windows HVM in Qubes 

REM OS 3.2 or in normal install of Windows 7/8.1/10 Pro 64-bit without Qubes OS.

REM In Windows 10 Pro 64-bit, this script is compatible with NordVPN, but only if all

REM network interfaces are configured to use DNS server 9.9.9.9.

REM This script is not compatible with DNS over HTTPS.

REM In Qubes OS, connections to TCP ports 80 and 443 need to be open in Windows HVM

REM Furthermore, Windows HVM needs to use sys-firewall as NetVM, not whonix

REM Recommended to run this script on SSDs, not on conventional hard disks....

REM This script installs 62 Windows applications and requires 11 GB of free disk space to install

REM Please run following commands in Windows command line (using cmd, NOT Powershell) as administrator:

echo "TO DO: In the RStudio interface, please click on"

echo "'Help', then click on 'Check for Updates' and then click on 'Quit and Download' to get the newest version of RStudio"

echo "TO DO:Replace 127.0.0.1 with 0.0.0.0 in Windows hosts file after running HostsMan application"

echo "in C:\ProgramData\chocolatey\lib\hostsman\"

echo "TO DO:Manually Configure network adapter to use DNS server 127.0.0.1 after installing and configuring Stubby DNS resolver"

echo on

pause
REM ***************************************************************************************************************************************************
echo "Copy newest version of shell scripts and chocolateypackages file to local drive:"
REM ***************************************************************************************************************************************************
mkdir C:\temp
cd c:\temp
git clone https://gitlab.com/Rijckenberg/shell-scripts
cd shell-scripts
git pull
REM ***************************************************************************************************************************************************
echo "Update all currently installed chocolatey packages:"
REM ***************************************************************************************************************************************************
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ChocolateyPackageUpdater"
cup -y all
pause

REM ***************************************************************************************************************************************************
echo "Show list of locally installed packages - installed using chocolatey package manager for Windows:"
REM ***************************************************************************************************************************************************
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco feature enable -n useFipsCompliantChecksums"
choco list -lo

echo "TO DO: In the RStudio interface, please click on"

echo "'Help', then click on 'Check for Updates' and then click on 'Quit and Download' to get the newest version of RStudio"

echo "TO DO:Replace 127.0.0.1 with 0.0.0.0 in Windows hosts file after running HostsMan application"

echo "in C:\ProgramData\chocolatey\lib\hostsman\"

echo "TO DO:Manually Configure network adapter to use DNS server 127.0.0.1 after installing and configuring Stubby DNS resolver"

echo on

pause

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco feature enable -n useFipsCompliantChecksums" 

REM ***************************************************************************************************************************************************
echo "Install new chocolatey packages:"
REM ***************************************************************************************************************************************************

REM *** Not working yet - List is too long as input for choco :-/
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "Get-PackageProvider -Name Chocolatey"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "Get-PackageSource"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "$file = Get-ChildItem chocolateypackages ; Get-Content $file| ForEach-Object -process { choco install $_ } "

REM Install of packages in alphabetical order:
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y 0patch"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y 7zip.portable"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y adblockplusie"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y adobedigitaleditions"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y adoptopenjdkjre"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y advanced-codecs"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y amazongames"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y amazon-workspaces"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y amd-ryzen-master"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y amd-ryzen-chipset"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y anydesk"
REM Install Android Studio to install Android TV + Android 13 in a VM in Windows 10:
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y androidstudio"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y axel"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y bind-toolsonly"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y bitwarden"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y bleachbit"
REM BlueStacks - Android emulator used for gaming:
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y bluestacks"
REM Bottom - customizable cross-platform graphical process/system monitor for the terminal.
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y bottom"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y brave"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y bulk-crap-uninstaller"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ccleaner"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y cdburnerxp"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y chocolatey"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ChocolateyGUI"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ChocolateyPackageUpdater"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y chromium"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y citrix-workspace"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y clamav"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y cmder"
REM Get Collabora Office here:  
REM https://www.collaboraoffice.com/downloads/Collabora-Office-23-Snapshot/Windows/
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y cpu-z"
REM croc - Easily and securely send things from one computer to another
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y croc"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y curl"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y cygwin"
REM Czkawka (tch•kav•ka, hiccup) is a simple, fast and free app to remove unnecessary files from your computer
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y czkawka"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dotnet4.7.1"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dotnetfx"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dotnet"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y drawio"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y drivereasyfree"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dumo"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y eid-belgium"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y eid-belgium-viewer"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y epicgameslauncher"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y fastfetch"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y filezilla"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y flameshot"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y flashplayerplugin"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y fluent-terminal"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y FoxitReader"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y freeoffice"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y geforce-game-ready-driver"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y geogebra6"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y geogebra-geometry"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y git"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y gitkraken"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y glasswire"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y gnumeric"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y goggalaxy"
REM Google Chrome browser is required when using Belgian eid card reader with opensc:
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y GoogleChrome"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y googleearth"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y gpu-z"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y greenshot"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y grep"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y grepwin"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y gsudo"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hashcheck"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hitmanpro"
REM Configure HostsMan - already installed via Windows-choco-install-script.cmd
REM - to only use "Peter Lowe's AdServers List" and "Cameleon" as up-to-date Hosts sources:
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hostsman"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y inkscape"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y intel-dsa"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y javaruntime"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y jre8"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y k-litecodecpackmega"
REM Legacy games launcher gives access to certain games that were awarded for free via Amazon Prime Gaming:
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y legacy-games-launcher"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y legendary"
REM Best free Office suite in 2022:
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y libreoffice-fresh"
REM Librewolf - a replacement for Firefox webbrowser:
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y librewolf"
REM ludusavi - Backup tool for PC game saves - https://github.com/mtkennerly/ludusavi
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ludusavi"
REM magic-wormhole: Securely transfer data between computers
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y magic-wormhole"
REM Enable Malwarebytes Windows firewall:
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y malwarebytes"
REM MATLAB Compiler Runtime R2022b 9.13.0.2166757
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y mcr-r2022b"
REM Microsoft Mathematics 4.0.1108.0000
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y microsoftmathematics"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y microsoft-r-open"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y microsoft-teams"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y mingw"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y mingw-get"
REM Use Miniconda in combination with PyScript.com website:
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y miniconda3"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y mpc-qt"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y msvisualcplusplus2012-redist"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y msvisualcplusplus2013-redist"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nilesoft-shell"
REM ninja - small build system with a focus on speed - required to build certain GPT/LLM AI models:
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ninja"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nodejs"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nomacs"
REM nteract is a literate coding environment that supports Python, R, JavaScript and other
REM Jupyter kernels.
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nteract.portable"
REM Use nushell in combination with starship
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nushell.portable"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nuget.commandline"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nugetpackageexplorer"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y obs-studio"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y office365proplus"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y okular"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y onlyoffice"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y open-shell"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y openoffice"
REM OpenSC software is required to support Belgian eid card readers:
REM OpenSC = Open source smart card tools and middleware. PKCS#11/MiniDriver/Tokend
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y opensc"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y openshot"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y openssl"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y openvpn"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y opera"
REM Origin is a digital distribution and digital rights management system from Electronic Arts.
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y origin"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y osquery"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y outlookconverter"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y outlookviewer"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y paint.net"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y pcsx2"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y pdf24"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y pdfsam.install"
REM Photoflare is a cross-platform image editor with an aim to balance between powerful features 
REM and a very friendly graphical user interface
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y photoflare"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y photorec"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y pip"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y playnite"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y powershell-core"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y program-install-and-uninstall-troubleshooter"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y python"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y qbittorrent"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y qemu"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y qemu-img"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y qemu-guest-agent"
REM Quarto is an open-source scientific and technical publishing system built on Pandoc:
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y quarto"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y r.project"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y r.studio"
REM rainmeter - Desktop customization tool
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y rainmeter"
REM RAPR package = Driver Store Explorer - see https://www.youtube.com/watch?v=R5RAgqB6YxM 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y rapr"
REM rare - GUI for legendary. An Epic Games Launcher open source alternative
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y rare"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y revo-uninstaller"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ripgrep-all"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y rockstar-launcher"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y runinbash"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y sandboxie"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y scribus"
REM Snappy Driver Installer Origin 1.12.9.749 = choco package "sdio"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y sdio"
REM Shotcut is a free, open source, cross-platform video editor.
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y shotcut"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y simplednscrypt"
REM SIW - System Information for Windows:
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y siw"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y skype"
REM sophiapp - The most powerful open source tweaker for fine-tuning Windows 10 and Windows 11.
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y sophiapp"
REM Speccy will give you detailed statistics on every piece of hardware in your computer.
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y speccy"
REM Use nushell in combination with starship
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y starship"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y steam"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y stellarium"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y streamlink"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y stremio"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y sumatrapdf.install"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y sysinternals"
REM Tabula - Tabula is a data science tool for liberating data tables locked inside PDF files.
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y tabula"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y teamviewer"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y testdisk"
REM Thorium browser replaces Chromium browser - Chromium fork for Windows named after radioactive element No. 90
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y thorium"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y thunderbird"
REM Time4Popcorn last updated in 2014:
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y time4popcorn"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y tor-browser"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ugrep"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y unzip"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vagrant"
REM Ventoy - multi-iso image utility for USB sticks or SSD sticks:
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ventoy"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist-all"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist140"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2005"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2008"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2010"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2012"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2013"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2015"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2017"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y virtualbox"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y virtualbox-guest-additions-guest.install"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y visioviewer"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y visualstudio2022community"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vivaldi.portable"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vlc"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vmware-workstation-player"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y waterfox"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y wave"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y webex-meetings"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y Wget"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y windirstat"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y windowsfirewallcontrol"
REM Optimize and Debloat Windows 10 and Windows 11 Deployments:
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y windows-optimize-debloat"
REM Winfetch - command-line system information utility written in PowerShell. Like Neofetch, but for Windows.
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y winfetch"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y winpcap"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y wireshark"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y wps-office-free"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y wsl"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y wsl2"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y wslgit"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y wsl-ubuntu-2204"
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y yed"
REM yt-dlp - youtube-dl fork with additional features and fixes
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y yt-dlp"
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y zoom"
REM zoxide = smarter cd command. Supports all major shells
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y zoxide"

REM *********************************************************************************************
echo "List all currently installed packages using winget package manager:"
REM *********************************************************************************************
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "winget list"
pause

REM *********************************************************************************************
echo "List all currently installed packages using chocolatey package manager:"
REM *********************************************************************************************
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco list --local-only"
pause

***************************************************************************************************
REM Use sysinternals utility called "psinfo -s" to show list of installed applications:
REM Combine with grep command to find a specific application
REM *********************************************************************************************
psinfo -s
pause 

REM *********************************************************************************************
REM Show list of installed applications using Get-AppxPackage:
REM *********************************************************************************************
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "Get-AppxPackage | Format-Table -property Name,Version,InstallLocation,Status -autoSize"
pause 


 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y powershell"
 
REM  the chocolatey package manager currently has 15055 packages (July 31,2015)
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y chocolatey"
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ChocolateyGUI"

REM  the nuget package manager currently has 40065 packages (July 31,2015)
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nuget.commandline"

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nugetpackageexplorer"

REM Command line utilities ######################################################################################

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y axel"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y curl"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y unzip"

REM Get Internet download utilities:
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y Wget"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y git"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y osquery"

REM  ##########################################################################################

REM Internet tools ##########################################################################################

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y qbittorrent-enhanced"

REM Greenshot is an excellent screenshot capture utility that can upload screenshot
REM directly to imgur.com cloud service

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y greenshot"

REM Manually install free version of Kaspersky Antivirus

REM Manually enable and configure firewall features in Glasswire firewall:

REM Install dig DNS query utility:

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y bind-toolsonly"

REM Use Wireshark to test DNS traffic - ensure Stubby's DNS traffic only goes via port 853 and not via standard port 53

REM install Hostsman hosts file updater

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hostsman"

REM uninstall Citrix Receiver as it interferes with full screen PC Gaming:

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco uninstall -y citrix-receiver"

REM Get newest Citrix app

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y citrix-workspace"

REM Get web browsers

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco uninstall -y vivaldi"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco uninstall -y vivaldi.portable"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y --force vivaldi.portable"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y waterfox"
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y GoogleChrome"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y Opera"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y flashplayerplugin"

REM Get E-mail clients and utilities

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y thunderbird"

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y outlookviewer"

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y outlookconverter"

REM  ##########################################################################################

REM Security software ##########################################################################################

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y glasswire"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y winpcap"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y wireshark"

REM Install OpenVPN client:

REM After installing OpenVPN client, import nordvpn udp config file into OpenVPN client

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y openvpn"
REM  ##########################################################################################

REM Programming related software ##########################################################################################

REM Install Miniconda 3

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y miniconda3"

REM Install middleware/libraries

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y adoptopenjdkjre"

REM Powershell requires install of DotNet4.5 first

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y DotNet4.5"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dotnetfx"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y powershell"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y powershell.portable"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y msvisualcplusplus2012-redist"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y msvisualcplusplus2013-redist"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2005"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2008"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2010"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2012"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2013"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2015"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vcredist2017"

REM Uninstall PowerIso because it is not for free

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco uninstall -y poweriso"

REM  ##########################################################################################




@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y grepwin"

REM streamlink is replacement for Minitube and allows streaming of Youtube video to VLC player:
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y streamlink"



 
REM ############################################################################################### 
REM Close .cmd command line window and open a new .cmd window to run the next commands.
REM Run following commands in Windows command line (using cmd, NOT Powershell) as administrator:
REM ############################################################################################### 

REM ############################################################################################### 
REM get sha256sum.exe to check SHA256SUM values of downloaded .iso image files:
REM mkdir C:\temp
REM cd c:\temp
REM wget  http://www.labtestproject.com/files/sha256sum/sha256sum.exe
REM ############################################################################################### 

REM https://github.com/gurnec/HashCheck
REM https://chocolatey.org/packages/hashcheck
REM The HashCheck Shell Extension makes it easy for anyone to calculate and 
REM verify checksums and hashes (SHA3-256 and SHA3-512) from Windows Explorer
REM License: Standard 3-Clause BSD License
REM Download location #1: https://github.com/gurnec/HashCheck/releases/tag/v2.4.0
REM Download location #2: https://chocolatey.org/packages/hashcheck/2.4.0
REM Download location #3 (newest): https://ci.appveyor.com/project/gurnec/hashcheck/build/2.4.1.58-alpha/artifacts

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hashcheck"

REM Get R and RStudio
 
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y r.project"
 
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y r.studio"

REM https://mran.microsoft.com/download/
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y microsoft-r-open"

REM show list of locally installed packages - installed using choco package manager for Windows:

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco list -lo"
 
REM jre8 installer hangs; so line below has been disabled
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y jre8"
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y 7zip.install"

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y 7zip.portable"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y cmder.portable -pre"
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y FoxitReader"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y eid-belgium"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y eid-belgium-viewer"

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y alcohol52-free"

 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y 7zip"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y aimp"
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vlc"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y mpc-hc-clsid2"

REM Use http://madshi.net/madVR.zip  as DirectShow video renderer for mpc-hc-clsid2 video player when watching 4K movies

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y k-litecodecpackmega"

REM Uninstall clipgrab, because it contains AdWare

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco uninstall -y clipgrab"
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y 7zip.commandline"
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y filezilla"
 
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y dropbox"
 
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y skype"
 
 
REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y paint.net"
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y virtualbox"

REM  @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y sandboxie"

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y VBoxGuestAdditions.install"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y virtualbox.extensionpack"

REM install Android emulator, which depends on Virtualbox:
 
REM Use Libreoffice to convert XLS to PDF file
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y libreoffice-fresh"

REM Use PDFsam Basic to merge 2 PDF files into one PDF file:

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y pdfsam.install"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y wps-office-free"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco uninstall -y visioviewer2013"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y visioviewer"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y pdf24"
 
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y windirstat"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y bulk-crap-uninstaller"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y nomacs"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y cpu-z"

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y duplicatecommander -pre"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y qbittorrent"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y steam"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y teamviewer"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y microsoft-teams.install"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y microsoft-teams"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y hitmanpro"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ccleaner"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y vagrant"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y yumi"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y unetbootin"

REM Get device driver management utilities:

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y DellCommandUpdate"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y googleearth"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y bleachbit"

REM Replace classic-shell  with open-shell

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco uninstall -y classic-shell"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y open-shell"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y javaruntime"



@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ie11"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y adblockplusie"

REM CD Burning software

REM @powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y win32diskimager"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y rufus"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y cdburnerxp"

echo "Use Stubby DNS server 127.0.0.1 via simplednscrypt package"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y simplednscrypt"

echo "Show list of locally installed packages - installed using choco package manager for Windows:"

choco list -lo

echo "TO DO: In the RStudio interface, please click on"

echo "'Help', then click on 'Check for Updates' and then click on 'Quit and Download' to get the newest version of RStudio"

echo "TO DO:Replace 127.0.0.1 with 0.0.0.0 in Windows hosts file after running HostsMan application"

echo "in C:\ProgramData\chocolatey\lib\hostsman\"

echo on

pause


echo off

echo "Update all currently installed choco packages:"

@powershell -NoProfile -ExecutionPolicy unrestricted -Command "choco install -y ChocolateyPackageUpdater"

cup -y all

echo "Show list of locally installed packages - installed using choco package manager for Windows:"

choco list -lo

echo "TO DO: In the RStudio interface, please click on"

echo "'Help', then click on 'Check for Updates' and then click on 'Quit and Download' to get the newest version of RStudio"

echo "TO DO:Replace 127.0.0.1 with 0.0.0.0 in Windows hosts file after running HostsMan application"

echo "in C:\ProgramData\chocolatey\lib\hostsman\"

echo "TO DO:Manually Configure network adapter to use DNS server 127.0.0.1 after installing and configuring Stubby DNS resolver"

echo on
