1) backup all data in home directory using organize-home-directory-files-before-backintime-backup.bash
2) backup game saves using ludusavi utility ( https://github.com/mtkennerly/ludusavi )
2) perform Secure Erase of SSD disk via UEFI BIOS
3) Use nwipe program during ShredOS LiveUSB session to securely erase the disks using default 3 pass DOD method
4) Install Windows 10 Home FR on an SSD
5) Create Windows restore point (créer un point de restauration) before running any Windows debloat utility
6) Install all Windows updates
7) Create another Windows restore point (créer un point de restauration) before running any Windows debloat utility
8) Run Windows-install-ChrisTitusTechWinUtil.ps1 to tweak and reduce number of Windows processes
9) Install and run Atlas OS on Windows 10 Home FR via
https://atlasos.net/ to improve Windows performance and reduce number of Windows processes 
Atlas is officially tested with Windows Professional and Enterprise (not Windows Home)
This reduces Windows 10 bloat down to 53 processes,644 threads and 23091 handles and 1,0 GB of RAM usage
10) Install and run Tron.bat on Windows desktop via 
 https://bmrf.org/repos/tron/ to improve Windows performance
11) Install and run Chocolatey package manager script - Windows-choco-install-script.cmd -  to install free programs
12) Install and run Ninite package manager script - Windows-ninite-install-powershell-script.ps1 -  to install free programs
13) Install and run Winget package manager script - Windows-winget-install-script.ps1 -  to install free programs
14) Install and run Snappy Driver Installer Origin to install newer drivers
15) Install and run Windows-device-specific-driver-install.cmd to install newer drivers
16) Remove duplicates: In Control Panel\All Control Panel Items\Programs and Features, sort by program name and uninstall duplicate programs, where one version of the program is older than the other
17) Configure HostsMan - already installed via Windows-choco-install-script.cmd - to only use "Peter Lowe's AdServers List" and "Cameleon" as up-to-date Hosts sources to block unwanted ads on websites
18) Use Bulk Crap Uninstaller to bulk uninstall unnecessary programs, especially programs with a one star User Rating in Bulk Crap Uninstaller
19) if Linux bootloader is ever destroyed by Windows update/BIOS update, fully reinstall NixOS from LiveUSB Session (quickest solution to restoring Linux bootloader)
20) Then set Linux bootloader to boot before Windows bootloader in BIOS settings
