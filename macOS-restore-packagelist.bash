#!/bin/bash

# Author: Mark Rijckenberg
# Latest modification date: 2023/8/23

# Prerequisites: MacOS, git, Xcode 8.2.1 or newer (=Apple CLI Dev tools)
# Minimum free disk space to install Mac OS X and the applications below: 30 GB

# 1) minimum version of Mac OS X required to install latest Xcode: https://en.wikipedia.org/wiki/Xcode
# 2) install git using this FAQ:
# http://sourceforge.net/projects/git-osx-installer/files/
# then clone this Github repository to your local home directory

# 3) install Xcode via this link:
# https://itunes.apple.com/be/app/xcode/id497799835?mt=12

# 4) execute the bash script below

defaults write com.apple.NetworkBrowser BrowseAllInterfaces -bool true
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
# sudo nvram SystemAudioVolume=" "


# Install Apple Mac OS X command line tools, java and my preferred homebrew and cask software
xcode-select --install 
# Works on Mavericks and hopefully above
# echo
read -p "Please wait until CLI tools are installed and press enter"  < /dev/tty

# Make sure ruby binary can be found in Mac OS X 10.10 or newer:
cd /System/Library/Frameworks/Ruby.framework/Versions
# sudo ln -s Current 1.8


# list of homebrew packages:  http://braumeister.org/
# Install Homebrew if not installed
if ! command -v brew >/dev/null 2>&1; then
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

echo "export PATH=/opt/homebrew/bin:$PATH" >> ~/.bashrc
source ~/.bashrc

#Symlink into the normal place. Add this to login scripts as well
# export HOMEBREW_CASK_OPTS="--appdir=/Applications"

sudo xcodebuild -license

# Tap into third-party repositories for Cask packages
brew tap caskroom/cask
brew tap caskroom/versions
brew tap staticfloat/julia
brew tap samueljohn/python
brew tap homebrew/science
brew tap homebrew/cask
brew tap homebrew/cask-versions
brew tap homebrew/cask-fonts

# Update Homebrew and Cask packages
brew update
brew upgrade
#brew upgrade brew-cask
brew doctor 
#should say "Your system is ready to brew."

brew cask fetch java
brew install --cask  java
brew install --cask  xquartz #Pre-req for some of the brew packages
brew install python --framework
brew install putty gtk+

brew install ack

# upgrade bash shell:
brew install bash
sudo sh -c 'echo "/usr/local/bin/bash" >> /etc/shells'
chsh -s /usr/local/bin/bash
sudo mv /bin/bash /bin/bash-backup
sudo ln -s /usr/local/bin/bash /bin/bash

brew install byacc
brew install cabextract
brew install coreutils
brew install emacs --with-cocoa
brew install findutils
brew install fish
brew install flex
brew install gawk
# brew install gcc # requires Xcode CLT (command line tools) to be installed first
brew install git
brew install gnu-sed
brew install go
#brew install ispell
brew install jdiskreport
brew install --HEAD --64bit julia
brew install kdiff3
#brew install lua52
brew install maven
#brew install mercurial
#brew install minicom
#brew install multimarkdown
#brew install mutt
brew install node
brew install offline-imap
brew install osquery
brew install p7zip
brew install python
# brew install sqlite
brew install ssh-copy-id
#brew install svn
#brew install the_silver_searcher
#brew install tmux
brew install wget
brew install yarn
brew remove wine winetricks
brew install --cask  wine-staging
#brew install z

brew install --cask  4k-youtube-to-mp3
#brew install --cask  adium
#brew install --cask  adobe-air 
#brew install --cask  adobe-reader
#brew install --cask  anki
#brew install --cask  arduino
brew install --cask  atom
#brew install --cask  bartender
brew install --cask  caffeine
brew install --cask  calibre
brew install --cask  ccleaner
brew install --cask  chromium
brew install --cask  citrix-receiver
#brew install --cask  chicken
#brew install --cask  controlplane
brew install --cask  dash
brew install --cask  dropbox
brew install --cask  dupeguru
#brew install --cask  eclipse-ide
brew install --cask  evernote
brew install --cask  filezilla
brew install --cask  firefox
brew install --cask  flash
brew install --cask  flip4mac
#brew install --cask  flux
#brew install --cask  freeplane
#brew install --cask  fritzing
brew install --cask  gimp
brew install --cask  gnucash
brew install --cask  google-chrome
brew install --cask  google-chrome-canary
#brew install --cask  google-drive
brew install --cask  google-earth
#brew install --cask  gpgtools
#brew install --cask  iterm2
brew install --cask  jdiskreport
brew install --cask  kdiff3
#brew install --cask  keepassx0
brew install --cask  kindle
#brew install --cask  legacy-keepassx
brew install --cask  libreoffice
#brew install --cask  mactex
#brew install --cask  macvim
brew install --cask  microsoft-teams
#brew install --cask  minecraft
brew install --cask  mp3gain-express
#brew install --cask  music-manager
#brew install --cask  openscad
brew install --cask  opera
brew install --cask  paintbrush
#brew install --cask  quicksilver
brew cask fetch r
brew install --cask  r
brew install --cask  rstudio
#brew install --cask  second-life-viewer
#brew install --cask  sketchup
brew install --cask  skype
#brew install --cask  slic3r
#brew install --cask  smcfancontrol
#brew install --cask  sourcetree
#brew install --cask  steam
brew install --cask  stellarium
brew cask fetch teamviewer
brew install --cask  teamviewer
#brew install --cask  the-unarchiver
#brew install --cask  thunderbird
brew install --cask  transmission
#brew install --cask  truecrypt71a
brew install --cask  unrarx
#brew install --cask  vagrant
brew install --cask  virtualbox
brew install --cask  vlc
# brew install --cask  vmware-fusion
brew install --cask whatsapp
# brew install --cask  xbmc

brew cleanup
brew cask cleanup

# Install Perlbrew
curl -L https://install.perlbrew.pl | bash
# add following source line into ~/.bash_profile file:
source $HOME/perl5/perlbrew/etc/bashrc
# open new Terminal and run following commands:
# cpan App::perlbrew
perlbrew init
perlbrew install perl-stable
perlbrew install-cpanm
cpanm --local-lib=~/perl5 local::lib && eval $(perl -I ~/perl5/lib/perl5/ -Mlocal::lib)
# cpanm install CPAN::Mini
# minicpan -l ~/perl5/minicpan -r http://mirror.internode.on.net/pub/cpan/


# Install keybase client (keybase.io)

npm install -g keybase-installer


#  Add local golang tools directory

#export GOPATH=/usr/local/gotools
#export GOBIN="$GOPATH/bin"
#mkdir "$GOPATH"
#echo "export GOPATH=\"\$PWD\"" >  $GOPATH/.envrc
#echo "export GOBIN=\"\$PWD/bin\"" >>  $GOPATH/.envrc
#direnv allow $GOPATH
#cd "$GOPATH""
#go get github.com/golang/lint/golint
#go get github.com/tools/godep
#go get code.google.com/p/go.tools/cmd/goimports
#echo Please add \"$GOBIN\" to your path

sudo easy_install pip
pip install --upgrade distribute
pip install --upgrade pip
# https://courses.edx.org/asset-v1:DelftX+EX101x+3T2015+type@asset+block/xlwingsguide.pdf
pip install --upgrade xlwings
pip install --upgrade nose
pip install --upgrade pyparsing
pip install --upgrade python-dateutil
pip install --upgrade pep8

brew install numpy
brew install scipy
brew install homebrew/python/matplotlib

#Make Fish your default shell:
echo "/usr/local/bin/fish" | sudo tee -a /etc/shells
chsh -s /usr/local/bin/fish

# remove ruby 1.8 symbolic link used for finding ruby in Mac OS X 10.10 or newer:
sudo rm /System/Library/Frameworks/Ruby.framework/Versions/1.8

# manual steps
echo "Install Monosnap (https://monosnap.com/) manually from the App Store"
echo "Make sure you have a strong password to protect your Mac's user account."
echo "Adjust Mac's security settings: enable FileVault and require password instantly."
echo "Make sure to create an SSH key pair and restrict access to .ssh using chmod 700 ~/.ssh"
