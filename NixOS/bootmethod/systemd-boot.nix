# change history
# 2024/3/13: change boot.loader.timeout value to lib.mkForce 2;
# 2024/7/24: added boot.loader.systemd-boot.bootCounting.enable = true;
# 2024/10/3: Performed successful bootloader migration from grub to systemd-boot on ulysses-laptop using following commands:
# reconfigure flake.nix to use systemd-boot
# sudo nixos-rebuild switch
# next command will install systemd-boot in EFI partition (under /boot):
# sudo nixos-rebuild --install-bootloader boot
# reboot and manually boot into EFI File systemd-boot in BIOS menu
# use sudo efibootmgr -o  -> choose systemd-boot as first item in boot order
# reboot
# use sudo bootctl status to verify that systemd-boot is now being used as default boot loader
# Next step will be to migrate from systemd-boot to lanzaboote bootloader and enable Secure Boot
# 2025/1/8: added boot.initrd.systemd.enable = true;

{ config, pkgs, lib, inputs, ... }:

{

  # Whether to enable systemd in initrd. The unit options such as 
  # boot.initrd.systemd.services are the same as their stage 2 counterparts
  # such as systemd.services, except that restartTriggers and reloadTriggers
  # are not supported.
  boot.initrd.systemd.enable = true;

  ##############################################################################################################################
  # 2024/7/24:Enable automatic boot assessment feature in systemd-boot:
  # See merged PR/pull request https://github.com/NixOS/nixpkgs/pull/330017 
  # See https://fosdem.org/2024/events/attachments/fosdem-2024-2204-fortifying-the-foundations-elevating-security-in-nix-and-nixos/slides/22545/FOSDEM_Nix_2024_1_x51h3Cw.pdf
  # See https://systemd.io/AUTOMATIC_BOOT_ASSESSMENT/
  # 2024/8/14:boot counting reverted by PR: https://github.com/NixOS/nixpkgs/pull/334526
  # boot.loader.systemd-boot.bootCounting.tries = 3;
  # boot.loader.systemd-boot.bootCounting.enable = true;

  # systemd.services.display-manager  = {
  # before = [ "boot-complete.target" ];
  # wantedBy = [ "boot-complete.target" ];
  # unitConfig.FailureAction = "reboot";
  #   };
  ##############################################################################################################################

  # Much easier to reinstall Linux Boot loader when systemd-boot.nix is being used (versus grub.nix)
  boot.loader.systemd-boot.enable = true;

  # Reduce bootloader timeout so that I do not have
  # to spam Enter key for a speedy boot
  boot.loader.timeout = lib.mkForce 2;

  # Delete files in /tmp during boot, because not used by heroic games launcher
  boot.tmp.cleanOnBoot = true;

  ##############################################################################################################################

  # limine is an alternative to grub bootloader:
  # boot.loader.limine.enable = true;

  #boot.loader.grub.enable = true;
  # This is for GRUB in EFI mode
  #boot.loader.grub.efiSupport = true;
  #boot.loader.grub.device = "/dev/sda1";
  #boot.loader.grub.useOSProber = true;
  # Maximum number of configurations in boot menu. GRUB has problems when there are too many entries:
  #boot.loader.grub.configurationLimit = 20;

  
  # disable support for ecryptfs encrypted home filesystems, as NixOS 23.05 cannot handle them correctly yet: 
  # boot.loader.grub.enableCryptodisk = false;
  # security.pam.enableEcryptfs = false; 

  #boot.loader.efi.canTouchEfiVariables = true; 

}


# Main configuration file for NixOS 23.05 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# Find all NixOS packages here: https://nixos.org/
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.

# File creation date: 2023/6/19
# Last modification date: 2024/8/7

# DISKSPACE: This script will initially use minimum 60 GB of diskspace ( 56 GB for root partition and 4.2 GB for home+dropbox data))
# DISKSPACE: Reserve 1 GB for FAT32 /boot partition and set boot flag on this partition
# DISKSPACE: Reserve 0 GB for SSD swap partition and 8 GB diskspace for mechanical HD swap partition
# DISKSPACE: Reserve at least 100 GB of diskspace for / (root) F2FS partition for SSD - F2FS compresses better than ext4 on SSD
# DISKSPACE: Install all games in /tmp to avoid clogging up /home directory with files that should not be backed up
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 26 minutes on old SSD drive from the year 2016

# PRE-INSTALL STEPS:
# 1) update BIOS
# 2) backup home directory, including ~/.config subfolder

# POST-INSTALL STEPS:
# 3) extract config_backup_<timestamp>.zip from usb stick into home directory , then reboot PC
# 4) copy backed up documents/files from usb stick to ~/backup in home directory
# 5) Improve privacy: Load chrome://flags/#encrypted-client-hello in the Chromium browser's address bar.
# Set the status of the Encrypted Client Hello (ECH) flag to Enabled. 
# https://blog.cloudflare.com/announcing-encrypted-client-hello/

# 6) Can be skipped thanks to step 3: Put following program icons in desktop panel for easy access: Terminal,spacefm,bitwarden,
# maestral,google chrome (also used to import/export bookmarks),chromium,brave,teamviewer,printer icon,proton vpn
# 7) Can be skipped thanks to step 3: copy all .desktop files from https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 8) Can be skipped thanks to step 3:Under Themes - set Icons to Numix-Circle-Light
# 9) Can be skipped thanks to step 3:force Chromium web browser to open
# https://searx.prvcy.eu/ (SearXNG meta search engine) on each startup (manually configure in Chromium settings)


# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
