# See https://github.com/mcdonc/.nixconfig/blob/master/videos/rpi3/script.rst
# See https://github.com/mcdonc/nixos-pi-zero-2

{ config, pkgs, lib, inputs, ... }:

{
   console.enable = false;

   hardware = {
    raspberry-pi."4".apply-overlays-dtmerge.enable = true;
    raspberry-pi."4".audio.enable = false;
    # Enable GPU acceleration
    # See https://nixos.wiki/wiki/NixOS_on_ARM/Raspberry_Pi
    raspberry-pi."4".fkms-3d.enable = true;
    deviceTree = {
      enable = true;
      filter = "*rpi-4-*.dtb";
    };
  };

  networking = {
    # If using dhcpcd:
    dhcpcd.extraConfig = "nohook resolv.conf";
    # dhcpcd should fork to background immediately
    # dhcpcd takes up a significant amount of time
    # blocking the boot process by preventing
    # network-online from firing
    dhcpcd.wait = "background";
    hostName = "rpi"; # Define your hostname.
    nameservers = [ "127.0.0.1" "::1" ];
    # if using NetworkManager:
    networkmanager.dns = "none";
    # networkmanager.enable = true; # Easiest to use and most distros use this by default.
    };

    # an overlay to enable Raspberry Pi support in libcec, and thus cec-client
  nixpkgs.overlays = [
    # nixos-22.11
    (self: super: { libcec = super.libcec.override { withLibraspberrypi = true; }; })
  ];

  services.fail2ban.enable = true;

  services.openssh.enable = true;
  # security through obscurity:
  services.openssh.ports = [ 57312 ];
  services.openssh.settings.AllowUsers = [ "ulysses" ];
  
  services.udev.extraRules = ''
    # allow access to raspi cec device for video group (and optionally register it as a systemd device, used below)
    SUBSYSTEM=="vchiq", GROUP="video", MODE="0660", TAG+="systemd", ENV{SYSTEMD_ALIAS}="/dev/vchiq"
  '';

  # optional: attach a persisted cec-client to `/run/cec.fifo`, to avoid the CEC ~1s startup delay per command
  # scan for devices: `echo 'scan' &gt; /run/cec.fifo ; journalctl -u cec-client.service`
  # set pi as active source: `echo 'as' &gt; /run/cec.fifo`
  systemd.sockets."cec-client" = {
    after = [ "dev-vchiq.device" ];
    bindsTo = [ "dev-vchiq.device" ];
    wantedBy = [ "sockets.target" ];
    socketConfig = {
      ListenFIFO = "/run/cec.fifo";
      SocketGroup = "video";
      SocketMode = "0660";
    };
  };
  systemd.services."cec-client" = {
    after = [ "dev-vchiq.device" ];
    bindsTo = [ "dev-vchiq.device" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = ''${pkgs.libcec}/bin/cec-client -d 1'';
      ExecStop = ''/bin/sh -c "echo q &gt; /run/cec.fifo"'';
      StandardInput = "socket";
      StandardOutput = "journal";
      Restart="no";
  };

}

# Main configuration file for NixOS 23.05 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# Find all NixOS packages here: https://nixos.org/
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.

# File creation date: 2023/6/19
# Last modification date: 2023/10/4
# DISKSPACE: This script will use minimum 58 GB of diskspace ( 54 GB for root partition and 4.2 GB for home+dropbox data))
# DISKSPACE: Reserve 1 GB for /boot and at least 100 GB of diskspace for / (root) partition; remaining space for /home
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 20 minutes on mechanical harddisk, faster on SSD

# PRE-INSTALL STEPS:
# 1) backup homedirectory
# 2) update BIOS

# POST-INSTALL STEPS:
# 1) Put following program icons in desktop panel for easy access: Terminal,krusader,bitwarden,
# maestral,google chrome (also used to import/export bookmarks),chromium,teamviewer,printer icon
# 2) copy all .desktop files from https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 3) Under Themes - set Icons to Numix-Circle-Light
# 4) copy backed up files from usb stick to ~/backup in home directory

# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

