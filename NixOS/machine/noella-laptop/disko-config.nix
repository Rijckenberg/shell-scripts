 {
  disko.devices = {
    disk = {
      main = {
        type = "disk";
        device = "/dev/nvme0n1";
        content = {
          type = "gpt";
          efiGptPartitionFirst = false;
          partitions = {
            TOW-BOOT-FI = {
              priority = 1;
              type = "EF00";
              size = "32M";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = null;
              };
              hybrid = {
                mbrPartitionType = "0x0c";
                mbrBootableFlag = false;
              };
            };
            ESP = {
              type = "EF00";
              # See https://9to5linux.com/arch-linux-installer-archinstall-2-8-increase-esp-size-to-1-gib-fixes-more-bugs
              size = "1024M";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = [ "umask=0077" ];
              };
            };
            root = {
              size = "100%";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/";
              };
            };
          };
        };
      };
    };
  };
}

# Creation date: 2024/7/28
# Last modification date: 2024/7/28

# Prerequisites: wired Internet connection and boot into 
# https://channels.nixos.org/nixos-23.11/latest-nixos-minimal-x86_64-linux.iso
# on target machine to minimize RAM requirements

# https://github.com/nix-community/disko
# see filesystem configuration examples here:
# https://github.com/nix-community/disko/tree/master/example
# configuration below based on 
# https://raw.githubusercontent.com/nix-community/disko/master/example/hybrid-mbr.nix
# shell command: sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko ./disko-config.nix
# nix run github:nix-community/nixos-anywhere -- --flake '.#ulysses-laptop' nixos@192.x.x.x
# where nixos@192.x.x.x is the target machine needing a new NixOS install
# follow these instructions:
# https://github.com/nix-community/nixos-anywhere/blob/main/docs/howtos/no-os.md
# non-working procedure:
# step 1) boot NixOS installer on target machine
# step 2) connect to Internet on target machine
# step 3) run passwd and set password for nixos user on target machine to make ssh connections to target machine possible
# step 4) run following command on target machine: nixos-generate-config --no-filesystems --root /mnt
# Option --no-filesystems is selected, because disko will configure the filesystems
# step 5) copy generated /mnt/etc/nixos/hardware-configuration.nix from target machine to source machine where flake.nix is located (~/shell-scripts/NixOS)
# step 6) run following command on source machine: nix run github:nix-community/nixos-anywhere -- --flake './#ulysses-laptop' nixos@192.x.x.x

# simpler NixOS install procedure requiring only the target machine (simpler is better):
# 1) CAREFUL:run following command on target machine while in NixOS 23.11 LiveUSB session, 
# which WIPES everything off the target system!!!!!:
#    sudo nix run github:nix-community/disko \
#        --extra-experimental-features "nix-command flakes" \
#        --no-write-lock-file \
#        -- \
#        --mode zap_create_mount \
#        "machine/ulysses-laptop/disko-config.nix"
# 2) before running nixos-install command: in Gitlab repo, copy hardware-configuration.nix file
# from machine/ulysses-laptop/hardware-configuration.nix to same directory as flake.nix
# on Gitlab repository
# or else nixos-install command gives error that hardware-configuration.nix cannot be found
# 3) delete flake.lock file on target machine
# 4) then run following command on target machine while in NixOS 23.11 LiveUSB session:
# sudo nixos-install --flake ".#ulysses-laptop"