{ config, pkgs, ... }:

{
  # Youtube video: https://www.youtube.com/watch?v=rCVW8BGnYIc&t=27s
  # based on following vm.nix file:  https://github.com/TechsupportOnHold/Nixos-VM
  # This nix file should allow you to run Windows 10 or 11 in a VM via libvirt+QEMU in NixOS 23.11 or newer
  # Alternatives like vmware and virtualbox did not work correctly for me in NixOS...
  # Enable dconf (System Management Tool)
  programs.dconf.enable = true;

  # see https://github.com/mcdonc/.nixconfig/blob/master/common.nix
  # run appimages directly (see https://nixos.wiki/wiki/Appimage)
  boot.binfmt = {
    registrations.appimage = {
      wrapInterpreterInShell = false;
      interpreter = "${pkgs.appimage-run}/bin/appimage-run";
      recognitionType = "magic";
      offset = 0;
      mask = "\\xff\\xff\\xff\\xff\\x00\\x00\\x00\\x00\\xff\\xff\\xff";
      magicOrExtension = "\\x7fELF....AI\\x02";
    };
    # run aarch64 and riscv64 binaries
    # see https://github.com/MatthewCroughan/raspberrypi-nixos-example
    emulatedSystems = [ "aarch64-linux" "riscv64-linux" ];
  };

  # Add user to libvirtd group
  users.users.ulysses.extraGroups = [ "libvirtd" ];

  # Install necessary packages
  environment.systemPackages = with pkgs; [
    # adwaita-icon-theme # causes conflict with numix circle icon theme
    runc
    spice-protocol
    spice
    spice-gtk
    virt-manager
    virt-viewer
    win-spice
    win-virtio
  ];

  # Manage the virtualisation services
  virtualisation = {
    libvirtd = {
      enable = true;
      qemu = {
        swtpm.enable = true;
        ovmf.enable = true;
        ovmf.packages = [ pkgs.OVMFFull.fd ];
      };
    };
    spiceUSBRedirection.enable = true;
  };
  services.spice-vdagentd.enable = true;

}
