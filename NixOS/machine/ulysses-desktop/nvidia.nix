# Change history:
# 2025/1/30: I adapted this file based on 
# https://gitlab.com/fazzi/nixohess/-/blob/main/hosts/modules/gpu/nvidia/default.nix#L33

{ config, pkgs, lib, ... }:

{

boot.extraModulePackages = [ config.boot.kernelPackages.nvidia_x11 ];

boot.initrd.kernelModules = [ "nvidia" "amdgpu" ];

# disable i915 and amdgpu drivers
# boot.kernelParams = [ "module_blacklist=i915" "module_blacklist=amdgpu" ];

# Install necessary packages
environment.systemPackages = with pkgs; [
    # gwe # System utility designed to provide information, control the fans and overclock your NVIDIA card
    # gwe only works with X11, not Wayland
    gwe
  ];

hardware.nvidia = {

    gsp.enable = config.hardware.nvidia.open; # if using closed drivers, lets assume you don't want gsp

    modesetting.enable = true; # Modesetting is required.

    # Use the NVidia open source kernel module (not to be confused with the
    # independent third-party "nouveau" open source driver).
    # Support is limited to the Turing and later architectures. Full list of 
    # supported GPUs is at: 
    # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus 
    # Only available from driver 515.43.04+
    # Do not disable this unless your GPU is unsupported or if you have a good reason to.
    open = true;

    # Choose correct NVIDIA Driver version here:

    # Optionally, you may need to select the appropriate driver version for your specific GPU
    # See https://nixos.wiki/wiki/Nvidia  for more driver options
    # NVIDIA stable package (version 560 Driver) causes sniper game to hang indefinitely on game menu 
    # after a few seconds (August 27, 2024)
    
    # Nvidia Beta driver
    package = config.boot.kernelPackages.nvidiaPackages.beta;

    # Nvidia Stable
    # package = config.boot.kernelPackages.nvidiaPackages.stable;
    
    # NVIDIA Production package installs more stable 550 drivers:
    # package = config.boot.kernelPackages.nvidiaPackages.production;

    # Nvidia-mkdriver version
    # package = config.boot.kernelPackages.nvidiaPackages.mkDriver {
          # Implementation steps:
          # 1) Find latest beta version from https://www.nvidia.com/en-us/drivers/unix/
          # 2) search in Github code for search term: 
          # https://github.com/search?q=nvidiaPackages.mkDriver+570.86.16&type=code 
          # replacing 570.86.16 with latest beta driver version
          # 3) copy the found code on Github into this nvidia.nix file
          # OPTIONAL) Procedure for determining the following 3 sha256 values:
          # Put "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA="; (51 characters)
          # as placeholder for openSha256 and sha256_64bit, then let the nixos-rebuild fail. The 
          # log will report what the correct sha256 is, and you just use the 
          # one it gives. Do that for each sha256 value:
          # open = true, open = false and for nvidiaSettings = true 
          # OPTIONAL) check here for nvidia driver updates:
          # https://gitlab.com/fazzi/nixohess/-/blob/main/hosts/modules/gpu/nvidia/default.nix#L33
    #     version = "570.86.16";
    #     openSha256 = "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
    #     sha256_64bit = "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
    #     settingsSha256 = lib.fakeSha256;
    #     usePersistenced = false;
    #   };

    # Optionally, you may need to select the appropriate driver version for your specific GPU.
    # package = config.boot.kernelPackages.nvidiaPackages.mkDriver {
    #  version = "570.124.04"; # use new 570 drivers
    #  sha256_64bit = "sha256-G3hqS3Ei18QhbFiuQAdoik93jBlsFI2RkWOBXuENU8Q=";
    #  openSha256 = "sha256-DuVNA63+pJ8IB7Tw2gM4HbwlOh1bcDg2AN2mbEU9VPE=";
    #  settingsSha256 = "sha256-9rtqh64TyhDF5fFAYiWl3oDHzKJqyOW3abpcf2iNRT8=";
    #  usePersistenced = false;
    # };

    # Enable power management (do not disable this unless you have a reason to).
    # Likely to cause problems on laptops and with screen tearing if disabled.
    # Fixes nvidia-vaapi-driver after suspend
    powerManagement.enable = true;

    prime = {
		# Make sure to use the correct Bus ID values for your system!
		# intelBusId = "PCI:0:2:0";
    nvidiaBusId = "PCI:1:0:0";
    amdgpuBusId = "PCI:22:0:0";
	    };

    # Disable the Nvidia settings menu,
  	# accessible via `nvidia-settings`.
    nvidiaSettings = false;

  };

  services.xserver.videoDrivers = [ "nvidia" "amdgpu" ];

}


# Main configuration file for NixOS 23.05 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# Find all NixOS packages here: https://nixos.org/
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.

# File creation date: 2023/6/19
# Last modification date: 2024/8/15
# DISKSPACE: This script will use minimum 58 GB of diskspace ( 54 GB for root partition and 4.2 GB for home+dropbox data))
# DISKSPACE: Reserve 1 GB for /boot and at least 100 GB of diskspace for / (root) partition; remaining space for /home
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 20 minutes on mechanical harddisk, faster on SSD

# PRE-INSTALL STEPS:
# 1) backup homedirectory
# 2) update BIOS

# POST-INSTALL STEPS:
# 1) Put following program icons in desktop panel for easy access: Terminal,krusader,bitwarden,
# maestral,google chrome (also used to import/export bookmarks),chromium,teamviewer,printer icon
# 2) copy all .desktop files from https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 3) Under Themes - set Icons to Numix-Circle-Light
# 4) copy backed up files from usb stick to ~/backup in home directory

# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
