{
  config,
  inputs,
  isLinux,
  lib,
  nur,
  pkgs,
  theme,
  ...
}:

# Change history:
# 2023/11/24: added section programs.firefox

with lib.hm.gvariant;

let
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  # inherit (config.my) username;
  username = "rijckenbergc";

  homeDirectory = "/home/${username}";
  configHome = "${homeDirectory}/.config";

  inherit (lib) mkIf mkDefault;
  inherit (pkgs.stdenv.hostPlatform) isLinux;
  dummyPackage = pkgs.runCommandLocal "dummy" { } "mkdir $out";
  packageIfLinux = x: if isLinux then x else dummyPackage;

in

{

    # See https://mynixos.com/home-manager/options/programs.firefox

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

   # See https://mynixos.com/home-manager/options/programs.librewolf
  # Use about:config in Librewolf search bar to verify if settings are correctly changed
  # programs = {
  #    librewolf = {
  #      enable = true;
        # Workaround to prioritize system wide installed package
        # package = pkgs.sl;
        # Enable WebGL, cookies and history
  #      settings = {
  #        "webgl.disabled" = false;
  #        "privacy.resistFingerprinting" = true;
  #        "privacy.clearOnShutdown.history" = true;
  #        "privacy.clearOnShutdown.cookies" = true;
  #        "network.cookie.lifetimePolicy" = 0;
  #        "general.useragent.override" = "Mozilla/5.0 (Windows NT 10.0; rv:101.0) Gecko/20100101 Firefox/101.0";
          # separate list of extensions using a comma:
          # "extensions.blocklist.enabled" = false;
          # following setting not working:
          # "browser.policies.runOncePerModification.extensionsInstall" = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi,https://addons.mozilla.org/firefox/downloads/latest/adblocker-ultimate/latest.xpi";
  #      };
        # Extensions
        # I still dont care about cookies, Privacy Badger,
        # Bypass Paywalls Clean, Cookie AutoDelete
        # uBlock Origin already included in LibreWolf
  #    };
  # };

   # See https://github.com/nix-community/home-manager/blob/master/modules/programs/zoxide.nix
   programs.zoxide.enable = true;
   programs.zoxide.enableBashIntegration = true;

    imports = builtins.concatMap import [
    #./modules
    #./programs
    #./scripts
    #./services
    #./themes
  ];

    xdg = {
    inherit configHome;
    enable = true;
  };

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change home.stateVersion value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  # Please read the comment before changing.

  home = {
    # Home Manager needs a bit of information about you and the paths it should
    # manage.
    inherit username homeDirectory;
    stateVersion = "24.11";

    sessionVariables = {
      BROWSER = "firefox";
      DISPLAY = ":0";
      EDITOR = "geany";
    };
  };

  # restart services on change
  systemd.user.startServices = "sd-switch";

  # notifications about home-manager news
  news.display = "silent";


  # The home.packages option allows you to install Nix packages into your
  # environment.
  # home.packages = with pkgs; [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # hello

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
  # ];


  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';

".config/fastfetch/config.jsonc".text = ''
{
    "$schema": "https://github.com/fastfetch-cli/fastfetch/raw/dev/doc/json_schema.json",
    "logo": {
        "padding": {
            "top": 2
        }
    },
    "display": {
        "separator": " -> "
    },
    "modules": [
        "title",
        "separator",
        {
            "type": "os",
            "key": " OS",
            "keyColor": "yellow",
            "format": "{2}"
        },
        {
            "type": "os",
            "key": "├", // Just get your distro's logo off nerdfonts.com
            "keyColor": "yellow",
            "format": "{6}{?6} {?}{10} {8}"
        },
        {
            "type": "kernel",
            "key": "├",
            "keyColor": "yellow"
        },
        {
            "type": "packages",
            "key": "├󰏖",
            "keyColor": "yellow"
        },
        {
            "type": "shell",
            "key": "└",
            "keyColor": "yellow"
        },
        "break",

        {
            "type": "wm",
            "key": " DE/WM",
            "keyColor": "blue"
        },
        {
            "type": "lm",
            "key": "├󰧨",
            "keyColor": "blue"
        },
        {
            "type": "wmtheme",
            "key": "├󰉼",
            "keyColor": "blue"
        },
        {
            "type": "icons",
            "key": "├󰀻",
            "keyColor": "blue"
        },
        {
            "type": "terminal",
            "key": "├",
            "keyColor": "blue"
        },
        {
            "type": "wallpaper",
            "key": "└󰸉",
            "keyColor": "blue"
        },

        "break",
        {
            "type": "host",
            "key": "󰌢 PC",
            "keyColor": "green"
        },
        {
            "type": "cpu",
            "key": "├󰻠",
            "keyColor": "green"
        },
        {
            "type": "gpu",
            "key": "├󰍛",
            "keyColor": "green"
        },
        {
            "type": "disk",
            "key": "├",
            "keyColor": "green"
        },
        {
            "type": "memory",
            "key": "├󰑭",
            "keyColor": "green"
        },
        {
            "type": "swap",
            "key": "├󰓡",
            "keyColor": "green"
        },
        {
            "type": "uptime",
            "key": "├󰅐",
            "keyColor": "green"
        },
        {
            "type": "display",
            "key": "└󰍹",
            "keyColor": "green"
        },

        "break",
        {
            "type": "sound",
            "key": " SOUND",
            "keyColor": "cyan"
        },
        {
            "type": "player",
            "key": "├󰥠",
            "keyColor": "cyan"
        },
        {
            "type": "media",
            "key": "└󰝚",
            "keyColor": "cyan"
        },

        "break"
    ]
}

'';

".newsboat/config".text = ''
# Automatically inserted by home-manager:    
refresh-on-startup yes
# reload-time desired_time_in_minutes
reload-time 60
    '';

".newsboat/urls".text = ''
# Automatically inserted by home-manager:    
https://www.aljazeera.com/xml/rss/all.xml
https://www.atptour.com/en/media/rss-feed/xml-feed
http://feeds.bbci.co.uk/news/world/rss.xml
http://feeds.bbci.co.uk/news/technology/rss.xml
https://www.thecipherbrief.com/feed
http://rss.cnn.com/rss/cnn_topstories.rss
http://rss.cnn.com/rss/edition_world.rss
http://lxer.com/module/newswire/headlines.rss
http://feeds.feedburner.com/tennisx
https://feeds.washingtonpost.com/rss/world
    '';


  };

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/<username>/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    # EDITOR = "emacs";
  };

  programs.bash.enable = true;
  programs.bash.bashrcExtra = ''
      export PATH="$PATH:$HOME/bin:$HOME/.local/bin:$HOME/go/bin"
    '';


      # To do: configure Numix circle icon theme here:
  # inspect output of following command:
  # dconf dump /org/cinnamon/ > cinnamon.dconf
  # use with https://github.com/gvolpe/dconf2nix
  # see list of other home-manager settings here: 
  # https://rycee.gitlab.io/home-manager/options.html#opt-dconf.settings

gtk.iconTheme.name = "Numix-Circle-Light";

dconf.settings = {
    
    # parameter org/cinnamon/desktop/a11y/mouse below should never contain a / before org
    # and never a / after mouse!

    "org/cinnamon/desktop/a11y/mouse" = {
      dwell-threshold = 10;
      dwell-time = 1.2;
      secondary-click-time = 1.2;
    };

    "org/cinnamon/desktop/applications/calculator" = {
      exec = "gnome-calculator";
    };

    "org/cinnamon/desktop/applications/terminal" = {
      exec = "gnome-terminal";
      exec-arg = "--";
    };

    "org/cinnamon/desktop/background/slideshow" = {
      delay = 15;
      image-source = "directory:///etc/nixos/common";
    };

    "org/cinnamon/desktop/interface" = {
      cursor-theme = "XCursor-Pro-Light";
      font-name = "Ubuntu 10";
      gtk-theme = "Mint-Y-Sand";
      icon-theme = "Numix-Circle-Light";
      text-scaling-factor = 1.0;
    };

    "org/cinnamon/desktop/media-handling" = {
      autorun-never = false;
      autorun-x-content-ignore = [];
      autorun-x-content-open-folder = [ "x-content/image-dcf" ];
      autorun-x-content-start-app = [ "x-content/unix-software" "x-content/image-dcf" ];
    };

    "org/cinnamon/desktop/sound" = {
      event-sounds = false;
    };

    "org/cinnamon/settings-daemon/peripherals/keyboard" = {
      numlock-state = "off";
    };

    "org/cinnamon/settings-daemon/plugins/power" = {
      lid-close-ac-action = "suspend";
      lid-close-battery-action = "suspend";
      sleep-display-ac = 1800;
      sleep-display-battery = 1800;
      sleep-inactive-ac-timeout = 0;
      sleep-inactive-battery-timeout = 0;
    };

    "org/cinnamon/settings-daemon/plugins/xsettings" = {
      buttons-have-icons = true;
    };

    "org/cinnamon/theme" = {
      name = "New-Minty";
    };

  };
    
}

  # Home Manager configuration file for NixOS 23.05 or newer 
  # Author: Mark Rijckenberg
  # Find all NixOS packages here: https://nixos.org/
  # NixOS manual: https://nixos.org/manual/nixos/stable/
  # Extra hardware support: https://github.com/NixOS/nixos-hardware
  # Distro comparisons: https://repology.org/
  # Home Manager option search: https://mipmip.github.io/home-manager-option-search/?query=
  # https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
  # https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
  # https://nixos.wiki/wiki/NixOS_Generations_Trimmer
  # File creation date: 2023/7/6
  # Last modification date: 2023/11/25
