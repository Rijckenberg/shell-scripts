{
  config,
  inputs,
  isLinux,
  lib,
  nur,
  pkgs,
  theme,
  ...
}:

# Change history:
# 2023/11/24: added section programs.firefox
# 2023/12/5: added teamviewer.desktop as test - autostart of teamviewer enabled for Gerard
# 2023/12/12: added extension-settings.json configuration for Mozilla Firefox
# 2023/12/18: added settings.json configuration to fix integration of Powershell in VSCodium
# 2025/1/2: configurations hardcoded here for following applications:
# fastfetch
# heroic
# newsboat
# qBittorrent
# VSCodium
# 2025/3/5: enabled custom librewolf configuration 

with lib.hm.gvariant;

let
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  # inherit (config.my) username;
  username = "ulysses";

  homeDirectory = "/home/${username}";
  configHome = "${homeDirectory}/.config";

  inherit (lib) mkIf mkDefault;
  inherit (pkgs.stdenv.hostPlatform) isLinux;
  dummyPackage = pkgs.runCommandLocal "dummy" { } "mkdir $out";
  packageIfLinux = x: if isLinux then x else dummyPackage;

in

{

    # See https://mynixos.com/home-manager/options/programs.firefox

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

   # See https://mynixos.com/home-manager/options/programs.librewolf
  # Use about:config in Librewolf search bar to verify if settings are correctly changed


programs.librewolf = {
          enable = true;
          settings = {
            "browser.bookmarks.addedImportButton" = true;
            "browser.policies.runOncePerModification.removeSearchEngines" = ''["Google" "Bing" "eBay" ]'';
            "browser.shell.checkDefaultBrowser" = false;
            "browser.shell.defaultBrowserCheckCount" = 1;
            "browser.tabs.warnOnClose" = true;
            "browser.uitour.enabled" = false;
            # Enable HTTPS-Only Mode
            "dom.security.https_only_mode" = true;
            "dom.security.https_only_mode_ever_enabled" = true;
            "extensions.update.enabled" = true;
            "extensions.update.autoUpdateDefault" = true;
            # See https://librewolf.net/docs/faq/#how-do-i-fully-prevent-autoplay for options
			      "media.autoplay.blocking_policy" = 2;
            "media.ffmpeg.vaapi.enabled" = true;
            "media.hardware-video-decoding.force-enabled" = true;
            "network.cookie.lifetimePolicy" = 2;
            "network.dns.disableIPv6" = true;
            "privacy.clearOnShutdown.cookies" = true;
            "privacy.clearOnShutdown.history" = true;
            "privacy.donottrackheader.enabled" = true;
            "privacy.fingerprintingProtection" = true;
            "privacy.resistFingerprinting" = true;
            # Harden
            "privacy.trackingprotection.enabled" = true;
            "trailhead.firstrun.didSeeAboutWelcome" = true;
            "webgl.disabled" = false;
          };
        };

   # See https://github.com/nix-community/home-manager/blob/master/modules/programs/zoxide.nix
   programs.zoxide.enable = true;
   programs.zoxide.enableBashIntegration = true;

    imports = builtins.concatMap import [
    #./modules
    #./programs
    #./scripts
    #./services
    #./themes
  ];

    xdg = {
    inherit configHome;
    enable = true;
  };

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change home.stateVersion value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  # Please read the comment before changing.

  home = {
    # Home Manager needs a bit of information about you and the paths it should
    # manage.
    inherit username homeDirectory;
    stateVersion = "24.11";

    sessionVariables = {
      BROWSER = "chromium-browser";
      DISPLAY = ":0";
      EDITOR = "geany";
    };
  };

  # restart services on change
  systemd.user.startServices = "sd-switch";

  # notifications about home-manager news
  news.display = "silent";


  # The home.packages option allows you to install Nix packages into your
  # environment.
  # home.packages = with pkgs; [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # hello

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
  # ];


  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';

 ".config/autostart/bitwarden.desktop".text = ''
# Automatically inserted by home-manager:    
[Desktop Entry]
Categories=Utility
Comment=A secure and free password manager for all of your devices
Exec=bitwarden %U
Icon=bitwarden
Name=Bitwarden
Type=Application
X-GNOME-Autostart-enabled=true
NoDisplay=false
Hidden=false
Name[en_US]=Bitwarden
Comment[en_US]=A secure and free password manager for all of your devices
X-GNOME-Autostart-Delay=0
    '';

".config/autostart/pcloud.desktop".text = ''
# Automatically inserted by home-manager:    
[Desktop Entry]
Type=Application
Name=pcloud
Icon=pcloud
GenericName=File Synchroniser
Comment=Sync your files with pCloud
TryExec=pcloud
Exec=pcloud
Hidden=false
Terminal=false
Categories=Network;FileTransfer;
X-GNOME-Autostart-enabled=true
NoDisplay=false
Name[en_US]=pcloud
Comment[en_US]=Sync your files with pCloud
X-GNOME-Autostart-Delay=0
    '';

# ".config/autostart/teamviewer.desktop".text = ''
# Automatically inserted by home-manager:    
# [Desktop Entry]
# Type=Application
# Name=Teamviewer
# Icon=teamviewer
# GenericName=Desktop sharing application, providing remote support and online meetings
# Comment=Desktop sharing application, providing remote support and online meetings
# TryExec=teamviewer
# Exec=teamviewer
# Hidden=false
# Terminal=false
# Categories=RemoteControl;
# X-GNOME-Autostart-enabled=true
# NoDisplay=false
# Name[en_US]=Teamviewer
# Comment[en_US]=Desktop sharing application, providing remote support and online meetings
# X-GNOME-Autostart-Delay=0
#     '';

".config/fastfetch/config.jsonc".text = ''
{
    "$schema": "https://github.com/fastfetch-cli/fastfetch/raw/dev/doc/json_schema.json",
    "logo": {
        "padding": {
            "top": 2
        }
    },
    "display": {
        "separator": " -> "
    },
    "modules": [
        "title",
        "separator",
        {
            "type": "os",
            "key": " OS",
            "keyColor": "yellow",
            "format": "{2}"
        },
        {
            "type": "os",
            "key": "├", // Just get your distro's logo off nerdfonts.com
            "keyColor": "yellow",
            "format": "{6}{?6} {?}{10} {8}"
        },
        {
            "type": "kernel",
            "key": "├",
            "keyColor": "yellow"
        },
        {
            "type": "packages",
            "key": "├󰏖",
            "keyColor": "yellow"
        },
        {
            "type": "shell",
            "key": "└",
            "keyColor": "yellow"
        },
        "break",

        {
            "type": "wm",
            "key": " DE/WM",
            "keyColor": "blue"
        },
        {
            "type": "lm",
            "key": "├󰧨",
            "keyColor": "blue"
        },
        {
            "type": "wmtheme",
            "key": "├󰉼",
            "keyColor": "blue"
        },
        {
            "type": "icons",
            "key": "├󰀻",
            "keyColor": "blue"
        },
        {
            "type": "terminal",
            "key": "├",
            "keyColor": "blue"
        },
        {
            "type": "wallpaper",
            "key": "└󰸉",
            "keyColor": "blue"
        },

        "break",
        {
            "type": "host",
            "key": "󰌢 PC",
            "keyColor": "green"
        },
        {
            "type": "cpu",
            "key": "├󰻠",
            "keyColor": "green"
        },
        {
            "type": "gpu",
            "key": "├󰍛",
            "keyColor": "green"
        },
        {
            "type": "disk",
            "key": "├",
            "keyColor": "green"
        },
        {
            "type": "memory",
            "key": "├󰑭",
            "keyColor": "green"
        },
        {
            "type": "swap",
            "key": "├󰓡",
            "keyColor": "green"
        },
        {
            "type": "uptime",
            "key": "├󰅐",
            "keyColor": "green"
        },
        {
            "type": "display",
            "key": "└󰍹",
            "keyColor": "green"
        },

        "break",
        {
            "type": "sound",
            "key": " SOUND",
            "keyColor": "cyan"
        },
        {
            "type": "player",
            "key": "├󰥠",
            "keyColor": "cyan"
        },
        {
            "type": "media",
            "key": "└󰝚",
            "keyColor": "cyan"
        },

        "break"
    ]
}

'';

".config/heroic/config.json".text = ''
{
  "defaultSettings": {
    "checkUpdatesInterval": 10,
    "enableUpdates": false,
    "addDesktopShortcuts": true,
    "addStartMenuShortcuts": true,
    "autoInstallDxvk": true,
    "autoInstallVkd3d": true,
    "autoInstallDxvkNvapi": false,
    "addSteamShortcuts": true,
    "preferSystemLibs": false,
    "checkForUpdatesOnStartup": false,
    "autoUpdateGames": true,
    "customWinePaths": [],
    "defaultInstallPath": "~/Games/Heroic",
    "libraryTopSection": "recently_played",
    "defaultSteamPath": "~/.steam/steam",
    "defaultWinePrefix": "~/Games/Heroic/Prefixes/default",
    "hideChangelogsOnStartup": false,
    "language": "en",
    "maxWorkers": 0,
    "minimizeOnLaunch": false,
    "nvidiaPrime": false,
    "enviromentOptions": [],
    "wrapperOptions": [],
    "showFps": false,
    "useGameMode": false,
    "wineCrossoverBottle": "Heroic",
    "winePrefix": "~/Games/Heroic/Prefixes/default",
    "wineVersion": {
      "bin": "~/.config/heroic/tools/proton/GE-Proton9-22/proton",
      "name": "Proton - GE-Proton9-22",
      "type": "proton"
    },
    "enableEsync": false,
    "enableFsync": true,
    "enableMsync": false,
    "eacRuntime": false,
    "battlEyeRuntime": false,
    "framelessWindow": false,
    "beforeLaunchScriptPath": "",
    "afterLaunchScriptPath": "",
    "maxRecentGames": 10,
    "showMangohud": true,
    "experimentalFeatures": {
      "enableNewDesign": false,
      "enableHelp": false,
      "automaticWinetricksFixes": false
    },
    "gamescope": {
      "enableUpscaling": false,
      "enableLimiter": false,
      "windowType": "fullscreen",
      "gameWidth": "",
      "gameHeight": "",
      "upscaleHeight": "",
      "upscaleWidth": "",
      "upscaleMethod": "fsr",
      "fpsLimiter": "100",
      "fpsLimiterNoFocus": "100",
      "additionalOptions": ""
    }
  },
  "version": "v0"
}

'';

".config/qBittorrent/qBittorrent.conf".text = ''

[AddNewTorrentDialog]
DialogSize=@Size(1274 647)
DownloadPathHistory=
RememberLastSavePath=false
SavePathHistory=~/Downloads

[Application]
FileLogger\Age=1
FileLogger\AgeType=1
FileLogger\Backup=true
FileLogger\DeleteOld=true
FileLogger\Enabled=true
FileLogger\MaxSizeBytes=66560
FileLogger\Path=~/.local/share/qBittorrent/logs
GUI\Notifications\TorrentAdded=false

[BitTorrent]
Session\AnonymousModeEnabled=true
Session\Encryption=1
Session\MaxActiveCheckingTorrents=14
Session\Port=59447
Session\QueueingSystemEnabled=false
Session\SSL\Port=43091
Session\StartPaused=false

[Core]
AutoDeleteAddedTorrentFile=Never

[GUI]
DownloadTrackerFavicon=false
Log\Enabled=false
Qt6\AddNewTorrentDialog\SplitterState=@ByteArray(\0\0\0\xff\0\0\0\x1\0\0\0\x2\0\0\x1\x8c\0\0\x1\x86\0\xff\xff\xff\xff\x1\0\0\0\x1\0)
Qt6\AddNewTorrentDialog\TreeHeaderState=@ByteArray(\0\0\0\xff\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x6\x34\0\0\0\x3\0\0\0\x5\0\0\0\x64\0\0\0\x2\0\0\0\x64\0\0\0\x4\0\0\0\x64\0\0\x2{\0\0\0\x6\x1\x1\0\x1\0\0\0\0\0\0\0\0\0\0\0\0\x64\xff\xff\xff\xff\0\0\0\x81\0\0\0\0\0\0\0\x6\0\0\x1\xb3\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\x3\xe8\0\0\0\0\x64\0\0\0\0)
Qt6\SearchTab\HeaderState=@ByteArray(\0\0\0\xff\0\0\0\0\0\0\0\x1\0\0\0\x1\0\0\0\x2\x1\0\0\0\0\0\0\0\0\0\0\0\t\x80\x1\0\0\0\x2\0\0\0\a\0\0\0\x64\0\0\0\b\0\0\0\x64\0\0\x5\x86\0\0\0\t\x1\x1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x64\xff\xff\xff\xff\0\0\0\x81\0\0\0\0\0\0\0\t\0\0\x3.\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\x3\xe8\0\xff\xff\xff\xff\0\0\0\0)
Qt6\TorrentProperties\FilesListState=@ByteArray(\0\0\0\xff\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x4\x98\0\0\0\x6\x1\x1\0\x1\0\0\0\0\0\0\0\0\0\0\0\0\x64\xff\xff\xff\xff\0\0\0\x81\0\0\0\0\0\0\0\x6\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\x2\xa4\0\0\0\x1\0\0\0\0\0\0\x3\xe8\0\0\0\0\x64\0\0\0\0)
Qt6\TorrentProperties\PeerListState=@ByteArray(\0\0\0\xff\0\0\0\0\0\0\0\x1\0\0\0\x1\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\xf@@\0\0\0\x2\0\0\0\xe\0\0\0\x64\0\0\0\x6\0\0\0\x64\0\0\x5\x14\0\0\0\xf\x1\x1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x64\xff\xff\xff\xff\0\0\0\x81\0\0\0\0\0\0\0\xf\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\x3\xe8\0\xff\xff\xff\xff\0\0\0\0)
Qt6\TorrentProperties\TrackerListState=@ByteArray(\0\0\0\xff\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x4L\0\0\0\v\x1\x1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x64\xff\xff\xff\xff\0\0\0\x81\0\0\0\0\0\0\0\v\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\x3\xe8\0\xff\xff\xff\xff\0\0\0\0)
Qt6\TransferList\HeaderState=@ByteArray(\0\0\0\xff\0\0\0\0\0\0\0\x1\0\0\0\x1\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0%\t\x90\xff\x7f\x1f\0\0\0\x18\0\0\0\x18\0\0\0\x64\0\0\0$\0\0\0\x64\0\0\0\x1b\0\0\0\x64\0\0\0#\0\0\0\x64\0\0\0\x14\0\0\0\x64\0\0\0\x17\0\0\0\x64\0\0\0\x11\0\0\0\x64\0\0\0\x1c\0\0\0\x64\0\0\0\x10\0\0\0\x64\0\0\0\x13\0\0\0\x64\0\0\0\"\0\0\0\x64\0\0\0\x12\0\0\0\x64\0\0\0!\0\0\0\x64\0\0\0\x16\0\0\0\x64\0\0\0\x1a\0\0\0\x64\0\0\0\x1d\0\0\0\x64\0\0\0\f\0\0\0\x64\0\0\0\x3\0\0\0\x64\0\0\0\0\0\0\0\x64\0\0\0 \0\0\0\x64\0\0\0\x1e\0\0\0\x64\0\0\0\xf\0\0\0\x64\0\0\0\x19\0\0\0\x64\0\0\0\x15\0\0\0\x64\0\0\x6\x16\0\0\0%\x1\x1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x64\xff\xff\xff\xff\0\0\0\x81\0\0\0\0\0\0\0%\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\x1\x66\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\x64\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\0\0\0\0\0\x1\0\0\0\0\0\0\x3\xe8\0\0\0\0\x64\0\0\0\0)
StartUpWindowState=Normal

[LegalNotice]
Accepted=true

[MainWindow]
geometry=@ByteArray(\x1\xd9\xd0\xcb\0\x3\0\0\xff\xff\xff\xfd\xff\xff\xff\xe2\0\0\x4\xfc\0\0\x2\x89\0\0\0\0\0\0\0\0\xff\xff\xff\xfe\xff\xff\xff\xfe\0\0\0\0\x2\0\0\0\x5\0\0\0\0\0\0\0\0\0\0\0\x4\xf9\0\0\x2\x86)

[Meta]
MigrationVersion=8

[Network]
Cookies=@Invalid()

[OptionsDialog]
HorizontalSplitterSizes=123, 630
LastViewedPage=1
Size=@Size(779 591)

[Preferences]
General\CloseToTrayNotified=true
General\Locale=en
Search\SearchEnabled=true

[RSS]
AutoDownloader\DownloadRepacks=true
AutoDownloader\SmartEpisodeFilter=s(\\d+)e(\\d+), (\\d+)x(\\d+), "(\\d{4}[.\\-]\\d{1,2}[.\\-]\\d{1,2})", "(\\d{1,2}[.\\-]\\d{1,2}[.\\-]\\d{4})"

[Search]
FilteringMode=OnlyNames

[SearchPluginSelectDialog]
Size=@Size(625 345)

[TorrentOptionsDialog]
Size=@Size(482 599)

[TorrentProperties]
CurrentTab=0
SplitterSizes="296,222"
Visible=true

'';

".config/VSCodium/User/settings.json".text = ''
{
    "powershell.cwd": "/etc/profiles/per-user/ulysses/bin/",
    "powershell.powerShellAdditionalExePaths": {
        "powershell.powerShellAdditionalExePaths": "/etc/profiles/per-user/ulysses/bin/pwsh"
    },
    "powershell.startAsLoginShell.osx": false,
    "powershell.startAsLoginShell.linux": true
}

'';

".mozilla/firefox/main/extension-settings.json".text = ''

# Automatically inserted by home-manager:    
{
  "version": 3,
  "url_overrides": {},
  "prefs": {
    "websites.hyperlinkAuditingEnabled": {
      "initialValue": {},
      "precedenceList": [
        {
          "id": "uBlock0@raymondhill.net",
          "installDate": 1702065313731,
          "value": false,
          "enabled": true
        }
      ]
    },
    "network.networkPredictionEnabled": {
      "initialValue": {},
      "precedenceList": [
        {
          "id": "uBlock0@raymondhill.net",
          "installDate": 1702065313731,
          "value": false,
          "enabled": true
        }
      ]
    }
  },
  "commands": {}
}

    '';

".newsboat/config".text = ''
# Automatically inserted by home-manager:    
refresh-on-startup yes
# reload-time desired_time_in_minutes
reload-time 60
    '';

".newsboat/urls".text = ''
# Automatically inserted by home-manager:    
https://www.aljazeera.com/xml/rss/all.xml
https://www.atptour.com/en/media/rss-feed/xml-feed
http://feeds.bbci.co.uk/news/world/rss.xml
http://feeds.bbci.co.uk/news/technology/rss.xml
https://www.thecipherbrief.com/feed
http://rss.cnn.com/rss/cnn_topstories.rss
http://rss.cnn.com/rss/edition_world.rss
http://lxer.com/module/newswire/headlines.rss
http://feeds.feedburner.com/tennisx
https://feeds.washingtonpost.com/rss/world
    '';


  };

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/<username>/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    # EDITOR = "emacs";
  };

  programs.bash.enable = true;
  programs.bash.bashrcExtra = ''
      export PATH="$PATH:$HOME/bin:$HOME/.local/bin:$HOME/go/bin:$HOME/.cargo/bin"
    '';

      # To do: configure Numix circle icon theme here:
  # inspect output of following command:
  # dconf dump /org/cinnamon/ > cinnamon.dconf
  # use with https://github.com/gvolpe/dconf2nix
  # see list of other home-manager settings here: 
  # https://rycee.gitlab.io/home-manager/options.html#opt-dconf.settings

gtk.iconTheme.name = "Numix-Circle-Light";

dconf.settings = {
    
    # parameter org/cinnamon/desktop/a11y/mouse below should never contain a / before org
    # and never a / after mouse!

    "com/github/tkashkin/gamehub/auth/steam" = {
      authenticated = true;
    };

    "com/github/tkashkin/gamehub/saved-state/window" = {
      height = 633;
      width = 1108;
      x = 95;
      y = 26;
    };

    "com/usebottles/bottles" = {
      show-sandbox-warning = false;
      startup-view = "page_list";
      window-height = 680;
      window-width = 1280;
    };

    "org/blueman/general" = {
      services-last-item = 0;
      window-properties = [ 1280 652 0 0 ];
    };

    "org/blueman/network" = {
      nap-enable = false;
    };

    "org/blueman/plugins/autoconnect" = {
      services = [ (mkTuple [ "/org/bluez/hci0/dev_C0_A5_3E_4E_01_86" "00000000-0000-0000-0000-000000000000" ]) ];
    };

    "org/blueman/plugins/powermanager" = {
      auto-power-on = false;
    };


    "org/cinnamon" = {
      enabled-applets = [ "panel1:left:0:menu@cinnamon.org:0" "panel1:left:1:separator@cinnamon.org:1" "panel1:left:2:grouped-window-list@cinnamon.org:2" "panel1:right:0:systray@cinnamon.org:3" "panel1:right:1:xapp-status@cinnamon.org:4" "panel1:right:2:notifications@cinnamon.org:5" "panel1:right:3:printers@cinnamon.org:6" "panel1:right:4:removable-drives@cinnamon.org:7" "panel1:right:5:keyboard@cinnamon.org:8" "panel1:right:6:favorites@cinnamon.org:9" "panel1:right:7:network@cinnamon.org:10" "panel1:right:8:sound@cinnamon.org:11" "panel1:right:9:power@cinnamon.org:12" "panel1:right:10:calendar@cinnamon.org:13" "panel1:right:11:cornerbar@cinnamon.org:14" ];
      enabled-desklets = [];
      next-applet-id = 15;
    };

    "org/cinnamon/cinnamon-session" = {
      quit-time-delay = 60;
    };

    "org/cinnamon/desktop/a11y/mouse" = {
      dwell-threshold = 10;
      dwell-time = 1.2;
      secondary-click-time = 1.2;
    };

    "org/cinnamon/desktop/applications/calculator" = {
      exec = "gnome-calculator";
    };

    "org/cinnamon/desktop/applications/terminal" = {
      exec = "gnome-terminal";
      exec-arg = "--";
    };

    "org/cinnamon/desktop/interface" = {
      cursor-blink-time = 1200;
      cursor-theme = "XCursor-Pro-Light";
      font-name = "Ubuntu 10";
      icon-theme = "Numix-Circle-Light";
      text-scaling-factor = 1.0;
    };

    "org/cinnamon/desktop/media-handling" = {
      autorun-never = false;
      autorun-x-content-ignore = [];
      autorun-x-content-open-folder = [ "x-content/image-dcf" ];
      autorun-x-content-start-app = [ "x-content/unix-software" "x-content/image-dcf" ];
    };

    "org/cinnamon/desktop/peripherals/keyboard" = {
      delay = mkUint32 500;
      numlock-state = true;
      repeat-interval = mkUint32 30;
    };

    "org/cinnamon/desktop/sound" = {
      event-sounds = false;
    };

    "org/cinnamon/gestures" = {
      swipe-down-2 = "PUSH_TILE_DOWN::end";
      swipe-down-3 = "TOGGLE_OVERVIEW::end";
      swipe-down-4 = "VOLUME_DOWN::end";
      swipe-left-2 = "PUSH_TILE_LEFT::end";
      swipe-left-3 = "WORKSPACE_NEXT::end";
      swipe-left-4 = "WINDOW_WORKSPACE_PREVIOUS::end";
      swipe-right-2 = "PUSH_TILE_RIGHT::end";
      swipe-right-3 = "WORKSPACE_PREVIOUS::end";
      swipe-right-4 = "WINDOW_WORKSPACE_NEXT::end";
      swipe-up-2 = "PUSH_TILE_UP::end";
      swipe-up-3 = "TOGGLE_EXPO::end";
      swipe-up-4 = "VOLUME_UP::end";
      tap-3 = "MEDIA_PLAY_PAUSE::end";
    };

    "org/cinnamon/muffin" = {
      experimental-features = [ "scale-monitor-framebuffer" "x11-randr-fractional-scaling" ];
    };

    "org/cinnamon/settings-daemon/peripherals/keyboard" = {
      numlock-state = "off";
    };

    "org/cinnamon/settings-daemon/plugins/power" = {
      lid-close-ac-action = "suspend";
      lid-close-battery-action = "suspend";
      sleep-display-ac = 1800;
      sleep-display-battery = 1800;
      sleep-inactive-ac-timeout = 0;
      sleep-inactive-battery-timeout = 0;
    };

    "org/cinnamon/settings-daemon/plugins/xsettings" = {
      buttons-have-icons = true;
    };

    "org/cinnamon/theme" = {
      name = "New-Minty";
    };

    "org/gnome/calculator" = {
      accuracy = 9;
      angle-units = "degrees";
      base = 10;
      button-mode = "basic";
      number-format = "automatic";
      show-thousands = false;
      show-zeroes = false;
      source-currency = "";
      source-units = "degree";
      target-currency = "";
      target-units = "radian";
      window-maximized = false;
      window-size = mkTuple [ 360 495 ];
      word-size = 64;
    };

    "org/gnome/calendar" = {
      active-view = "week";
      window-maximized = true;
      window-size = mkTuple [ 768 600 ];
    };

    "org/gnome/desktop/a11y" = {
      always-show-text-caret = false;
    };

    "org/gnome/desktop/a11y/applications" = {
      screen-keyboard-enabled = false;
      screen-reader-enabled = false;
    };

    "org/gnome/desktop/a11y/mouse" = {
      dwell-click-enabled = false;
      dwell-threshold = 10;
      dwell-time = 1.2;
      secondary-click-enabled = false;
      secondary-click-time = 1.2;
    };

    "org/gnome/desktop/interface" = {
      can-change-accels = false;
      clock-show-date = false;
      clock-show-seconds = false;
      cursor-blink = true;
      cursor-blink-time = 1200;
      cursor-blink-timeout = 10;
      # following gnome settings should be disabled when running Plasma desktop:
      # cursor-size = 32;
      # cursor-theme = "Vanilla-DMZ";
      # document-font-name = "DejaVu Serif  11";
      enable-animations = true;
      # following gnome settings should be disabled when running Plasma desktop:
      # font-name = "DejaVu Sans 12";
      # gtk-color-palette = "black:white:gray50:red:purple:blue:light blue:green:yellow:orange:lavender:brown:goldenrod4:dodger blue:pink:light green:gray10:gray30:gray75:gray90";
      # gtk-color-scheme = "";
      # gtk-enable-primary-paste = true;
      # gtk-im-module = "";
      # gtk-im-preedit-style = "callback";
      # gtk-im-status-style = "callback";
      # gtk-key-theme = "Default";
      # gtk-theme = "adw-gtk3";
      # gtk-timeout-initial = 200;
      # gtk-timeout-repeat = 20;
      icon-theme = "Numix-Circle-Light";
      menubar-accel = "F10";
      menubar-detachable = false;
      menus-have-tearoff = false;
      # following gnome setting should be disabled when running Plasma desktop:
      # monospace-font-name = "DejaVu Sans Mono 12";
      scaling-factor = mkUint32 1;
      text-scaling-factor = 1.0;
      toolbar-detachable = false;
      toolbar-icons-size = "large";
      toolbar-style = "text";
      toolkit-accessibility = false;
    };

    "org/gnome/desktop/peripherals/mouse" = {
      accel-profile = "default";
      double-click = 400;
      drag-threshold = 8;
      left-handed = false;
      middle-click-emulation = false;
      natural-scroll = false;
      speed = 0.0;
    };

    "org/gnome/desktop/privacy" = {
      disable-camera = false;
      disable-microphone = false;
      disable-sound-output = false;
      old-files-age = mkUint32 30;
      recent-files-max-age = 7;
      remember-recent-files = true;
      remove-old-temp-files = false;
      remove-old-trash-files = false;
    };

    "org/gnome/desktop/sound" = {
      event-sounds = false;
      input-feedback-sounds = false;
      theme-name = "ocean";
    };

    "org/gnome/desktop/wm/preferences" = {
      action-double-click-titlebar = "toggle-maximize";
      action-middle-click-titlebar = "lower";
      action-right-click-titlebar = "menu";
      audible-bell = false;
      auto-raise = false;
      auto-raise-delay = 500;
      button-layout = "icon:minimize,maximize,close";
      disable-workarounds = false;
      focus-mode = "click";
      focus-new-windows = "smart";
      mouse-button-modifier = "<Alt>";
      num-workspaces = 4;
      raise-on-click = true;
      resize-with-right-button = true;
      theme = "Mint-Y";
      titlebar-font = "Ubuntu Medium 10";
      titlebar-uses-system-font = false;
      visual-bell = false;
      visual-bell-type = "fullscreen-flash";
      workspace-names = [];
    };

    "org/gnome/evolution-data-server" = {
      migrated = true;
    };

    "org/gnome/gedit/state/window" = {
      bottom-panel-size = 140;
      side-panel-active-page = "GeditWindowDocumentsPanel";
      side-panel-size = 200;
      size = mkTuple [ 900 633 ];
      state = 87168;
    };

    "org/gnome/gnumeric/plugins" = {
      active = [ "GOffice_plot_pie" "GOffice_plot_barcol" "GOffice_plot_radar" "GOffice_plot_distrib" "GOffice_reg_linear" "GOffice_smoothing" "GOffice_lasem" "GOffice_plot_xy" "GOffice_plot_surface" "GOffice_reg_logfit" "Gnumeric_dif" "Gnumeric_applix" "Gnumeric_xbase" "Gnumeric_fnmath" "Gnumeric_excelplugins" "Gnumeric_sylk" "Gnumeric_fnerlang" "Gnumeric_fndate" "Gnumeric_plan_perfect" "Gnumeric_numtheory" "Gnumeric_fncomplex" "Gnumeric_lpsolve" "Gnumeric_oleo" "Gnumeric_fnflt" "Gnumeric_fneng" "Gnumeric_fninfo" "Gnumeric_fnfinancial" "Gnumeric_OpenCalc" "Gnumeric_fnlogical" "Gnumeric_sc" "Gnumeric_lotus" "Gnumeric_fndatabase" "Gnumeric_html" "Gnumeric_derivatives" "Gnumeric_fnrandom" "Gnumeric_r" "Gnumeric_fnstring" "Gnumeric_fnhebdate" "Gnumeric_fnchristiandate" "Gnumeric_QPro" "Gnumeric_nlsolve" "Gnumeric_Excel" "Gnumeric_glpk" "Gnumeric_fnlookup" "Gnumeric_mps" "Gnumeric_fnstat" ];
    };

    "org/gnome/nautilus/preferences" = {
      default-folder-viewer = "icon-view";
      migrated-gtk-settings = true;
      search-filter-time-type = "last_modified";
    };

    "org/gnome/nautilus/window-state" = {
      initial-size = mkTuple [ 890 550 ];
    };

    "org/gnome/shell/extensions/user-theme" = {
      name = "Stylix";
    };

    "org/gnome/shotwell/preferences/window" = {
      direct-height = 652;
      direct-maximize = false;
      direct-width = 1024;
    };

    "org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9" = {
      background-color = "rgb(46,52,54)";
      foreground-color = "rgb(211,215,207)";
      use-theme-colors = false;
    };

    "org/gtk/gtk4/settings/file-chooser" = {
      date-format = "regular";
      location-mode = "path-bar";
      show-hidden = false;
      show-size-column = true;
      show-type-column = true;
      sidebar-width = 140;
      sort-column = "name";
      sort-directories-first = true;
      sort-order = "ascending";
      type-format = "category";
      view-type = "list";
      window-size = mkTuple [ 811 384 ];
    };

    "org/gtk/settings/file-chooser" = {
      date-format = "regular";
      location-mode = "path-bar";
      show-hidden = false;
      show-size-column = true;
      show-type-column = true;
      sidebar-width = 148;
      sort-column = "name";
      sort-directories-first = true;
      sort-order = "ascending";
      type-format = "category";
      window-position = mkTuple [ 180 63 ];
      window-size = mkTuple [ 1096 652 ];
    };

    "org/virt-manager/virt-manager" = {
      manager-window-height = 550;
      manager-window-width = 550;
    };

    "org/virt-manager/virt-manager/confirm" = {
      delete-storage = true;
      forcepoweroff = true;
      unapplied-dev = true;
    };

    "org/virt-manager/virt-manager/connections" = {
      autoconnect = [ "qemu:///system" ];
      uris = [ "qemu:///system" ];
    };

    "org/virt-manager/virt-manager/conns/qemu:system" = {
      window-size = mkTuple [ 800 600 ];
    };

    "org/virt-manager/virt-manager/details" = {
      show-toolbar = true;
    };

    "org/virt-manager/virt-manager/paths" = {
      media-default = "/run/media/ulysses/Ventoy/mark/installers";
    };


    "org/virt-manager/virt-manager/vmlist-fields" = {
      disk-usage = false;
      network-traffic = false;
    };

    "org/virt-manager/virt-manager/vms/bd203aab4a71454db7ce506c97d190b7" = {
      autoconnect = 1;
      scaling = 1;
      vm-window-size = mkTuple [ 1024 841 ];
    };

    "org/virt-manager/virt-manager/vms/d3193c09a7e24716807b4209c326e910" = {
      autoconnect = 1;
      scaling = 2;
      vm-window-size = mkTuple [ 1280 652 ];
    };

    "org/x/editor/plugins" = {
      active-plugins = [ "filebrowser" "spell" "sort" "docinfo" "modelines" "time" ];
    };

    "org/x/editor/preferences/ui" = {
      statusbar-visible = true;
    };

    "org/x/editor/state/window" = {
      bottom-panel-size = 140;
      side-panel-active-page = 827629879;
      side-panel-size = 200;
      size = mkTuple [ 650 500 ];
      state = 43780;
    };

    "org/x/player" = {
      active-plugins = [ "autoload-subtitles" "vimeo" "dbusservice" "chapters" "recent" "movie-properties" "screensaver" "opensubtitles" "media_player_keys" "apple-trailers" "skipto" "screenshot" ];
      debug = false;
      subtitle-encoding = "UTF-8";
    };

  };
    
}

  # Home Manager configuration file for NixOS 23.05 or newer 
  # Author: Mark Rijckenberg
  # Find all NixOS packages here: https://nixos.org/
  # NixOS manual: https://nixos.org/manual/nixos/stable/
  # Extra hardware support: https://github.com/NixOS/nixos-hardware
  # Distro comparisons: https://repology.org/
  # Home Manager option search: https://mipmip.github.io/home-manager-option-search/?query=
  # https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
  # https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
  # https://nixos.wiki/wiki/NixOS_Generations_Trimmer
  # File creation date: 2023/7/6
  # Last modification date: 2025/1/2
