     # Based on https://github.com/vimjoyer/custom-installer-video :
     # Also based on https://haseebmajid.dev/posts/2024-02-04-how-to-create-a-custom-nixos-iso/
     # Command to create custom NixOS iso image: 
     # cd /tmp;nix build /etc/nixos/#nixosConfigurations.gerard-laptop-iso.config.system.build.isoImage
     # Takes around 24 minutes to build iso image.
     # See possible installation cd options here:
     # https://github.com/NixOS/nixpkgs/tree/master/nixos/modules/installer/cd-dvd
     
{ config, inputs, lib, modulesPath, pkgs, ... }: {

  users.extraUsers.root.password = "nixos";

    services = {
    qemuGuest.enable = true;
    openssh.settings.PermitRootLogin = lib.mkForce "yes";
  };
  
  boot.supportedFilesystems = lib.mkForce ["btrfs" "cifs" "ext4" "f2fs" "iso9660" "ntfs" "reiserfs" "vfat" "xfs"];

  imports = [
    # "${modulesPath}/installer/cd-dvd/installation-cd-minimal.nix" # nix build not working!
    "${modulesPath}/installer/cd-dvd/iso-image.nix" # nix build works, but boot from iso fails
    "${modulesPath}/installer/cd-dvd/channel.nix"
  ];

  nixpkgs.hostPlatform = "x86_64-linux";

}