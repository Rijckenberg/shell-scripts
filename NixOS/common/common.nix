# Check https://github.com/mcdonc/.nixconfig/blob/master/common.nix  for interesting changes

{ config
, inputs
, lib
, pkgs
, system
, ... 
}:

{

    # Change history:
    # 2023/10/16: Solved USB keyboard and mouse stuck issue on didi-laptop and ulysses-desktop 
    # by disabling powertop service in ./common/common.nix
    # powerManagement.powertop.enable = false;
    # 2023/11/16: enabled pcscd service to enable hardware support for eid card reader
    # 2023/11/30: added services.pipewire.wireplumber.enable = true;
    # 2023/12/1: added  security.sudo-rs.enable = true;
    # 2023/12/17: added services.redshift.enable = true;
    # 2024/1/4: added nix-script.cachix.org-1:czo3tF6XERpkDdMd6m84XjmgeHQXNeIooSt7b0560+c=
    # 2024/1/24: added 4 wmem and rmem parameters
    # 2024/2/7: added  services.touchegg.enable = false;
    # 2024/2/12: option `services.avahi.nssmdns' defined in ./common/common.nix has been renamed to `services.avahi.nssmdns4'.
    # 2024/2/16: added nix.registry and nix.nixPath to ./common/common.nix
    # 2024/2/29: disabled services.pipewire.lowLatency and disabled inputs.nix-gaming.nixosModules.pipewireLowLatency
    # because not supported anymore in NixOS 24.05
    # 2024/3/2: added system.activationScripts.diff
    # 2024/3/2: enabled repl-flake experimental feature
    # 2024/3/2: added custom scripts start-virsh and nixos-repl
    # 2024/4/15: added programs.firefox.preferences
    # 2024/4/17: added programs.firefox.policies
    # 2024/4/25: added programs.seahorse.enable  to manage keyring passwords and to delete problematic login keyrings
    # 2024/7/31: added pipewire option resample.quality = 10; to improve sound quality
    # 2024/9/27: disabled port 631 due to CVE-2024-47176 (cups-browsed up to version 2.0.1)
    # 2024/9/27: cups-browsed is a helper daemon to browse for remote CUPS queues and IPP network printers
    # 2024/10/23: disabled repl-flake experimental feature because nix does not recognize it anymore
    # 2024/11/30: removed nerd-fonts package because it is broken
    # 2025/1/8: made changes to ./common/common.nix based on https://kokada.dev/blog/an-unordered-list-of-hidden-gems-inside-nixos/
    # 2025/1/13: added security hardening NixOS option as recommended by Anduril NixOS STIG rules and 
    # https://www.stigaview.com/products/srg-gpos/latest/
    # 2025/1/13: DoD's STIG viewer (to view Anduril NixOS STIG rules) is available here: https://public.cyber.mil/stigs/srg-stig-tools/

    boot = {

       kernel.sysctl = {

    # Gaming optimizations:
    # NixOS has the same vm.max_map_count=1048576 issue as Ubuntu but can be fixed with following setting:
    # "vm.max_map_count" = 1048576; # replaced by nix-gaming platformOptimizations module configuration
    "net.ipv4.tcp_mtu_probing" = 1;

    ## TCP optimization
    # TCP Fast Open is a TCP extension that reduces network latency by packing
    # data in the sender’s initial TCP SYN. Setting 3 = enable TCP Fast Open for
    # both incoming and outgoing connections:
    "net.ipv4.tcp_fastopen" = 3;
    # Bufferbloat mitigations + slight improvement in throughput & latency
    # Requires >= 4.9 & kernel module
    "net.ipv4.tcp_congestion_control" = "bbr";
    # Requires >= 4.19
    # "net.core.default_qdisc" = "cake";
    "net.core.default_qdisc" = "fq";
    "net.core.wmem_max" = 1073741824;
    "net.core.rmem_max" = 1073741824;
    "net.ipv4.tcp_rmem" = "4096 87380 1073741824";
    "net.ipv4.tcp_wmem" = "4096 87380 1073741824";
     };
    };

  console = {
    packages=[ pkgs.terminus_font ];
    font="${pkgs.terminus_font}/share/consolefonts/ter-i22b.psf.gz";
    useXkbConfig = false; # use xkbOptions in tty.
  };

  # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
       environment.etc."audit/auditd.conf".text = ''
        admin_space_left = 10%
        admin_space_left_action = syslog
        max_log_file = 100
        max_log_file_action = rotate
        num_logs = 10
        # needed for apparmor
        priority_boost = 0
        space_left = 10%
        space_left_action = syslog
      '';

  # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:  
  # Configure NixOS to store only encrypted representations of passwords. 
environment.etc."login.defs".text = pkgs.lib.mkForce ''
ENCRYPT_METHOD YESCRYPT
'';

  # Configure NixOS to enforce password complexity.
  # added security hardening NixOS option as recommended by Anduril NixOS STIG rules: 
  # lcredit=-1 requires 1 lowercase character, ucredit=-1 requires 1 uppercase character
  environment.etc."/security/pwquality.conf".text = ''
lcredit=-1
ucredit=-1
'';

  # List of shells
  environment.shells = with pkgs; [
    bash
    nushell
  ];

  environment.variables = {
    BROWSER = "chromium";
    # Prevent Wine from changing filetype associations.
    # https://wiki.winehq.org/FAQ#How_can_I_prevent_Wine_from_changing_the_filetype_associations_on_my_system
    # _or_adding_unwanted_menu_entries.2Fdesktop_links.3F
    WINEDLLOVERRIDES = "winemenubuilder.exe=d";
  };

  fonts = {
    packages = with pkgs; [
      fira-code
      font-awesome # required for typst projects
      hack-font
      montserrat
      noto-fonts
      open-sans
      roboto # required for typst projects
      source-code-pro
      source-sans # required for typst projects
      source-sans-pro # required for typst projects
      source-serif
    ];
    fontconfig = {
      enable = true;
      antialias = true;
      defaultFonts = {
	      monospace = [ "Meslo LG M Regular Nerd Font Complete Mono" ];
	      serif = [ "Noto Serif" ];
	      sansSerif = [ "Noto Sans" ];
      };
    };
};

  # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
  # Disable bluetooth support
  hardware.bluetooth.enable = false;

  # Disable A2DP Sink for modern bluetooth headsets:
  #hardware.bluetooth.settings = {
  #General = {
  #  Enable = "Source,Sink,Media,Socket";
  #   };
  #  };

  # enable microcode updates:
  hardware.cpu.amd.updateMicrocode = true;
  hardware.cpu.intel.updateMicrocode = true;
  
  hardware.enableAllFirmware = true;

# enable hardware video acceleration and enable Nix User
# Repository (NUR) - community-driven meta repository for Nix packages
# Search for NUR packages here: https://nur.nix-community.org/
   hardware.graphics = {
    enable = true;
    enable32Bit = true;
    # extraPackages = with pkgs; [
      #intel-media-driver # LIBVA_DRIVER_NAME=iHD
      #vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Chromium)
      #vaapiVdpau
      #libvdpau-va-gl
    # ];
    # extraPackages32 = with pkgs.pkgsi686Linux; [ 
      #intel-media-driver # LIBVA_DRIVER_NAME=iHD
      #vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Chromium)
      #vaapiVdpau
      #libvdpau-va-gl
    # ];
  };

  # HARDWARE-SPECIFIC configuration:
    imports =
    [ # Include the results of the hardware scan.
       ./../hardware-configuration.nix
       #inputs.nix-gaming.nixosModules.pipewireLowLatency
       #inputs.nix-gaming.nixosModules.steam-compat
      #(fetchTarball "https://github.com/czapek1337/limine-nix/tarball/master")
      # https://github.com/fufexan/nix-gaming
      #"${nix-gaming}/modules/pipewireLowLatency.nix"
      #"${nix-gaming}/modules/steamCompat.nix"
      # "${builtins.fetchGit { url = "https://github.com/NixOS/nixos-hardware.git"; }}/hp/elitebook/845/g9"
      # ./vm.nix
      # <nixpkgs/nixos/modules/profiles/hardened.nix>
    ];
    ################################################################
     
    # redshift service requires use of geoclue2:
    location.provider = "geoclue2";

    networking.enableIPv6 = false;

    # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
    # Open ports in the firewall 
    networking.firewall = {

    # HomeAssistant does not allow using ssh via port 22, so port 22 is blocked
    # Port 123 is used by Network Time Protocol (NTP)
    # Port 443 is used by Anydesk and Teamviewer
    # Port 563 is used for encrypted Network News Transfer Protocol/Usenet connections (used by pan application)
    # Enable port 631 for IPP/network printing in CUPS
    # Enable port 853 for DNS-over-TLS
    # Disable gnunet ports  1080 2086 2088 2186 13881
    # Enable port 1194 for OpenVPN
    # Enable Soulseek/nicotine-plus ports  2234 2239 2240 2242
    # Enable port 4444 for HTTP proxy for i2pd
    # Enable port 4445 for HTTPS proxy for i2pd
    # Enable port 4447 for SOCKS proxy for i2pd
    # Disable amule ports  4662 and 4672 and 4711 -> amule fails to compile
    # Enable port 5222 for Google Talk Jabber instant messaging software client-to-server connection
    # Enable port 5353 to allow autodiscovery of network printers
    # Port 5938 is used by Teamviewer
    # Enable gnutella port 65535
    # Port 7654, 7655 and 7656 are used by i2pd (I2P daemon to encrypt and anonymize connections)
    # Port 7070 is used by AnyDesk
    # Port 8080 is used by Stirling-PDF (locally hosted web-based PDF manipulation tool using Docker)
    # Port 9050 is used by Tor SOCKS port
    # Port 11371 is used by OpenPGP HTTP key server
    # Port 30747 is used as i2pd listen port
    # Block port 51820 to avoid authentication issues with Wireguard protocol in Proton VPN client
    # Port 64747 is used as ntcp2 port for i2pd
    # Port 65535 is used by Oracle cloud infra and by fopnu filesharing application and gnutella
    allowedTCPPorts = [ 53 80 123 443 563 853 2234 2239 2240 2242 5222 5938 7070 11371 65535 ];
    allowedUDPPorts = [ 53 80 123 443 563 853 2234 2239 2240 2242 5222 5938 7070 11371 65535 ];
    allowPing = false;
    enable = true;
    # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
    extraCommands = ''
  ip46tables --append INPUT --protocol tcp --dport 80 --match hashlimit --hashlimit-name stig_conn_limit --hashlimit-mode srcip --hashlimit-above 1000/minute --jump nixos-fw-refuse
  ip46tables --append INPUT --protocol tcp --dport 443 --match hashlimit --hashlimit-name stig_conn_limit --hashlimit-mode srcip --hashlimit-above 1000/minute --jump nixos-fw-refuse
    '';
    logReversePathDrops = true;
  };

    # change default wifi backend from wpa_supplicant to iwd:
    # see benefits of iwd in https://wiki.gentoo.org/wiki/Iwd
    networking.networkmanager.wifi.backend = "iwd";

    nix = {
    # Free up to 1GiB whenever there is less than 100MiB left.
    extraOptions = ''
      min-free = ${toString (100 * 1024 * 1024)}
      max-free = ${toString (1024 * 1024 * 1024)}
    '';
  };

  # How often automatic garbage collection is performed. 
  # For most desktop and server systems
  # a sufficient garbage collection is once a week.
  nix.gc = {
                automatic = true;
                dates = "weekly";
                options = "--delete-older-than 7d";
        };

  # This will additionally add your inputs to the system's legacy channels
  # Making legacy nix commands consistent as well, awesome!
  # nix.nixPath = ["/etc/nix/path"];
  # environment.etc =
  #   lib.mapAttrs'
  #   (name: value: {
  #     name = "nix/path/${name}";
  #     value.source = value.flake;
  #   })
  #   config.nix.registry;

# NVIDIA requires unfree packages
# Allow unfree packages.
  nixpkgs.config = {
    allowUnfree = true;
    allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) [
        # "steam"
        # "steam-original"
        # "steam-runtime"
      ];
  };

  nixpkgs.config.permittedInsecurePackages = [
      # use nix-tree command to see which insecure electron packages are still in use today:
      # electron-xx.x.x is insecure, but used by an application
      # "dotnet-sdk-6.0.428"
              ];

  # This will add each flake input as a registry
  # To make nix3 commands consistent with your flake
   nix.registry =
     (lib.mapAttrs (_: flake: {inherit flake;}))
     ((lib.filterAttrs (_: lib.isType "flake")) inputs);

   # Deduplicate and optimize nix store
   nix.settings.auto-optimise-store = true;

   # Enable flakes and new 'nix' command
   # Enable repl-flake experimental feature to be able to consult flake using
   # cd /etc/nixos; nix repl ".#" 
   nix.settings.experimental-features = "nix-command flakes";

   nix.settings.substituters = [
    #"https://cache.dhall-lang.org"
    #"https://cache.floxdev.com"
    #"https://cache.lix.systems"
    "https://cache.nixos.org"
    #"https://cachix.cachix.org"
    #"https://dconf2nix.cachix.org"
    #"https://dhall.cachix.org"
    #"https://garuda-linux.cachix.org"
    #"https://nix-community.cachix.org"
    #"https://nix-gaming.cachix.org"
    #"https://nixos-homepage.cachix.org"
    #"https://nixpkgs-wayland.cachix.org"
    #"https://nix-script.cachix.org"
  ];

    nix.settings.trusted-public-keys = [
    # "cache.dhall-lang.org:I9/H18WHd60olG5GsIjolp7CtepSgJmM2CsO813VTmM="
    #"flox-store-public-0:8c/B+kjIaQ+BloCmNkRUKwaVPFWkriSAd0JJvuDu4F0="
    #"cache.lix.systems:aBnZUw8zA7H35Cz2RyKFVs3H4PlGTLawyY5KRbvJR8o="
    "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
    #"cachix.cachix.org-1:eWNHQldwUO7G2VkjpnjDbWwy4KQ/HNxht7H4SSoMckM="
    #"dconf2nix.cachix.org-1:BlFKnaWTqtISQFyb3aYGQ31aoWscUKHU3eYVWraZTyw="
    #"dhall.cachix.org-1:8laGciue2JBwD49ICFtg+cIF8ddDaW7OFBjDb/dHEAo="
    #"garuda-linux.cachix.org-1:tWw7YBE6qZae0L6BbyNrHo8G8L4sHu5QoDp0OXv70bg="
    #"nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    #"nix-gaming.cachix.org-1:nbjlureqMbRAxR1gJ/f3hxemL9svXaZF/Ees8vCUUs4="
    #"nixos-homepage.cachix.org-1:NHKBt7NjLcWfgkX4OR72q7LVldKJe/JOsfIWFDAn/tE="
    #"nixpkgs-wayland.cachix.org-1:3lwxaILxMRkVhehr5StQprHdEo4IrE8sRho9R9HOLYA="
    #"nix-script.cachix.org-1:czo3tF6XERpkDdMd6m84XjmgeHQXNeIooSt7b0560+c="
  ];

    programs.bash.interactiveShellInit = ''
  # Temporary workaround when using Plasma desktop:
  # rm -rf ~/.gtkrc-2.0*
  fastfetch --battery-temp --cpu-temp --gpu-temp --load-config ~/.config/fastfetch/config.jsonc --logo none --physicaldisk-temp
  inxi -b -c 28 |egrep "Kernel|Device|Desktop|Display" |sort
  # uname -a # replaced by inxi -b |egrep "Kernel|Device"
  # nvidia-smi |grep Driver # replaced by inxi -b |egrep "Kernel|Device"
  # echo $XDG_SESSION_TYPE # replaced by inxi -b |egrep "Desktop|Display"
  # echo $XDG_SESSION_DESKTOP # replaced by inxi -b |egrep "Desktop|Display"
  echo $WAYLAND_DISPLAY
  nix --version
  findmnt | grep pCloud
  # lynx -crawl -dump https://ipaddress.my | egrep "City|Country" | egrep -v "details"|head -n 2
  curl --silent "https://ipinfo.io/city"; curl --silent "https://ipinfo.io/country"
  dmesg|grep BIOS|grep DMI
  dmesg|egrep 'bad|broken|eltdown|GDS|IBPB|IBRS|itigation|leak|ownfall|PTE|Spectre|STIBP|VMX|vuln'
  '';

  programs.chromium = {
    enable = true;
    homepageLocation = "https://www.startpage.com/";
    defaultSearchProviderSearchURL = "https://www.startpage.com/";
    defaultSearchProviderEnabled = true;
    # See available extensions at https://chrome.google.com/webstore/category/extensions
    # Following extensions will automatically be enabled on Chromium+Google Chrome+Brave browsers
    extensions = [
      "ohahllgiabjaoigichmmfljhkcfikeof" # Adblock Ultimate - free ad blocker
      "eedlgdlajadkbbjoobobefphmfkcchfk" # Ecosia - The search engine that plants trees
      "mlomiejdfkolichcflejclcbmpeaniij" # Ghostery ad blocker
      "bmnlcjabgnpnenekpadlanbbkooimhnj" # Honey- Automatic Coupons & Rewards
      "gcbommkclmclpchllfjekcdonpmejbdp" # HTTPS Everywhere
      # "ofpnmcalabcbjgholdjcjblkibolbppb" # Monica - Your AI Copilot powered by GPT-4
      # "pkehgijcmpdhfbdbbnkijodmdjhbjlgp" # Privacy Badger automatically learns to block invisible trackers.
      "gebbhagfogifgggkldgodflihgfeippi" # Return YouTube Dislike
      "mnjggcdmjocbbbhaepdhchncahnbgone" # SponsorBlock for YouTube
      "lgblnfidahcdcjddiepkckcfdhpknnjh" # Stands AdBlocker
      "ddkjiahejlhfcafbddmgiahcphecmpfh" # uBlock Origin lite ad blocker
      "fiaeclpicddpifeflpmlgmbjgaedladf" # YouTube Caption
    ];
    # See available options at https://chromeenterprise.google/policies/
    extraOpts = {
      "BrowserSignin" = 0;
      "BrowserAddPersonEnabled" = false;
      "PasswordManagerEnabled" = false;
      "AutofillAddressEnabled" = true;
      "AutofillCreditCardEnabled" = false;
      "BuiltInDnsClientEnabled" = false;
      "MetricsReportingEnabled" = true;
      "SearchSuggestEnabled" = false;
      "AlternateErrorPagesEnabled" = false;
      "UrlKeyedAnonymizedDataCollectionEnabled" = false;
      "BlockThirdPartyCookies" = false; # Disabled to allow use of web version of MS Teams (teams.microsoft.com)
    };
  };

    programs.command-not-found.enable = false;

    programs.dconf.enable = true;

    # see https://github.com/viperML/nh
    programs.nh = {
      enable = true;
      # clean.enable = true; # avoid conflict with nix.gc.automatic options
      # clean.extraArgs = "--keep-since 4d --keep 3"; # avoid conflict with nix.gc.automatic options
      flake = "/etc/nixos";
  };

    # Enable noise torch
    programs.noisetorch.enable = true;

  # Seahorse, a GNOME application for managing encryption keys and deleting problematic login keyrings
  # Seahorse conflicts with Plasma Wayland
  programs.seahorse.enable = false;

  programs.starship.enable = true;

  programs.starship.settings = {
    scan_timeout = 1;
    add_newline = false;

    username.format = "[$user]($style)[@](bold green)";
    hostname = {
      ssh_only = false;
      format = "[$hostname]($style) ";
      style = "bold blue";
    };
    directory = {
      style = "bold purple";
      truncation_length = 2;
    };
    git_branch = {
      format = "[$branch]($style) ";
      style = "bold green";
    };
    git_commit.format = "[$tag]($style)";
    git_state.format = "[$state( $progress_current/$progress_total)]($style)";
    git_status = {
      style = "yellow";
      format = "([\\[$all_status$ahead_behind\\]]($style) )";
    };
    package.disabled = true;
    perl.disabled = true;
    line_break.disabled = true;
    status = {
      format = "[$status ]($style)";
      disabled = false;
    };
    sudo.disabled = false;
    character.format = "[⋉ ](white)";
  };

    # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
    security.auditd.enable = true;
    security.audit.enable = true;

    # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
    security.audit.rules = [
  "-a always,exit -F path=/run/current-system/sw/bin/chage -F perm=x -F auid>=1000 -F auid!=unset -k privileged-chage"
  "-a always,exit -F path=/run/current-system/sw/bin/chcon -F perm=x -F auid>=1000 -F auid!=unset -k perm_mod"
  "-a always,exit -F arch=b32 -S chmod,fchmod,fchmodat -F auid>=1000 -F auid!=unset -k perm_mod"
  "-a always,exit -F arch=b64 -S chmod,fchmod,fchmodat -F auid>=1000 -F auid!=unset -k perm_mod"
  "-a always,exit -F arch=b32 -S lchown,fchown,chown,fchownat -F auid>=1000 -F auid!=unset -F key=perm_mod"
  "-a always,exit -F arch=b64 -S chown,fchown,lchown,fchownat -F auid>=1000 -F auid!=unset -F key=perm_mod"
  "-a always,exit -F arch=b32 -S execve -C uid!=euid -F euid=0 -k execpriv"
  "-a always,exit -F arch=b64 -S execve -C uid!=euid -F euid=0 -k execpriv"
  "-a always,exit -F arch=b32 -S execve -C gid!=egid -F egid=0 -k execpriv"
  "-a always,exit -F arch=b64 -S execve -C gid!=egid -F egid=0 -k execpriv"
  "-a always,exit -F arch=b32 -S init_module,finit_module,delete_module -F auid>=1000 -F auid!=unset -k module_chng"
  "-a always,exit -F arch=b64 -S init_module,finit_module,delete_module -F auid>=1000 -F auid!=unset -k module_chng"
  "-a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=unset -k privileged-mount"
  "-a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=unset -k privileged-mount"
  "-a always,exit -F arch=b32 -S open,creat,truncate,ftruncate,openat,open_by_handle_at -F exit=-EACCES -F auid>=1000 -F auid!=unset -F key=access"
  "-a always,exit -F arch=b32 -S open,creat,truncate,ftruncate,openat,open_by_handle_at -F exit=-EPERM -F auid>=1000 -F auid!=unset -F key=access"
  "-a always,exit -F arch=b64 -S open,creat,truncate,ftruncate,openat,open_by_handle_at -F exit=-EACCES -F auid>=1000 -F auid!=unset -F key=access"
  "-a always,exit -F arch=b64 -S open,creat,truncate,ftruncate,openat,open_by_handle_at -F exit=-EPERM -F auid>=1000 -F auid!=unset -F key=access"
  "-a always,exit -F arch=b32 -S rename,unlink,rmdir,renameat,unlinkat -F auid>=1000 -F auid!=unset -k delete"
  "-a always,exit -F arch=b64 -S rename,unlink,rmdir,renameat,unlinkat -F auid>=1000 -F auid!=unset -k delete"
  "-a always,exit -F arch=b32 -S setxattr,fsetxattr,lsetxattr,removexattr,fremovexattr,lremovexattr -F auid>=1000 -F auid!=-1 -k perm_mod"
  "-a always,exit -F arch=b32 -S setxattr,fsetxattr,lsetxattr,removexattr,fremovexattr,lremovexattr -F auid=0 -k perm_mod"
  "-a always,exit -F arch=b64 -S setxattr,fsetxattr,lsetxattr,removexattr,fremovexattr,lremovexattr -F auid>=1000 -F auid!=-1 -k perm_mod"
  "-a always,exit -F arch=b64 -S setxattr,fsetxattr,lsetxattr,removexattr,fremovexattr,lremovexattr -F auid=0 -k perm_mod"
  "-a always,exit -F path=/run/current-system/sw/bin/usermod -F perm=x -F auid>=1000 -F auid!=unset -k privileged-usermod"
  "--loginuid-immutable"
  "-w /etc/gshadow -p wa -k identity"
  "-w /etc/group -p wa -k identity"
  "-w /etc/passwd -p wa -k identity"
  "-w /etc/shadow -p wa -k identity"
  "-w /etc/security/opasswd -p wa -k identity"
  "-w /etc/sudoers -p wa -k identity"
  # "-w /var/cron/tabs/ -p wa -k services"
  # "-w /var/cron/cron.allow -p wa -k services"
  # "-w /var/cron/cron.deny -p wa -k services"
  "-w /var/log/lastlog -p wa -k logins"
 ];

    # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
    security.pam.loginLimits = [
 {
  domain = "*";
  item = "maxlogins";
  type = "hard";
  value = "10";
 }
];

    # Allow normal users to mount devices
    security.polkit.enable = true;
    security.polkit.extraConfig = ''
    polkit.addRule(function(action, subject) {
    var YES = polkit.Result.YES;
    var permission = {
      "org.freedesktop.udisks2.filesystem-mount": YES,
      "org.freedesktop.udisks2.filesystem-mount-system": YES,
      "org.freedesktop.udisks2.eject-media": YES
    };
    return permission[action.id];
  });
'';

    # enable memory-safe version of sudo rewritten in Rust:
    security.sudo-rs.enable = true;
    security.sudo.enable = false;

    # services.atuin.enable = true;

    # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
    # Disable bluetooth support
    services.blueman.enable = false; # blueman is a GTK-based Bluetooth Manager

    # use dbus-broker, which aims to provide high performance and reliability, while keeping
    # compatibility to the D-Bus reference implementation
    services.dbus.implementation = "broker";

    services.dnscrypt-proxy2 = {
    enable = true;
    settings = {
      ipv6_servers = false;
      require_dnssec = true;

      sources.public-resolvers = {
        urls = [
          "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
          "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
        ];
        cache_file = "/var/lib/dnscrypt-proxy2/public-resolvers.md";
        minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
        refresh_delay = 72;
      };

      # You can choose a specific set of servers from https://github.com/DNSCrypt/dnscrypt-resolvers/blob/master/v3/public-resolvers.md
      # Choose server that supports DNS-over-HTTPS (DoH) & DNS-over-TLS (DoT), DNSSEC and blocks ransomware
      # server_names = [ "ahadns-doh-la ahadns-doh-nl " ];  # not working
      server_names = [ "doh-ibksturm" "doh.appliedprivacy.net" "meganerd-doh-ipv4" "sth-ads-doh-se" "sth-doh-se" "uncensoreddns-dk-ipv4" ];
    };
  };

    services.fail2ban.enable = true;

    services.fstrim.enable = true; # enable periodic SSD TRIM of mounted partitions in background.

    # enable firmware updates:
    services.fwupd.enable = true;
    
    services.pipewire = {
       alsa.enable = false;
       alsa.support32Bit = false;
       enable = true;
       # If you want to use JACK applications, uncomment this
       jack.enable = false;
       pulse.enable = true;
       wireplumber.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
    #       lowLatency = {
    #       # enable this module      
    #       enable = true;
    #       # defaults (no need to be set unless modified)
    #       quantum = 64;
    #       rate = 48000;
    #        };
  };

# improve sound quality
  services.pipewire.extraConfig.pipewire."92-low-latency" = {
       stream.properties = {
       resample.quality = 10;
  };
};

# improve sound quality
  services.pipewire.extraConfig.pipewire-pulse."92-low-latency" = {
       stream.properties = {
       resample.quality = 10;
  };
};

    # laptop powermanagement configuration:

    # Disable GNOMEs power management
    services.power-profiles-daemon.enable = false;

    services.pulseaudio.support32Bit = true;

    # All values except 'enable' are optional.
    services.redshift = {
      enable = true;
      brightness = {
        # Note the string values below.
        day = "1";
        night = "1";
      };
      temperature = {
        # During  the  day, the  color  temperature  should match the light from 
        #outside, typically around  5500K-6500K  (default  is  5500K).
        day = 5500;

        # At night the color temperature should be set to match the lamps in your room.
        # This is typically a low temperature at around 3000K-4000K (default is 3700K).
        night = 3200;
      };
    };

    services.resolved.enable = false;

    security.rtkit.enable = true;

    # Better scheduling for CPU cycles - thank you System76
    services.system76-scheduler.settings.cfsProfiles.enable = true;

    # enable teamviewer service that the teamviewer GUI depends on:
    services.teamviewer.enable = true;

    # Enable thermald (only necessary if on Intel CPUs)
    services.thermald.enable = true;

    # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
    services.timesyncd = {
      enable = lib.mkForce true;
      extraConfig = ''
         PollIntervalMaxSec=60
         '';
      servers = [ "ntp1.rwth-aachen.de" "ntp2.rwth-aachen.de" "tick.usnogps.navy.mil" "tock.usnogps.navy.mil" ];
    };

    # Enable TLP (better than Gnome's internal power manager)
    services.tlp = {
       enable = true;
       settings = {
                 CPU_BOOST_ON_AC = 1;
                 CPU_BOOST_ON_BAT = 0;
                 CPU_SCALING_GOVERNOR_ON_AC="performance";
                 CPU_SCALING_GOVERNOR_ON_BAT="powersave";
                 START_CHARGE_THRESH_BAT1=75;
                 STOP_CHARGE_THRESH_BAT1=80;
                 };
      };

    # disable Multi-touch gesture recognizer:
    services.touchegg.enable = false;

    # Nvidia
    # services.xserver.videoDrivers = [ "nvidia" ];

    # Copy the NixOS configuration file and link it from the resulting system
    # (/run/current-system/configuration.nix). This is useful in case you
    # accidentally delete configuration.nix.
    system.autoUpgrade = {
     enable = false;
     # dates = "daily";
     allowReboot = false; 
     # channel = "https://channels.nixos.org/nixos-23.05";
     channel = "https://channels.nixos.org/nixos-unstable";
  };
    system.copySystemConfiguration = false;

    # restart faster
    systemd.extraConfig = ''
    # Change DefaultLimit values to avoid distrobox/podman error when starting container:
    # Error: unable to start container crun: mkdir `/sys/fs/selinux`:
    #  Permission denied: OCI permission denied
    DefaultLimitMEMLOCK=infinity
    DefaultLimitNOFILE=1048576
    DefaultTimeoutStopSec=10s
  '';

  systemd.services.dnscrypt-proxy2.serviceConfig = {
    StateDirectory = "dnscrypt-proxy";
  };

  system.rebuild.enableNg = true; # Whether to use ‘nixos-rebuild-ng’ in place of ‘nixos-rebuild’,
  # the Python-based re-implementation of the original in Bash.

  ####################################################################################################################################
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
  ####################################################################################################################################

  system.switch = {
    enable = false;
    enableNg = true; # use switch-to-configuration-ng, the Rust-based 
    # re-implementation of the original Perl switch-to-configuration
  };

  # Use bash by default for all users
  users.defaultUserShell = pkgs.bash;

  # added security hardening NixOS option as recommended by Anduril NixOS STIG rules: 
  # ensure that the root user is locked by adding the following text:
  # users.mutableUsers = false;

  # based on https://github.com/CosmicHalo/AndromedaNixos/blob/ca47fc1656643166a227141a62feb18de4c2174b/modules/nixos/desktop/kde/apps.nix#L110

    xdg.mime.defaultApplications = let
      # Take from the respective mimetype files
      images = [
        "image/bmp"
        "image/gif"
        "image/jpeg"
        "image/jpg"
        "image/pjpeg"
        "image/png"
        "image/svg+xml"
        "image/svg+xml-compressed"
        "image/tiff"
        "image/vnd.wap.wbmp;image/x-icns"
        "image/x-bmp"
        "image/x-gray"
        "image/x-icb"
        "image/x-ico"
        "image/x-pcx"
        "image/x-png"
        "image/x-portable-anymap"
        "image/x-portable-bitmap"
        "image/x-portable-graymap"
        "image/x-portable-pixmap"
        "image/x-xbitmap"
        "image/x-xpixmap"
      ];
      urls = [
        "text/html"
        "x-scheme-handler/about"
        "x-scheme-handler/chrome"
        "x-scheme-handler/http"
        "x-scheme-handler/https"
        "x-scheme-handler/unknown"
      ];
      documents = [
        "application/illustrator"
        "application/oxps"
        "application/pdf"
        "application/postscript"
        "application/vnd.comicbook+zip"
        "application/vnd.comicbook-rar"
        "application/vnd.ms-xpsdocument"
        "application/x-bzdvi"
        "application/x-bzpdf"
        "application/x-bzpostscript"
        "application/x-cb7"
        "application/x-cbr"
        "application/x-cbt"
        "application/x-cbz"
        "application/x-dvi"
        "application/x-ext-cb7"
        "application/x-ext-cbr"
        "application/x-ext-cbt"
        "application/x-ext-cbz"
        "application/x-ext-djv"
        "application/x-ext-djvu"
        "application/x-ext-dvi"
        "application/x-ext-eps"
        "application/x-ext-pdf"
        "application/x-ext-ps"
        "application/x-gzdvi"
        "application/x-gzpdf"
        "application/x-gzpostscript"
        "application/x-xzpdf"
        "image/tiff"
        "image/vnd.djvu+multipage"
        "image/x-bzeps"
        "image/x-eps"
        "image/x-gzeps"
      ];
      audioVideo = [
        "application/mxf"
        "application/ogg"
        "application/sdp"
        "application/smil"
        "application/streamingmedia"
        "application/vnd.apple.mpegurl"
        "application/vnd.ms-asf"
        "application/vnd.rn-realmedia"
        "application/vnd.rn-realmedia-vbr"
        "application/x-cue"
        "application/x-extension-m4a"
        "application/x-extension-mp4"
        "application/x-matroska"
        "application/x-mpegurl"
        "application/x-ogg"
        "application/x-ogm"
        "application/x-ogm-audio"
        "application/x-ogm-video"
        "application/x-shorten"
        "application/x-smil"
        "application/x-streamingmedia"
        "audio/3gpp"
        "audio/3gpp2"
        "audio/AMR"
        "audio/aac"
        "audio/ac3"
        "audio/aiff"
        "audio/amr-wb"
        "audio/dv"
        "audio/eac3"
        "audio/flac"
        "audio/m3u"
        "audio/m4a"
        "audio/mp1"
        "audio/mp2"
        "audio/mp3"
        "audio/mp4"
        "audio/mpeg"
        "audio/mpeg2"
        "audio/mpeg3"
        "audio/mpegurl"
        "audio/mpg"
        "audio/musepack"
        "audio/ogg"
        "audio/opus"
        "audio/rn-mpeg"
        "audio/scpls"
        "audio/vnd.dolby.heaac.1"
        "audio/vnd.dolby.heaac.2"
        "audio/vnd.dts"
        "audio/vnd.dts.hd"
        "audio/vnd.rn-realaudio"
        "audio/vorbis"
        "audio/wav"
        "audio/webm"
        "audio/x-aac"
        "audio/x-adpcm"
        "audio/x-aiff"
        "audio/x-ape"
        "audio/x-m4a"
        "audio/x-matroska"
        "audio/x-mp1"
        "audio/x-mp2"
        "audio/x-mp3"
        "audio/x-mpegurl"
        "audio/x-mpg"
        "audio/x-ms-asf"
        "audio/x-ms-wma"
        "audio/x-musepack"
        "audio/x-pls"
        "audio/x-pn-au"
        "audio/x-pn-realaudio"
        "audio/x-pn-wav"
        "audio/x-pn-windows-pcm"
        "audio/x-realaudio"
        "audio/x-scpls"
        "audio/x-shorten"
        "audio/x-tta"
        "audio/x-vorbis"
        "audio/x-vorbis+ogg"
        "audio/x-wav"
        "audio/x-wavpack"
        "video/3gp"
        "video/3gpp"
        "video/3gpp2"
        "video/avi"
        "video/divx"
        "video/dv"
        "video/fli"
        "video/flv"
        "video/mkv"
        "video/mp2t"
        "video/mp4"
        "video/mp4v-es"
        "video/mpeg"
        "video/msvideo"
        "video/ogg"
        "video/quicktime"
        "video/vnd.divx"
        "video/vnd.mpegurl"
        "video/vnd.rn-realvideo"
        "video/webm"
        "video/x-avi"
        "video/x-flc"
        "video/x-flic"
        "video/x-flv"
        "video/x-m4v"
        "video/x-matroska"
        "video/x-mpeg2"
        "video/x-mpeg3"
        "video/x-ms-afs"
        "video/x-ms-asf"
        "video/x-ms-wmv"
        "video/x-ms-wmx"
        "video/x-ms-wvxvideo"
        "video/x-msvideo"
        "video/x-ogm"
        "video/x-ogm+ogg"
        "video/x-theora"
        "video/x-theora+ogg"
      ];
      archives = [
        "application/bzip2"
        "application/gzip"
        "application/vnd.android.package-archive"
        "application/vnd.debian.binary-package"
        "application/vnd.ms-cab-compressed"
        "application/x-7z-compressed"
        "application/x-7z-compressed-tar"
        "application/x-ace"
        "application/x-alz"
        "application/x-ar"
        "application/x-archive"
        "application/x-arj"
        "application/x-brotli"
        "application/x-bzip"
        "application/x-bzip-brotli-tar"
        "application/x-bzip-compressed-tar"
        "application/x-bzip1"
        "application/x-bzip1-compressed-tar"
        "application/x-cabinet"
        "application/x-cd-image"
        "application/x-chrome-extension"
        "application/x-compress"
        "application/x-compressed-tar"
        "application/x-cpio"
        "application/x-deb"
        "application/x-ear"
        "application/x-gtar"
        "application/x-gzip"
        "application/x-gzpostscript"
        "application/x-java-archive"
        "application/x-lha"
        "application/x-lhz"
        "application/x-lrzip"
        "application/x-lrzip-compressed-tar"
        "application/x-lz4"
        "application/x-lz4-compressed-tar"
        "application/x-lzip"
        "application/x-lzip-compressed-tar"
        "application/x-lzma"
        "application/x-lzma-compressed-tar"
        "application/x-lzop"
        "application/x-ms-dos-executable"
        "application/x-ms-wim"
        "application/x-rar"
        "application/x-rar-compressed"
        "application/x-rpm"
        "application/x-rzip"
        "application/x-rzip-compressed-tar"
        "application/x-source-rpm"
        "application/x-stuffit"
        "application/x-tar"
        "application/x-tarz"
        "application/x-tzo"
        "application/x-war"
        "application/x-xar"
        "application/x-xz"
        "application/x-xz-compressed-tar"
        "application/x-zip"
        "application/x-zip-compressed"
        "application/x-zoo"
        "application/x-zstd-compressed-tar"
        "application/zip"
        "application/zstd"
      ];
      code = [
        "application/x-shellscript"
        "text/english"
        "text/plain"
        "text/x-c"
        "text/x-c++"
        "text/x-c++hdr"
        "text/x-c++src"
        "text/x-chdr"
        "text/x-csrc"
        "text/x-java"
        "text/x-makefile"
        "text/x-moc"
        "text/x-pascal"
        "text/x-tcl"
        "text/x-tex"
      ];
    in
      (lib.genAttrs code (_: ["gitkraken.desktop"]))
      // (lib.genAttrs archives (_: ["ark.desktop"]))
      // (lib.genAttrs audioVideo (_: ["vlc.desktop"]))
      // (lib.genAttrs documents (_: ["okular.desktop"]))
      // (lib.genAttrs images (_: ["loupe.desktop"]))
      // (lib.genAttrs urls (_: ["chromium.desktop"]));

}

################################################################
# Enable fingerprint reader:
# Run fprintd-enroll to setup fingerprints
# services.fprintd.enable = true;
# services.fprintd.tod.enable = true;
# install correct fingerprint driver:
# services.fprintd.tod.driver = pkgs.libfprint-2-tod1-vfs0090; # try one of these 3 drivers
# services.fprintd.tod.driver = pkgs.libfprint-2-tod1-goodix-550a; # try one of these 3 drivers
# services.fprintd.tod.driver = pkgs.libfprint-2-tod1-goodix; # try one of these 3 drivers
# starship - allows customizing the shell prompt
# run following query on Github: language:nix programs.starship.settings
# programs.bash.completion.enable = true;

  # security.pam.services.greetd.enableGnomeKeyring = true;
  # security.pam.services.login.enableGnomeKeyring = true;

  # services.stubby.enable = true;
  # services.stubby.settings.resolution_type = "GETDNS_RESOLUTION_STUB";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # networking.wireless.enable = true; # conflicts with networking.networkmanager

  # services.picom.enable = true;

  # Automatic device mounting daemon
  # Fix USB sticks not mounting or being listed:
  # services.devmon.enable = true;
  # services.udisks2.enable = true;
  # gvfs - to mount Android phones over mtp://
  # services.gvfs.enable = true;

  # Enable sound with pipewire.
  # sound.enable = true;
  # sound.mediaKeys.enable = true;
  # services.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

# Copied from https://chattingdarkly.org/@lhf@fosstodon.org/110661879831891580
# Copied from https://github.com/mcdonc/.nixconfig/blob/master/videos/tipsntricks/script.rst
# system.activationScripts.diff = {
#   supportsDryActivation = true;
#   text = ''
#     ${pkgs.nvd}/bin/nvd --nix-bin-dir=${pkgs.nix}/bin diff \
#          /run/current-system "$systemConfig"
#   '';
# };

  #nixpkgs.overlays = [
  #  (import (builtins.fetchTarball {
  #    url = https://github.com/nix-community/neovim-nightly-overlay/archive/master.tar.gz;
  #  }))
  #  (self: super: {
  #   neovim = super.neovim.override {
  #     viAlias = true;
  #     vimAlias = true;
  #   };
  # })
  #];

  # enable flatpak support
  # services.flatpak.enable = true;

  # XDG Desktop Portal stuff
  # xdg.portal = {
  # enable = true;
  # wlr.enable = true;
  # gtk portal needed to make gtk apps happy
  # extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  #};
 
 # autostart polkit-gnome-authentication-agent-1 via systemd
 #systemd = {
 # user.services.polkit-gnome-authentication-agent-1 = {
 #   description = "polkit-gnome-authentication-agent-1";
 #   wantedBy = [ "graphical-session.target" ];
 #   wants = [ "graphical-session.target" ];
 #   after = [ "graphical-session.target" ];
 #   serviceConfig = {
 #       Type = "simple";
 #       ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
 #       Restart = "on-failure";
 #       RestartSec = 1;
 #       TimeoutStopSec = 10;
 #     };
 # };
 #  extraConfig = ''
 #    DefaultTimeoutStopSec=10s
 #  '';
 #  }; 

# Main configuration file for NixOS 23.05 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# Find all NixOS packages here: https://nixos.org/
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.

# File creation date: 2023/6/19
# Last modification date: 2025/1/10

# DISKSPACE: This script will initially use minimum 60 GB of diskspace ( 56 GB for root partition and 4.2 GB for home+dropbox data))
# DISKSPACE: Reserve 1 GB for FAT32 /boot partition and set boot flag on this partition
# DISKSPACE: Reserve 0 GB for SSD swap partition and 8 GB diskspace for mechanical HD swap partition
# DISKSPACE: Reserve at least 100 GB of diskspace for / (root) F2FS partition for SSD - F2FS compresses better than ext4 on SSD
# DISKSPACE: Install all games in /tmp to avoid clogging up /home directory with files that should not be backed up
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 26 minutes on old SSD drive from the year 2016

# PRE-INSTALL STEPS:
# 1) update BIOS
# 2) backup home directory, including ~/.config subfolder

# POST-INSTALL STEPS:
# 3) extract config_backup_<timestamp>.zip from usb stick into home directory , then reboot PC
# 4) copy backed up documents/files from usb stick to ~/backup in home directory
# 5) Improve privacy: Load chrome://flags/#encrypted-client-hello in the Chromium browser's address bar.
# Set the status of the Encrypted Client Hello (ECH) flag to Enabled. 
# https://blog.cloudflare.com/announcing-encrypted-client-hello/

# 6) Can be skipped thanks to step 3: Put following program icons in desktop panel for easy access: Terminal,spacefm,bitwarden,
# maestral,google chrome (also used to import/export bookmarks),chromium,brave,teamviewer,printer icon,proton vpn
# 7) Can be skipped thanks to step 3: copy all .desktop files from 
# https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 8) Can be skipped thanks to step 3:Under Themes - set Icons to Numix-Circle-Light
# 9) Can be skipped thanks to step 3:force Chromium web browser to open
# https://searx.prvcy.eu/ (SearXNG meta search engine) on each startup (manually configure in Chromium settings)


# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
