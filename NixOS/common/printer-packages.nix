# 2024/4/5: creation of ./common/printer-packages.nix
# 2024/9/27: disabled port 631 due to CVE-2024-47176 (cups-browsed up to version 2.0.1)
# 2024/9/27: cups-browsed is a helper daemon to browse for remote CUPS queues and IPP network printers


{ config
, inputs
, lib
, pkgs
, ... 
}:


{

environment.systemPackages = with pkgs; [
      cups
      ipp-usb  # Daemon to use the IPP everywhere protocol with USB printers
      system-config-printer
];

services.avahi = {
  enable = true;
  nssmdns4 = true;
  openFirewall = true; # for a WiFi printer
  };

  services.printing = {
  enable = true;
  drivers = with pkgs; [
      brgenml1cupswrapper
      brgenml1lpr
      brlaser
      # canon-cups-ufr2
      # cnijfilter2 # Canon InkJet printer drivers for many Pixma series printers.
      cups
      # cups-drv-rastertosag-gdi # CUPS driver for Ricoh Aficio SP 1000S and SP 1100S printers
      # epson-escpr
      # epson-workforce-635-nx625-series
      # foo2zjs
      foomatic-db
      foomatic-db-engine
      # foomatic-db-nonfree
      foomatic-db-ppds-withNonfreeDb
      # foomatic-db-ppds
      # foomatic-filters
      gutenprint
      gutenprintBin
      # hplip
      # hplipWithPlugin
      ijs # Raster printer driver architecture
      ipp-usb  # Daemon to use the IPP everywhere protocol with USB printers
      # postscript-lexmark
      # samsung-unified-linux-driver
      splix # CUPS drivers for SPL (Samsung Printer Language) printers
      system-config-printer
    ];

  browsing = true;
  defaultShared = true;
  };

  # match "cupsd[2350]: Expiring subscriptions..."
  systemd.services.cups = {
    overrideStrategy = "asDropin";
    serviceConfig.LogFilterPatterns = "~.*Expiring subscriptions.*";
  };

}
