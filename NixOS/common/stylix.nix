# Change history:
# 2024/5/19: creation of ./common/stylix.nix
# 2024/11/21: issue between stylix and base16:  https://github.com/danth/stylix/issues/642

# Configuration for Stylix
# https://github.com/danth/stylix
# With additional modifications

# stylix.nix sets wallpaper on desktop only when using plasmawayland = best choice
# stylix.nix sets wallpaper on login screen only when using cinnamon

# !!! use command  nix hash file <filename> to calculate sha256 value !!!

{ base16, config, inputs, lib, pkgs, stylix, ... }:

let
  wallpaper = pkgs.fetchurl {
    url = "https://images.pexels.com/photos/842711/pexels-photo-842711.jpeg";
    sha256 = "r+Nbg8zTVjW56n3Em6WAjCglM0mSdcjIEdohOMkLWzg=";
  };

  # logoOriginal = pkgs.fetchurl {
  #   url = "https://static.wikia.nocookie.net/soma/images/f/f8/Haimatsu.svg";
  #   sha256 = "1Hvq4xYvEPZHPq2Xrl/YpKviKMKU12vsuX/pzJEiE1k=";
  # };

  # logoSvg = pkgs.runCommand "logo.svg" {} ''
  #   sed <${logoOriginal} >$out \
  #     -e 's/0b4301/${config.lib.stylix.colors.base05-hex}/g' \
  #     -e 's/fd0000/${config.lib.stylix.colors.base05-hex}/g'
  # '';

  # logoPng = pkgs.runCommand "logo.png" {} ''
  #   ${pkgs.resvg}/bin/resvg ${logoSvg} --width 300 $out
  # '';

  # font = {
  #  name = "Fira Code";
  #  package = pkgs.fira-code;
  # };

in {
  
  stylix = {
    enable=true;
    image = wallpaper;
    polarity = "light";

    # fonts = {
    #  serif = font;
    #  sansSerif = font;
    #  monospace = font;

    #  sizes = {
    #    applications = 11;
    #    desktop = 11;
    #   };
    #  };

    #targets.plymouth = {
    #  logo = logoPng;
    #  logoAnimated = false;
    # };

    # targets.nixos-icons.enable = false;
  };

}