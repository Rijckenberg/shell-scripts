# 2024/5/2: creation of ./common/gaming.nix
# 2024/5/16: added chaotic packages in common/gaming.nix
# 2024/5/16: minimum RAM requirement for compiling packages: 37 GB of RAM

{ chaotic, config, inputs, lib, pkgs, ... }:

{

environment.systemPackages = with pkgs; [
   # pkgsx86_64_v3.brave #  take too long to compile!
   # pkgsx86_64_v3.chromium #  take too long to compile!
   # firedragon # marked as broken
   # firefox-bin # firefox sells your data now to advertisers
   # pkgsx86_64_v3.firefox
   # pkgsx86_64_v3.mullvad-browser # best browser in 2025
];


}