# See https://github.com/romatthe/systems/blob/5ef349bca9d30bec116276b2166363df75914e8a/modules/apps/games.nix
# for inspiration

# Change log:

# 2024/5/2: creation of ./common/gaming-packages.nix
# 2024/5/16: added chaotic packages in ./common/gaming-packages.nix
# 2024/7/31: added asdf-vm in ./common/gaming-packages.nix
# 2024/7/31: added protonge-upgrade-script in ./common/gaming-packages.nix
# 2024/11/12: enabled programs.steam.platformOptimizations.enable = true;
# 2025/2/7: disabled vulkanPackages_latest.vulkan-tools as it fails to compile

{ chaotic, config, inputs, lib, nix-gaming, pkgs, ... }:

{

chaotic.nyx.cache.enable = true;

chaotic.nyx.overlay.enable = true;

environment.systemPackages = with pkgs; [
   # list of chaotic packages to choose from: https://www.nyx.chaotic.cx/
   ################################################################################################################################
   # use asdf as described here: https://github.com/GloriousEggroll/proton-ge-custom?tab=readme-ov-file#manual
   # https://github.com/augustobmoura/asdf-protonge
   asdf-vm # Multiple Runtime Version Manager to install latest version of Proton-GE
   ################################################################################################################################
   # bottles  - easy to use Windows game/application launcher
   # Install Epic Games Store launcher and any other Windows application
   # only via bottles application
   bottles # can use soda (similar to wine-GE) to launch Windows game installers
   gamehub # gives access to Steam, GOG, Humble Bundle and itch.io games
   gamemode # Optimise Linux system performance on demand
   gamescope_git # SteamOS session compositing window manager
   gogdl # GOG Downloading module for Heroic Games Launcher
   goverlay # GUI tool used to manage MangoHud, vkBasalt and Replay-Sorcery on Linux
   gzdoom # Modder-friendly OpenGL and Vulkan source port based on the DOOM engine
   # also get Brutal Doom and Reshade mods for Doom
   # press home key in gzdoom to access Reshade menu 
  
   #######################################################################################
   (heroic.override {
	       extraPkgs = pkgs: [
                     # List package dependencies here
                     wine64
                     # wine-staging
                     winetricks
                     wineWowPackages.full
	       ];
	    })

      # best choice,works well in NixOS 23.11 - Native Epic,GOG,Prime Gaming launcher
      # heroic is preferable to Steam, because Steam client updates sometimes break the installed Windows game!
      # for Linux, Windows and Mac; only use Proton-GE-Proton,Lutris-GE-Proton, Wine-GE-Proton or Proton-Proton
      # versions installed by ProtonUp-QT
      # But some games require Proton 7 or Proton 4 instead of the newest Proton-GE version
      # heroic can access far more Proton and Wine versions than Steam
      # If heroic fails to refresh list of available games, then run  rm -rf /home/ulysses/.config/heroic/Cache
      # Best performance in heroic launching Unreal Engine 5 games by enabling Proton-GE-Proton and
      # following environment variables in heroic or Steam:
      # DXVK_ASYNC=1: helps alleviate issues with stuttering and massive frame drops.
      # List of all Proton-GE environment variables is listed here:
      # https://github.com/GloriousEggroll/proton-ge-custom
      # List of new vkd3d-proton features is listed here:
      # https://github.com/HansKristian-Work/vkd3d-proton/releases
      # config 1: VKD3D_FEATURE_LEVEL=12_2 VKD3D_SHADER_MODEL=6_7 PROTON_HIDE_NVIDIA_GPU=0 VKD3D_CONFIG=dxr11,dxr PROTON_ENABLE_NVAPI=1 PROTON_ENABLE_NGX_UPDATER=1 DXVK_ASYNC=1 RADV_PERFTEST=gpl DXVK_ENABLE_NVAPI=1 mangohud %command%
      # config 2: DXVK_ASYNC=1 DXVK_FILTER_DEVICE_NAME="NVIDIA" PROTON_ENABLE_NVAPI=1 PROTON_HIDE_NVIDIA_GPU=0 PROTON_LARGE_ADDRESS_AWARE=1 PROTON_NO_ESYNC=1 PROTON_NO_FSYNC=1 PULSE_LATENCY_MSEC=60 VKD3D_FEATURE_LEVEL=12_2 VKD3D_SHADER_MODEL=6_7 WINE_FULLSCREEN_FSR=1 WINE_LARGE_ADDRESS_AWARE=1 mangohud %command%  -noLauncher -noLogs -noSplash -skipIntro
      # because VKD3D-Proton 2.10 now has Shader Model 6.7 support
      # config 3: PROTON_USE_WINED3D=1 PULSE_LATENCY_MSEC=60 mangohud %command%
      # config 4: PROTON_NO_D3D11=1 PULSE_LATENCY_MSEC=60 mangohud %command%
      # config 5: VKD3D_CONFIG=dxr11,dxr PROTON_ENABLE_NVAPI=1 PROTON_ENABLE_NGX_UPDATER=1 DXVK_ASYNC=1 mangohud %command%
      # config 6: Create new bottle in Bottles app - install Epic Games Launcher in bottle and then install game
      # config 7: use wine-ge instead of proton-ge
      # config 8: enable Nvidia RTX and DLSS using following guide: https://wiki.archlinux.org/title/Hardware_raytracing#NVIDIA
      # config 8: enable DLSS on NVIDIA RTX using PROTON_ENABLE_NVAPI=1
      # config 8: enable following: VKD3D_CONFIG=dxr11,dxr PROTON_ENABLE_NVAPI=1 PROTON_ENABLE_NGX_UPDATER=1
      # via Epic Games Launcher installed in that same Bottle. Configure Bottle to use Proton-GE-Proton
      # May need to remove VKD3D_FEATURE_LEVEL=12_2 option

   #######################################################################################
   legendary-gl # Free and open-source Epic Games Launcher alternative
   lgogdownloader # Unofficial downloader to GOG.com for Linux users.
   # It uses the same API as the official GOGDownloader
   libratbag # Configuration library for gaming mice
   ludusavi # Backup tool for PC game saves
   lutris # Open Source gaming platform for GNU/Linux # use lutris to start EA app / EA Origin launcher
   luxtorpeda # Steam Play compatibility tool to run games using native Linux engines
   # mangohud # Vulkan and OpenGL overlay for monitoring FPS, temperatures, CPU/GPU load and more
   (mangohud_git.overrideAttrs { # workaround fix for https://github.com/NixOS/nixpkgs/pull/370750
      dontCheckForBrokenSymlinks = true;
     })
   (mangohud32_git.overrideAttrs { # workaround fix for https://github.com/NixOS/nixpkgs/pull/370750
      dontCheckForBrokenSymlinks = true;
     })
   # nile # Not working well # Unofficial Amazon Games client
   (pkgs.writers.writeBashBin "protonge-upgrade-script" { } ''
      # See https://github.com/GloriousEggroll/proton-ge-custom
      #!/bin/bash
      set -euxo pipefail
      asdf plugin add protonge
      asdf install protonge latest
      asdf list
      asdf version
      asdf plugin-list-all |wc -l
    '')
   proton-ge-custom # Compatibility tool for heroic/Steam Play based on Wine and additional components
   # protonplus - https://github.com/Vysp3r/protonplus
   protonplus # Simple Wine and Proton-based compatibility tools manager
   protontricks # simple wrapper for running Winetricks commands for Proton-enabled games
   protonup-qt # for Linux; only use Proton-GE-Proton,Lutris-GE-Proton, Wine-GE-Proton or Proton-Proton
   # versions installed by ProtonUp-QT
   # But some games require Proton 7 or Proton 4 instead of the newest Proton-GE version
   # heroic can access far more Proton and Wine versions than Steam
   qzdl # ZDoom WAD Launcher
   # ryujinx # project is shut down # Experimental Nintendo Switch Emulator written in C# - 84% of games are playable
      # steam-small
      # (steam-small.override { extraBwrapArgs = [ "--tmpfs /run/current-system/sw/lib/" ]; }).run
      # (steam.override { extraBwrapArgs = [ "--tmpfs /run/current-system/sw/lib/" ]; }).run
      # steam # best choice,works well in NixOS 23.11 - Game launcher
      # for Linux; only use Proton-GE-Proton,Lutris-GE-Proton, Wine-GE-Proton or Proton-Proton 
      # versions installed by ProtonUp-QT
      # But some games require Proton 7 or Proton 4 instead of the newest Proton-GE version
      # steam-run: use it to run a non-Nixos executable on Nixos
      # steam-run
      # steamtinkerlaunch # Linux wrapper tool for use with the Steam client for custom launch options and 3rd party programs
   vulkanPackages_latest.vulkan-extension-layer # Layers providing Vulkan features when native support is unavailable
   vulkanPackages_latest.vulkan-headers # Vulkan Header files and API registry
   vulkanPackages_latest.vulkan-loader # LunarG Vulkan loader
   # vulkanPackages_latest.vulkan-tools # Khronos official Vulkan Tools and Utilities
];


programs.steam = {
      enable = true;
      # following platformOptimizations option is made available via nix-gaming module in flake.nix:
      platformOptimizations.enable = true;
      remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
      dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
	 };

}