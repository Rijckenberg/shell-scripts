# 2024/8/7: creation of ./common/astronomy-packages.nix

{ config, lib, pkgs, ... }:

{

environment.systemPackages = with pkgs; [

    calcmysky # Simulator of light scattering by planetary atmospheres
    celestia # Real-time 3D simulation of space
    # openspace # open source astrovisualization project
    gnuastro # GNU astronomy utilities and library # uses wcslib library
    kstars # Virtual planetarium astronomy software # uses wcslib library
    stellarium # Free open-source planetarium
    tinymist # Tinymist is an integrated language service for Typst
    typst # new markup-based typesetting system that is powerful and easy to learn - easier than LaTeX
    typstwriter # Integrated editor for the typst typesetting system
    typstyle # Format your typst source code
    utpm # Package manager for typst
    wcslib # used by gnuastro and kstars
    xplanet # Renders an image of the earth or other planets into the X root window

];

}
