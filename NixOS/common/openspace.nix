# 2024/8/13: creation of ./common/openspace.nix
# See https://docs.openspaceproject.com/en/latest/contribute/development/compiling/ubuntu.html for dependencies 

{ lib, stdenv, fetchFromGitHub
, file, which, zip
, autoconf
, boost
, cmake
, curl
, freeglut
, gcc13
, gdal
, glew
, libgcc
, libpng
, libtool
, libXcursor
, libXi
, libXinerama
, libXrandr
, mesa
, mpv
, vulkan-headers
 }:


stdenv.mkDerivation rec {

  name = "openspace-${version}";
  version = "0.20.1";

# rev corresponds to the Git commit hash or tag (e.g v1.0) that will be downloaded from Git.
# Finally, hash corresponds to the hash of the extracted directory. Again, other hash algorithms
# are also available, but hash is currently preferred.

  src = fetchFromGitHub {
    owner = "OpenSpace";
    repo = "OpenSpace";
    rev = "c26f9db7d427c9d72f339f745cb2e5fb243ca6aa";
    sha256 = "009fvr6mjr73np2rg82nykjgq3blwh50ykj2cgq5spwckcnrrarq";
  };

  # try and hack the build
  patches = [

  ];

  # It is easiest for me (at the moment) to have the Makefile patch
  # use `which xml2-config` rather than try and do this "properly"
  # (maybe by having the patch add in @foo@ which is then changed in
  # the preConfigure step)
  #
  nativeBuildInputs = [
  autoconf
, boost
, cmake
, curl
, freeglut
, gcc13
, gdal
, glew
, libgcc
, libpng
, libtool
, libXcursor
, libXi
, libXinerama
, libXrandr
, mesa
, mpv
, vulkan-headers
  ];

  # I have not spent the time to work out whether any of these can
  # go into one of the other *Inputs attributes; that is, I am sticking
  # everything in buildInputs
  #
  buildInputs = [
  autoconf
, boost
, cmake
, curl
, freeglut
, gcc13
, gdal
, glew
, libgcc
, libpng
, libtool
, libXcursor
, libXi
, libXinerama
, libXrandr
, mesa
, mpv
, vulkan-headers
  ];

  # I don't understand why it is only funtools/mkconfigure that needs
  # this replacement
  #
  preConfigure = ''
    # export TLSFLAGS="--with-ssl-dir=${openssl_1_1.dev}"

    # substituteInPlace tksao/configure --replace /usr/include/libxml2 ${libxml2.dev}/include/libxml2

    # substituteInPlace funtools/mkconfigure --replace /bin/bash $SHELL

  '';
  postConfigure = ''
    TLSFLAGS=
  '';
  configureScript = "cmake CMakeLists.txt";
  
  # I think at least one piece of code needs this, so just apply it
  # to everything for now.
  #
  # hardeningDisable = [ "format" ];

  installPhase = ''
    echo "*** In install phase"
    mkdir -p $out/bin
    for x in openspace ; do
      echo "*** copying $x"
      cp bin/$x $out/bin
    done

    # only copy over the XPA man pages
    mkdir -p $out/man/man1
    cp man/man1/openspace* $out/man/man1
    
  '';

  # I assume that something in the stripping messes up all of
  # DS9's internal paths, since it can not find files within its
  # own file system after the standard fixupPhase, so skip for now.
  #
  dontStrip = false;
  
  #
  meta = {
    description = "open source, non-commercial, and freely available interactive data visualization software designed to visualize the entire known universe and portray our ongoing efforts to investigate the cosmos";
    longDescription = ''
OpenSpace is an open source, non-commercial, and freely available interactive data visualization software designed to visualize the entire known universe and portray our ongoing efforts to investigate the cosmos. Bringing the latest techniques from data visualization research to the general public, OpenSpace supports interactive presentation of dynamic data from observations, simulations, and space mission planning and operations. The software works on multiple operating systems (Windows, Linux, MacOS) with an extensible architecture capable of powering both personal computers and also high resolution tiled displays and planetarium domes. In addition, OpenSpace enables simultaneous connections across the globe creating opportunity for shared experiences among audiences worldwide. The target audience of the software reaches from the general public who wishes to explore our universe, enthusiasts interested in hacking the underlying components in OpenSpace to create unique experiences, informal science institutions wishing to create a low-cost, yet powerful exhibition piece, but also scientists desiring to visualize their datasets in a contextualized, powerful software.
    '';

    homepage = https://www.openspaceproject.com/;
    # license = stdenv.lib.licenses.mit;
    # platforms = stdenv.lib.platforms.linux;
  };
}
