# 2024/8/7: creation of ./common/minimal-packages.nix
# 2024/8/15: czkawka and mypaint fail to compile
# 2024/12/26: added package nixos-rebuild-ng
# 2024/12/30: temporarily commented out spaceFM package due to build issues related to gcc 14 and Gdkwindow

{ config
, inputs
, lib
, pkgs
, ... 
}:

let

  nixos-repl = pkgs.writeScriptBin "nixos-repl" ''
    #!/usr/bin/env ${pkgs.expect}/bin/expect
    set timeout 120
    spawn -noecho nix --extra-experimental-features repl-flake repl nixpkgs
    expect "nix-repl> " {
      send ":a builtins\n"
      send "pkgs = legacyPackages.x86_64-linux\n"
      interact
    }
  '';

  start-virsh = pkgs.writeShellScriptBin "start-virsh" ''
    sudo virsh net-list --all
    sudo virsh net-autostart default
    sudo virsh net-start default
  '';

in

{

environment.systemPackages = with pkgs; [
      aacgain # ReplayGain for AAC files
      # kdePackages.akregator # RSS feed aggregator
      anydesk # Desktop sharing application, providing remote support and online meetings
      kdePackages.ark
      ath9k-htc-blobless-firmware-unstable
      audit # Audit Library
      autoconf 
      autoconf-archive 
      autogen
      automake # GNU standard-compliant makefile generator
      axel 
      b43Firmware_6_30_163_46
      bash-completion
      bat # cat clone with syntax highlighting and Git integration
      bitwarden
      # bitwarden-cli
      # bleachbit 
      bluez-alsa
      bluez-tools 
      bottom # customizable cross-platform graphical process/system monitor for the terminal
      bpftop # dynamic real-time view of running eBPF programs
      brave # not replaced by ./common/install-x86_64_v3-pkgs.nix package
      btop # Resource monitor that shows usage and stats for processor, memory, disks, network and processes.
      cabextract 
      cachix
      cardpeek # tool to read the contents of ISO7816 eid smart cards and of Calypso / MOBIB transport cards
      # see https://github.com/L1L1/cardpeek/blob/master/doc/cardpeek_ref.en.pdf
      # cargo - used to install hdn home manager package management utility:
      cargo # used to install hdn and utpm package managers
      chafa # Terminal graphics for the 21st century - https://hpjansson.org/chafa/
      checksec # Tool for checking security bits on executables - Full 
      # RELRO, NX enabled/disabled, PIE enabled/disabled
      # cheese # Take photos and videos with your webcam, with fun graphical effects
      chkrootkit
      chrome-export  # Scripts to save Google Chrome's bookmarks and history as HTML bookmarks files 
      chrome-token-signing # Chrome extension for signing with your eID on the web
      chromium # open source browser from Google
      cmake
      # colmena # simple, stateless NixOS deployment tool modeled after NixOps and morph, written in Rust
      coreboot-utils # Various coreboot-related tools
      # corectrl # Control your computer hardware via application profiles
      corefonts
      coreutils-full
      # cpu-x # Free software that gathers information on CPU, motherboard and more
      croc # more features than magic-wormhole # fast,simple,secure file transfer between PCs
      cups-pdf-to-pdf
      curl
      # czkawka # gui: cleaner - A simple, fast and easy to use app to remove unnecessary files from your computer
      dconf2nix # Convert dconf files to Nix, as expected by Home Manager
      # detox 
      # dialog 
      # diffpdf
      dmidecode
      dnscrypt-proxy
      dos2unix
      # drawio
      du-dust # disk space usage analyzer
      dua # disk space usage analyzer
      duf # Disk Usage/Free Utility
      efibootmgr
      exfat # required to access main Ventoy partition on Ventoy multi-iso usb sticks
      expect # required for running nixos-rebuild switch commands
      eza # modern, maintained replacement for ls
      # nur.repos.vanilla.fastfetch # cli: fork of neofetch - fastfetch is faster but inaccurate!
      # fastfetch displays information about your operating system, 
      # software and hardware in an aesthetic and visually pleasing way. 
      fastfetch # actively maintained, feature-rich and performance oriented, neofetch like system information tool
      ffmpeg # Complete, cross-platform solution to record, convert and stream audio and video
      fh # official CLI for FlakeHub: search for flakes, and add new inputs to your Nix flake
      file-roller # Archive manager for the GNOME desktop environment
      # firefox-bin # Mozilla Firefox sells your data to advertisers
      # firmware-manager
      # firmware-updater
      flac # Library and tools for encoding and decoding the FLAC lossless audio file format
      flameshot # Powerful yet simple to use screenshot software
      flatpak # Linux application sandboxing and distribution framework
      # foliate # simple and modern GTK eBook reader
      # foremost
      # frostwire-bin
      fzf # Command-line fuzzy finder written in Go
      gcc # GNU Compiler Collection,
      # ghostty # no vertical scrollbar! # Fast, native, feature-rich terminal emulator pushing modern features
      gitFull # Distributed version control system
      glances # similar to htop # Cross-platform curses-based monitoring tool
      gedit # Former GNOME text editor
      # gnome-terminal # faster than konsole # GNOME Terminal Emulator - copy-paste works in wayland session
      gnugrep # GNU implementation of the Unix grep command
      gnumake # Tool to control the generation of non-source files from sources
      # gnumeric # GNOME Office Spreadsheet
      google-chrome # Freeware web browser developed by Google
      gparted # Graphical disk partitioning tool
      gptfdisk # Set of text-mode partitioning tools for Globally Unique 
      # Identifier (GUID) Partition Table (GPT) disks
      # gradle 
      # graphviz # Graph visualization tools
      # gsmartcontrol # Hard disk drive health inspection tool
      # gst_all_1.gst-libav # FFmpeg/libav plugin for GStreamer
      # gst_all_1.gst-plugins-bad # GStreamer Bad Plugins
      # gst_all_1.gst-plugins-base # Base GStreamer plug-ins and helper libraries
      # gst_all_1.gst-plugins-good # GStreamer Good Plugins
      # gst_all_1.gst-plugins-ugly # Gstreamer Ugly Plugins
      # gst_all_1.gst-vaapi # Set of VAAPI GStreamer Plug-ins
      # gst_all_1.gstreamer # Open source multimedia framework
      # hardinfo # crashes - do not use
      hardinfo2 # System information and benchmarks for Linux systems - uses sysbench
      hunspell # spell checker for LibreOffice
      hunspellDicts.de_DE # Hunspell dictionary for German (Germany)
      hunspellDicts.en_US-large # Hunspell dictionary for English (United States) Large from Wordlist
      hunspellDicts.fr-moderne # Hunspell dictionary for French (modern) from Dicollecte
      hunspellDicts.nl_nl # Hunspell dictionary for Dutch (Netherlands) from OpenTaal
      # hypnotix # IPTV player not loading m3u playlists anymore -> replaced now by vlc player
      ifuse # fuse filesystem implementation to access the contents of iOS devices
      imagemagick # Software suite to create, edit, compose, or convert bitmap images
      inxi # full featured CLI system information tool
      iucode-tool # Intel 64 and IA-32 processor microcode tool
      iw # wireless tool to use nl80211
      iwgtk # Lightweight, graphical wifi management utility for Linux
      iwqr # Tool for generating qr codes for iwd networks
      # jami # Video-conferences and Rendezvous points with no third-party hosting
      # communication platform that
      # respects the privacy and freedoms of its users (alternative for zoom)
      # kazam # screencasting program created with design in mind
      kdePackages.kcalc # Scientific calculator
      killall
      # kitty # terminal emulator # copy-paste not working in wayland sessions! # replaced by gnome-terminal
      # kdePackages.konsole # Terminal emulator by KDE # replaced by gnome-terminal
      # koreader # KOReader is a document viewer for E-Ink devices. Supported fileformats include EPUB, PDF, DjVu, 
      # XPS, CBT, CBZ, FB2, PDB, TXT, HTML, RTF, CHM, DOC, MOBI and ZIP files. It’s available for Kindle,
      # Kobo, PocketBook, Android and desktop Linux.
      krename # Powerful batch renamer for KDE
      # krita # Free and open source painting application
      krusader # Norton/Total Commander clone for KDE
      lame # high quality MPEG Audio Layer III (MP3) encoder
      libheif # ISO/IEC 23008-12:2017 HEIF image file format decoder and encoder
      libimobiledevice # software library that talks the protocols to support iPhone®, iPod Touch® and iPad® devices on Linux
      libjxl # JPEG XL image format reference implementation.
      libmad 
      libmtp
      libpsl
      libraw # Library for reading RAW files obtained from digital photo cameras (CRW/CR2, NEF, RAF, DNG, and others)
      # libreoffice-collabora # not working
      # libreoffice-fresh # not compiling + takes too long to compile
      libreoffice-qt6-fresh # Comprehensive, professional-quality productivity suite, a variant of openoffice.org
      # librewolf
      librsvg
      kdePackages.kate # advanced text editor
      # KDE Connect app exists in Apple iphone App Store as well:
      kdePackages.kdeconnect-kde # also used to exchange files between computers on same network
      libusb1
      libusb-compat-0_1 
      libxml2
      lightdm
      linux-firmware 
      lm_sensors
      # localsend # open source cross-platform alternative to AirDrop
      loudgain # versatile ReplayGain 2.0 loudness normalizer, based on the EBU R128/ITU BS.1770 standard 
      loupe # Simple image viewer application written with GTK4 and Rust
      lsb-release
      lshw
      lynis # Security auditing tool for Linux, macOS, and UNIX-based systems
      lynx # command line web browser used in common.nix
      lz4
      lzip 
      # lzma # replaced by xz
      lzop
      # media-downloader # Qt/C++ GUI front end to yt-dlp, youtube-dl, gallery-dl, lux, you-get, svtplay-dl, 
      # aria2c, wget and safari books.
      microcodeAmd # AMD Processor microcode patch
      microcodeIntel
      # mission-center # Monitor your CPU, Memory, Disk, Network and GPU usage
      # mousam # Beautiful and lightweight weather app based on Python and GTK4
      mp3gain # Lossless mp3 normalizer with statistical analysis
      # mpack # Utilities for encoding and decoding binary files in MIME
      mplayer
      mpv
      mtpfs
      # mucommander # dual pane, cross-platform file manager
      mypaint # graphics application for digital painters
      nautilus
      navi # interactive cheatsheet tool for the command-line and application launchers
      ncdu # ncdu is very powerful tool for showing disk space usage in all subdirectories
      # newsflash # modern feed reader designed for the GNOME desktop
      niv # Easy dependency management for Nix projects
      nix-bash-completions
      nix-du # tool to determine which gc-roots take space in your nix store
      nix-index # files database for nixpkgs
      nix-info
      nixos-generators
      nixos-rebuild-ng # Rebuild your NixOS configuration and switch to it, on local hosts and remote
      nixos-repl # package defined within let statement at the start of this file
      nix-output-monitor
      inputs.nix-software-center.packages.${system}.nix-software-center # uses adwaita-icon-theme-legacy
      nix-tree # Interactively browse a Nix store paths dependencies
      normalize
      # notepadqq # Notepad++-like editor for the Linux desktop
      numix-icon-theme-circle
      numix-cursor-theme
      nvd # Nix/NixOS package version diff tool - used as part of d-n command alias
      nvme-cli
      kdePackages.okular # procedure to digitally sign PDF using Belgian eid card:
      # 1) run bash shell script eid-card-reader-activation-script
      # 2) go to okular::settings::configure backends::PDF::set signature backend to NSS 
      # and set Certificate database to  custom value ~/.pki/nssdb
      # 3) go to okular::Tools::Digitally sign
      # onlyoffice-bin_latest # worse than LibreOffice - Office suite that combines text, spreadsheet and presentation editors 
      # allowing to create, view and edit local documents 
      opencore-amr # Library of OpenCORE Framework implementation of Adaptive
      # Multi Rate Narrowband and Wideband (AMR-NB and AMR-WB) speech codec. 
      # Library of VisualOn implementation of Adaptive Multi Rate Wideband (AMR-WB)
      open-in-mpv # Simple web extension to open videos in mpv
      os-prober # Utility to detect other OSs on a set of drives # depends on dmraid package
      p7zip
      # use https://pairdrop.net/ to transfer files between ios and GNU/Linux on same network
      pan # GTK-based Usenet newsreader good at both text and binaries
      pandoc # Conversion between documentation formats, including typst format
      parallel 
      pavucontrol
      # pdfarranger # replaced by stirling-pdf 
      # pdfgrep # replaced by ripgrep-all
      pdfminer
      # pdfmixtool # replaced by stirling-pdf 
      photoflare # Photoflare is a cross-platform image editor with an aim to balance between powerful
      # features and a very friendly graphical user interface
      # pika-backup # Simple backups based on borg
      pipelight
      pipewire # Server and user space API to deal with multimedia pipelines
      pkg-config # tool that allows packages to find out information about other packages (wrapper script)
      plocate
      kdePackages.poppler # PDF rendering library
      poppler_utils # PDF rendering library
      powerstat
      # powertop # causes USB keyboard and mouse to get stuck on didi-laptop and ulysses-desktop -> disable!
      pulseaudioFull
      qalculate-qt # Ultimate desktop calculator
      qdirstat # Graphical disk usage analyzer
      # rar # fails to build - cannot find working mirror
      rclone 
      ripgrep-all # rga is very powerful tool for searching word in PDFs/pdf files in all subdirectories:
      # rsync # Fast incremental file transfer utility # has 6 critical vulnerabilities
      sbctl # Secure Boot key manager
      scdl # Download Music from Soundcloud
      screen 
      screenfetch # Fetches system/theme information in terminal for Linux desktop screenshots
      # scribus # Desktop Publishing (DTP) and Layout program
      sd-switch # systemd unit switcher for Home Manager 
      shellcheck # Shell script analysis tool
      shellharden # Corrective bash syntax highlighter
      shim-unsigned # UEFI shim loader
      # shotwell # Popular photo organizer for the GNOME desktop
      # sigil # Free, open source, multi-platform ebook (ePub) editor
      # simplescreenrecorder # screen recorder for Linux, can record video game sessions
      smartmontools # Tools for monitoring the health of hard drives
      # smplayer 
      spaceFM # 4-pane file manager - excellent alternative to krusader
      spectre-meltdown-checker # Spectre & Meltdown vulnerability/mitigation checker for Linux
      # speedcrunch # calculator
      stacer # Linux System Optimizer and Monitoring
      # run following query on Github: language:nix programs.starship.settings
      # starship # Minimal, blazing fast, and extremely customizable prompt for any shell
      start-virsh # package defined within let statement at the start of this file
      sudo-rs # memory safe implementation of sudo and su
      sysbench # Modular, cross-platform and multi-threaded benchmark tool - used by hardinfo2
      # tauon # Linux desktop music player from the future -> caused crash and reboot of PC
      # teams # Download links have probably been pulled as Microsoft has sunset the Linux client in favour 
      # of their web version. In my experience the web client does work much better than the native one, 
      # including desktop sharing through Pipewire, for what it's worth. See https://github.com/NixOS/nixpkgs/issues/217473
      teamviewer # desktop sharing application, providing remote support
      # tesseract # OCR engine
      # testdisk-qt # free data recovery utilities
      tor-browser-bundle-bin
      udiskie # Removable disk automounter for udisks and iso images
      unp # Command line tool for unpacking archives easily
      unrar # Utility for RAR archives 
      unzip # Extraction utility for archives compressed in .zip format
      usbmuxd # socket daemon to multiplex connections from and to iOS devices
      usbutils # Tools for working with USB devices, such as lsusb
      # util-linux # set of system utilities for Linux
      # variety # wallpaper manager for Linux systems
      # ventoy - https://www.ventoy.net/en/download.html
      # ventoy - can boot multiple iso images from same USB stick - much easier to use than multisystem utility
      # Simply erase and format multi-iso usb stick using ventoy utility in NixOS or Windows
      # Then copy iso images of Windows and NixOS or any other Linux distro to the ventoy exfat partition on the multi-iso USB stick
      ventoy-full # New Bootable USB Solution
      vlc # Cross-platform media player and streaming server - use as IPTV player to load m3u playlists
      # vlc can load following playlist: https://iptv-org.github.io/iptv/index.country.m3u
      warp-terminal # CLOSED SOURCE - Rust-based terminal reimagined with AI and collaborative tools for better productivity
      waveterm # OPEN-SOURCE - cross-platform terminal for seamless workflows
      wget # Tool for retrieving files using HTTP, HTTPS, and FTP
      # whatsapp-for-linux # Whatsapp desktop messaging app
      # wike # Wikipedia reader
      wine64 # use wine64 via bottles application to launch Collabora Office (Windows app)
      winetricks # Script to install DLLs needed to work around problems in Wine
      wineWowPackages.full # Open Source implementation of the Windows API on top of X, OpenGL, and Unix
      wireplumber # modular session / policy manager for PipeWire
      wpsoffice # Office suite, formerly Kingsoft Office
      wv # Converter from Microsoft Word formats to human-editable ones
      xcb-util-cursor
      xlsx2csv 
      xorg.libpciaccess
      xorg.libX11
      xorg.libX11.dev
      xorg.libxcb
      xorg.libXft
      xorg.libXinerama
      xorg.xhost # to allow users in distrobox containers to launch GUI applications and connect to Xorg server
      xorg.xinit
      xorg.xwininfo # is build dependency for steamlinkerlaunch gaming utility
      xorg.xinput
      xz # general-purpose data compression software, successor of LZMA
      # yed # powerful desktop application that can be used to quickly and effectively generate high-quality diagrams
      # youtube-music # Electron wrapper around YouTube Music # sound not working!
      yt-dlp # cli: fork of youtube-dl with additional features
      zenity # Tool to display dialogs from the commandline and shell scripts
      zip # Compressor/archiver for creating and modifying zipfiles
      zoom-us # video conferencing application
      zoxide # fast cd command that learns your habits
      zstd # Zstandard real-time compression algorithm, required by protonup-qt 
        

];

}
