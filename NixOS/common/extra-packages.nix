# Revision history:
# 2024/8/7:  creation of ./common/extra-packages.nix
# 2024/8/15: quickemu fails to compile
# 2024/8/15: pcloud uses 100% CPU in plasmawayland -> use pcloud in cinnamon x11 only
# 2024/8/15: only use pcloud in X11/Xorg sessions to avoid 100% CPU usage
# 2024/8/19: commented out element-desktop package, because it depends on jitsi-meet
# package which is now marked as insecure, because it uses libolm library
# 2024/9/8: gpu-burn-unstable-2024-04-09 is marked as broken
# 2025/1/29: added 5 P2P file sharing applications besides qbittorrent

{ config
, inputs
, lib
, pkgs
, ... 
}:

let
  patchelfFixes = pkgs.patchelfUnstable.overrideAttrs (_finalAttrs: _previousAttrs: {
    src = pkgs.fetchFromGitHub {
      owner = "Patryk27";
      repo = "patchelf";
      rev = "527926dd9d7f1468aa12f56afe6dcc976941fedb";
      sha256 = "sha256-3I089F2kgGMidR4hntxz5CKzZh5xoiUwUsUwLFUEXqE=";
    };
  });
  pcloudFixes = pkgs.pcloud.overrideAttrs (_finalAttrs:previousAttrs: {
    nativeBuildInputs = previousAttrs.nativeBuildInputs ++ [ patchelfFixes ];
    dontCheckForBrokenSymlinks = true; # workaround fix for https://github.com/NixOS/nixpkgs/pull/370750
  });
in

{

environment.systemPackages = with pkgs; [
      amdgpu_top # Tool to display AMDGPU usage
      amdvlk # AMD Open Source Driver For Vulkan
      # amule # FAILS TO COMPILE # P2P file sharing application for the eD2K and Kademlia networks
      # audacious # Lightweight and versatile audio player
      # audacity # Sound editor with graphical UI
      # caprice32 # complete emulation of CPC464, CPC664 and CPC6128
      # get files for CPC464 at pouet.net
      # clipgrab # Video downloader for YouTube and other sites
      easytag # View and edit tags for various audio files
      # egl-wayland # EGLStream-based Wayland external platform
      # element-desktop # feature-rich collaboration/chat client for Matrix.org
      firejail # Namespace-based sandboxing tool for Linux
      # flashrom # Utility for reading, writing, erasing and verifying flash ROM chips
      fopnu # P2P file sharing application # slow transfers below 50 KB/s
      # furmark # OpenGL and Vulkan Benchmark and Stress Test
      fuseiso # FUSE module to mount ISO filesystem images
      # geekbench # Cross-platform benchmark
      gitkraken # downright luxurious and most popular Git client for Windows, Mac & Linux
      # glxinfo # Test utilities for OpenGL
      # gnunet-gtk # not user friendly # P2P file sharing application for GNUnet mesh network # unrelated to gnutella
      google-drive-ocamlfuse
      # gpu-burn # Multi-GPU CUDA stress test - use command " gpu_burn -tc 360 "
      gpu-viewer # gui: A front-end to glxinfo, vulkaninfo, clinfo and es2_info
      gtkgnutella # P2P file sharing application which runs on the gnutella network # only working in rhino-linux container
      # gnutella has decent file transfer speeds
      # gtkgnutella works in rhino-linux container, but not in NixOS host
      # i2pd # Minimal I2P router written in C++
      # jellyfin # supports IPTV playlists and IPTV TV guides
      # kiwix - https://library.kiwix.org/?lang=eng&q=course
      # kiwix - https://library.kiwix.org/?lang=eng&q=edu
      # kiwix - free offline access to 12 GB of "Boundless" course material
      # kiwix
      # naps2 # build fails on insecure dotnet-sdk package # Merge multiple PDF files, Scan documents to PDF and more, as simply as possible
      newsboat # fork of Newsbeuter, an RSS/Atom feed reader for the text console
      # nheko # Desktop client for the Matrix protocol
      nicotine-plus # P2P file sharing application for the SoulSeek peer-to-peer system # works well!
      nvtopPackages.nvidia # (h)top like task monitor for AMD, Adreno, Intel and NVIDIA GPUs
      nu_scripts # Place to share Nushell scripts with each other
      nushell # Modern shell written in Rust
      # oil # upgrade path from bash to a better language and runtime
      onefetch # Git repository summary on your terminal
      orjail # cli: Force programs to exclusively use Tor network - but use Tor browser for webbrowsing!
      # passmark-performancetest # Software tool that allows everybody to quickly assess the performance 
      # of their computer and compare it to a number of standard 'baseline' computer systems
      # pcloud uses 100% CPU in plasmawayland
      # only use pcloud in X11/Xorg sessions to avoid 100% CPU usage
      pcloudFixes # Secure and simple to use cloud storage for your files; pCloud Drive, Electron Edition 
      # phoronix-test-suite # Open-Source, Automated Benchmarking
      popcorntime # gui: An application that streams movies and TV shows from torrents
      # powershell # Powerful cross-platform (Windows, Linux, and macOS) shell and scripting language based on .NET
      protonvpn-cli # FRITZ!Box can connect to ProtonVPN via Wireguard (since FRITZ!OS 7.57 in September 2023)
      protonvpn-gui # FRITZ!Box can connect to ProtonVPN via Wireguard (since FRITZ!OS 7.57 in September 2023)
      qbittorrent # Featureful free software BitTorrent client # P2P file sharing application
      # quarto # Open-source scientific and technical publishing system built on Pandoc
      # quickemu # Windows 11/Linux/Mac OS VM launcher
      # retroshare # Decentralized peer to peer chat application
      # shortwave # unstable audio connection, unreliable player
      # simplex-chat-desktop # Desktop application for SimpleX Chat - first messenger app without user IDs
      # SimpleX Chat is available on mobile phones too
      # spotify # does not work, cannot open browser window in order to log in!
      st # replaces putty client
      stirling-pdf # Locally hosted web application that allows you to perform 50+ operations on PDF files
      # stirling-pdf uses port 8080 - signing PDF with certificate is supported (like okular app)
      stremio # modern media center that gives you the freedom to watch everything you want
      tinymist # Tinymist is an integrated language service for Typst
      trivy # Simple and comprehensive vulnerability scanner for containers, suitable for CI
      typst # new markup-based typesetting system that is powerful and easy to learn - easier than LaTeX
      typstwriter # Integrated editor for the typst typesetting system
      typstyle # Format your typst source code
      udevil # mount ISO files, nfs://, smb://, ftp://, ssh:// and WebDAV URLs, and tmpfs/ramfs filesystems
      utpm # Package manager for typst
      # fuseiso and udevil can be used to mount iso files via spacefm application
      # wireshark
      # xLights is a sequencer for (Tesla) Lights. xLights has usb and E1.31 drivers. You can
      # create sequences in this object oriented program. You can create playlists, 
      # schedule them, test your hardware, convert between different sequencers
      # See: https://xlights.org/
      # See: https://github.com/teslamotors/light-show
      # xlights
      yad # GUI dialog tool for shell scripts
      # ytdownloader # Modern GUI video and audio downloader

];

}
