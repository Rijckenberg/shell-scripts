# 2024/4/4: creation of ./common/ai-packages.nix

{ config, pkgs, ... }:

{

environment.systemPackages = with pkgs; [

      (pkgs.writers.writeBashBin "activate-python-env-script" { } ''
     
#!/usr/bin/env bash
# Author: Mark Rijckenberg 
# Latest modification date: 2025/2/26
# Prerequisites: NixOS 24.11, micromamba, python 3.12 or newer
# Based on application list on https://datlinux.com

micromamba config append channels conda-forge
micromamba config set channel_priority strict
micromamba self-update
echo 'Run following 3 commands manually (one by one):'
echo 'eval "$(micromamba shell hook --shell bash)"'
echo 'micromamba create -n python-environment-name'
echo 'micromamba activate python-environment-name'

    '')


      aichat # Use GPT-4(V), Gemini, LocalAI, Ollama and other LLMs in the terminal
      bun # Incredibly fast JavaScript runtime, bundler, transpiler and package manager – all in one
      # chatblade
      code-cursor # AI-powered code editor built on vscode
      # codon # high-performance, zero-overhead, extensible Python compiler using LLVM
      conda # Conda is a package manager for Python -> use uv command instead (written in Rust)
      # datasette # Multi-tool for exploring and publishing data
      # dbeaver-bin # Universal SQL Client for developers, DBA and analysts. Supports MySQL, PostgreSQL, MariaDB, SQLite, and more
      # druid # Apache Druid - a high performance real-time analytics database
      # gephi # Platform for visualizing and manipulating large graphs
      # grafana # Gorgeous metric viz, dashboards & editors for Graphite, InfluxDB & OpenTSDB
      jan # open source alternative to ChatGPT that runs 100% offline on your computer
      # jasp-desktop # Complete statistical package for both Bayesian and Frequentist statistical methods
      # julia # High-level performance-oriented dynamical language for technical computing
      jupyter # High-level dynamically-typed programming language
      labplot # LabPlot is a FREE, open source and cross-platform Data Visualization and Analysis software accessible to everyone
      lmstudio # LM Studio is an easy to use desktop app for experimenting with local and open-source Large Language Models (LLMs)
      # luigi # Python package that helps you build complex pipelines of batch jobs
      # meld # Visual diff and merge tool
      metabase # Easy, open source business intelligence tool for everyone in your company to ask questions and learn from data
      micromamba # Reimplementation of the conda package manager, but faster -> use uv command instead (written in Rust)
      ninja # Small build system with a focus on speed
      # ninja is required to be able to locally build certain GPT/LLM models like Open Interpreter
      # nodejs_21 # Event-driven I/O framework for the V8 JavaScript engine
      # openrefine # Power tool for working with messy data and improving it
      # oterm # text-based terminal client for Ollama
      # paraview # 3D Data analysis and visualization application
      # pspp # Free replacement for SPSS, a program for statistical analysis of sampled data
      python313
      # python313Full
      python313Packages.beautifulsoup4
      python313Packages.dask # Minimal task scheduling abstraction
      # python313Packages.datatable # data.table for Python
      # python313Packages.glueviz # BROKEN - Linked Data Visualizations Across Multiple Files
      # python313Packages.holoviews # Python data analysis and visualization seamless and simple
      python313Packages.ipython # IPython - Productive Interactive Computing
      python313Packages.jupyter # Installs all the Jupyter components in one go
      python313Packages.jupyterlab # Jupyter lab environment notebook server extension
      # python313Packages.maestral
      python313Packages.matplotlib # used by GPT/LLM AI models
      python313Packages.numpy # used by GPT/LLM AI models
      # python313Packages.orange3 # BROKEN # Data mining and visualization toolbox for novice and expert alike
      python313Packages.pandas # used by GPT/LLM AI models
      python313Packages.pip # used by GPT/LLM AI models
      python313Packages.playwright # used by GPT/LLM AI models - Playwright testing and automation library
      python313Packages.polars # Fast multi-threaded DataFrame library in Rust | Python | Node.js
      python313Packages.pygame # used by GPT/LLM AI models
      python313Packages.qdarkstyle # dark stylesheet for Python and Qt applications
      # python313Packages.qiskit # BROKEN # Software for developing quantum computing programs
      python313Packages.spyder # depends on python313Packages.qdarkstyle >= 3.2
      python313Packages.scikit-learn # used by GPT/LLM AI models
      python313Packages.scipy # used by GPT/LLM AI models
      python313Packages.seaborn # used by GPT/LLM AI models
      # python313Packages.sklearn-deap # is marked as broken by NixOS
      (pkgs.python3.withPackages (python-pkgs: [python-pkgs.spyder]))
      # python313Packages.spyder # BROKEN - Scientific python development environment -> depends on python313Packages.qdarkstyle >= 3.2
      # python313Packages.spyder-kernels # BROKEN - Jupyter kernels for Spyder's console
      # python313Packages.tensorflow # is marked as broken by NixOS
      python313Packages.tkinter # standard Python interface to the Tcl/Tk GUI toolkit
      python313Packages.uv # Extremely fast Python package installer and resolver, written in Rust
      # python313Packages.xhtml2pdf # broken
      python313Packages.yfinance # used by GPT/LLM AI models
      # qgis # Free and Open Source Geographic Information System
      # quarto # Open-source scientific and technical publishing system built on Pandoc
      # rstudio # Set of integrated tools for the R language
      # scilab-bin # Scientific software package for numerical computations (Matlab lookalike)
      # tabula-java # Library for extracting tables from PDF files
      uv # extremely fast Python package installer and resolver, written in Rust -> use uv instead of conda or micromamba 
      # uv is drop-in replacement for common pip, pip-tools, and virtualenv commands
      # use uv instead of conda/micromamba/pip
      # if uv is not working correctly, use micromamba as fallback option
      # spyder # BROKEN - scientific python development environment
      # veusz # Scientific plotting and graphing program with a GUI
      virtualenv # tool to create isolated Python environments
      # visidata # interactive terminal multitool for tabular data
      vscode-extensions.eamodio.gitlens # Visual Studio Code extension that improves its built-in Git capabilities
      vscode-extensions.esbenp.prettier-vscode # Code formatter using prettier
      vscode-extensions.genieai.chatgpt-vscode # Visual Studio Code extension to support ChatGPT, GPT-3 and Codex conversations
      vscode-extensions.github.copilot # GitHub Copilot uses OpenAI Codex to suggest code and entire functions in real-time right from your editor
      # vscode-extensions.github.copilot-chat 
      vscode-extensions.grapecity.gc-excelviewer # Edit Excel spreadsheets and CSV files in Visual Studio Code and VS Code for the Web
      vscode-extensions.jnoortheen.nix-ide # Nix language support with formatting and error report
      vscode-extensions.myriad-dreamin.tinymist # VSCode extension for providing an integration solution for Typst
      # vscode-extensions.ms-python.python # broken # Visual Studio Code extension with rich support for the Python language
      vscode-extensions.ms-toolsai.jupyter # Jupyter extension for vscode
      vscode-extensions.ms-vscode.powershell # Visual Studio Code extension for PowerShell language support
      vscode-extensions.wmaurer.change-case # VSCode extension for quickly changing the case (camelCase, CONSTANT_CASE, snake_case, etc) of the current selection or current word
      vscode-with-extensions # Open source source code editor developed by Microsoft for Windows 
      # weka # Collection of machine learning algorithms for data mining tasks
      # wxmaxima # Cross platform GUI for the computer algebra system Maxima
];


}
