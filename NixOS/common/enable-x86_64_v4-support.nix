# 2024/5/2: creation of ./common/gaming.nix
# 2024/5/16: added chaotic packages in common/gaming.nix
# 2024/5/16: compile of x86-64-v4 packages fails on libvorbis package on desktop pc
# 2024/5/16: minimum RAM requirement for compiling packages: 37 GB of RAM
# enable this and rebuild once before enabling ./common/install-x86_64_v4-pkgs.nix

{ chaotic, config, inputs, lib, pkgs, ... }:

{

nix.settings.system-features = [ "nixos-test" "benchmark" "big-parallel" "kvm" "gccarch-x86-64-v4" ];

}