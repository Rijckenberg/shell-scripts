#!/usr/bin/env bash
# Creation date: 2023/9/7
# Modification date: 2023/11/9
# Author: Mark Rijckenberg
# Required diskspace: at least 10 GB
# List of available container images:  https://github.com/89luca89/distrobox/blob/main/docs/compatibility.md#containers-distros
# Installed container distro:  DISTRIB_DESCRIPTION="Debian (latest)"

# run following xhost command to allow launching collaboraoffice GUI inside debian-latest container in distrobox:
#    xhost local:ulysses
# ubuntu:latest tag points to the "latest LTS", since that is the version recommended for general use:
# distrobox create --image docker.io/library/ubuntu:latest
# distrobox create --image quay.io/toolbx-images/ubuntu-toolbox:latest
# ubuntu:rolling tag points to the latest release (regardless of LTS status):
# https://hub.docker.com/_/ubuntu/tags?page=1&name=rolling
# debian:latest points to the latest Debian release:
# https://hub.docker.com/_/debian/tags?page=1&name=latest

# when switching from latest kernel to cachyos gaming kernel or vice versa,
# you need to delete the old distrobox container and create a replacement container
distrobox create --image docker.io/library/debian:latest
distrobox list
echo "If you cannot enter the debian-latest distrobox container after a NixOS upgrade, please run following command to fix the issue:"
echo "distrobox create -c debian-latest tmp && podman rm debian-latest; podman rename tmp debian-latest; distrobox enter debian-latest"
echo "Run  distrobox-export --app collaboraoffice-writer inside debian-latest container in distrobox after installing collaboraoffice"
distrobox enter debian-latest

