#!/usr/bin/env bash
# Authors: Mark Rijckenberg, CodeLlama-34b-Instruct-hf (https://huggingface.co/chat)  and Grok 3
# Latest modification date: 2025/3/3
# This bash shell script is compatible with NixOS and Debian Bookworm running in podman container
# Download newest iso images to ~/backup/installers
# Based on https://github.com/DarwinToledo/Absolute-USB-MultiBoot-Installer---A.U.M.B.I./blob/master/Resources/Scripts/DistroList.nsh

# Change history:
# 2025/1/9: fixed URL for lernstick, openbsd and supergrub2

# Get iso images (to copy to Ventoy USB stick / SSD stick)
ISOTOTAL=`cat get-latest-iso-images.bash |grep axel|wc -l`
echo "Attempting to download $ISOTOTAL iso images..."
echo "Minimum of 46 GB of free disk space required..."
echo "The downloads will take at least 120 minutes to complete on a 150 Mb/s Internet connection"

# Variables
cd /tmp
rm default*; rm download*; rm index*; rm Readme*; rm Releases*; rm *.xz; rm *.zip
wget https://wiki.ubuntu.com/Releases
UBUNTUCODENAME=`cat Releases|grep discourse | head -n 1| cut -d"/" -f5 | cut -d "-" -f1`
echo $UBUNTUCODENAME

mkdir -p ~/backup
mkdir -p ~/backup/installers
cd ~/backup/installers
rm default*; rm download*; rm index*; rm Readme*; rm Releases*; rm *.xz; rm *.zip

# Boot Repair disk -> just reinstall Linux boot loader from distro's LiveUSB session (easier solution)
# URL="https://sourceforge.net/projects/boot-repair-cd/files/latest/download"
# wget $URL

# Clonezilla Live iso - use with GParted Live iso when cloning disks (SSDs)
URL1="https://sourceforge.net/projects/clonezilla/files/clonezilla_live_alternative_testing/"
URL2=`curl $URL1 |grep $UBUNTUCODENAME |head -1| cut -d'"' -f2`
axel $URL1$URL2/clonezilla-live-$URL2-amd64.iso/download
URL3="https://sourceforge.net/projects/clonezilla/files/latest/download"
axel $URL3
unp clonezilla*.zip

# EasyOS (RAM-only secure web browsing)
URL="https://distro.ibiblio.org/easyos/amd64/releases/scarthgap/2025/6.6.3/easy-6.6.3-amd64.img"
axel $URL

# Finnix Rescue iso - not enough documentation...
# URL=`curl https://www.finnix.org/ |grep iso |head -1 | cut -d'"' -f2`
# wget $URL

# G4L Disk imaging
URL="http://sourceforge.net/projects/g4l/files/latest/download"
axel $URL
unp g4l*.zip

# GhostBSD (FreeBSD distro for secure banking)
# GhostBSD supports exfat partition on Ventoy USB stick
# See https://gitlab.com/Rijckenberg/shell-scripts/-/edit/master/NixOS/scripts/install-freebsd-ventoy-compatibility-fix.bash
#URL1="https://download.fr.ghostbsd.org/releases/amd64/latest/"
#URL2=`curl $URL1 | grep XFCE | grep iso | grep -v sha256 | grep -v torrent | cut -d"\"" -f2  | head -n 1`

#axel $URL1$URL2
rm GhostBSD*

# Array of official GhostBSD mirror base URLs
mirrors=(
    "download.ghostbsd.org"    # Canada
    "download.fr.ghostbsd.org" # France
    "download.za.ghostbsd.org" # South Africa
)

# Directory path for the latest releases
dir_path="/releases/amd64/latest/"

# Temporary files for downloading
tmp_iso="GhostBSD.iso.tmp"
tmp_checksum="checksum.tmp"

# Loop through each mirror until successful
for mirror in "${mirrors[@]}"; do
    echo "Trying mirror: $mirror"
    
    # Construct the full directory URL
    dir_url="https://$mirror$dir_path"
    
    # Fetch the directory listing and extract the latest XFCE ISO filename
    iso_file=$(curl -s "$dir_url" | grep XFCE | grep iso | grep -v sha256 | grep -v torrent | cut -d'"' -f2 | head -n 1)
    
    # Check if an ISO file was found
    if [ -z "$iso_file" ]; then
        echo "No XFCE ISO found on $mirror. Trying next mirror."
        continue
    fi
    
    # Construct the full ISO and checksum URLs
    iso_url="$dir_url$iso_file"
    checksum_file="$iso_file.sha256"
    checksum_url="$dir_url$checksum_file"
    
    # Download the checksum file
    if ! curl -s "$checksum_url" > "$tmp_checksum"; then
        echo "Failed to download checksum file from $mirror. Trying next mirror."
        continue
    fi
    
    # Extract the expected checksum
    expected_checksum=$(awk '{print $1}' "$tmp_checksum")
    if [ -z "$expected_checksum" ]; then
        echo "Failed to extract checksum from $tmp_checksum. Trying next mirror."
        rm -f "$tmp_checksum"
        continue
    fi
    
    # Download the ISO using axel
    echo "Downloading $iso_file from $mirror"
    if axel -o "$tmp_iso" "$iso_url"; then
        # Calculate the checksum of the downloaded ISO
        calculated_checksum=$(sha256sum "$tmp_iso" | awk '{print $1}')
        if [ "$calculated_checksum" == "$expected_checksum" ]; then
            # Success: Rename the temporary ISO file and clean up
            mv "$tmp_iso" "$iso_file"
            rm -f "$tmp_checksum"
            echo "Download successful from $mirror and checksum verified."
        else
            echo "Checksum mismatch. Trying next mirror."
            rm -f "$tmp_iso"
        fi
    else
        echo "Download failed from $mirror. Trying next mirror."
    fi
    
    # Clean up temporary checksum file
    rm -f "$tmp_checksum"
done

# If all mirrors fail
echo "Failed to download from all mirrors or checksum verification failed."

# GParted Live iso - use with Clonezilla Live iso when cloning disks (SSDs)
URL=`curl https://gparted.org/download.php |grep amd64.iso |cut -d '"' -f2`
axel $URL
URL="https://sourceforge.net/projects/gparted/files/latest/download"
axel $URL

# HardenedBSD distro
# HardenedBSD distro does not support exfat partition on Ventoy USB stick
# See https://gitlab.com/Rijckenberg/shell-scripts/-/edit/master/NixOS/scripts/install-freebsd-ventoy-compatibility-fix.bash
# wget https://installers.hardenedbsd.org/pub/current/amd64/amd64/installer/LATEST/memstick.img
# mv memstick.img hardenedbsd.img

# Hiren BootCD
URL=`curl https://www.hirensbootcd.org/download/ |grep HBCD | cut -d '"' -f4`
axel $URL

# Ikki boot (Rescue)
URL="https://sourceforge.net/projects/ikkiboot/files/latest/download"
axel $URL

# Lernstick iso
URL1="https://releases.lernstick.ch/"
URL2=`curl -s $URL1 | grep -v md5 | grep -v sha256| grep lernstick_debian| grep latest.iso | tail -3 | head -1 | cut -d'"' -f4`
axel $URL1$URL2

# Linux Mint Cinnamon

# Array of Linux Mint mirrors (HTTP/HTTPS only, up to 5)
mirrors=(
  "https://mirrors.edge.kernel.org/linuxmint/stable/"
  "https://mirror.math.princeton.edu/pub/linuxmint/stable/"
  "https://mirrors.mit.edu/linuxmint/stable/"
  "https://mirror.csclub.uwaterloo.ca/linuxmint/stable/"
  "https://mirror.umd.edu/linuxmint/stable/"
)

# Function to list directories from a URL
list_dirs() {
  local url=$1
  curl -s "$url" | grep -o '<a href="[^"]*/">' | sed 's/<a href="//; s/">//' | sed 's|/$||' | grep -v '^\.'
}

# Function to extract ISO file info (URL and modification date)
extract_iso_info() {
  local version_url=$1
  local listing=$(curl -s "$version_url")
  # Match Cinnamon amd64 ISO files only
  local lines=$(echo "$listing" | grep -i 'linuxmint-[0-9]\+\.[0-9]\+-cinnamon-64bit\.iso' | grep -v '\.md5\|\.sha256\|\.torrent')
  if [ -n "$lines" ]; then
    while IFS= read -r line; do
      local file=$(echo "$line" | grep -o '<a href="[^"]*">' | sed 's/<a href="//; s/">//' | head -1)
      local date=$(echo "$line" | grep -o '[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\} [0-9]\{2\}:[0-9]\{2\}' | head -1)
      if [ -n "$file" ] && [ -n "$date" ] && [[ "$file" =~ cinnamon-64bit\.iso$ ]]; then
        echo "${version_url}${file} $date"
      fi
    done <<< "$lines"
  fi
}

# Function to collect all Cinnamon ISO files from a mirror
collect_iso_files() {
  local mirror=$1
  echo "Scanning $mirror..." >&2
  local version_list=$(list_dirs "$mirror")
  if [ -z "$version_list" ]; then
    echo "No versions found on $mirror" >&2
    return 1
  fi
  local iso_info=""
  for version in $version_list; do
    echo "Checking version: $version" >&2
    local version_url="${mirror}${version}/"
    local info=$(extract_iso_info "$version_url")
    if [ -n "$info" ]; then
      iso_info="$iso_info"$'\n'"$info"
      echo "Found: $info" >&2
    fi
  done
  echo "$iso_info" | sed '/^$/d'
}

# Function to download the latest Cinnamon ISO from a mirror
download_from_mirror() {
  local mirror=$1
  echo "Trying mirror: $mirror"
  local iso_list=$(collect_iso_files "$mirror")
  if [ -z "$iso_list" ]; then
    echo "Error: No valid Cinnamon .iso files found on $mirror" >&2
    return 1
  fi
  # Sort by date using Unix timestamp
  local latest_iso=$(echo "$iso_list" | while read -r url date time; do
    timestamp=$(date -d "$date $time" +%s 2>/dev/null || echo "0")
    echo "$timestamp $url $date $time"
  done | sort -n | tail -n 1)
  
  local url=$(echo "$latest_iso" | awk '{print $2}')
  local date=$(echo "$latest_iso" | awk '{print $3, $4}')
  echo "Latest Cinnamon ISO: $url (modified on $date)"
  axel "$url"
  if [ $? -eq 0 ]; then
    echo "Download successful: $url"
    return 0
  else
    echo "Download failed: $url" >&2
    return 1
  fi
}

# Main loop to try each mirror
for mirror in "${mirrors[@]}"; do
  if download_from_mirror "$mirror"; then
  fi
done

echo "Failed to download from all mirrors" >&2

# Linux Mint Deutsch
URL="https://sourceforge.net/projects/linuxmintdeutsch/files/latest/download"
axel $URL

# NixOS Plasma 6 iso 
# Written using codellama/CodeLlama-34b-Instruct-hf on https://huggingface.co/chat :
URL1="https://nixos.org/download.html"
URL2=`curl -s $URL1 | grep "plasma6-" |grep x86_64 | cut -d '"' -f4`
axel $URL2

# NixOS minimal iso
# Written using codellama/CodeLlama-34b-Instruct-hf on https://huggingface.co/chat :
URL1="https://nixos.org/download.html"
URL2=`curl -s $URL1 | grep "minimal-" |grep x86_64 | cut -d '"' -f4`
axel $URL2

# NomadBSD (portable FreeBSD distro for secure banking)
# issue: NomadBSD 132R or older does not support exfat partition on Ventoy USB stick
# See https://gitlab.com/Rijckenberg/shell-scripts/-/edit/master/NixOS/scripts/install-freebsd-ventoy-compatibility-fix.bash

# Function to get the latest amd64 UFS image file from a mirror
get_latest_file() {
  local mirror=$1
  # Fetch directory listing with timeouts to prevent hanging
  local files=$(curl -s --connect-timeout 10 --max-time 30 "$mirror" | grep -o '<a href="nomadbsd-[^"]*\.img\.lzma">' | sed 's/<a href="//; s/">//')
  local amd64_ufs_files=$(echo "$files" | grep '\.amd64\.ufs\.img\.lzma$')
  if [ -n "$amd64_ufs_files" ]; then
    # Sort files by version and select the latest
    echo "$amd64_ufs_files" | sort -V | tail -n 1
  else
    echo ""
  fi
}

# Initialize mirrors array with the main site
main_site="https://nomadbsd.org/download/"
mirrors=("$main_site")

# Fetch additional mirrors from GitHub, excluding commented lines
while IFS= read -r line; do
  mirrors+=("$line")
done < <(curl -s https://raw.githubusercontent.com/nomadbsd/website/master/mirrors | grep -v '^#' | cut -f2)

# Limit to the first 5 mirrors
if [ ${#mirrors[@]} -gt 5 ]; then
  mirrors=("${mirrors[@]:0:5}")
fi

# Flag to track download success
downloaded=false

# Loop through mirrors to attempt download
for mirror in "${mirrors[@]}"; do
  echo "Trying mirror: $mirror"
  latest_file=$(get_latest_file "$mirror")
  if [ -n "$latest_file" ]; then
    echo "Found latest file: $latest_file"
    # Attempt to download with resume capability, following redirects, and failing on HTTP errors
    curl -C - -O --fail --location "$mirror$latest_file"
    if [ $? -eq 0 ]; then
      echo "Download successful from $mirror"
      downloaded=true
      break
    else
      echo "Download failed from $mirror"
    fi
  else
    echo "Could not find amd64 UFS image on $mirror"
  fi
done

# Check if download was successful
if $downloaded; then
  echo "File downloaded successfully: $latest_file"
else
  echo "Failed to download file from all mirrors."
fi


# OpenBSD (can run Plasma 5 and Plasma 6 desktop environments)

# List of mirrors to try (all provide HTTP access to OpenBSD files)
mirrors=(
  "https://ftp.openbsd.org/pub/OpenBSD/"
  "https://ftp.eu.openbsd.org/pub/OpenBSD/"
  "https://ftp.usa.openbsd.org/pub/OpenBSD/"
  "https://mirror.leaseweb.com/pub/OpenBSD/"
  "https://mirrors.mit.edu/pub/OpenBSD/"
)

# Function to get the latest version from a mirror
get_latest_version() {
  local mirror=$1
  # Fetch directory listing silently
  local listing=$(curl -s "$mirror")
  if [ $? -ne 0 ]; then
    echo ""
    return
  fi
  # Extract version directories (e.g., 7.6/) from HTML links
  local versions=$(echo "$listing" | grep -o '<a href="[0-9]\.[0-9]/">' | sed 's/<a href="//; s/">//')
  if [ -z "$versions" ]; then
    echo ""
    return
  fi
  # Sort versions numerically and select the highest one, removing trailing slash
  echo "$versions" | sort -V | tail -n 1 | sed 's|/||'
}

# Iterate over each mirror
for mirror in "${mirrors[@]}"; do
  echo "Trying mirror: $mirror"

  # Get the latest version from the current mirror
  latest_version=$(get_latest_version "$mirror")
  if [ -z "$latest_version" ]; then
    echo "Failed to determine the latest version from $mirror"
    continue
  fi
  echo "Latest version on $mirror: $latest_version"

  # Construct the ISO filename (e.g., install76.iso for version 7.6)
  iso_file="install${latest_version//./}.iso"
  # Construct the full URL for the amd64 ISO file
  url="${mirror}${latest_version}/amd64/${iso_file}"
  echo "Attempting to download $url"

  # Attempt to download the ISO file
  axel "$url"
  if [ $? -eq 0 ]; then
    echo "Download successful from $mirror"
  else
    echo "Download failed from $mirror"
  fi
done

# If all mirrors fail, report an error
echo "Failed to download from all mirrors"

# Rescatux (Rescue iso)
URL="https://sourceforge.net/projects/rescatux/files/latest/download"
axel $URL

# Rescuezilla 
URL=`curl https://rescuezilla.com/download |grep iso | cut -d '"' -f2 | head -1`
axel $URL

# ShredOS iso
URL="https://sourceforge.net/projects/shredos.mirror/files/latest/download"
axel $URL

# Sparky Linux Rescue edition
rm index*
wget https://sourceforge.net/projects/sparkylinux/files/rescue/
URL=`cat index.html|grep -v sig|grep rescue.iso|grep sourceforge|cut -d '"' -f6 | head -n 1`
axel $URL

# Super Grub2 Disk Stable
rm *.zip
URL="https://sourceforge.net/projects/supergrub2/files/latest/download"
axel $URL
unp super*.zip

# System Rescue  (Rescue iso)
URL="https://sourceforge.net/projects/systemrescuecd/files/latest/download"
axel $URL

# Tails
rm index*
URL1="https://tails.net/install/download/index.en.html"
URL2=`curl $URL1 |grep amd64 |grep iso|head -1`
axel $URL2

# Ubuntu Cinnamon Remix - iso download speed is too slow...
# URL="https://sourceforge.net/projects/ubuntu-cinnamon-remix/files/latest/download"
# wget $URL

# Ultimate Boot CD (Rescue iso)
URL=`curl https://www.ultimatebootcd.com/download.html |grep iso | cut -d'"' -f2 |head -10|tail -1`
axel $URL

echo "Attempted to download $ISOTOTAL iso images"

# Cleanup
rm download*; rm index*; rm Readme*; rm Releases; rm *.xz; rm *.zip
