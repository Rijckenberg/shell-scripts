#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2025/1/27
# This bash shell script is compatible with Debian Bookworm running in Qubes OS 4 or running in podman container
# install newest version of Powershell
############## nala package manager install ##################################################
# https://github.com/volitank/nala
# https://gitlab.com/volian/nala/-/wikis/Installation

VERSIONSHORT="7.5.0"
VERSIONLONG="7.5.0-1"

sudo rm /etc/apt/sources.list.d/volian*
sudo DEBIAN_FRONTEND=noninteractive apt install debian-keyring
gpg --keyserver keyserver.ubuntu.com --recv-keys 0135B53B
gpg --armor --export 0135B53B | sudo apt-key add -
gpg --keyserver keyserver.ubuntu.com --recv-keys F4BA284D
gpg --armor --export F4BA284D | sudo apt-key add -
gpg --keyserver keyserver.ubuntu.com --recv-keys A87015F3DA22D980
gpg --armor --export A87015F3DA22D980 | sudo apt-key add -
sudo DEBIAN_FRONTEND=noninteractive apt update
###############################################################################################
# install dependencies for powershell:

sudo DEBIAN_FRONTEND=noninteractive apt install wget
sudo DEBIAN_FRONTEND=noninteractive apt upgrade
sudo DEBIAN_FRONTEND=noninteractive dpkg --configure -a
sudo DEBIAN_FRONTEND=noninteractive apt upgrade
sudo DEBIAN_FRONTEND=noninteractive dpkg --configure -a

# Download the PowerShell package file
cd /tmp
sudo rm powershell*.deb
wget https://github.com/PowerShell/PowerShell/releases/download/v$VERSIONSHORT/powershell_$VERSIONLONG.deb_amd64.deb

###################################
# Install the PowerShell package
sudo dpkg -i powershell_$VERSIONLONG.deb_amd64.deb

# Resolve missing dependencies and finish the install (if necessary)
sudo apt-get install -f

# Delete the downloaded package file
rm powershell_$VERSIONLONG.deb_amd64.deb
###############################################################################################

