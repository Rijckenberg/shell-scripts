#!/usr/bin/env bash
# Author: Mark Rijckenberg 
# Latest modification date: 2025/1/27
# Prerequisites: NixOS 24.05, Brave web browser, ACR38 AC1038-based Smart Card Reader
# Prerequisites: acsccid driver for ACR38 AC1038-based card reader
# This bash shell script is compatible with NixOS 24.05 and latest version of Brave web browser
# This script does NOT work reliably with Firefox browser, nor with the libacr38u drivers
# This script only needs to be run once, not after every reboot of the PC
# This script can also be used right BEFORE attempting to digitally sign a PDF
# okular # procedure to electronically sign PDF using Belgian eid card:
# 1) run bash shell script eid-card-reader-activation-script
# 2) go to okular::settings::configure backends::PDF::set signature backend to NSS 
# and set Certificate database to  custom value ~/.pki/nssdb
# 3) go to okular::Tools::Digitally sign

############## nala package manager install ##################################################
# https://github.com/volitank/nala
# https://gitlab.com/volian/nala/-/wikis/Installation
echo "WARNING: This script will first close all your webbrowsers to ensure a correct activation of the eid card reader"
pause

sudo DEBIAN_FRONTEND=noninteractive apt install debian-keyring
gpg --keyserver keyserver.ubuntu.com --recv-keys 0135B53B
gpg --armor --export 0135B53B | sudo apt-key add -
gpg --keyserver keyserver.ubuntu.com --recv-keys F4BA284D
gpg --armor --export F4BA284D | sudo apt-key add -
gpg --keyserver keyserver.ubuntu.com --recv-keys A87015F3DA22D980
gpg --armor --export A87015F3DA22D980 | sudo apt-key add -
# sudo wget https://collaboraoffice.com/downloads/gpg/collaboraonline-snapshot-keyring.gpg
sudo DEBIAN_FRONTEND=noninteractive apt update
###############################################################################################
sudo DEBIAN_FRONTEND=noninteractive apt purge eid-*
sudo DEBIAN_FRONTEND=noninteractive apt purge snapd
sudo DEBIAN_FRONTEND=noninteractive apt install lsb-release
sudo DEBIAN_FRONTEND=noninteractive apt install ucf
# find correct eid-archive version number at https://eid.belgium.be/en/linux-eid-software-installation
cd /tmp
sudo rm /etc/apt/sources.list.d/volian*
sudo rm eid-archive*.deb
wget https://eid.belgium.be/sites/default/files/software/eid-archive_latest.deb
sudo dpkg -i eid-archive_latest.deb
sudo DEBIAN_FRONTEND=noninteractive apt update
sudo DEBIAN_FRONTEND=noninteractive apt install cackey
sudo DEBIAN_FRONTEND=noninteractive apt install eid-mw
sudo DEBIAN_FRONTEND=noninteractive apt install eid-viewer 
sudo DEBIAN_FRONTEND=noninteractive apt install firefox-esr
rpk install google-chrome-unstable-deb
rpk remove google-chrome-stable
sudo DEBIAN_FRONTEND=noninteractive apt install gnupg-pkcs11-scd
sudo DEBIAN_FRONTEND=noninteractive apt install libacsccid1
sudo DEBIAN_FRONTEND=noninteractive apt install libccid
sudo DEBIAN_FRONTEND=noninteractive apt install libnss3-tools
sudo DEBIAN_FRONTEND=noninteractive apt install libpcsclite1 
sudo DEBIAN_FRONTEND=noninteractive apt install pcscd
sudo DEBIAN_FRONTEND=noninteractive apt install pcsc-tools
sudo DEBIAN_FRONTEND=noninteractive apt install zoxide
sudo DEBIAN_FRONTEND=noninteractive apt upgrade
sudo DEBIAN_FRONTEND=noninteractive dpkg --configure -a
sudo DEBIAN_FRONTEND=noninteractive apt upgrade
sudo DEBIAN_FRONTEND=noninteractive dpkg --configure -a

# See https://nixos.wiki/wiki/Web_eID
# kill all open browsers:
killall brave
killall chromium
killall firefox
killall google-chrome-unstable
killall google-chrome-stable
killall google-chrome
cd
eid-nssdb remove
modutil -dbdir sql:.pki/nssdb -remove "Belgium eID" -libfile /usr/lib/libbeidpkcs11.so
# crucial step to enable eid card reader:
eid-nssdb add # this step causes crashloop of steam client -> use firefox instead and avoid eid-nssdb add
modutil -dbdir sql:.pki/nssdb -add "Belgium eID" -libfile /usr/lib/libbeidpkcs11.so
eid-viewer
echo "Retest eid card reader on www.cm.be using brave web browser"
# google-chrome-stable www.cm.be
brave www.cm.be
