#!/usr/bin/env bash
# Modification date: 2024/3/15
# Author: Mark Rijckenberg
# Required diskspace: at least 10 GB
# List of container images:  https://github.com/89luca89/distrobox/blob/main/docs/compatibility.md#containers-distros

# run following xhost command to allow launching GUI application inside Arch Linux container in distrobox:
xhost local:ulysses
distrobox create --image ghcr.io/void-linux/void-glibc-full:latest
distrobox list
distrobox enter void-glibc-full-latest