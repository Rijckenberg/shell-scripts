#!/usr/bin/env bash
# Modification date: 2023/9/23
# Author: Mark Rijckenberg
# How I successfully ran this script in NixOS 23.05 in an Ubuntu distrobox container:
# Ran: sudo bash install-debian-gpt-open-interpreter.bash inside ubuntu-22-04 distrobox container running in NixOS 23.05
# Used Open Interpreter, codellama-13b-instruct.Q5_K_M.gguf, python 3.10, seaborn and yfinance API to 
# download and analyze stock price data on my local PC, without depending on an OpenAI API key. Ran correctly
# generated code in Google Colab Jupyter notebook.

# first install Nvidia container toolkit:
curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
&& curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list | \
sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list \
&& \
sudo apt-get update
sudo apt install -y nvidia-container-toolkit
sudo apt install -y python3.13-venv

sudo apt install libxcb-cursor0  
sudo apt install libxcb-icccm4 
sudo apt install libxcb-image0 
sudo apt install libxcb-keysyms1 
sudo apt install libxcb-render-util0 
sudo apt install libxcb-xinerama0 
sudo apt install libxkbcommon-x11-0
sudo apt install python3-pip
python3 -m venv openinterpreter
source openinterpreter/bin/activate

pip install --upgrade pip
pip install bauh
pip install matplotlib
pip install numpy
pip install opencv-python-headless
pip install open-interpreter
pip install pandas
pip install PyQt5 # to enable graphical visualizations
pip install seaborn
pip install yfinance==0.2.28 # pin right version to avoid issues with python 3.10
# run following command to launch code-llama (free, but less capable)
# Choose LLM model that uses around 11GB of RAM, when PC has 16 GB of RAM
sudo interpreter --local
# run following command to launch GPT4 (open interpreter) which requires OpenAPI key:
# interpreter
