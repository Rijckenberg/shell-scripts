#!/usr/bin/env bash
# Authors: Mark Rijckenberg
# Latest modification date: 2024/2/10
# This bash shell script is compatible with NixOS and Debian Bookworm running in podman container
mkdir -p ~/backup
mkdir -p ~/backup/savegames
cd ~
find . -name '*utosave*' -exec cp -r -p  --parents \{\} ~/backup/savegames/  \;
find . -name 'SaveGame*' -exec cp -r -p  --parents \{\} ~/backup/savegames/  \;
cd /media/windows
find . -name '*utosave*' -exec cp -r -p  --parents \{\} ~/backup/savegames/  \;
find . -name 'SaveGame*' -exec cp -r -p  --parents \{\} ~/backup/savegames/  \;
# https://github.com/mtkennerly/ludusavi
ludusavi &
