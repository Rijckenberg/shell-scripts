#!/usr/bin/env bash 
# Latest modification date: 2024/9/5
# Prerequisites: Any Linux distribution that does not use Home Manager (to avoid conflicts with Home Manager)
# See: https://github.com/ChrisTitusTech/linutil/blob/main/README.md
# Linutil is a distro-agnostic toolbox designed to simplify everyday Linux tasks. 
# It helps you set up applications and optimize your system for specific use cases.
# The utility is actively developed in Rust, providing performance and reliability.
curl -fsSL https://christitus.com/linux | sh
