#! /bin/sh   
# Copied from: https://pclosmag.com/Misc-Downloads/Img-resize5b.sh.txt                                                    

n=1

FILES=$(zenity --file-selection --multiple --separator=" " --filename "${HOME}/" --file-filter='Graphic Files | *.png *.PNG *.jpg *.JPG *.webp *.WEBP *.avif *.AVIF *.gif *.GIF *.bmp *.BMP *.tiff *.TIFF' --title="Select Graphic Files To Resize")
   if [ $? == 1 ]; then
      exit
   fi
read -r -a "FILES" <<< $FILES
count=${#FILES[@]}

RES=`zenity --title="Image Width" --width=150 --height=100 --entry --text="Image Resizer -- Please enter the \nimage width in pixels (e.g. 600)"`
   if [ $? == 1 ]; then
      exit
   fi

EXT=$(zenity --list --column="Select One" --title="Convert Image" --width=250 --height=250 --text="Select the image format\nto convert to:" jpg png webp avif tiff bmp gif)
   if [ $? == 1 ]; then
      exit
   fi
declare -l EXT
EXT=$EXT

QUAL=`zenity --title="Convert Image" --width=250 --height=250 --entry --text="Enter the quality level:\n\n100 = Full Quality\n75 = 75% Quality\n50 = 50% Quality\n\nAny number between\n1 and 100 accepted."`
   if [ $? == 1 ]; then
      exit
   fi
   if [[ ( $(($QUAL)) -lt 1 ) || ( $(($QUAL)) -gt 100 ) ]]; then
      zenity --title="Convert Image" --width=200 --height 100 --error --text="Exiting.\n\nNumber entry\nout of range."
      exit
   fi

DIR=`zenity --title="Destination Directory" --width=350 --entry --text="Enter the directory to save your\nresized images in.\n\nLeave the entry BLANK if you want\nthe files saved in the same directory\nas the original files.\n"`
   if [ $? == 1 ]; then
      exit
   fi
CURR_PATH=$(dirname "${FILES[0]}")
NEW_DIR=$CURR_PATH"/"$DIR
   if [ ! -d $NEW_DIR ]; then
      mkdir $NEW_DIR
   fi

if [ $EXT == "png" ];
   then
      BACKGROUND="none"
      QUALITY=$(($QUAL / 10))
   else
      BACKGROUND="white"
      QUALITY="$QUAL"
fi

REZ=$((RES-2))

for file in "${FILES[@]}"; do
    if [ ! -e $file ]; then
        continue
    fi
    
    nopath=$(basename "$file")
    name=$( echo $nopath | cut -f1 -d.)
    convert $file -units pixelsperinch -resample 72x72 -resize $REZ -quality $QUALITY -fuzz 20% -background $BACKGROUND -bordercolor black -border 1x1 $NEW_DIR/${name}_$RES-$QUALITY.$EXT
    echo $(($n * 100 / $count))
    echo "# Processing file: $file"
    let "n = n+1"

done | (zenity  --progress --title "Resizing..." --percentage=0 --auto-close --auto-kill)

exit 0
