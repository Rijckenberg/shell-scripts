#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2024/01/12
# This bash shell script is compatible with Debian Bookworm running in podman container
# compile newest version of libreboot ROM
# Only run this script on HP Elitebook 820 G2 laptop!!!!
# Procedure 1:  https://libreboot.org/docs/build/
# Procedure 2: https://libreboot.org/docs/hardware/hp820g2.html
# https://doc.coreboot.org/mainboard/hp/elitebook_820_g2.html?highlight=elitebook

###### WARNING: Only run this script on HP Elitebook 820 G2 laptop!!!! #################
###### WARNING: Flash from bin/, NOT elf/  #############################################
###### WARNING: elf/ coreboot ROMs do not contain payloads and will brick your PC ######

echo "Run flashrom commands on NixOS host, not in Debian container"
# https://wiki.archlinux.org/title/Flashing_BIOS_from_Linux
# get info about currently running BIOS ROM:
# sudo flashrom --programmer internal
# sudo flashrom --programmer internal -L | grep 820

# install dependencies for compiling libreboot:
sudo apt update
sudo apt install git libflashrom-dev libflashrom1 nano python-is-python3
cd
# libreboot’s build system is named lbmk, short for LibreBoot MaKe:
git clone https://codeberg.org/libreboot/lbmk
cd lbmk
# Change the name and email address to whatever you want, when doing this.
# git config --global user.name "John Doe"
# git config --global user.email johndoe@example.com

# Check: config/dependencies/ for list of supported distros.
# install dependencies for compiling libreboot:
sudo ./build dependencies debian
# get a list of supported build targets:
./build roms list
./build roms list | grep 820
# Build ROM for HP Elitebook 820 G2:
# NOTE: The actual flash is 16MB, but you must flash only the first 12MB of it.
# The ROM images provided by Libreboot are 12MB.
date > /tmp/1
./build roms hp820g2_12mb
date > /tmp/2
echo 'build start time: ' ; cat /tmp/1
echo 'build end time: '; cat /tmp/2
cd ~/lbmk/bin/hp820g2_12mb
# https://www.flashrom.org/classic_cli_manpage.html
# Create a dummy 16MB ROM like so:
dd if=/dev/zero of=new.bin bs=16M count=1
# Then insert your 12MB Libreboot ROM image into the dummy file:
dd if=seabios_withgrub_hp820g2_12mb_libgfxinit_txtmode_frazerty.rom of=new.bin bs=12M count=1 conv=notrunc
# inspect ROM contents for errors like "CBFS ERROR: CBFS mcache is not terminated!"
nano new.bin
echo "Flash from bin/, NOT elf/"
echo "Flash each region individually:"
echo "Run flashrom commands on NixOS host, not in Debian container"
echo "Please make a backup of your flash chip before writing to it."
# sudo flashrom -p internal --ifd -i gbe -w new.bin --noverify-all
# sudo flashrom -p internal --ifd -i bios -w new.bin --noverify-all
# sudo flashrom -p internal --ifd -i me -w new.bin --noverify-all
# sudo flashrom -p internal --ifd -i ifd -w new.bin --noverify-all

###### WARNING: Only run this script on HP Elitebook 820 G2 laptop!!!! #################
###### WARNING: Flash from bin/, NOT elf/  #############################################
###### WARNING: elf/ coreboot ROMs do not contain payloads and will brick your PC ######
