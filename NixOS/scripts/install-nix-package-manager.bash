#!/usr/bin/env bash
# Modification date: 2024/8/23
# Author: Mark Rijckenberg
# Based on https://lix.systems/install/
# How to install nix package manager in any GNU/Linux distro
sudo install -d -m755 -o $(id -u) -g $(id -g) /nix
sudo install -d -m755 -o $(id -u) -g $(id -g) /nix/store
# curl -L https://nixos.org/nix/install | sh
curl -sSf -L https://install.lix.systems/lix | sh -s -- install
source $HOME/.nix-profile/etc/profile.d/nix.sh
sudo --preserve-env=PATH nix run \
     --experimental-features "nix-command flakes" \
     --extra-substituters https://cache.lix.systems --extra-trusted-public-keys "cache.lix.systems:aBnZUw8zA7H35Cz2RyKFVs3H4PlGTLawyY5KRbvJR8o=" \
     'git+https://git.lix.systems/lix-project/lix?ref=refs/tags/2.91.0' -- \
     upgrade-nix \
     --extra-substituters https://cache.lix.systems --extra-trusted-public-keys "cache.lix.systems:aBnZUw8zA7H35Cz2RyKFVs3H4PlGTLawyY5KRbvJR8o="
nix --version
