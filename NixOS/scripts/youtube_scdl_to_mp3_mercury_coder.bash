#!/bin/bash
# Author: Mercury Coder at https://chat.inceptionlabs.ai/
# Mercury Coder is really fast, wow!!

# Function to detect the operating system
detect_os() {
    if command -v lsb_release &> /dev/null; then
        echo $(lsb_release -si)
    elif [ -f /etc/os-release ]; then
        source /etc/os-release
        echo $ID
    else
        echo "Unknown OS"
        exit 1
    fi
}

# Function to install prerequisites based on the detected OS
install_prerequisites() {
    local os=$1

    case $os in
        Ubuntu|Debian)
            sudo apt update
            sudo apt install -y yt-dlp mp3gain detox
            ;;
        Fedora)
            sudo dnf install -y yt-dlp mp3gain detox
            ;;
        Arch|ArchLinux)
            sudo pacman -Syu
            sudo pacman -S --noconfirm yt-dlp mp3gain detox
            ;;
        *)
            echo "Unsupported OS: $os"
            exit 1
            ;;
    esac
}

# Function to download and process playlists
download_and_process_playlist() {
    local url=$1
    local output_dir=$2

    # Create output directory if it doesn't exist
    mkdir -p "$output_dir"

    # Download playlist
    echo "Downloading playlist from $url..."
    yt-dlp -x --audio-format mp3 --audio-quality 0 -o "$output_dir/%(title)s.mp3" "$url"

    # Normalize volume
    echo "Normalizing volume in MP3 files..."
    find "$output_dir" -type f -name "*.mp3" -exec mp3gain -a {} +

    # Clean up filenames
    echo "Cleaning up filenames..."
    find "$output_dir" -type f -name "*.mp3" -exec detox {} +
}

# Main script execution
os=$(detect_os)
echo "Detected OS: $os"

# Install prerequisites
install_prerequisites "$os"

# Prompt user for playlist URL
read -p "Enter the URL of the playlist: " playlist_url

# Prompt user for output directory
read -p "Enter the output directory (default: $HOME/Music): " output_dir

# Use default output directory if not provided
if [ -z "$output_dir" ]; then
    output_dir="$HOME/Music"
fi

# Process the playlist
download_and_process_playlist "$playlist_url" "$output_dir"

echo "Playlist has been downloaded and processed."
