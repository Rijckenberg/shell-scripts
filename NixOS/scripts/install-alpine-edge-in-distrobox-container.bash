#!/usr/bin/env bash
# Creation date: 2024/4/10
# Modification date: 2025/1/27
# Author: Mark Rijckenberg
# Required diskspace: at least 10 GB
# List of available container images:  https://github.com/89luca89/distrobox/blob/main/docs/compatibility.md#containers-distros
# Installed container distro:  DISTRIB_DESCRIPTION="Alpine (edge)"
# Alpine Linux is one of the safest GNU/Linux distributions, with protection against 0-day exploits
# Alpine Linux was designed with security in mind. All userland binaries are compiled as Position Independent Executables 
# (PIE) with stack smashing protection. These proactive security features prevent exploitation of entire classes of zero-day
# and other vulnerabilities.
# See https://github.com/redoracle/nixos
# See https://linuxiac.com/alpine-apk-command-guide/
# Use Alpine Linux container for secure web banking operations
# run following xhost command to allow launching collaboraoffice GUI inside alpine-edge container in distrobox:
#      xhost local:ulysses
# ubuntu:latest tag points to the "latest LTS", since that is the version recommended for general use:
# distrobox create --image docker.io/library/ubuntu:latest
# distrobox create --image quay.io/toolbx-images/ubuntu-toolbox:latest
# ubuntu:rolling tag points to the latest release (regardless of LTS status):
# https://hub.docker.com/_/ubuntu/tags?page=1&name=rolling
# alpine:edge points to the latest Alpine release:
# https://docker.io/library/alpine:edge
# when switching from latest kernel to cachyos gaming kernel or vice versa,
# you need to delete the old distrobox container and create a replacement container
trivy image ghcr.io/linuxcontainers/alpine:latest
distrobox create --image docker.io/library/alpine:edge
echo "alpine:edge contains newer version of Firefox compared to"
echo "alpine:latest => So avoid alpine:latest images"
echo "This is important to ensure Firefox version on host OS"
echo "and Firefox version on container OS do not drift too far apart."
distrobox list
echo "If you cannot enter the alpine-edge distrobox container after a NixOS upgrade, please run following command to fix the issue:"
echo "distrobox create -c alpine-edge tmp && podman rm alpine-edge; podman rename tmp alpine-edge; distrobox enter alpine-edge"
echo "Use alpine-edge in container for secure banking using firefox browser"
distrobox enter alpine-edge