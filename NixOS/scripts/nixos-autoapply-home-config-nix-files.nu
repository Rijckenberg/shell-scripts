# Written in nushell
# First creation date: 2023/7/5
# Modification date: 2023/8/3
# Author: Mark Rijckenberg

# Find all NixOS packages here: https://nixos.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# NixOS Foundation: https://www.linkedin.com/company/nixos-foundation/posts
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# https://www.nushell.sh/book/
# Home Manager option search: https://mipmip.github.io/home-manager-option-search/?query=

# define currently logged in user:
let USER = (echo $nu.home-path | cut -d/  -f3)
print $USER

# Install home manager in NixOS as stand-alone install
nix-channel --add https://github.com/nix-community/home-manager/archive/release-23.05.tar.gz home-manager
nix-channel --update
nix-shell '<home-manager>' -A install

# Then replace all occurrences of "ulysses" with the right username in all .nix files
# sed -i 's/ulysses/`echo $USER`/g' *.nix

# User specific configurations:

# Copy all .desktop files to ~/.config/autostart 
# to autostart them after login, if username is ulysses:
if $USER == "ulysses" {
  print "\n\nulysses account has been detected! Getting newest version of configuration.nix and home.nix from my own Gitlab repository"
  # Get newest version of configuration.nix and home.nix from my own Gitlab repository:
  git pull
  print "\n\nulysses account has been detected! Copying all .desktop files to right spot"
  cp *.desktop ~/.config/autostart
  # install newest version of home.nix configuration:
  sudo rm ~/.config/home-manager/home.nix
  cp home.nix ~/.config/home-manager/home.nix
  # apply the home manager configuration in home.nix to the system:
  home-manager -f ~/.config/home-manager/home.nix switch
  # install newest version of configuration.nix
  sudo cp -R *  /etc/nixos/
  # set default wallpaper/background image on desktop:
  ln -s ~/.background-image /etc/nixos/common/wallpaper.jpg
  # apply the configuration in configuration.nix to the system:
  date now | save -f /tmp/1
  sudo time nixos-rebuild switch --upgrade-all
  date now | save -f /tmp/2
  print "Start time"
  cat /tmp/1
  print "\nEnd time"
  cat /tmp/2
}

if $USER == "didi" {
    # Only copy maestral.desktop (Dropbox client) file to ~/.config/autostart 
    # to autostart them after login, if username is didi:
    print "\n\ndidi account has been detected! Copying maestral.desktop file to right spot"
    cp maestral.desktop ~/.config/autostart
    # install newest version of configuration.nix
    sudo cp -R *  /etc/nixos/
    # set default wallpaper/background image on desktop:
    ln -s ~/.background-image /etc/nixos/common/wallpaper.jpg
    # apply the configuration in configuration.nix to the system:
    date now | save -f /tmp/1
    sudo time nixos-rebuild switch --upgrade-all
    date now | save -f /tmp/2
    print "Start time"
    cat /tmp/1
    print "\nEnd time"
    cat /tmp/2
    cargo install hdn
}

if $USER == "gerard" {
    # install hdn - hdn is a small utility that provides a poetry/cargo-like
    # interface for quickly adding and removing your home-manager packages.
    # It’s mainly geared towards people like me who use home-manager only to 
    # install packages, and not manage dotfiles.
    print "\n\ngerard account has been detected! Installing hdn and astronomy packages"
    # install newest version of configuration.nix
    sudo cp -R *  /etc/nixos/
    # set default wallpaper/background image on desktop:
    ln -s ~/.background-image /etc/nixos/common/wallpaper.jpg
    # apply the configuration in configuration.nix to the system:
    date now | save -f /tmp/1
    sudo time nixos-rebuild switch --upgrade-all
    date now | save -f /tmp/2
    print "Start time"
    cat /tmp/1
    print "\nEnd time"
    cat /tmp/2
    cargo install hdn
    ~/.cargo/bin/hdn add pkgs.celestia pkgs.kstars pkgs.stellarium
}

if $USER == "rijckenbergc" {
    print "\n\nrijckenbergc account has been detected!"
     # install newest version of configuration.nix
    sudo cp -R *  /etc/nixos/
    # apply the configuration in configuration.nix to the system:
    date now | save -f /tmp/1
    sudo time nixos-rebuild switch --upgrade-all
    date now | save -f /tmp/2
    print "Start time"
    cat /tmp/1
    print "\nEnd time"
    cat /tmp/2
    cargo install hdn
}

