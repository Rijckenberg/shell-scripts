#!/bin/bash
# Apple IIe emulator
# last modification date: 2025/1/31
# install instructions: https://github.com/linappleii/linapple/blob/master/INSTALL.md
cd
git clone  https://git.ocjtech.us/jeff/nixos-linapple
cd nixos-linapple/
nix build .#linapple
~/nixos-linapple/result/bin/linapple --help

# Press F3 in linapple and navigate to ~/linapple/disks and select the game to load
# Then press CTRL-SHIFT-F2 to restart the linapple emulator and load the game