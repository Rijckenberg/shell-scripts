#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2023/11/30
# This bash shell script is compatible with Debian Bookworm running in Qubes OS 4 or running in podman container
# install newest version of megacubo IPTV player
############## nala package manager install ##################################################
# https://github.com/volitank/nala
# https://gitlab.com/volian/nala/-/wikis/Installation
sudo rm /etc/apt/sources.list.d/volian*
sudo DEBIAN_FRONTEND=noninteractive apt install debian-keyring
gpg --keyserver keyserver.ubuntu.com --recv-keys 0135B53B
gpg --armor --export 0135B53B | sudo apt-key add -
gpg --keyserver keyserver.ubuntu.com --recv-keys F4BA284D
gpg --armor --export F4BA284D | sudo apt-key add -
gpg --keyserver keyserver.ubuntu.com --recv-keys A87015F3DA22D980
gpg --armor --export A87015F3DA22D980 | sudo apt-key add -
cd /usr/share/keyrings
sudo DEBIAN_FRONTEND=noninteractive apt update
###############################################################################################
# install dependencies for megacubo IPTV player application:
sudo DEBIAN_FRONTEND=noninteractive apt install wget libxss* pipewire wireguard
sudo DEBIAN_FRONTEND=noninteractive apt upgrade
sudo DEBIAN_FRONTEND=noninteractive dpkg --configure -a
sudo DEBIAN_FRONTEND=noninteractive apt upgrade
sudo DEBIAN_FRONTEND=noninteractive dpkg --configure -a
###############################################################################################
wget -qO- https://megacubo.tv/install.sh | bash
/opt/megacubo/megacubo
