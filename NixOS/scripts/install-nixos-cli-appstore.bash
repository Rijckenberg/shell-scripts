#!/usr/bin/env bash
# Modification date: 2023/9/12
# Author: Youtuber "Gosh-Its-Arch"
# Based on https://www.youtube.com/watch?v=DR2cjDI1n68
# How to install NixOS cli appstore shell script:
nix-shell -p git
git clone https://gitlab.com/gosh-its-arch-linux/nixos.git
cd nixos
cd scripts
chmod +x *.sh
