#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Creation date: 2023/7/22
# Last modification date: 2023/7/22
# add following lines in configuration.nix in NixOS:
# virtualisation.docker.enable = true;
# virtualisation.docker.enableOnBoot = true;

sudo docker volume create i2psnark_state
sudo docker run --restart=unless-stopped -d --name=i2psnark \
    -v /my/torrents/path:/snark/downloads \
    -v i2psnark_state:/snark/config \
    -e I2CP_HOST="172.17.0.1" \
    ypopovych/i2psnark
