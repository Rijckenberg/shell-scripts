# Authors: Jon Seager, Mark Rijckenberg and mistralai/Mixtral-8x7B-Instruct-v0.1 on HuggingChat
# Based on https://github.com/jnsgruk/nixos-config/blob/main/scripts/install-with-disko
# Thank you very much, Jon Seager, for making the disko script
# Last modification date: 2024/8/16

# WARNING: only run this script in a NixOS 23.11 Minimal LiveUSB session
# on the target machine containing no operating system
# !!!!!!!!!!!! This script will overwrite all the contents on the target machine  !!!!!!!!!!!!!
# Script should NOT be used in dual boot scenarios (Windows + GNU/Linux)

# Prerequisites: wired Internet connection and boot into 
# https://channels.nixos.org/nixos-23.11/latest-nixos-minimal-x86_64-linux.iso
# on target machine to minimize RAM requirements
# Prerequisites: target machine must have at least 8 GB of RAM
# Prerequisites: nixos-install command must download less than 9500 MB of nix packages
# to avoid out-of-memory issues - around 9500 MB really is the hard limit!

#!/usr/bin/env bash

set -euo pipefail

# register start time
rm -f /tmp/1
rm -f /tmp/2
date > /tmp/1

TARGET_HOST="${1:-}"

echo "Downloading flake.nix file to use for deployment on PC"
rm -rf shell-scripts
git clone https://gitlab.com/Rijckenberg/shell-scripts
cd shell-scripts/NixOS
rm -f flake.lock

TARGET_HOST_CANDIDATES=`ls machine/ | grep -v gitkeep | awk '{print $NF}' | sort `

# LiveUSB session check - check if output of following command is empty:
if [[ -z "$(mount |grep squashfs)" ]]; then
  echo "ERROR! This is NOT a LiveUSB session! This script should only be"
  echo "run during a NixOS minimal LiveUSB session!"
  exit 1
fi

# Host operating system check
if [[ "$(cat /etc/lsb-release | grep DISTRIB_ID)" != "DISTRIB_ID=nixos" ]]; then
  echo "ERROR! Wrong operating system! This script should only be run during NixOS minimal LiveUSB session"
  exit 1
fi

# RAM size check:
# written with help of mistralai/Mixtral-8x7B-Instruct-v0.1 on HuggingChat
ram=$(free --giga | awk '/^Mem/{print $2}')
if [[ "$ram" -ge "8" ]]; then
    echo "Your PC has sufficient memory (at least 8GB), continuing with execution..."
    # Add commands here that you want to execute if the system meets the requirement
else
    echo "Your PC does not have sufficient memory (less than 8GB)."
    echo "It is highly recommended to use an 8GB swap space while installing NixOS for this machine" 
    echo "Exiting now."
    exit 1
fi

# check if script is being run by root:
if [ "$(id -u)" -eq 0 ]; then
  echo "ERROR! $(basename "${0}") should be run as a regular user"
  exit 1
fi

# check if output of following command is empty:
if [[ -z "$TARGET_HOST" ]]; then
    echo "ERROR! $(basename "${0}") requires a hostname as the first argument"
    echo "Choose one of the following target hosts:" 
    echo "$TARGET_HOST_CANDIDATES"
    exit 1
fi

# check if following disko configuration file cannot be found:
if [ ! -e "machine/${TARGET_HOST}/disko-config.nix" ]; then
  echo "ERROR! $(basename "${0}") could not find the required machine/${TARGET_HOST}/disko-config.nix"
  echo "Choose one of the following target hosts:" 
  echo "$TARGET_HOST_CANDIDATES"
  exit 1
fi

echo "6 system checks are all OK. System meets minimum specification requirements for running this script"
echo
echo "WARNING! The disks in ${TARGET_HOST} are about to get wiped"
echo "         NixOS will be re-installed"
echo "         This is a destructive operation"
echo "This script should only be run during NixOS minimal LiveUSB session"
echo "And nixos-install command must download less than 9500 MB of nix packages"
echo "to avoid out-of-memory issues for a PC with 8 GB of RAM"
echo
echo "This script should NOT be used in dual boot scenarios (Windows + GNU/Linux)"
echo
echo "3 choices:"
echo "d) For destructive partitioning using disko, press d and then <ENTER>"
echo "n) To install nix packages using nixos-install command, press n and then <ENTER>"
echo "b) For destructive partitioning and then nix package install using nixos-install, press b and then <ENTER>"
read -p "Please choose: [d/n/b]" -n 1 -r
echo
if [[ $REPLY =~ ^[Dd]$ ]]; then
    sudo true

    sudo time nix run github:nix-community/disko \
        --extra-experimental-features "nix-command flakes" \
        --no-write-lock-file \
        -- \
        --mode zap_create_mount \
        "machine/${TARGET_HOST}/disko-config.nix"

date > /tmp/2; echo "start time:" ; cat /tmp/1; echo "end time:"; cat /tmp/2

fi

if [[ $REPLY =~ ^[Nn]$ ]]; then
    sudo true

    sudo time nixos-install --flake ".#${TARGET_HOST}"

    # register end time
    date > /tmp/2; echo "start time:" ; cat /tmp/1; echo "end time:"; cat /tmp/2
    
    echo "nixos-enter command runs a command in a NixOS chroot environment, "
    echo "that is, in a filesystem hierarchy previously prepared using nixos-install."
    echo "Please set new password for user on target machine"
    sudo nixos-enter

fi

if [[ $REPLY =~ ^[Bb]$ ]]; then
    sudo true

    sudo time nix run github:nix-community/disko \
        --extra-experimental-features "nix-command flakes" \
        --no-write-lock-file \
        -- \
        --mode zap_create_mount \
        "machine/${TARGET_HOST}/disko-config.nix"

    sudo time nixos-install --flake ".#${TARGET_HOST}"

    # register end time
    date > /tmp/2; echo "start time:" ; cat /tmp/1; echo "end time:"; cat /tmp/2

    echo "nixos-enter command runs a command in a NixOS chroot environment, "
    echo "that is, in a filesystem hierarchy previously prepared using nixos-install."
    echo "Please set new password for user on target machine"
    sudo nixos-enter

fi


