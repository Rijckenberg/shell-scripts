#!/usr/bin/env bash
# Modification date: 2023/9/7
# Author: Mark Rijckenberg
# Required diskspace: at least 10 GB
# List of container images:  https://github.com/89luca89/distrobox/blob/main/docs/compatibility.md#containers-distros

# run following xhost command to allow launching GUI application inside Arch Linux container in distrobox:
xhost local:ulysses
distrobox create --image quay.io/toolbx/arch-toolbox:latest
distrobox list
distrobox enter archlinux-toolbox
