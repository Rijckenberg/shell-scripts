#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2024/01/26
# This bash shell script is compatible with Debian Bookworm running in podman container
cd
rm Easy-Diffusion-Linux.zip
wget https://github.com/cmdr2/stable-diffusion-ui/releases/latest/download/Easy-Diffusion-Linux.zip
unzip Easy-Diffusion-Linux.zip
rm Easy-Diffusion-Linux.zip
cd easy-diffusion/
bash start.sh
