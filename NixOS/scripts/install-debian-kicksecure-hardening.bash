#!/usr/bin/env bash
# Author: Mark Rijckenberg 
# Latest modification date: 2024/6/18
# Prerequisites: Debian 12 (bookworm) or newer

wget https://www.kicksecure.com/keys/derivative.asc
sudo cp ~/derivative.asc /usr/share/keyrings/derivative.asc
echo "deb [signed-by=/usr/share/keyrings/derivative.asc] https://deb.kicksecure.com bookworm main contrib non-free" | sudo tee /etc/apt/sources.list.d/derivative.list
sudo userdel -r debian-tor
sudo adduser ulysses console
sudo adduser root console
sudo apt-get update
sudo apt-get remove kicksecure-base-files kicksecure-dependencies-cli systemcheck tirdad
sudo apt-get install kicksecure-base-files kicksecure-dependencies-cli systemcheck tirdad
sudo apt-get install lynis
sudo apt install -f
sudo dpkg --configure -a