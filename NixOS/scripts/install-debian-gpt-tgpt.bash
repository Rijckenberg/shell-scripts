#!/usr/bin/env bash
# Modification date: 2023/10/26
# Author: Mark Rijckenberg
# How to install tgpt in Ubuntu container
# See NixOS/scripts/install-ubuntu-gpt-ollama.bash
curl -sSL https://raw.githubusercontent.com/aandrew-me/tgpt/main/install | bash -s /usr/local/bin