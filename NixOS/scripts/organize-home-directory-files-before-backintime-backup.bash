#!/usr/bin/env bash
# backup duration: around 2h20min minutes on my SSD and for my files on ulysses-desktop

echo "Copy this script to ~ directory (home directory) and then run from ~ path only"

##################################################
# 1) Back up various file types in home directory
# Create the backup folder in home directory, if it doesn't exist
  backup_folder="backup"
################################################
  if [ ! -d "$backup_folder" ]; then
      mkdir -p "$backup_folder"
  fi

# Function to copy files to their respective subfolders
copy_files() {
  local source_file="$1"
  local target_folder="$2"
  local filename="$(basename "$source_file")"

  # Get the extension of the file in lowercase
  local extension="${filename##*.}"
  local lowercase_extension="${extension,,}"
  
  # Replace the extension with the lowercase version
  local new_filename="${filename%.*}.$lowercase_extension"

  case "$lowercase_extension" in
      apk|apkm)
      target_folder="androidtv"
      ;;
      # bz2|gz|xz
      rar|7z|zip|zipx)
      target_folder="archives"
      # unp "$source_file"  # Causes script to crash too often
      ;;
      aac|AAC|aif|AIF|aiff|AIFF|alac|ALAC|amr|AMR|flac|FLAC|mid|MID|midi|MIDI|mp3|MP3|ogg|OGG|pcm|PCM|vorbis|VORBIS|wav|WAV|wma|WMA)
      target_folder="audio"
      ;;
      # https://icalendar.org/
      ical|icalendar|ics|ifb)
      target_folder="calendars"
      ;;
      # db|mdb
      accdb|dbf|sql)
      target_folder="databases"
      ;;
      ipynb|py|r)
      target_folder="datascience"
      ;;
      cer|crt|export|m3u|m3u8)
      target_folder="deviceconfig"
      ;;
      azw|azw3|bat|cfg|doc|docx|epub|kfx|mobi|odf|odg|odp|ods|odt|pdf|rtf|sh|typ|txt)
      target_folder="documents"
      ;;
      # msg|vcf
      email|eml|emlx|mbox|oft|ost|pst)
      target_folder="email"
      ;;
      image)
      target_folder="firmware"
      ;;
      dsg|DSG|kpf|KPF|pk3|PK3|pk4|PK4|pk7|PK7|pke|PKE|wad|WAD)
      target_folder="gzdoom"
      ;;
      # Following file extensions can be booted via Ventoy USB (SSD) stick:
      # efi|img
      iso|vhd|vhdx|vtoy|wim)
      target_folder="installers"
      ;;
      rss|opml)
      target_folder="newsreaders"
      ;;
      jpg|JPG|jpeg|JPEG|heic|HEIC|png|PNG|gif|GIF|bmp|BMP|tiff|TIFF|tif|TIF|eps|EPS|ai|AI|psd|PSD|svg|SVG)
      target_folder="photos"
      ;;
      ppt|pptx)
      target_folder="presentations"
      ;;
      ps1)
      target_folder="programming"
      ;;
      xls|xlsx|csv)
      target_folder="spreadsheets"
      ;;
      avi|AVI|flv|FLV|mkv|MKV|mp4|MP4|mpg|MPG|mpeg|MPEG|mov|MOV|ogv|OGV|qt|QT|swf|SWF|webm|WEBM|wmv|WMV)
      target_folder="video"
      ;;
      # html|htm|css|js|php|asp|aspx|jsp|xml|json)
      # target_folder="webdesign"
      # ;;
    *)
      # If the extension is not recognized, skip moving the file
      return
      ;;
  esac

  # Create the target folder if it doesn't exist
  if [ ! -d "$backup_folder/$target_folder" ]; then
    mkdir -p "$backup_folder/$target_folder"
  fi

  # copy the file to the target folder
  rsync -a --times --atimes "$source_file" "$backup_folder/$target_folder/$new_filename"
}

# Recursively find files and copy them to appropriate subfolders
find . -type f -not -path '*/.*' | while read -r file; do
  copy_files "$file"
done

echo "File organization complete."

###############################################################################################################################
# 2) Backup browser bookmarks
# Function to copy bookmarks from browser to backup folder
copy_bookmarks() {
    local browser_name="$1"
    local bookmarks_file="$2"
    local backup_folder="backup"

    if [ -f "$bookmarks_file" ]; then
        echo "Copying bookmarks from $browser_name..."
          # Create the bookmarks subfolder if it doesn't exist
           if [ ! -d "$backup_folder/${browser_name}_bookmarks" ]; then
           mkdir -p "$backup_folder/${browser_name}_bookmarks"
           fi
        cp -p "$bookmarks_file" "$backup_folder/${browser_name}_bookmarks"
        echo "Bookmarks from $browser_name copied to $backup_folder/${browser_name}_bookmarks"
    else
        echo "Unable to find bookmarks file for $browser_name"
    fi
}

# Top 10 web browsers and their bookmark locations
browsers=(
    "BraveBrowser:$HOME/.config/BraveSoftware/Brave-Browser/Default/Bookmarks"
    "Chromium:$HOME/.config/chromium/Default/Bookmarks"
    "GoogleChrome:$HOME/.config/google-chrome/Default/Bookmarks"
    "LibreWolf:$HOME/.librewolf/*/places.sqlite"
    "MicrosoftEdge:$HOME/.config/microsoft-edge/Default/Bookmarks"
    "MozillaFirefox:$HOME/.mozilla/firefox/*.default/places.sqlite"
    "Opera:$HOME/.config/opera/Bookmarks"
    "PaleMoon:$HOME/.moonchild productions/pale moon/bookmarks.html"
    "Thorium:$HOME/.config/thorium/Default/Bookmarks"
    "Vivaldi:$HOME/.config/vivaldi/Default/Bookmarks"
    "Waterfox:$HOME/.waterfox/*/places.sqlite"
    # Add more browsers and their bookmark locations here
)

# Loop through each browser and copy bookmarks to the backup folder
for browser_info in "${browsers[@]}"; do
    IFS=":" read -r browser_name bookmarks_file <<< "$browser_info"
    copy_bookmarks "$browser_name" "$bookmarks_file"
done

echo "All bookmarks have been exported and stored in the backup folder: $backup_folder"

echo "Use backintime application to perform backup to USB stick / SSD stick"

###########################################################################################
# 3) Backup contents of ~/.config :
# Check if the ~/.config folder exists
if [ ! -d "$HOME/.config" ]; then
    echo "Error: The ~/.config folder does not exist."
    exit 1
fi

# Archive filename and path
archive_filename="config_backup_$(date +%Y%m%d%H%M%S).zip"
archive_path="$HOME/$backup_folder/$archive_filename"

# Navigate to the user's home directory
cd "$HOME" || exit 1

# Compress the ~/.config folder to a zip archive
zip -r "$archive_path" "$HOME/.config"

# Check if zip command executed successfully
if [ $? -eq 0 ]; then
    echo "Compression completed successfully. Archive saved to: $archive_path"
else
    echo "Compression failed."
fi

# backup hypnotix configuration:
  if [ ! -d "$backup_folder/hypnotix" ]; then
      mkdir -p "$backup_folder/hypnotix"
  fi
  cp -R ~/.cache/hypnotix/*  "$backup_folder/hypnotix"

