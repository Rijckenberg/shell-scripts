#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2025/1/26
# This bash shell script is compatible with Rhino Linux running in podman container
# Required free disk space for new install: at least 3.5 GB free disk space in / (root) directory
# Required free disk space for software updates: at least 2.4 GB free disk space in / (root) directory
# install newest version of Collabora Office 

distrobox-export --delete -a collaboraoffice
distrobox-export --delete -a collaboraoffice
distrobox-export --delete -a collaboraoffice
distrobox-export --delete -a collaboraoffice
distrobox-export --delete -a collaboraoffice
distrobox-export --delete -a collaboraoffice
distrobox-export --delete -a collaboraoffice
distrobox-export --delete -a collaboraoffice

distrobox-export --delete -a collaboraoffice-calc
distrobox-export --delete -a collaboraoffice-draw
distrobox-export --delete -a collaboraoffice-impress
distrobox-export --delete -a collaboraoffice-math
distrobox-export --delete -a collaboraoffice-writer

sudo apt install zoxide
sudo rm /etc/apt/sources.list.d/collaboraoffice.sources*
sudo rm /etc/apt/trusted.gpg.d/collaboraonline-snapshot-keyring*
sudo DEBIAN_FRONTEND=noninteractive apt install debian-keyring
cd /etc/apt/trusted.gpg.d
sudo wget https://collaboraoffice.com/downloads/gpg/collaboraonline-snapshot-keyring.gpg
sudo tee /etc/apt/sources.list.d/collaboraoffice.sources > /dev/null << EOF 
Types: deb
URIs: https://www.collaboraoffice.com/downloads/Collabora-Office-24-Snapshot/Linux/apt
Suites: ./
Signed-By: /etc/apt/trusted.gpg.d/collaboraonline-snapshot-keyring.gpg
EOF

sudo DEBIAN_FRONTEND=noninteractive apt update
###############################################################################################
sudo DEBIAN_FRONTEND=noninteractive apt purge collabora*
sudo DEBIAN_FRONTEND=noninteractive apt install collaboraoffice-desktop default-jre libcairo2 libxinerama-dev libxinerama1 wget
sudo DEBIAN_FRONTEND=noninteractive apt upgrade
sudo DEBIAN_FRONTEND=noninteractive dpkg --configure -a
sudo DEBIAN_FRONTEND=noninteractive apt upgrade
sudo DEBIAN_FRONTEND=noninteractive dpkg --configure -a
echo "Run  distrobox-export --app collaboraoffice  inside debian-latest container in distrobox after installing collaboraoffice"
distrobox-export --app collaboraoffice-calc
distrobox-export --app collaboraoffice-draw
distrobox-export --app collaboraoffice-impress
distrobox-export --app collaboraoffice-math
distrobox-export --app collaboraoffice-writer
distrobox-export --app collaboraoffice
