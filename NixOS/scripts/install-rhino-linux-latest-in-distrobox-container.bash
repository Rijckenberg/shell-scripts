#!/usr/bin/env bash
# Creation date: 2025/1/26
# Modification date: 2025/1/26
# Author: Mark Rijckenberg
# Required diskspace: at least 10 GB
# List of available container images:  https://wiki.rhinolinux.org/dev/docker/rhino-docker
# Installed container distro:  DISTRIB_DESCRIPTION="Rhino-Linux (latest)"
# Rhino Linux has a much more powerful meta-package manager than Ubuntu or Linux Mint

# https://rhinolinux.org/statement
# https://wiki.rhinolinux.org/
# https://wiki.rhinolinux.org/user/switch

# run following xhost command to allow launching collaboraoffice GUI inside rhino-linux-latest container in distrobox:
#    xhost local:ulysses

# when switching from latest kernel to cachyos gaming kernel or vice versa,
# you need to delete the old distrobox container and create a replacement container
distrobox create --image ghcr.io/rhino-linux/docker:latest --name rhino-linux-latest
distrobox list
echo "If you cannot enter the rhino-linux-latest distrobox container after a NixOS upgrade, please run following command to fix the issue:"
echo "distrobox create -c rhino-linux-latest tmp && podman rm rhino-linux-latest; podman rename tmp rhino-linux-latest; distrobox enter rhino-linux-latest"
echo "Run  distrobox-export --app collaboraoffice-writer inside rhino-linux-latest container in distrobox after installing collaboraoffice"
distrobox enter rhino-linux-latest