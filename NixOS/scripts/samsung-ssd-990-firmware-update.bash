#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2024/2/27
# Prerequisites: NixOS 24.05 and Samsung SSD 990 PRO
# This bash shell script is compatible with NixOS 24.05
# Get the URL pointing to the newest firmware .iso file for Samsung SSD 990 PRO here:
# https://semiconductor.samsung.com/consumer-storage/support/tools/
sudo nvme list
cd $(mktemp -d)
rm -rf *
wget https://semiconductor.samsung.com/consumer-storage/support/tools/
URL=`cat index.html |grep SSD_990_PRO |grep software | cut -d"\"" -f2`
wget $URL
mkdir ./iso
sudo mount -o loop Samsung_SSD_990_PRO_*.iso ./iso
gzip -dc iso/initrd | cpio -div --no-absolute-filenames
cd root/fumagician
sudo ./fumagician
sudo nvme list
