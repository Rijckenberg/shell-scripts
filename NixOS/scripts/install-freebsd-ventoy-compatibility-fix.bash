#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2024/04/8
# See https://github.com/ventoy/Ventoy/issues/2636
# Get binaries from https://people.freebsd.org/~emaste/Ventoy/
cd /tmp
git clone https://github.com/ventoy/Ventoy/
cd ./Ventoy/Unix/ventoy_unix_src/FreeBSD/geom_ventoy_src
cp -a 13.x 14.x
cd 14.x/sys/modules/geom/geom_ventoy
mkdir -p geom/ventoy
cp ../../../geom/ventoy/* geom/ventoy
cp ../../../geom/ventoy/* .
export CC=$(which clang)
make
xz geom_ventoy.ko
#################################################################
mkdir /tmp/fix
cd /tmp/fix
axel https://people.freebsd.org/~emaste/Ventoy/geom_ventoy.ko.xz
axel https://people.freebsd.org/~emaste/Ventoy/ventoy_unix.cpio