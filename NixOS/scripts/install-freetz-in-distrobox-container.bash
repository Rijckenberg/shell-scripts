#!/usr/bin/env bash
# Creation date: 2025/1/11
# Modification date: 2025/1/11
# Author: Mark Rijckenberg
# Required diskspace: at least 7.1 GB in the freetz container
# List of available container images:  "docker search freetz"
# Installed container distro:  freetz (based on Ubuntu 20.04 LTS)
# Show vulnerabilities in freetz image:
trivy image docker.io/pfichtner/freetz
distrobox create --image docker.io/pfichtner/freetz
distrobox list
echo "If you cannot enter the freetz distrobox container after a NixOS upgrade, please run following command to fix the issue:"
echo "distrobox create -c freetz tmp && podman rm freetz; podman rename tmp freetz; distrobox enter freetz"
echo "Use freetz in container to compile new freetz firmware for Fritz!Box 7530 router"
distrobox enter freetz
