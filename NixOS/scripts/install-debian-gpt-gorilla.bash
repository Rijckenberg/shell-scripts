#!/usr/bin/env bash
# Modification date: 2024/2/1
# Author: Mark Rijckenberg
# How to install Gorilla GPT in NixOS
# See https://github.com/ShishirPatil/gorilla
# See https://pypi.org/project/gorilla-cli/

# first install Nvidia container toolkit:
curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
&& curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list | \
sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list \
&& \
sudo apt-get update
sudo apt install -y nvidia-container-toolkit
sudo apt install -y python3.13-venv

python -m venv gorilla
source gorilla/bin/activate
pip install --upgrade pip
pip install bauh
pip install gorilla-cli

# then manually run following commands to ask gorilla AI a question:
# python -m venv .venv
# source .venv/bin/activate
# gorilla 'What is the leading cause of deaths in babies in Ethiopia in 2023?'  "
