#!/usr/bin/env bash
# Procedure to copy mp3 files from Youtube or Soundcloud playlist to a smartphone
# Prerequisites: Ubuntu 24.10 or newer, Alpine Linux, bash shell, detox, 
# Prerequisites: yt-dlp version >= 2025.02.19, mp3gain
# Prerequisites: Google Chrome Web browser in Ubuntu 24.10 or newer, 
# Prerequisites: AirDroid on Android smartphone, 
# Prerequisites: Cloud Music Player - Listener on Apple iPhone
# Prerequisites: Only ports to keep open for this are the
# DNS or DNS-over-HTTPS port, port 80 and port 443
# Author: Grok 3 (with some help from Mark Rijckenberg :-)
# Last modification date: 2025/3/1

date > /tmp/1

# Function to install tools based on package manager
install_tool() {
    local tool="$1"
    if command -v apt &> /dev/null; then
        # Run apt update only once
        if [ -z "$apt_updated" ]; then
            sudo apt update || { echo "Error: apt update failed"; exit 1; }
            apt_updated=1
        fi
        sudo apt install -y "$tool" || { echo "Error: Failed to install $tool"; exit 1; }
    elif command -v apk &> /dev/null; then
        sudo apk add "$tool" || { echo "Error: Failed to install $tool"; exit 1; }
    elif command -v dnf &> /dev/null; then
        sudo dnf install -y "$tool" || { echo "Error: Failed to install $tool"; exit 1; }
    elif command -v pacman &> /dev/null; then
        sudo pacman -Syu --noconfirm "$tool" || { echo "Error: Failed to install $tool"; exit 1; }
    else
        echo "Unsupported package manager. Please install $tool manually."
        exit 1
    fi
}

# Check and install required tools
for tool in yt-dlp mp3gain detox; do
    if ! command -v "$tool" &> /dev/null; then
        echo "$tool is not installed. Installing..."
        install_tool "$tool"
    fi
done

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -u|--url) playlist_url="$2"; shift ;;
        -d|--dir) output_dir="$2"; shift ;;
        -a|--no-airdroid) airdroid_advice=false ;;
        *) echo "Unknown option: $1"; echo "Usage: $0 [-u URL] [-d DIR] [-a]"; exit 1 ;;
    esac
    shift
done

# Prompt for inputs if not provided
if [ -z "$playlist_url" ]; then
    read -p "Enter the YouTube or Soundcloud playlist URL: " playlist_url
fi
if [ -z "$output_dir" ]; then
    read -p "Enter the directory to store MP3 files: " output_dir
fi

# Default AirDroid advice to true unless disabled
airdroid_advice=${airdroid_advice:-true}

# Create output directory
echo "Setting up directory $output_dir..."
mkdir -p "$output_dir" || { echo "Error: Failed to create directory $output_dir"; exit 1; }
if [ ! -w "$output_dir" ]; then
    echo "Error: Directory $output_dir is not writable"; exit 1
fi

# Log start time
start_time=$(date)
echo "Start time: $start_time" > "$output_dir/process.log"

# Download playlist with progress logging
echo "Downloading playlist to $output_dir..."
yt-dlp --ignore-errors -x --audio-format mp3 -o "$output_dir/%(title)s.%(ext)s" "$playlist_url" | tee -a "$output_dir/process.log"

# Check if MP3 files were downloaded
if ! find "$output_dir" -maxdepth 1 -name "*.mp3" -print -quit | grep -q .; then
    echo "Error: No MP3 files were downloaded. Check the playlist URL and network."
    exit 1
fi

# Apply mp3gain to normalize volume
echo "Applying mp3gain to normalize volume..."
find "$output_dir" -maxdepth 1 -name "*.mp3" -print0 | while IFS= read -r -d '' file; do
    mp3gain -r "$file" || echo "Warning: Failed to apply mp3gain to $file"
done

# Clean up filenames
echo "Cleaning up filenames using detox utility..."
detox "$output_dir"/*.mp3 || echo "Warning: detox encountered an issue"

# Log end time
end_time=$(date)
echo "End time: $end_time" >> "$output_dir/process.log"

# Display completion message
echo "Conversion, normalization, and filename cleanup finished. MP3 files are in $output_dir."
echo "Process log saved to $output_dir/process.log"

# Optional AirDroid advice
if [ "$airdroid_advice" = true ]; then
    echo "Temporarily disable all extensions in Google Chrome and reboot your Android smartphone before transferring MP3 files via AirDroid."
    echo "Note: Mozilla Firefox is not yet compatible with AirDroid. Use Google Chrome."
fi

date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2
