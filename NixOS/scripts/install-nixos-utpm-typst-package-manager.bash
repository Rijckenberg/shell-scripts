#!/usr/bin/env bash
# Modification date: 2024/12/15
# Author: Mark Rijckenberg
# See https://github.com/Thumuss/utpm
# UTPM (Unofficial Typst package manager) is a package manager for local and remote Typst packages.
# to install utpm package manager for Typst in NixOS:
cargo install --git https://github.com/Thumuss/utpm
