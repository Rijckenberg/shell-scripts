#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2025/3/4
# This bash shell script is compatible with Rhino Linux running in podman container
# install newest version of firefox and geany in Rhino Linux
# Rhino Linux has a much more powerful meta-package manager than Ubuntu or Linux Mint

cd /tmp
rm releases*
rm index.html*
rm *.deb
rpk update
# rpk install firefox-bin # firefox sells data to advertisers now
# rpk install librewolf-app # uses AppImage -> avoid
rpk install librewolf-deb
rpk install geany
rpk cleanup
