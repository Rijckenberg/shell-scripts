# See https://freetz-ng.github.io/freetz-ng/FIRMWARES.html
# See https://hub.docker.com/search?q=freetz&sort=updated_at&order=desc
# See "docker search freetz" command output
# See https://hub.docker.com/r/pfichtner/freetz 
# See https://github.com/Freetz-NG/freetz-ng
# 1) First run these commands in NixOS host OS:
# 
# installing freetz-ng firmware for Fritz!Box 7530 in Ubuntu 20.04 LTS: (based on https://github.com/Freetz-NG/freetz-ng)
# 2) Run following Ubuntu 20.04 commands in distrobox container called freetz:

cd

sudo apt -y update

sudo apt -y dist-upgrade

git clone https://github.com/Freetz-NG/freetz-ng

cd ~/freetz-ng

git pull

tools/prerequisites install # -y

sudo apt -y install imagemagick subversion git bc wget sudo gcc g++ binutils autoconf automake autopoint libtool-bin make bzip2 libncurses5-dev libreadline-dev zlib1g-dev flex bison patch texinfo tofrodos gettext pkg-config  ecj  fastjar  manpages-fr-extra perl libstring-crc32-perl ruby gawk python libusb-dev unzip intltool libacl1-dev libcap-dev libc6-dev-i386 lib32ncurses5-dev gcc-multilib lib32stdc++6 libglib2.0-dev

cd ~/freetz-ng
make menuconfig
make # make command takes around 25 minutes to finish on ulysses-desktop
# make help

# FLASH FIRMWARE TO FRITZ!BOX ROUTER:
# cd ~/freetz-ng
# tools/push_firmware -h

# After running make command, last lines in Terminal output should look something like this output:

#STEP 3: PACK/SIGN
#  checking for left over version-control-system files
#  integrate freetz info file into image
#packing var.tar
#  checking signature key files
#    adding public signature key file
#creating filesystem image (SquashFS4_le-xz)
#  SquashFS block size: 64 kB (65536 bytes)
#copying kernel image
#  kernel image size: 3.2 MB, max 4.0 MB, free 0.8 MB (874496 bytes)
#copying filesystem image
#  filesystem image size: 38.3 MB, max 44.0 MB, free 5.7 MB (5931008 bytes)
#adding checksum to kernel.image
#adding checksum to filesystem.image
#packing images/7530_08.02.all_freetz-ng-25240-f036057485_20250118-155322.image
#  packed image file size: 45.9 MB (48148480 bytes)
#signing packed .image file
#  signed image file size: 45.9 MB (48148480 bytes)
#source firmware: 7530_de-es-fr-it-nl-pl 164.08.02 rev117978 {ALL} [Smart24 P1 NL1] <20.12.2024 15:46:16> 
#  source image file size: 38.0 MB (39813120 bytes)
#done.
#
#FINISHED
