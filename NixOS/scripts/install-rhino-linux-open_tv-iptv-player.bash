#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2025/1/26
# This bash shell script is compatible with Rhino Linux running in podman container
# install newest version of open_tv player in Rhino Linux
# Rhino Linux has a much more powerful meta-package manager than Ubuntu or Linux Mint

distrobox-export --delete -a open_tv
distrobox-export --delete -a open_tv
distrobox-export --delete -a open_tv
distrobox-export --delete -a open_tv
distrobox-export --delete -a open_tv
distrobox-export --delete -a open_tv
cd /tmp
rm releases*
rm index.html*
rm *.deb
wget https://github.com/Fredolx/open-tv/releases/download/v1.4.1/Open.TV_1.4.1_amd64.deb
sudo dpkg -i Open*.deb 
# install missing dependencies:
sudo apt install -f
distrobox-export --app open_tv