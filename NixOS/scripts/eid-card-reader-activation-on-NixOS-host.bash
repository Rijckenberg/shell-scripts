#!/usr/bin/env bash
# Author: Mark Rijckenberg 
# Latest modification date: 2024/12/19
# Prerequisites: NixOS 24.05, Brave web browser, ACR38 AC1038-based Smart Card Reader
# Prerequisites: acsccid driver for card reader
# This bash shell script is compatible with NixOS 24.05 and latest version of Brave web browser
# This script does NOT work reliably with Firefox browser, nor with the libacr38u drivers
# This script can also be used right before attempting to digitally sign a PDF
# okular # procedure to electronically sign PDF using Belgian eid card:
# 1) run bash shell script eid-card-reader-activation-script
# 2) go to okular::settings::configure backends::PDF::set signature backend to NSS 
# and set Certificate database to  custom value ~/.pki/nssdb
# 3) go to okular::Tools::Digitally sign

# Yes, there was a BeID-specific fork of OpenSC once, and it was the preferred version of the BeID software at that point in time.
# However, this fork is no longer maintained by anyone, and will also not work with Applet 1.8 cards, issued since ~2019. So unless 
# your Belgian eID card is older than the year 2020 (at this point), you can't use OpenSC.
# See https://github.com/ValveSoftware/steam-runtime/issues/667#issuecomment-2107095792

# See https://nixos.wiki/wiki/Web_eID
# See https://github.com/NixOS/nixpkgs/issues/121121#issuecomment-1919452393
# See https://github.com/ValveSoftware/steam-runtime/issues/667

killall brave
killall chrome
killall chromium
killall firefox
killall .firefox-wrapped
killall steamwebhelper
killall steam

echo -n "Press 1 and <ENTER> if you want to try to fix the crashlooping of the Steam client by deleting the contents of the Steam directory "
read VAR
if [[ $VAR -eq 1 ]]
then
   # try to fix crashlooping of steam client by deleting the steam install
   # and forcing a reinstall of steam from scratch:
   rm -rf ~/.local/share/Steam
fi

# Following step PREVENTS crashloop of steam/steamwebhelper client:
rm -rf ~/.pki # delete NSSDB in case it is corrupted by previous operations
# Enable use of Belgian eid card reader in brave web browser:
# Edit eid-nssdb script and replace /run/current-system/sw/lib/libbeidpkcs11.so 
# with /run/current-system/sw/lib/opensc-pkcs11.so
# Using /run/current-system/sw/lib/libbeidpkcs11.so in eid-nssdb script caused the crashloop of steam
# this step CAUSED crashloop of steam/steamwebhelper client
# bash ~/shell-scripts/NixOS/scripts/eid-nssdb.bash add
eid-nssdb add
echo "Retest eid card reader on www.cm.be using brave web browser"
# google-chrome-stable www.cm.be
brave www.cm.be
# Following step PREVENTS crashloop of steam/steamwebhelper client:
rm -rf ~/.pki # delete NSSDB in case it is corrupted by previous operations
# Launch steam and make sure steamwebhelper does not crashloop anymore:
steam


