{ 
  # argsOverride ? {},
  buildLinux,
  config,
  extraMeta ? {},
  fetchurl,
  inputs,
  kernelPatches ? [ ],
  lib,
  pkgs,
  structuredExtraConfig ? {},
  ... } @ args:

 # Use bleeding edge latest GNU/Linux kernel for all computers except for gaming PC and Raspberry Pi 4
 # List available kernels using nix repl as explained here:  https://nixos.wiki/wiki/Linux_kernel
 # In bash, enter nix repl
 # In nix repl, enter  :l <nixpkgs>
 # Enter linuxPackages_ and then hit the tab key to see the list of available GNU/Linux kernels
 # "cachyos" kernel works better and is more recent than "latest" kernel for gaming:
 # when switching from latest kernel to cachyos gaming kernel or vice versa,
 # you need to delete the old distrobox container and create a replacement container
 # boot.kernelPackages = pkgs.linuxPackages_xanmod_latest;

 # based on https://nixos.wiki/wiki/Linux_kernel
 # see https://github.com/NixOS/nixpkgs/issues/287189
 
 {
  boot.kernelPackages = let
      pkgs.linux_tkg_pkg =

      { 
  # argsOverride {},
  buildLinux,
  config,
  extraMeta ? {},
  fetchurl,
  kernelPatches ? [ ],
  lib,
  pkgs,
  structuredExtraConfig ? {}
  }:

        buildLinux (args // rec {
          version = "6.11";
          suffix = "6";
          modDirVersion = version;

          src = fetchurl {
            url = "https://github.com/Frogging-Family/linux-tkg/archive/refs/tags/v${version}.${suffix}.tar.gz";
            # After the first build attempt, look for "hash mismatch" and then 2 lines below at the "got:" line.
            # Use "sha256-....." value here.
            hash = "sha256-uvV4q0pxX96V0AKNbYHsDjV+xl/wPgSfhekP4IrnpIM=";
          };

  kernelPatches = let tkg-patch = {
    name = "tkg";
    patch = fetchurl {
      url = "https://github.com/Frogging-Family/linux-tkg/tree/master/linux-tkg-patches/${version}/*.patch";
      sha256 = "12c2qpifcgij7hilhd7xrnqaz04gqf41m93pmlm8cv4nxz58cy36";
    };
  }; in [ tkg-patch ] ++ lib.remove tkg-patch kernelPatches;

  structuredExtraConfig = with lib.kernel; {

  } // structuredExtraConfig;

          extraMeta.branch = "${version}";
            # } // (args.argsOverride or {}));
            } // ({}));
      linux_tkg = pkgs.callPackage linux_tkg_pkg{};
    in 
      pkgs.recurseIntoAttrs (pkgs.linuxPackagesFor linux_tkg);
}

# Main configuration file for NixOS 23.05 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# Find all NixOS packages here: https://nixos.org/
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.

# File creation date: 2024/11/5
# Last modification date: 2024/11/5

# DISKSPACE: This script will initially use minimum 60 GB of diskspace ( 56 GB for root partition and 4.2 GB for home+dropbox data))
# DISKSPACE: Reserve 1 GB for FAT32 /boot partition and set boot flag on this partition
# DISKSPACE: Reserve 0 GB for SSD swap partition and 8 GB diskspace for mechanical HD swap partition
# DISKSPACE: Reserve at least 100 GB of diskspace for / (root) F2FS partition for SSD - F2FS compresses better than ext4 on SSD
# DISKSPACE: Install all games in /tmp to avoid clogging up /home directory with files that should not be backed up
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 26 minutes on old SSD drive from the year 2016

# PRE-INSTALL STEPS:
# 1) update BIOS
# 2) backup home directory, including ~/.config subfolder

# POST-INSTALL STEPS:
# 3) extract config_backup_<timestamp>.zip from usb stick into home directory , then reboot PC
# 4) copy backed up documents/files from usb stick to ~/backup in home directory
# 5) set background wallpaper to /etc/nixos/desktopmanager/wallpaper.jpg
# 6) Improve privacy: Load chrome://flags/#encrypted-client-hello in the Chromium browser's address bar.
# Set the status of the Encrypted Client Hello (ECH) flag to Enabled. 
# https://blog.cloudflare.com/announcing-encrypted-client-hello/

# 7) Can be skipped thanks to step 3: Put following program icons in desktop panel for easy access: Terminal,spacefm,bitwarden,
# maestral,google chrome (also used to import/export bookmarks),chromium,brave,teamviewer,printer icon,proton vpn
# 8) Can be skipped thanks to step 3: copy all .desktop files from https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 9) Can be skipped thanks to step 3:Under Themes - set Icons to Numix-Circle-Light
# 10) Can be skipped thanks to step 3:force Chromium web browser to open
# https://searx.prvcy.eu/ (SearXNG meta search engine) on each startup (manually configure in Chromium settings)


# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).