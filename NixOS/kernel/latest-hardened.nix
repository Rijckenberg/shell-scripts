{ chaotic, config, pkgs, lib, inputs, ... }:

with lib;

{

  # Issues with the hardened kernel:
  # Average FPS framerate for hardened kernel
  # is 11 FPS lower compared to ./kernel/cachyos.nix
  # Can connect to wifi network using iwd, not using wpa_supplicant
  # Cannot mount usb sticks
  # Not compatible with waydroid 
  # Plantronics Blackwire Headsets not working in Google Meet
  # Compiling cachyos hardened kernel takes more than 8 hours

  # Change history:
  # 2024/7/12: Added kernel parameters "lockdown=confidentiality" , "kslr"
  # 2025/1/16: implemented this: https://nixos.wiki/wiki/Workgroup:SELinux
  # 2025/1/16: it took around 4h20min to successfully recompile pkgs.linuxPackages_cachyos-hardened on ulysses-laptop, but only 2 minutes to recompile pkgs.linuxPackages_hardened on ulysses-laptop
  # 2025/1/27: I commented out SELinux part in this file, because it caused compilation of
  # pkgs.linuxPackages_hardened to fail

 # Based on https://raw.githubusercontent.com/NixOS/nixpkgs/master/nixos/modules/profiles/hardened.nix

 # ./kernel/cachyos-hardened.nix # average FPS framerate for hardened kernel
 # is 11 FPS lower compared to ./kernel/cachyos.nix + can connect to wifi network using iwd, not wpa_supplicant

 # Use cachyos GNU/Linux kernel for all computers except for Raspberry Pi 4
 # List available kernels using nix repl as explained here:  https://nixos.wiki/wiki/Linux_kernel
 # In bash, enter nix repl
 # In nix repl, enter  :l <nixpkgs>
 # Enter linuxPackages_ and then hit the tab key to see the list of available GNU/Linux kernels
 # "cachyos" kernel works better and is more recent than "latest" kernel for gaming:
 # when switching from latest kernel to cachyos gaming kernel or vice versa,
 # you need to delete the old distrobox container and create a replacement container

  boot.blacklistedKernelModules = [
    # Obscure network protocols
    "ax25"
    "netrom"
    "rose"

    # Old or rare or insufficiently audited filesystems
    "adfs"
    "affs"
    "bfs"
    "befs"
    "cramfs"
    "efs"
    "erofs"
    "exofs"
    "freevxfs"
    "hfs"
    "hpfs"
    "jfs"
    "minix"
    "nilfs2"
    "omfs"
    "qnx4"
    "qnx6"
    "sysv"
    "ufs"
  ];

  boot.kernelModules = ["tcp_bbr"];

  # List of all chaotic packages: https://www.nyx.chaotic.cx/#lists
  # boot.kernelPackages = pkgs.linuxPackages_latest;
  # boot.kernelPackages = pkgs.linuxPackages_cachyos-hardened; # wifi only connecting via iwd/iw, not wpa_supplicant + slow fps in games + takes more than 4h20min to recompile on ulysses-laptop
  # boot.kernelPackages = pkgs.linuxPackages_cachyos; # Kernel and modules for Linux EEVDF-BORE scheduler
  boot.kernelPackages = pkgs.linuxPackages_hardened;  # wifi only connecting via iwd/iw, not wpa_supplicant + takes 2 minutes to recompile on ulysses-laptop
  # pkgs.linuxPackages_hardened cannot reliably connect to wifi networks using wpa_supplicant, sometimes yes, sometimes no
  # + cannot mount usb sticks 
  # + average FPS framerate for hardened kernel is 11 FPS lower compared to ./kernel/cachyos.nix

  boot.kernelParams = [

    # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
    "audit=1"

    # added security hardening NixOS option as recommended by Anduril NixOS STIG rules:
    "audit_backlog_limit=8192"

    # Disable debugfs
    "debugfs=off"

    # Enabling kernel self-protection
    # See https://www.itprotoday.com/linux-os/enhance-linux-kernel-security-using-lockdown-mode-and-kernel-self-protection
    "kslr"

    # See https://www.itprotoday.com/linux-os/enhance-linux-kernel-security-using-lockdown-mode-and-kernel-self-protection
    "lockdown=confidentiality"

    # Enable page allocator randomization
    "page_alloc.shuffle=1"

    # Overwrite free'd pages
    "page_poison=1"

    # 2025/1/27:commented out SELinux part, because it causes compilation of
    # pkgs.linuxPackages_hardened to fail
    # "security=selinux"

    # Don't merge slabs
    "slab_nomerge"

  ];

  
  # 2025/1/27:commented out SELinux part, because it causes compilation of
  # pkgs.linuxPackages_hardened to fail
  # compile kernel with SELinux support - but also support for other LSM modules
  #boot.kernelPatches = [ {
  #      name = "selinux-config";
  #      patch = null;
  #      extraConfig = ''
  #              SECURITY_SELINUX y
  #              SECURITY_SELINUX_BOOTPARAM n
  #              SECURITY_SELINUX_DISABLE n
  #              SECURITY_SELINUX_DEVELOP y
  #              SECURITY_SELINUX_AVC_STATS y
  #              SECURITY_SELINUX_CHECKREQPROT_VALUE 0
  #              DEFAULT_SECURITY_SELINUX n
  #            '';
  #      } ];

  # Restrict loading TTY line disciplines to the CAP_SYS_MODULE capability.
  boot.kernel.sysctl."dev.tty.ldisc_autoload" = 0;

  # Disable ftrace debugging
  boot.kernel.sysctl."kernel.ftrace_enabled" = mkDefault false;

  # Hide kernel pointers from processes without the CAP_SYSLOG capability.
  # Configure NixOS to prevent internal kernel addresses from being leaked:
  boot.kernel.sysctl."kernel.kptr_restrict" = 1;

  boot.kernel.sysctl."kernel.printk" = "3 3 3 3";

  # Make it so a user can only use the secure attention key which is required to access root securely.
  boot.kernel.sysctl."kernel.sysrq" = 4;

  # to allow starting protonup-qt and heroic games launcher:
  boot.kernel.sysctl."kernel.unprivileged_userns_clone"=1;

  # Restrict abritrary use of ptrace to the CAP_SYS_PTRACE capability.
  boot.kernel.sysctl."kernel.yama.ptrace_scope" = 2;

  # Do not accept IP source route packets (we're not a router)
  boot.kernel.sysctl."net.ipv4.conf.all.accept_source_route" = 0;
  boot.kernel.sysctl."net.ipv6.conf.all.accept_source_route" = 0;
  # Enable strict reverse path filtering (that is, do not attempt to route
  # packets that "obviously" do not belong to the iface's network; dropped
  # packets are logged as martians).
  boot.kernel.sysctl."net.ipv4.conf.all.log_martians" = mkDefault true;
  boot.kernel.sysctl."net.ipv4.conf.all.rp_filter" = mkDefault "1";
  boot.kernel.sysctl."net.ipv4.conf.default.log_martians" = mkDefault true;
  # Reverse path filtering causes the kernel to do source validation of
  # packets received from all interfaces. This can mitigate IP spoofing.
  # Enables source route verification:
  boot.kernel.sysctl."net.ipv4.conf.default.rp_filter" = mkDefault "1";

  # Protect against SMURF attacks and clock fingerprinting via ICMP timestamping.
  boot.kernel.sysctl."net.ipv4.icmp_echo_ignore_all" = "1";
  # Ignore ICMP broadcasts to avoid participating in Smurf attacks
  boot.kernel.sysctl."net.ipv4.icmp_echo_ignore_broadcasts" = mkDefault true;

  # Ignore incoming ICMP redirects (note: default is needed to ensure that the
  # setting is applied to interfaces added after the sysctls are set)
  # Do not accept ICMP redirects (prevent MITM attacks)
  boot.kernel.sysctl."net.ipv4.conf.all.accept_redirects" = mkDefault false;
  boot.kernel.sysctl."net.ipv4.conf.all.secure_redirects" = mkDefault false;
  boot.kernel.sysctl."net.ipv4.conf.default.accept_redirects" = mkDefault false;
  boot.kernel.sysctl."net.ipv4.conf.default.secure_redirects" = mkDefault false;
  boot.kernel.sysctl."net.ipv6.conf.all.accept_redirects" = mkDefault false;
  boot.kernel.sysctl."net.ipv6.conf.default.accept_redirects" = mkDefault false;

  # Ignore outgoing ICMP redirects (this is ipv4 only) (we are not a router)
  boot.kernel.sysctl."net.ipv4.conf.all.send_redirects" = mkDefault false;
  boot.kernel.sysctl."net.ipv4.conf.default.send_redirects" = mkDefault false;

  ## TCP hardening - based on 
  ## https://github.com/Ataraxxia/secure-arch/blob/main/02_basic_hardening.md
  # Prevent bogus ICMP errors from filling up logs.
  boot.kernel.sysctl."net.ipv4.icmp_ignore_bogus_error_responses" = 1;

    # Protects against SYN flood attacks
    # See https://www.ndchost.com/wiki/server-administration/hardening-tcpip-syn-flood
    # NixOS must manage excess capacity, bandwidth, or other redundancy to limit the effects 
    # of information flooding types of denial-of-service (DoS) attacks.
    boot.kernel.sysctl."net.ipv4.tcp_syncookies" = 1;
    # Decrease the time default value for tcp_fin_timeout connection
    boot.kernel.sysctl."net.ipv4.tcp_fin_timeout" = 15;
    # Decrease the time default value for connections to keep alive
    boot.kernel.sysctl."net.ipv4.tcp_keepalive_time" = 300;
    boot.kernel.sysctl."net.ipv4.tcp_keepalive_probes" = 5;
    boot.kernel.sysctl."net.ipv4.tcp_keepalive_intvl" = 15;
    boot.kernel.sysctl."net.ipv4.tcp_max_syn_backlog" = 2048;
    # Number of times SYNACKs for passive TCP connection.
    # https://gist.github.com/voluntas/bc54c60aaa7ad6856e6f6a928b79ab6c
    boot.kernel.sysctl."net.ipv4.tcp_synack_retries" = 3;
    # Disables packet forwarding - Do not act as a router
    boot.kernel.sysctl."net.ipv4.ip_forward" = 0;
    boot.kernel.sysctl."net.ipv6.conf.all.forwarding" = 0;
    # Protect against tcp time-wait assassination hazards
    boot.kernel.sysctl."net.ipv4.tcp_rfc1337" = 1;
    # implement address space layout randomization (ASLR) to protect its memory from unauthorized code execution:
    boot.kernel.sysctl."kernel.randomize_va_space" = 2;
    boot.kernel.sysctl."vm.swappiness" = 10;
  
  # following environment option prevents me from logging into Cinnamon desktop environment, so skipped:
  # environment.memoryAllocator.provider = mkDefault "scudo";

  # Kernel by CachyOS with other patches and improvements, provided by chaotic flake input
  # List of all chaotic packages: https://www.nyx.chaotic.cx/#lists
  # environment.systemPackages =  [ pkgs.scx ];

  # policycoreutils is for load_policy, fixfiles, setfiles, setsebool, semodile, and sestatus.
  # environment.systemPackages = with pkgs; [ policycoreutils ]; # build issue with libsemanage

  environment.variables.SCUDO_OPTIONS = mkDefault "ZeroContents=1";

  security.allowSimultaneousMultithreading = mkDefault false;

  security.apparmor.enable = mkDefault true;

  security.apparmor.killUnconfinedConfinables = mkDefault true;

  security.forcePageTableIsolation = mkDefault true;

  security.lockKernelModules = mkDefault true;

  security.protectKernelImage = mkDefault true;

  # This is required by podman to run containers in rootless mode.
  security.unprivilegedUsernsClone = mkDefault config.virtualisation.containers.enable;

  security.virtualisation.flushL1DataCache = mkDefault "always";

  # 2025/1/27:commented out SELinux part, because it causes compilation of
  # pkgs.linuxPackages_hardened to fail
  # build systemd with SELinux support so it loads policy at boot and supports file labelling
  # systemd.package = pkgs.systemd.override { withSelinux = true; };

}

# Main configuration file for NixOS 23.11 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# Find all NixOS packages here: https://nixos.org/
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.

# File creation date: 2024/6/10
# Last modification date: 2025/1/16

# DISKSPACE: This script will initially use minimum 60 GB of diskspace ( 56 GB for root partition and 4.2 GB for home+dropbox data))
# DISKSPACE: Reserve 1 GB for FAT32 /boot partition and set boot flag on this partition
# DISKSPACE: Reserve 0 GB for SSD swap partition and 8 GB diskspace for mechanical HD swap partition
# DISKSPACE: Reserve at least 100 GB of diskspace for / (root) F2FS partition for SSD - F2FS compresses better than ext4 on SSD
# DISKSPACE: Install all games in /tmp to avoid clogging up /home directory with files that should not be backed up
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 26 minutes on old SSD drive from the year 2016

# PRE-INSTALL STEPS:
# 1) update BIOS
# 2) backup home directory, including ~/.config subfolder

# POST-INSTALL STEPS:
# 3) extract config_backup_<timestamp>.zip from usb stick into home directory , then reboot PC
# 4) copy backed up documents/files from usb stick to ~/backup in home directory
# 5) set background wallpaper to /etc/nixos/desktopmanager/wallpaper.jpg
# 6) Improve privacy: Load chrome://flags/#encrypted-client-hello in the Chromium browser's address bar.
# Set the status of the Encrypted Client Hello (ECH) flag to Enabled. 
# https://blog.cloudflare.com/announcing-encrypted-client-hello/

# 7) Can be skipped thanks to step 3: Put following program icons in desktop panel for easy access: Terminal,spacefm,bitwarden,
# maestral,google chrome (also used to import/export bookmarks),chromium,brave,teamviewer,printer icon,proton vpn
# 8) Can be skipped thanks to step 3: copy all .desktop files from https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 9) Can be skipped thanks to step 3:Under Themes - set Icons to Numix-Circle-Light
# 10) Can be skipped thanks to step 3:force Chromium web browser to open
# https://searx.prvcy.eu/ (SearXNG meta search engine) on each startup (manually configure in Chromium settings)


# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).