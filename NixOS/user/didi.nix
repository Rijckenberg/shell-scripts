{ config, pkgs, lib, inputs, ... }:

{

# Following configuration required to allow launching collaboraoffice GUI in distrobox container:
environment.interactiveShellInit = "xhost local:didi";

environment.shellAliases = {
  "a-u" = "docker run --rm '--pull=always' alpine:edge apk --quiet --no-cache list --upgradeable";
  "c-c" = "xhost local:didi; distrobox-enter  -n debian-latest --   collaboraoffice --calc";
  "c-i" = "xhost local:didi; distrobox-enter  -n debian-latest --   collaboraoffice --impress";
  "c-p" = "xhost local:didi; distrobox-enter  -n debian-latest --   cardpeek";
  "c-w" = "xhost local:didi; distrobox-enter  -n debian-latest --   collaboraoffice --writer";
  "d" = "xhost local:didi; distrobox enter debian-latest";
  "d-b" = "date > /tmp/1; sudo unbuffer nixos-rebuild dry-build --upgrade-all; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "d-c" = "xhost local:didi; distrobox create -c debian-latest tmp && podman rm debian-latest; podman rename tmp debian-latest; distrobox enter debian-latest";
  "d-n" = "date > /tmp/1; sudo rm /etc/nixos/flake.lock; sudo nix flake update /etc/nixos; sudo unbuffer nixos-rebuild switch --flake /etc/nixos |& nom ; nvd history; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "d-n-ng" = "date > /tmp/1; sudo rm /etc/nixos/flake.lock; sudo nix flake update /etc/nixos; sudo unbuffer nixos-rebuild-ng switch --flake /etc/nixos |& nom ; nvd history; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "d-n-nh" = "date > /tmp/1; nh os switch ; nvd history; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "d-u" = "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "df" = "duf --hide special";
  "f-c" = "nix config check;nix run github:DeterminateSystems/flake-checker /etc/nixos/flake.lock --no-write-lock-file ; nix run github:astro/deadnix /etc/nixos/flake.nix";
  "f-r" = "fwupdmgr report-history";
  "f-u" = "date > /tmp/1; sudo time fwupdmgr get-devices; sudo time fwupdmgr refresh ; sudo time fwupdmgr get-updates ; sudo time fwupdmgr update; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "g-p" = "cd ; git clone https://gitlab.com/Rijckenberg/shell-scripts; cd ~/shell-scripts; git pull; sudo cp -R ~/shell-scripts/NixOS/*  /etc/nixos/";
  "grep" = "rga";
  # use sudo inxi -Fazi to check current TBW (total bytes written) value of SSD drive:
  # Samsung SSD 990 PRO 2TB has maximum TBW of 1200 TB
  "i" = "sudo inxi -Fazi";
  "l-i" = "echo 'Creation date of root file system '; stat / | grep Modify";
  "lsl" = "eza --all --git --group-directories-first --hyperlink --long --mounts --time-style long-iso";
  "lsu" = "eza --all --git --group-directories-first --hyperlink --long --mounts --time-style long-iso --total-size";
  "n-b" = "date > /tmp/1; sudo time nixos-rebuild build --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "n-c" = "date > /tmp/1; sudo nix-collect-garbage -d; sudo nix-store --gc; sudo nix-store --optimise; nh clean all; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "n-d" = "date > /tmp/1; sudo time nix-du -s=500MB | dot -Tpng > /tmp/store.png ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  # n-l command is used to find which package contains a specific file:
  # For example: nix-locate -w   opensc-pkcs11.so
  "n-l" = "nix-index; nix-locate -w ";
  "n-r" = "cd /etc/nixos; nix repl '.#' ";
  "n-s" = "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "n-t" = "date > /tmp/1; sudo time nixos-rebuild test --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  # nix-channel commands below required to upgrade nix to latest unstable version:
  "n-u" = "sudo nix-channel --list; sudo nix-channel --remove nixos; sudo nix-channel --add https://nixos.org/channels/nixos-unstable ; sudo nix-channel --update; sudo nix-channel --list";
  "n-v" = "date > /tmp/1; sudo time nix-store --verify --check-contents --repair; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2";
  "p" = "cd ~/shell-scripts/NixOS/scripts/; python -m venv .venv; source .venv/bin/activate";
  "s-c" = "sudo /run/current-system/bin/switch-to-configuration boot";
  "t-i" = "trivy image alpine:edge";
  "uuid" = "sudo fdisk -l|grep 'Microsoft basic data'; ls -lart /dev/disk/by-uuid;findmnt -D -s -t ntfs,ntfs-3g";
  "v-s"="export LANG=C.UTF-8;vulnix --system";
  "w"="lynx -print -dump  -width=150 https://wttr.in/ghent";
  "w-s" = "rm -rf  ~/.config/websurfx/; cd ~/.config; git clone https://github.com/neon-mmd/websurfx.git; cd ~/.config/websurfx; websurfx";
  "x" = "echo 'Running special xhost command so that GUI applications can start in distrobox containers'; xhost local:didi ";
  };

# Allow unfree packages.
  nixpkgs.config = {
    allowUnfree = true;
    allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) [
      ];
  };

####################################################################################################################################
  # Define a user account. Don't forget to set a password with ‘passwd’.
  ####################################################################################################################################
  users.users.didi = {
    isNormalUser = true;
    description = "user";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
        # maestral # Open-source Dropbox client for macOS and Linux
        # maestral-gui # GUI front-end for maestral (an open-source Dropbox client) for Linux
  ];

  };

}

# Main configuration file for NixOS 23.11 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# Find all NixOS packages here: https://nixos.org/
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.

# File creation date: 2023/6/19
# Last modification date: 2024/8/7
# DISKSPACE: This script will initially use minimum 60 GB of diskspace ( 56 GB for root partition and 4.2 GB for home+dropbox data))
# DISKSPACE: Reserve 1 GB for FAT32 /boot partition and set boot flag on this partition
# DISKSPACE: Reserve 0 GB for SSD swap partition and 8 GB diskspace for mechanical HD swap partition
# DISKSPACE: Reserve at least 100 GB of diskspace for / (root) F2FS partition for SSD - F2FS compresses better than ext4 on SSD
# DISKSPACE: Install all games in /tmp to avoid clogging up /home directory with files that should not be backed up
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 20 minutes on mechanical harddisk, faster on SSD

# PRE-INSTALL STEPS:
# 1) update BIOS
# 2) backup home directory, including ~/.config subfolder

# POST-INSTALL STEPS:
# 3) extract config_backup_<timestamp>.zip from usb stick into home directory , then reboot PC
# 4) copy backed up documents/files from usb stick to ~/backup in home directory
# 5) set background wallpaper to /etc/nixos/common/wallpaper.jpg

# 6) Can be skipped thanks to step 3: Put following program icons in desktop panel for easy access: warp-terminal,krusader,bitwarden,
# maestral,google chrome (also used to import/export bookmarks),chromium,teamviewer,printer icon
# 7) Can be skipped thanks to step 3: copy all .desktop files from https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 8) Can be skipped thanks to step 3:Under Themes - set Icons to Numix-Circle-Light
# 9) Can be skipped thanks to step 3:force Chromium web browser to open https://www.startpage.com/ on each startup (manually configure in Chromium settings)

# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
