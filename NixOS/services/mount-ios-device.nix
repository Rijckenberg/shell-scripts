# 2024/4/1: usbmuxd2 is compatible with ios 16, not ios 17

{ config, pkgs, ... }:

{

services.gvfs.enable = true;

# enable usbmuxd service to allow mounting ios devices:
services.usbmuxd = {
  enable = true;
  package = pkgs.usbmuxd; # Socket daemon to multiplex connections from and to iOS devices
  # package = pkgs.usbmuxd2; # Socket daemon to multiplex connections from and to iOS devices
};

 # See https://nixos.wiki/wiki/IOS
environment.systemPackages = with pkgs; [
  go-ios # supports iOS 17 - Operating system independent implementation of iOS device features
  gvfs # Virtual Filesystem support library
  ifuse # to mount filesystem using 'ifuse'
  kio-fuse # FUSE Interface for KIO
  libimobiledevice # supports iOS 16 or older - Software library that talks the protocols to support 
  # iPhone, iPod Touch and iPad devices on Linux
  libusbmuxd # Client library to multiplex connections from and to iOS devices
];


}
