{ config, pkgs , ... }:

{

  environment.systemPackages = with pkgs; [
     boxbuddy # Unofficial GUI for managing your Distroboxes, written with GTK4 + Libadwaita
     crun # Fast and lightweight fully featured OCI runtime and C library for running containers
     cudaPackages.cudatoolkit # compiler for NVIDIA GPUs, math libraries, and tools
     distrobox # Use podman in rootless mode to run Collabora Office 
     # on Ubuntu container in distrobox!
     ollama-cuda # Get up and running with large language models locally, using CUDA for NVIDIA GPU acceleration
     # best ollama model at the moment: ollama run deepseek-r1:14b-qwen-distill-q4_K_M
  ];

  # following setting conflicts with nvidiaBeta drivers:
  # hardware.nvidia-container-toolkit.enable = true;

  # Enable CUDA
  nixpkgs.config.cudaSupport = true;

  services.ollama.enable = true; # Whether to enable ollama server for local large language models
  services.ollama.acceleration = "cuda"; # supported by modern NVIDIA GPUs


  # enable docker daemon on boot:
  # virtualisation.docker.enable = true;
  # virtualisation.docker.enableOnBoot = true;

  # List services that you want to enable:
  # virtualisation.libvirtd.enable = true; 

  # Use podman in rootless mode to run Collabora Office on Ubuntu container in distrobox!
   virtualisation = {
      podman = {
             enable = true;
             # enableNvidia = true;
             # Create a `docker` alias for podman, to use it as a drop-in replacement
             dockerCompat = true;
             # Required for containers under podman-compose to be able to talk to each other.
             defaultNetwork.settings.dns_enabled = true;
               };
             };

  # virtualisation.waydroid.enable = true; 
  # only works with non-hardened latest Linux kernel
  # issues with waydroid:
  # 1) crashes too much
  # 2) issues logging into Google Play Store
  # 3) cannot install Android apps that I want

}

