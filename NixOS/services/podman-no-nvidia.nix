{ config, pkgs, ... }:

{

   environment.systemPackages = with pkgs; [
     boxbuddy # Unofficial GUI for managing your Distroboxes, written with GTK4 + Libadwaita
     crun # Fast and lightweight fully featured OCI runtime and C library for running containers
     distrobox # Use podman in rootless mode to run Collabora 
     # Office on Ubuntu container in distrobox!
  ];

  hardware.nvidia-container-toolkit.enable = false;

  services.ollama.enable = true; # Whether to enable ollama server for local large language models

  # enable docker daemon on boot:
  # virtualisation.docker.enable = true;
  # virtualisation.docker.enableOnBoot = true;

  # List services that you want to enable:
  # virtualisation.libvirtd.enable = true; 

  # Use podman in rootless mode to run Collabora Office on Ubuntu container in distrobox!
   virtualisation = {
      podman = {
             enable = true;
             # enableNvidia = false;
             # Create a `docker` alias for podman, to use it as a drop-in replacement
             dockerCompat = true;
             # Required for containers under podman-compose to be able to talk to each other.
             defaultNetwork.settings.dns_enabled = true;
               };
             };

  # virtualisation.waydroid.enable = true; 
  # only works with non-hardened latest Linux kernel
  # issues with waydroid:
  # 1) crashes too much
  # 2) issues logging into Google Play Store
  # 3) cannot install Android apps that I want

}

