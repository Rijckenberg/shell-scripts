{ config
, lib
, pkgs
, ... 
}:

{

 # See https://github.com/NixOS/nixpkgs/issues/121121#issuecomment-1919452393
 # See https://github.com/ValveSoftware/steam-runtime/issues/667
 # Procedure to make Brave web browser work with eid-mw package in NixOS:
 # 1) Close all webbrowsers
 # 2) Run "eid-nssdb remove" and rm -rf ~/.pki
 # 3) Edit eid-nssdb script and replace /run/current-system/sw/lib/libbeidpkcs11.so
 # with /run/current-system/sw/lib/opensc-pkcs11.so
 # Using /run/current-system/sw/lib/libbeidpkcs11.so in eid-nssdb script caused the crashloop of steam
 # 4) Run bash ~/shell-scripts/NixOS/scripts/eid-nssdb.bash add # this step caused crashloop of steam client
 # but only when libbeidpkcs11.so is used by eid-nssdb.bash
 # 5) Go to www.cm.be and retest eid card reader using Brave web browser (does NOT work with Firefox browser)

 # For Windows 11: install following packages:
 # https://www.acs.com.hk/en/driver/302/acr39u-smart-card-reader/
 # https://www.acs.com.hk/download-driver-unified/13142/ACS-Unified-MSI-Win-4420-1.zip
 # https://community.chocolatey.org/packages/eid-belgium
 # https://community.chocolatey.org/packages/GoogleChrome 

  environment.systemPackages = with pkgs; [
      ccid # PC/SC driver for USB CCID smart card readers, including Nox CardID eid card reader
      chrome-token-signing # Chrome and Firefox extension for signing with your eID on the web
      
      # Procedure to make Brave web browser work with eid-mw package:
      # See https://wiki.nixos.org/wiki/Nix-writers
      (pkgs.writers.writeBashBin "eid-card-reader-activation-script" { } ''
     
#!/usr/bin/env bash
# Author: Mark Rijckenberg 
# Latest modification date: 2025/3/8
# Prerequisites: NixOS 24.05, Brave web browser, ACR38 AC1038-based Smart Card Reader
# Prerequisites: acsccid driver for card reader
# This bash shell script is compatible with NixOS 24.05 and latest version of Brave web browser
# This script does NOT work reliably with Firefox browser, nor with the libacr38u drivers
# This script only needs to be run once, not after every reboot of the PC

# Yes, there was a BeID-specific fork of OpenSC once, and it was the preferred version of the BeID software at that point in time.
# However, this fork is no longer maintained by anyone, and will also not work with Applet 1.8 cards, issued since ~2019. So unless 
# your Belgian eID card is older than the year 2020 (at this point), you can't use OpenSC.
# See https://github.com/ValveSoftware/steam-runtime/issues/667#issuecomment-2107095792

# See https://nixos.wiki/wiki/Web_eID
# See https://github.com/NixOS/nixpkgs/issues/121121#issuecomment-1919452393
# See https://github.com/ValveSoftware/steam-runtime/issues/667

killall brave
killall chrome
killall chromium
killall firefox
killall .firefox-wrapped
killall steamwebhelper
killall steam

echo -n "Press 1 and <ENTER> if you want to try to fix the crashlooping of the Steam client by deleting the contents of the Steam directory "
read VAR
if [[ $VAR -eq 1 ]]
then
   # try to fix crashlooping of steam client by deleting the steam install
   # and forcing a reinstall of steam from scratch:
   rm -rf ~/.local/share/Steam
fi

# Following step PREVENTS crashloop of steam/steamwebhelper client:
rm -rf ~/.pki # delete NSSDB in case it is corrupted by previous operations
# Enable use of Belgian eid card reader in Brave web browser:
# Edit eid-nssdb script and replace /run/current-system/sw/lib/libbeidpkcs11.so 
# with /run/current-system/sw/lib/opensc-pkcs11.so
# Using /run/current-system/sw/lib/libbeidpkcs11.so in eid-nssdb script caused the crashloop of steam
# this step CAUSED crashloop of steam/steamwebhelper client
# bash ~/shell-scripts/NixOS/scripts/eid-nssdb.bash add
eid-nssdb add
echo "Retest eid card reader on www.cm.be using Brave web browser"
# google-chrome-stable www.cm.be
brave www.cm.be
# Following step PREVENTS crashloop of steam/steamwebhelper client:
rm -rf ~/.pki # delete NSSDB in case it is corrupted by previous operations
# Launch steam and make sure steamwebhelper does not crashloop anymore:
steam

    '')
    
     eid-mw # libbeidpkcs11.so in eid-mw causes coredump and constant restarting of steamwebhelper/steam client!!
     # Belgian electronic identity card (eID) middleware, required for Belgian eid cards
     # See https://wiki.archlinux.org/title/Electronic_identification
     nss # set of libraries for development of security-enabled client and server applications
     kdePackages.okular # procedure to electronically sign PDF using Belgian eid card:
     # 1) insert eid card into eid card reader and connect eid card reader to PC
     # 2) run bash shell script eid-card-reader-activation-script
     # 3) go to okular::settings::configure backends::PDF::set signature backend to NSS 
     # and set Certificate database to  custom value ~/.pki/nssdb
     # 4) go to okular::Tools::Digitally sign
     # opensc # beid specific fork of opensc not maintained anymore and only works for eid cards <= year 2020
     p11-kit # Library for loading and sharing PKCS#11 modules, required for Belgian eid cards
     # pcsc-cyberjack # REINER SCT cyberJack USB chipcard reader user space driver
     # pcsclite # Middleware to access a smart card using SCard API
     pcscliteWithPolkit # Middleware to access a smart card using SCard API (PC/SC), required for Belgian eid cards
     pcsctools # Tools used to test a PC/SC driver, card or reader, required for Belgian eid cards
     web-eid-app # signing and authentication operations with smart cards for the Web eID browser extension
  ];

# Manual procedure to make Firefox/Firedragon/Floorp/Librewolf browser work with eid-mw package:
# 1) eid browser extension for Firefox should be uninstalled
# 2) perform steps in following 2 websites for Firefox to manually load 'libbeidpkcs11.so' : 
# https://eid.belgium.be/en/faq/firefox-how-do-i-install-and-activate-eid-add#7504
# and also https://www.aideacces.be/135.php?langue=EN
# 3) !!! import /etc/nixos/common/belgiumrca4-import-in-firefox.crt into Mozilla Firefox
# That is the Belgium Root CA4 certificate
# 4) !!! import BEID library/security device /run/current-system/sw/lib/libbeidpkcs11.so into Firefox 
# (location of security device was found by running "eid-nssdb show")
# 5) enable DNS over https and choose NextDNS (https://nextdns.io/)
# 6) configure all other settings too in about:preferences#privacy in Mozilla Firefox

# Needed for Yubikey and eid card reader support:
# Enabling the pcscd service and pkgs.acsccid plugin makes it unnecessary to install any Belgian 
# beid/eid webbrowser extensions
services.pcscd.enable = true;
services.pcscd.extraArgs = [ "-d" ]; 

# !!!! only enable one of these pcscd plugins; never enable more than one plugin at the same time !!!!
# use following commands to troubleshoot:
# sudo systemctl start pcscd.service
# sudo journalctl -f -u pcscd
# services.pcscd.plugins = [ pkgs.acsccid ]; # is right driver for ACR38 AC1038-based Smart Card Reader
services.pcscd.plugins = [ pkgs.ccid ]; # is right driver for Nox Lite Card ID DNIe smart card reader (April 2022)
# services.pcscd.plugins = [ pkgs.libacr38u ];
# services.pcscd.plugins = [ pkgs.scmccid ];

# Bus 001 Device 002: ID 072f:9000 Advanced Card Systems, Ltd ACR38 AC1038-based Smart Card Reader
# This ACR38U seems to require use of acsccid plugin for pcscd
services.udev.extraRules = ''
    SUBSYSTEM=="usb", ATTR{idVendor}=="072f", ATTR{idProduct}=="9000", MODE="0660", GROUP="wheel"
    # add following entry for Nox CardID eid card reader:
    SUBSYSTEM=="usb", ATTR{idVendor}=="0bda", ATTR{idProduct}=="0165", MODE="0660", GROUP="wheel"
  '';

security.polkit.extraConfig = ''
      polkit.addRule(function(action, subject) {
        if ((action.id == "org.debian.pcsc-lite.access_pcsc" ||
          action.id == "org.debian.pcsc-lite.access_card") &&
          subject.isInGroup("wheel")) {
          return polkit.Result.YES;
        }
      });
  '';

}