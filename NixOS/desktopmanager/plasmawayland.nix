{ config, pkgs, lib, inputs, ... }:

{

    # Change history:
    # 2023/12/2: enabled support for wireless adapters in plasmawayland.nix 
    # 2024/3/1: switched from plasma5 to plasma6
    # 2024/5/20: disable "adaptive sync" in Plasma 6 display settings to avoid a black screen
    # when trying to play games in full screen mode
    # Only use plasmawayland.nix for gaming

    # wallpaper for login screen is managed by ./common/stylix.nix now: 
    # environment.etc."wallpaper.jpg".source = /etc/nixos/desktopmanager/wallpaper.jpg;

    environment.sessionVariables = {
       # GBM_BACKEND = "nvidia-drm";
       # __GLX_VENDOR_LIBRARY_NAME = "nvidia";
       # LIBVA_DRIVER_NAME = "nvidia";
       # Hint Electron apps to use wayland :
       # NIXOS_OZONE_WL = "1";
       # __VK_LAYER_NV_optimus="NVIDIA_only";
       # WLR_NO_HARDWARE_CURSORS = "1";
       # WLR_RENDERER= "vulkan";
       # XDG_SESSION_TYPE = "wayland";
     };

      environment.systemPackages = with pkgs; [
      input-remapper # tool to change the behaviour of your input 
      # devices. Supports X11, Wayland, combinations, programmable macros, joysticks, wheels,
      # triggers, keys, mouse-movements and more. Maps any input to any other input.
  ];

    # Most wayland compositors need this
    hardware.nvidia.modesetting.enable = true;

    # Networkmanager must be enabled to allow wireless to work in Plasma desktop:
    networking.networkmanager.enable = true;
    
  # Enable the desktop environment
  # Based on https://nixos.wiki/wiki/KDE
  services = {
    xserver.enable = true;
    xserver.displayManager.gdm.enable = true;
    xserver.displayManager.gdm.wayland = true;
    
    # For KDE Plasma 6, the defaults have changed. KDE Plasma 6 runs on
    # Wayland with the default session set to plasma. If you want to use
    # the X11 session as your default session, change it to plasmax11.
    # xserver.displayManager.session = "plasma";
    # xserver.displayManager.lightdm.enable = true;
    # wallpaper for login screen is managed by ./common/stylix.nix now: 
    # xserver.displayManager.lightdm.background = ./wallpaper.jpg; # sets lightdm wallpaper using file in /etc/nixos/desktopmanager/wallpaper.jpg
    desktopManager.plasma6.enable = true; # plasma desktop supports wayland

    # xserver.desktopManager.cinnamon.enable = true;
    # xserver.displayManager.gdm.enable = true;
    # xserver.displayManager.sddm.enable = true;
    # Wireless not working under KDE Plasma
    # The user systemd session is degraded under KDE Plasma and XFCE, use Cinnamon instead:
    # UNIT                             LOAD   ACTIVE SUB    DESCRIPTION                              
    # app-bitwarden@autostart.service  loaded failed failed Bitwarden
    # app-maestral@autostart.service   loaded failed failed Maestral
    xserver.xkb.variant = ""; # Configure keymap in X11
  };


  # autostart maestral_qt on boot (open-source replacement for Dropbox)
  # This section is required so that home-manager can apply correct icon theme via home.nix:
  services.xserver.desktopManager.session = [{
  name = "plasma";
  bgSupport = true;
  start = ''
  #  ${pkgs.maestral-gui}/bin/maestral_qt
  '';
  }];
 
}

# Main configuration file for NixOS 23.05 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# Find all NixOS packages here: https://nixos.org/
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.

# File creation date: 2023/6/19
# Last modification date: 2023/10/17

# DISKSPACE: This script will initially use minimum 60 GB of diskspace ( 56 GB for root partition and 4.2 GB for home+dropbox data))
# DISKSPACE: Reserve 1 GB for FAT32 /boot partition and set boot flag on this partition
# DISKSPACE: Reserve 0 GB for SSD swap partition and 8 GB diskspace for mechanical HD swap partition
# DISKSPACE: Reserve at least 100 GB of diskspace for / (root) F2FS partition for SSD - F2FS compresses better than ext4 on SSD
# DISKSPACE: Install all games in /tmp to avoid clogging up /home directory with files that should not be backed up
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 26 minutes on old SSD drive from the year 2016

# PRE-INSTALL STEPS:
# 1) update BIOS
# 2) backup home directory, including ~/.config subfolder

# POST-INSTALL STEPS:
# 3) extract config_backup_<timestamp>.zip from usb stick into home directory , then reboot PC
# 4) copy backed up documents/files from usb stick to ~/backup in home directory
# 5) Improve privacy: Load chrome://flags/#encrypted-client-hello in the Chromium browser's address bar.
# Set the status of the Encrypted Client Hello (ECH) flag to Enabled. 
# https://blog.cloudflare.com/announcing-encrypted-client-hello/

# 6) Can be skipped thanks to step 3: Put following program icons in desktop panel for easy access: Terminal,spacefm,bitwarden,
# maestral,google chrome (also used to import/export bookmarks),chromium,brave,teamviewer,printer icon,proton vpn
# 7) Can be skipped thanks to step 3: copy all .desktop files from https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 8) Can be skipped thanks to step 3:Under Themes - set Icons to Numix-Circle-Light
# 9) Can be skipped thanks to step 3:force Chromium web browser to open
# https://searx.prvcy.eu/ (SearXNG meta search engine) on each startup (manually configure in Chromium settings)


# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
