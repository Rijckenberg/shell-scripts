# Change history:

# See https://nixos.wiki/wiki/Hyprland
# https://www.drakerossman.com/blog/wayland-on-nixos-confusion-conquest-triumph
# https://github.com/NixOS/nixpkgs/issues/90152
# https://wiki.hyprland.org/hyprland-wiki/pages/Nvidia/
# https://www.youtube.com/watch?v=61wGzIv12Ds

{ config, pkgs, lib, inputs, ... }:

{

# rofi keybind to put in hyprland configuration:
# bind = $mainMod, S, exec, rofi -show drun -show-icons

  # sway only supports nouveau driver, not nvidia driver
  # environment.etc."greetd/environments".text = ''
  #  sway 
  # '';

  # environment.etc."wallpaper.jpg".source = /etc/nixos/desktopmanager/wallpaper.jpg;

  environment.sessionVariables = {
  GBM_BACKEND = "nvidia-drm";
  __GLX_VENDOR_LIBRARY_NAME = "nvidia";
  LIBVA_DRIVER_NAME = "nvidia";
   # Hint Electron apps to use wayland :
  NIXOS_OZONE_WL = "1";
  __VK_LAYER_NV_optimus="NVIDIA_only";
  WLR_NO_HARDWARE_CURSORS = "1";
  WLR_RENDERER= "vulkan";
  XDG_SESSION_TYPE = "wayland";
};

 environment.systemPackages = with pkgs; [
      # dmenu is NOT available on wayland
      foot # fast, lightweight and minimalistic terminal emulator tailored specifically for Wayland
      i3status-rust # Very resource-friendly and feature-rich replacement for i3status
      # = machinery that powers the waybar and actually retrieves all that info for it to show
      input-remapper # tool to change the behaviour of your input 
      # devices. Supports X11, Wayland, combinations, programmable macros, joysticks, wheels,
      # triggers, keys, mouse-movements and more. Maps any input to any other input.
      libsForQt5.qt5ct # Qt5 Configuration Tool
      libsForQt5.qt5.qtwayland # cross-platform application framework for C++
      libva # implementation for VA-API (Video Acceleration API)
      light # GNU/Linux application to control backlights
      mako # highly customizable notifications manager
      rofi
      rofi-wayland # Window switcher, run dialog and dmenu replacement for Wayland
      swww # Efficient animated wallpaper daemon for wayland, controlled at runtime
      termite
      waybar # highly customizable Wayland bar for Sway and Wlroots based compositors
      # waypaper # GUI wallpaper setter for Wayland-based window managers
      wl-clipboard # utility which enables the clipboard for wayland to work accross all applications
      xwayland
  ];

 # Most wayland compositors need this
 hardware.nvidia.modesetting.enable = true;

nixpkgs.overlays = [
  (self: super: {
    waybar = super.waybar.overrideAttrs (oldAttrs: {
      mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true" ];
    });
  })
];

 # enable hyprland on wayland compositor:
 # nvidiaPatches are no longer needed
 programs.hyprland = {
   enable = true; 
   package = inputs.hyprland.packages.${pkgs.system}.hyprland;
   # xwayland.hidpi = true; # option is now deprecated
   xwayland.enable = true;
};

# sway only supports nouveau driver, not nvidia driver
# programs.sway = {
#  enable = true; 
#  extraPackages = with pkgs; 
#     [
#      dmenu # "dynamic menu", which creates a "menu" - in this case, 
#      # a line on the top of your screen with some output that is selectable via arrow keys
#      foot # fast, lightweight and minimalistic terminal emulator tailored specifically for Wayland
#      i3status # machinery that powers the waybar and actually retrieves all that info for it to show
#      i3status-rust
#      kdePackages.qt5ct
#      kdePackages.qt5.qtwayland
#      libva
#      light # GNU/Linux application to control backlights
#      mako # highly customizable notifications manager
#      rofi
#      swayidle
#      swaylock 
#      termite
#      waybar # highly customizable Wayland bar for Sway and Wlroots based compositors
#      wl-clipboard # utility which enables the clipboard for wayland to work accross all applications
#      xwayland
#     ];
# };

# sway only supports nouveau driver, not nvidia driver 
# services.greetd = {
#    enable = true;
#    settings = {
#     default_session.command = ''
#      ${pkgs.greetd.tuigreet}/bin/tuigreet \
#        --time \
#        --asterisks \
#        --user-menu \
#        --cmd sway
#    '';
#    };
#  };

xdg.portal.enable = true;
xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

}

# Main configuration file for NixOS 23.05 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# Find all NixOS packages here: https://nixos.org/
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.

# File creation date: 2023/6/19
# Last modification date: 2023/11/2

# DISKSPACE: This script will initially use minimum 60 GB of diskspace ( 56 GB for root partition and 4.2 GB for home+dropbox data))
# DISKSPACE: Reserve 1 GB for FAT32 /boot partition and set boot flag on this partition
# DISKSPACE: Reserve 0 GB for SSD swap partition and 8 GB diskspace for mechanical HD swap partition
# DISKSPACE: Reserve at least 100 GB of diskspace for / (root) F2FS partition for SSD - F2FS compresses better than ext4 on SSD
# DISKSPACE: Install all games in /tmp to avoid clogging up /home directory with files that should not be backed up
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 26 minutes on old SSD drive from the year 2016

# PRE-INSTALL STEPS:
# 1) update BIOS
# 2) backup home directory, including ~/.config subfolder

# POST-INSTALL STEPS:
# 3) extract config_backup_<timestamp>.zip from usb stick into home directory , then reboot PC
# 4) copy backed up documents/files from usb stick to ~/backup in home directory
# 5) Improve privacy: Load chrome://flags/#encrypted-client-hello in the Chromium browser's address bar.
# Set the status of the Encrypted Client Hello (ECH) flag to Enabled. 
# https://blog.cloudflare.com/announcing-encrypted-client-hello/

# 6) Can be skipped thanks to step 3: Put following program icons in desktop panel for easy access: Terminal,spacefm,bitwarden,
# maestral,google chrome (also used to import/export bookmarks),chromium,brave,teamviewer,printer icon,proton vpn
# 7) Can be skipped thanks to step 3: copy all .desktop files from https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 8) Can be skipped thanks to step 3:Under Themes - set Icons to Numix-Circle-Light
# 9) Can be skipped thanks to step 3:force Chromium web browser to open
# https://searx.prvcy.eu/ (SearXNG meta search engine) on each startup (manually configure in Chromium settings)


# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
