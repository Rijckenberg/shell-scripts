{
  description = "NixOS configuration of desktop PC and various laptops";

  # Hard-coded version number for lix-module needs to be updated manually,
  # because the lix-module is not stored in a Gitlab or Github repo
  # Only Gitlab/Github repositories support references to stable/unstable instead of version numbers
  # See following URL for latest lix release info:
  # https://git.lix.systems/lix-project/nixos-module/tags
  # inputs.lix-module.url = "https://git.lix.systems/lix-project/nixos-module/archive/2.91.1-1.tar.gz";
  # inputs.lix-module.inputs.nixpkgs.follows = "nixpkgs";

  # RAM requirements to use this flake: at least 8 GB of RAM required!
  # NixOS version in flake.nix overrides/overrules version in configuration.nix file
  
  # Change history:
  # 2023/09/27: Build issue with ripgrep-all-0.9.6 blocked upgrade to nixos-unstable
  # 2023/10/01: Used config.nur.repos.fliegendewurst.ripgrep-all as workaround solution to upgrade to nixos-unstable
  # 2023/10/05: Deleted configuration.nix file
  # 2023/10/06: Added nixosConfiguration for Raspberry Pi 4
  # 2023/10/15: Defined custom GNU/Linux kernel for Raspberry Pi 4
  # 2023/10/16: Solved USB keyboard and mouse stuck issue on didi-laptop and ulysses-desktop 
  # by disabling powertop service in ./common/common.nix
  # 2023/11/16: eid card reader not detected in NixOS 23.11 -> solved by enabling pcscd service (in ./common/common.nix) which
  # is provided by pcscliteWithPolkit package -> see https://github.com/NixOS/nixpkgs/issues/121121#issuecomment-1918232797
  # Enabling the pcscd service makes it unnecessary to install any Belgian eid webbrowser extensions
  # 2023/11/23: Manually added mail.google.com as exception in popup blocker settings in Mozilla Firefox browser, 
  # so that popup window for print requests in Firefox does not get blocked anymore
  # 2023/11/24: added section about programs.firefox in home.nix
  # 2023/12/2: enabled support for wireless adapters in desktopmanager/plasmawayland.nix 
  # 2023/12/3: desktopmanager/plasmawayland.nix causes screen tearing issues in games 
  # 2024/1/4: added inputs.nix-script.url in flake.nix
  # (https://github.com/BrianHicks/nix-script)
  # 2024/2/3: added ./machine/ulysses-desktop/libvirtd.nix
  # 2024/2/6: added ./machine/ulysses-desktop/ntfs-3g.nix
  # 2024/2/12: switched boot method from grub to systemd-boot for ulysses-desktop because of switch to different hardware
  # 2024/2/12: enabled 3 AMD modules and 1 Nvidia module under nixos-hardware.nixosModules for ulysses-desktop because
  # of switch to different hardware
  # 2024/2/16: added nix.registry and nix.nixPath to ./common/common.nix
  # 2024/2/16: added inputs.nix-index-database.url in flake.nix
  # 2024/2/24: added disko-config.nix for ulysses-laptop
  # 2024/2/25: added ./machine/ulysses-desktop/hardware-configuration.nix and ./machine/ulysses-laptop/hardware-configuration.nix
  # 2024/2/25: split ./user/ulysses.nix into ./user/ulysses-desktop.nix and ./user/ulysses-laptop.nix
  # 2024/3/1: added ./services/pcscd.nix to enable eid card reader
  # 2024/3/8: added ./kernel/cachyos.nix as gaming/business/work kernel
  # 2024/3/13: added nixosConfigurations.gerard-laptop-iso
  # 2024/3/15: cachyos kernel 6.8.0 caused boot failure on ulysses-desktop
  # because it could not mount NTFS partition -> switched to "latest" kernel on ulysses-desktop
  # 2024/3/19: added desktopmanager/hyprland.nix
  # 2024/3/21: removed self in outputs line below
  # 2024/3/21: replaced config.nur.repos.fliegendewurst.ripgrep-all with standard nixpkgs version
  # 2024/3/24: libbeidpkcs11.so in eid-mw causes coredump and constant restarting/crashloop of steamwebhelper part of steam client!!
  # 2024/3/24: so cannot use ./services/pcscd.nix and steam at same time, unfortunately
  # 2024/3/24: see https://github.com/NixOS/nixpkgs/issues/298662 for details about conflict between
  # eid-mw and steam
  # 2024/3/29: added ./services/podman-nvidia.nix and ./services/podman-no-nvidia.nix
  # 2024/3/31: added ./services/mount-ios-device.nix
  # 2024/4/1: ./services/mount-ios-device.nix is compatible with ios 16, not ios 17
  # 2024/4/4: added ./common/ai-packages.nix
  # 2024/4/5: added ./common/printer-packages.nix
  # 2024/4/13: improved ./kernel/cachyos-hardened.nix and applied to didi-laptop
  # 2024/4/22: ./kernel/cachyos-hardened.nix cannot reliably connect to wifi networks, sometimes yes, sometimes no
  # 2024/4/22: added programs.bash.enable = true and programs.zoxide.enableBashIntegration = true to
  # ./machine/$USER-desktop/home.nix so that z and zi commands can be used as replacement for cd command
  # 2024/4/26: added ./kernel/zen.nix and applied to ulysses-desktop
  # 2024/5/2: added ./common/gaming-packages.nix and ./kernel/xanmod.nix
  # 2024/5/16: added chaotic packages in ./common/gaming-packages.nix
  # 2024/5/16: applied ./common/enable-x86_64_v3-support.nix and ./common/install-x86_64_v3-pkgs.nix to ulysses-desktop
  # 2024/5/19: added ./common/stylix.nix
  # stylix.nix sets wallpaper on desktop only when using plasmawayland = best choice
  # stylix.nix sets wallpaper on login screen only when using cinnamon
  # 2024/5/20: disable "adaptive sync" in Plasma 6 display settings to avoid a black screen
  # when trying to play games in full screen mode
  # 2024/5/20: Only use plasmawayland.nix for gaming
  # 2024/5/20: added home-manager.backupFileExtension = "backup";
  # 2024/7/30: added boot.loader.systemd-boot.bootCounting.enable = true; in ./bootmethod/systemd-boot.nix
  # 2024/7/31: added resample.quality = 10; in ./common/common.nix to improve sound quality
  # 2024/8/2: temporarily disabled ollama service on ulysses-desktop and ulysses-laptop
  # becauses ollama fails to compile
  # 2024/8/15: nixos-hardware.nixosModules.common-gpu-nvidia-nonprime is not 
  # compatible with combination of nvidia driver + cinnamon X11 -> need to use 
  # ./machine/ulysses-desktop/nvidia.nix instead
  # 2024/8/15: nvidia driver does not work well anymore with plasmawayland.nix and plasmax11.nix
  # 2024/8/16: creation of ./services/i2pd.nix file
  # 2024/8/23: replaced nix package manager with lix version using lix-module -> https://lix.systems/add-to-config/
  # 2024/8/26: latest NVIDIA driver version 560 causes sniper game to crash
  # 2024/8/26: had to revert to NVIDIA driver version 555.58.02
  # 2024/9/28: added lanzaboote as input
  # 2024/10/3: Performed successful bootloader migration from grub to systemd-boot on ulysses-laptop using following commands:
  # reconfigure flake.nix to use systemd-boot
  # sudo nixos-rebuild switch
  # next command will install systemd-boot in EFI partition (under /boot):
  # sudo nixos-rebuild --install-bootloader boot
  # reboot and manually boot into EFI File systemd-boot in BIOS menu
  # use sudo efibootmgr -o  -> choose systemd-boot as first item in boot order
  # reboot
  # use sudo bootctl status to verify that systemd-boot is now being used as default boot loader
  # 2024/10/6: enabling lanzaboote caused the BIOS menu and laptop's boot menu/list of NixOS 
  # generations to become inaccessible on HP Elitebook 820 G4 laptop (ulysses-laptop) -> it is now bricked and useless
  # 2024/10/23: replaced lix with nix package manager because lix does not detect enough errors and
  # because lix did not upgrade linux kernels for several weeks - it is clearly broken
  # 2024/10/23: ulysses-laptop has best performance using cinnamon + wayland
  # 2024/11/12: enabled nix-gaming platformOptimizations module for ulysses-desktop
  # 2024/11/21: issue between stylix and base16:  https://github.com/danth/stylix/issues/642
  # 2024/11/21: newest nvidia drivers cannot compile successfully yet for kernel 6.12
  # 2024/11/21: temporarily downgraded kernel on ulysses-desktop
  # 2024/11/28: commented out inputs.nix-software-center.packages.${system}.nix-software-center 
  # due to unresolved error with adwaita icon theme
  # 2024/12/27: added Jovian-Experiments/Jovian-NixOS (jovian) as input
  # 2025/1/5: replaced github:vlinkz/nix-software-center with github:ljubitje/nix-software-center
  # 2025/1/13: added security hardening NixOS options to ./common/common.nix as recommended by Anduril NixOS STIG rules and 
  # https://www.stigaview.com/products/srg-gpos/latest/
  # 2025/1/13: DoD's STIG viewer (to view Anduril NixOS STIG rules) is available here: https://public.cyber.mil/stigs/srg-stig-tools/
  # 2025/3/7: added inputs.determinate.url = "https://flakehub.com/f/DeterminateSystems/determinate/*";

  # base16 is used by stylix
  # See https://github.com/danth/stylix/issues/642
  inputs.base16.url = "github:Noodlez1232/base16.nix/slugify-fix";
  # inputs.base16.url = "github:SenchoPens/base16.nix?ref=refs/pull/19/head";

  inputs.chaotic.url = "github:chaotic-cx/nyx/nyxpkgs-unstable";
  # See https://www.nyx.chaotic.cx/#Lists%20of%20options%20and%20packages
  # following nyx flakehub input not working anymore since 2025/2/17:
  # inputs.chaotic.url = "https://flakehub.com/f/chaotic-cx/nyx/*.tar.gz";
  inputs.chaotic.inputs.nixpkgs.follows = "nixpkgs";

  # use latest version of Determinate Nix:
  # https://flakehub.com/flake/DeterminateSystems/determinate?view=readme
  inputs.determinate.url = "https://flakehub.com/f/DeterminateSystems/determinate/*";

  inputs.disko.url = "github:nix-community/disko";
  inputs.disko.inputs.nixpkgs.follows = "nixpkgs";

  # inputs.fh.url = "https://flakehub.com/f/DeterminateSystems/fh/*.tar.gz";
  # inputs.fh.inputs.nixpkgs.follows = "nixpkgs";

  inputs.home-manager.url = "github:nix-community/home-manager";
  inputs.home-manager.inputs.nixpkgs.follows = "nixpkgs";

  inputs.hosts.url = "github:StevenBlack/hosts";
  inputs.hosts.inputs.nixpkgs.follows = "nixpkgs";

  # Jovian-Experiments/Jovian-NixOS includes gaming optimizations:
  inputs.jovian.url = "github:Jovian-Experiments/Jovian-NixOS";
  inputs.jovian.inputs.nixpkgs.follows = "nixpkgs";

  inputs.nix-gaming.url = "github:fufexan/nix-gaming";
  inputs.nix-gaming.inputs.nixpkgs.follows = "nixpkgs";

  inputs.nix-index-database.url = "github:Mic92/nix-index-database";
  inputs.nix-index-database.inputs.nixpkgs.follows = "nixpkgs";

  inputs.nixos-flake.url = "github:srid/nixos-flake";

  inputs.nixos-hardware.url = "github:nixos/nixos-hardware";

  # Get the current unstable Nixpkgs:
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  # flakehub nixpkgs are slightly more cutting-edge/up-to-date than nixos-unstable:
  # inputs.nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/0.1.0.tar.gz";
  
  # Use a pinned version of nixpkgs from flakehub:
  # inputs.nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/0.2405.635914.tar.gz";

  # Get the current stable Nixpkgs:
  # inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
  # inputs.nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/*.tar.gz";
  
  # inputs.nix-software-center.url = "github:vlinkz/nix-software-center";
  inputs.nix-software-center.url = "github:ljubitje/nix-software-center";
  inputs.nix-software-center.inputs.nixpkgs.follows = "nixpkgs";

  # use the nur repo for firefox extensions
  inputs.nur.url       = "github:nix-community/NUR";

  inputs.stylix.url = "github:danth/stylix";
  inputs.stylix.inputs.base16.follows = "base16";
  inputs.stylix.inputs.home-manager.follows = "nixpkgs";
  inputs.stylix.inputs.nixpkgs.follows = "nixpkgs";

  # websurfx - modern-looking, lightning-fast, privacy-respecting, secure meta search engine 
  # written in Rust. It provides a  quick and secure search experience while completely respecting 
  # user privacy.
  # websurfx - post-install steps:
  # Run following bash shell commands: 
  # rm -rf  ~/.config/websurfx/
  # cd ~/.config; git clone https://github.com/neon-mmd/websurfx.git
  # cd ~/.config/websurfx; websurfx
  # inputs.websurfx.url = "github:neon-mmd/websurfx";
  # See https://nix-community.github.io/home-manager/index.html#ch-nix-flakes

  # disabled flake-parts to avoid conflicts with lix-module
  # inputs.flake-parts.url = "github:hercules-ci/flake-parts";

  # inputs.nix-script.url = "https://github.com/BrianHicks/nix-script/archive/main.tar.gz";
  # inputs.nix-script.inputs.nixpkgs.follows = "nixpkgs";
  # see https://github.com/nix-community/nix-index-database

  # inputs.hyprland.url = "github:hyprwm/Hyprland";

  # outputs = {base16, chaotic, disko, home-manager, hosts, jovian, nix-gaming, nix-index-database, nixos-hardware, nixpkgs, nix-software-center, nur, stylix, ...}@inputs: 

  outputs = {chaotic, determinate,  disko, home-manager, hosts, nix-index-database, nixos-hardware, nixpkgs, nur, stylix, ...}@inputs: 
  
    let

      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
        overlays = [ 
           chaotic.overlays.default 
           #(final: prev: {
           #     mplayer = prev.mplayer.overrideAttrs (old: {
           #       NIX_CFLAGS_COMPILE = "-Wno-incompatible-pointer-types -Wno-implicit-function-declaration -Wno-int-conversion";
           #       });
           # })
           # (self: super: {utillinux = super.util-linux; })
           ]; # replace lix with nix package manager
        # overlays = [ chaotic.overlays.default lix-module.overlays.default (self: super: {utillinux = super.util-linux; })]; # ensures use of lix version of nix package manager
      };

    in

  {

  # Run following command to change hostname from nixos to ulysses-desktop
  # for the very first time and to apply the ulysses-desktop configuration
  # sudo nixos-rebuild switch --flake /etc/nixos#ulysses-desktop
  # Once hostname is changed to hostname described in flake.nix,
  # just run  sudo nixos-rebuild switch to apply newest configuration linked to ulysses-desktop

  nixosConfigurations.ulysses-desktop = nixpkgs.lib.nixosSystem {
       # pkgs-unstable can then be referenced in a Home Manager submodule
       system = "x86_64-linux";
       # specialArgs -> Set all inputs parameters as special arguments for all submodules,
       # so you can directly use all dependencies in inputs in submodules
       # specialArgs = inputs; -> do not use, as it causes an infinite recursion error!
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/nixos-flake-and-module-system
       specialArgs = {inherit inputs pkgs;};
       modules = [ 
          # Much easier to reinstall Linux Boot loader when systemd-boot.nix is being used (versus grub.nix)
          ./bootmethod/systemd-boot.nix
          chaotic.nixosModules.default # Their default module
          # ./common/ai-packages.nix
          ./common/common.nix
          ./common/enable-x86_64_v3-support.nix # enable this and rebuild once before enabling ./common/install-x86_64_v3-pkgs.nix
          ./common/extra-packages.nix
          ./common/gaming-packages.nix
          ./common/install-x86_64_v3-pkgs.nix # only enable this after rebuilding once with ./common/enable-x86_64_v3-support.nix
          ./common/minimal-packages.nix
          ./country/belgium-english.nix
          ./desktopmanager/cinnamon.nix
          # https://flakehub.com/flake/DeterminateSystems/determinate?view=readme
          determinate.nixosModules.default
          # hyprland.nixosModules.default
          #./desktopmanager/hyprland.nix
          #./desktopmanager/plasmawayland.nix # causes pcloud client to use 100% CPU
          #./desktopmanager/plasmax11.nix
          # See https://nix-community.github.io/home-manager/index.html#ch-nix-flakes
          home-manager.nixosModules.home-manager
          {
            # Purpose of home-manager.backupFileExtension:
            # On activation move existing files by appending the given 
            # file extension rather than exiting with an error.
            home-manager.backupFileExtension = "backup";
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.ulysses = import ./machine/ulysses-desktop/home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
          }
         hosts.nixosModule {
             networking.stevenBlackHosts = {
                enable = true;
                blockFakenews = true;
                blockGambling = true;
                blockPorn = true;
                };
          }
          inputs.jovian.nixosModules.default # includes gaming optimizations
          inputs.nix-gaming.nixosModules.platformOptimizations # includes gaming optimizations
          # In bash, enter nix repl
          # In nix repl, enter  :l <nixpkgs>
          # Enter linuxPackages_ and then hit the tab key to see the list of available GNU/Linux kernels
          # See https://www.youtube.com/watch?v=EfUCWy4kzZc
          # ./kernel/cachyos.nix # sometimes, cachyos kernel cannot mount NTFS partitions
          # ./kernel/cachyos-hardened.nix # cannot reliably connect to wifi networks, sometimes yes, sometimes no
          # + cannot mount usb sticks 
          # + average FPS framerate for hardened kernel is 11 FPS lower compared to ./kernel/cachyos.nix
          ./kernel/latest.nix # Plantronics Blackwire Headsets working in Google Meet 
          #./kernel/testing.nix # causes pcloud client to use 100% CPU
          #./kernel/xanmod.nix # this gaming kernel seems to be stable and working well in games
          #./kernel/zen.nix
          # lix-module.nixosModules.default # replaces nix package manager
          ./machine/ulysses-desktop/hardware-configuration.nix
          ./machine/ulysses-desktop/hostname.nix
          ./machine/ulysses-desktop/libvirtd.nix # enable libvirtd
          ./machine/ulysses-desktop/ntfs-3g.nix # mount ntfs partition
          ./machine/ulysses-desktop/nvidia.nix # replaces common-gpu-nvidia-nonprime module
          # 2024/8/15: nixos-hardware.nixosModules.common-gpu-nvidia-nonprime is not 
          # compatible with combination of nvidia driver + cinnamon X11 -> need to use 
          # nvidia.nix instead to make cinnamon x11 work
          nix-index-database.nixosModules.nix-index
          nixos-hardware.nixosModules.common-cpu-amd
          nixos-hardware.nixosModules.common-cpu-amd-raphael-igpu
          nixos-hardware.nixosModules.common-gpu-amd
          # nixos-hardware.nixosModules.common-gpu-nvidia-nonprime # avoid, does not work with cinnamon x11 + nvidia
          nur.modules.nixos.default
          ./services/mount-ios-device.nix # to allow mounting ios 16 devices (iphone/ipad) - not working on ios 17
          # libbeidpkcs11.so in eid-mw causes coredump and 
          # constant restarting/crashloop of steamwebhelper part of steam client!!
          # So cannot use ./services/pcscd.nix and steam at same time, unfortunately
          # ./services/i2pd.nix # use i2pd to anonymize and encrypt bittorrent connections
          ./services/pcscd.nix # to enable eid card reader -> eid-mw causes steam to crashloop!
          ./services/podman-nvidia.nix # to enable ollama AI model loader and podman + cuda-toolkit 
          # ./services/podman-nvidia.nix # consumes 392 MB of video RAM, so disable when not needed! 
          stylix.nixosModules.stylix ./common/stylix.nix
          ./user/ulysses-desktop.nix
       ];
       extraArgs = let flake = builtins.getFlake (builtins.toString ./.);
        in { inherit (flake) inputs; };
   };

   nixosConfigurations.ulysses-laptop = nixpkgs.lib.nixosSystem {
       # pkgs-unstable can then be referenced in a Home Manager submodule
        system = "x86_64-linux";
       # specialArgs -> Set all inputs parameters as special arguments for all submodules,
       # so you can directly use all dependencies in inputs in submodules
       # specialArgs = inputs; -> do not use, as it causes an infinite recursion error!
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/nixos-flake-and-module-system
        specialArgs = {inherit inputs pkgs;};
        modules = [ 
          #./bootmethod/grub.nix
          ./bootmethod/systemd-boot.nix
          chaotic.nixosModules.default # Their default module
          ./common/ai-packages.nix
          #./common/astronomy-packages.nix # to test packages before deploying on Gerard's PC
          ./common/common.nix
          ./common/extra-packages.nix
          #./common/gaming-packages.nix
          # keep using printer-packages.nix on ulysses-laptop to check if any of the printer packages are broken or not:
          ./common/minimal-packages.nix
          ./common/printer-packages.nix # to test packages before deploying on Gerard's PC
          ./country/belgium-english-qwerty.nix
          ./desktopmanager/cinnamon.nix # cinnamon uses too much CPU on ulysses-laptop using xorg/x11
          # ./desktopmanager/lxqt.nix # broken!
          # ./desktopmanager/plasmax11.nix # Plasma 6 uses too much RAM nowadays
          # ./desktopmanager/xfce.nix
          # https://flakehub.com/flake/DeterminateSystems/determinate?view=readme
          determinate.nixosModules.default
          disko.nixosModules.disko
          # See https://nix-community.github.io/home-manager/index.html#ch-nix-flakes
          home-manager.nixosModules.home-manager
          {
            # Purpose of home-manager.backupFileExtension:
            # On activation move existing files by appending the given 
            # file extension rather than exiting with an error.
            home-manager.backupFileExtension = "backup";
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.ulysses = import ./machine/ulysses-laptop/home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
          }
         hosts.nixosModule {
             networking.stevenBlackHosts = {
                enable = true;
                blockFakenews = true;
                blockGambling = true;
                blockPorn = true;
                };
          }
          #./kernel/cachyos.nix # sometimes, cachyos kernel cannot mount NTFS partitions
          # ./kernel/cachyos-hardened.nix # cannot reliably connect to wifi networks, sometimes yes, sometimes no
          # + cannot mount usb sticks 
          # + average FPS framerate for hardened kernel is 11 FPS lower compared to ./kernel/cachyos.nix
          ./kernel/latest.nix # Plantronics Blackwire Headsets working in Google Meet 
          #./kernel/latest-hardened.nix # cannot mount usb sticks+not compatible with 
          # waydroid+Plantronics Blackwire Headsets not working in Google Meet + compiling
          # cachyos hardened kernel takes more than 8 hours
          #./kernel/tkg.nix # not working
          #./kernel/xanmod.nix
          #./kernel/zen.nix
          # lix-module.nixosModules.default # replaces nix package manager
          ./machine/ulysses-laptop/disko-config.nix
          ./machine/ulysses-laptop/hardware-configuration.nix
          ./machine/ulysses-laptop/hostname.nix
          nix-index-database.nixosModules.nix-index
          nixos-hardware.nixosModules.common-cpu-intel
          nixos-hardware.nixosModules.common-pc-laptop-ssd
          nur.modules.nixos.default
          ./services/mount-ios-device.nix # to allow mounting ios 16 devices (iphone/ipad) - not working on ios 17
          # libbeidpkcs11.so in eid-mw causes coredump and 
          # constant restarting/crashloop of steamwebhelper part of steam client!!
          # So cannot use ./services/pcscd.nix and steam at same time, unfortunately
          ./services/pcscd.nix # to enable eid card reader -> eid-mw causes steam to crashloop!
          ./services/podman-no-nvidia.nix # to enable ollama AI model loader and podman
          stylix.nixosModules.stylix ./common/stylix.nix
          ./user/ulysses-laptop.nix
       ];
       extraArgs = let flake = builtins.getFlake (builtins.toString ./.);
        in { inherit (flake) inputs; };
    };

    nixosConfigurations.didi-laptop = nixpkgs.lib.nixosSystem {
       # 2023/10/16: Solved USB keyboard and mouse stuck issue on didi-laptop and ulysses-desktop 
       # by disabling powertop service in ./common/common.nix

       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/nixos-with-flakes-enabled
       # specialArgs = inputs; -> do not use, as it causes an infinite recursion error!
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/downgrade-or-upgrade-packages
       # The `specialArgs` parameter passes the
       # non-default nixpkgs instances to other nix modules
       # pkgs-unstable can then be referenced in a Home Manager submodule
       system = "x86_64-linux";
       specialArgs = {inherit inputs pkgs;};
       modules = [ 
          ./bootmethod/systemd-boot.nix
          chaotic.nixosModules.default # Their default module
          ./common/common.nix
          ./common/minimal-packages.nix
          ./common/printer-packages.nix
          ./country/belgium-english.nix
          ./desktopmanager/cinnamon.nix
          # https://flakehub.com/flake/DeterminateSystems/determinate?view=readme
          determinate.nixosModules.default
          disko.nixosModules.disko
          # See https://nix-community.github.io/home-manager/index.html#ch-nix-flakes
          home-manager.nixosModules.home-manager
          {
            # Purpose of home-manager.backupFileExtension:
            # On activation move existing files by appending the given 
            # file extension rather than exiting with an error.
            home-manager.backupFileExtension = "backup";
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.didi = import ./machine/didi-laptop/home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
          }
         hosts.nixosModule {
             networking.stevenBlackHosts = {
                enable = true;
                blockFakenews = true;
                blockGambling = true;
                blockPorn = true;
                blockSocial = true;
                };
          }
          #./kernel/cachyos.nix # sometimes, cachyos kernel cannot mount NTFS partitions
          # ./kernel/cachyos-hardened.nix # cannot reliably connect to wifi networks, sometimes yes, sometimes no
          # + cannot mount usb sticks 
          # + average FPS framerate for hardened kernel is 11 FPS lower compared to ./kernel/cachyos.nix
          #./kernel/latest.nix # Plantronics Blackwire Headsets working in Google Meet 
          ./kernel/latest-hardened.nix # cannot mount usb sticks+not compatible with 
          # waydroid+Plantronics Blackwire Headsets not working in Google Meet + compiling
          # cachyos hardened kernel takes more than 8 hours
          #./kernel/xanmod.nix
          #./kernel/zen.nix
          # lix-module.nixosModules.default # replaces nix package manager
          ./machine/didi-laptop/disko-config.nix
          ./machine/didi-laptop/hostname.nix
         nix-index-database.nixosModules.nix-index
          nur.modules.nixos.default
          ./services/mount-ios-device.nix # to allow mounting ios 16 devices (iphone/ipad) - not working on ios 17
          # libbeidpkcs11.so in eid-mw causes coredump and 
          # constant restarting/crashloop of steamwebhelper part of steam client!!
          # So cannot use ./services/pcscd.nix and steam at same time, unfortunately
          ./services/pcscd.nix # to enable eid card reader -> eid-mw causes steam to crashloop!
          stylix.nixosModules.stylix ./common/stylix.nix
          ./user/didi.nix
       ];
       extraArgs = let flake = builtins.getFlake (builtins.toString ./.);
        in { inherit (flake) inputs; };
    };

    nixosConfigurations.noella-laptop = nixpkgs.lib.nixosSystem {
       # 2023/10/16: Solved USB keyboard and mouse stuck issue on noella-laptop and ulysses-desktop 
       # by disabling powertop service in ./common/common.nix

       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/nixos-with-flakes-enabled
       # specialArgs = inputs; -> do not use, as it causes an infinite recursion error!
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/downgrade-or-upgrade-packages
       # The `specialArgs` parameter passes the
       # non-default nixpkgs instances to other nix modules
       # pkgs-unstable can then be referenced in a Home Manager submodule
       system = "x86_64-linux";
       specialArgs = {inherit inputs pkgs;};
       modules = [ 
          ./bootmethod/systemd-boot.nix
          chaotic.nixosModules.default # Their default module
          ./common/common.nix
          ./common/minimal-packages.nix
          ./common/printer-packages.nix
          ./country/belgium-french.nix
          ./desktopmanager/cinnamon.nix
          # https://flakehub.com/flake/DeterminateSystems/determinate?view=readme
          determinate.nixosModules.default
          disko.nixosModules.disko 
          # See https://nix-community.github.io/home-manager/index.html#ch-nix-flakes
          home-manager.nixosModules.home-manager
          {
            # Purpose of home-manager.backupFileExtension:
            # On activation move existing files by appending the given 
            # file extension rather than exiting with an error.
            home-manager.backupFileExtension = "backup";
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.noella = import ./machine/noella-laptop/home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
          }
         hosts.nixosModule {
             networking.stevenBlackHosts = {
                enable = true;
                blockFakenews = true;
                blockGambling = true;
                blockPorn = true;
                blockSocial = false;
                };
          }
          #./kernel/cachyos.nix # sometimes, cachyos kernel cannot mount NTFS partitions
          # ./kernel/cachyos-hardened.nix # cannot reliably connect to wifi networks, sometimes yes, sometimes no
          # + cannot mount usb sticks 
          # + average FPS framerate for hardened kernel is 11 FPS lower compared to ./kernel/cachyos.nix
          ./kernel/latest.nix # Plantronics Blackwire Headsets working in Google Meet 
          #./kernel/latest-hardened.nix # cannot mount usb sticks+not compatible with 
          # waydroid+Plantronics Blackwire Headsets not working in Google Meet + compiling
          # cachyos hardened kernel takes more than 8 hours
          #./kernel/xanmod.nix
          #./kernel/zen.nix
          # lix-module.nixosModules.default # replaces nix package manager
          ./machine/noella-laptop/disko-config.nix
          ./machine/noella-laptop/hostname.nix
         nix-index-database.nixosModules.nix-index
          nur.modules.nixos.default
          ./services/mount-ios-device.nix # to allow mounting ios 16 devices (iphone/ipad) - not working on ios 17
          # libbeidpkcs11.so in eid-mw causes coredump and 
          # constant restarting/crashloop of steamwebhelper part of steam client!!
          # So cannot use ./services/pcscd.nix and steam at same time, unfortunately
          ./services/pcscd.nix # to enable eid card reader -> eid-mw causes steam to crashloop!
          stylix.nixosModules.stylix ./common/stylix.nix
          ./user/noella.nix
       ];
       extraArgs = let flake = builtins.getFlake (builtins.toString ./.);
        in { inherit (flake) inputs; };
    };


      nixosConfigurations.rijckenbergc-laptop = nixpkgs.lib.nixosSystem {
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/nixos-with-flakes-enabled
       # specialArgs = inputs; -> do not use, as it causes an infinite recursion error!
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/downgrade-or-upgrade-packages
       # The `specialArgs` parameter passes the
       # non-default nixpkgs instances to other nix modules
       # pkgs-unstable can then be referenced in a Home Manager submodule
       system = "x86_64-linux";
       specialArgs = {inherit inputs pkgs;};
       modules = [ 
          ./bootmethod/grub.nix
          chaotic.nixosModules.default # Their default module
          ./common/common.nix
          ./common/minimal-packages.nix
          ./common/printer-packages.nix
          ./country/germany.nix
          ./desktopmanager/cinnamon.nix
          # https://flakehub.com/flake/DeterminateSystems/determinate?view=readme
          determinate.nixosModules.default
          disko.nixosModules.disko
          home-manager.nixosModules.home-manager
          {
            # Purpose of home-manager.backupFileExtension:
            # On activation move existing files by appending the given 
            # file extension rather than exiting with an error.
            home-manager.backupFileExtension = "backup";
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.rijckenbergc = import ./machine/rijckenbergc-laptop/home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
          }
          hosts.nixosModule {
             networking.stevenBlackHosts = {
                enable = true;
                blockFakenews = true;
                blockGambling = true;
                blockPorn = true;
                blockSocial = true;
                };
          }
          ./kernel/latest.nix # Plantronics Blackwire Headsets working in Google Meet 
          # lix-module.nixosModules.default # replaces nix package manager
          ./machine/rijckenbergc-laptop/disko-config.nix
          ./machine/rijckenbergc-laptop/hostname.nix
          nix-index-database.nixosModules.nix-index
          nur.modules.nixos.default
          stylix.nixosModules.stylix ./common/stylix.nix
          ./user/rijckenbergc.nix
       ];
       extraArgs = let flake = builtins.getFlake (builtins.toString ./.);
        in { inherit (flake) inputs; };
    };

       nixosConfigurations.gerard-laptop = nixpkgs.lib.nixosSystem {
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/nixos-with-flakes-enabled
       # specialArgs = inputs; -> do not use, as it causes an infinite recursion error!
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/downgrade-or-upgrade-packages
       # The `specialArgs` parameter passes the
       # non-default nixpkgs instances to other nix modules
       # pkgs-unstable can then be referenced in a Home Manager submodule
       system = "x86_64-linux";
       specialArgs = {inherit inputs pkgs;};
       modules = [ 
          ./bootmethod/systemd-boot.nix
          chaotic.nixosModules.default # Their default module
          ./common/astronomy-packages.nix
          ./common/common.nix
          ./common/minimal-packages.nix
          ./common/printer-packages.nix
          ./country/belgium-english.nix
          ./desktopmanager/cinnamon.nix
          # https://flakehub.com/flake/DeterminateSystems/determinate?view=readme
          determinate.nixosModules.default
          disko.nixosModules.disko
          # See https://nix-community.github.io/home-manager/index.html#ch-nix-flakes
          home-manager.nixosModules.home-manager
          {
            # Purpose of home-manager.backupFileExtension:
            # On activation move existing files by appending the given 
            # file extension rather than exiting with an error.
            home-manager.backupFileExtension = "backup";
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.gerard = import ./machine/gerard-laptop/home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
          }
          hosts.nixosModule {
             networking.stevenBlackHosts = {
                enable = true;
                blockFakenews = true;
                blockGambling = true;
                blockPorn = true;
                blockSocial = true;
                };
          }
          #./kernel/cachyos.nix # sometimes, cachyos kernel cannot mount NTFS partitions
          # ./kernel/cachyos-hardened.nix # cannot reliably connect to wifi networks, sometimes yes, sometimes no
          # + cannot mount usb sticks 
          # + average FPS framerate for hardened kernel is 11 FPS lower compared to ./kernel/cachyos.nix
          ./kernel/latest.nix # Plantronics Blackwire Headsets working in Google Meet 
          #./kernel/xanmod.nix
          #./kernel/zen.nix
          # lix-module.nixosModules.default # replaces nix package manager
          ./machine/gerard-laptop/disko-config.nix
          ./machine/gerard-laptop/hostname.nix
          nix-index-database.nixosModules.nix-index
          nur.modules.nixos.default
          stylix.nixosModules.stylix ./common/stylix.nix
          ./user/gerard.nix
       ];
       extraArgs = let flake = builtins.getFlake (builtins.toString ./.);
        in { inherit (flake) inputs; };
    };

    nixosConfigurations.gerard-laptop-iso = nixpkgs.lib.nixosSystem {
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/nixos-with-flakes-enabled
       # Based on https://github.com/vimjoyer/custom-installer-video
       # specialArgs = inputs; -> do not use, as it causes an infinite recursion error!
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/downgrade-or-upgrade-packages
       # The `specialArgs` parameter passes the
       # non-default nixpkgs instances to other nix modules
       # pkgs-unstable can then be referenced in a Home Manager submodule
       system = "x86_64-linux";
       specialArgs = {inherit inputs pkgs;};
       modules = [ 
          ./bootmethod/systemd-boot.nix # systemd-boot must be used to build iso image based on minimal installer
          chaotic.nixosModules.default # Their default module
          #./common/astronomy-packages.nix
          ./common/common.nix
          ./common/minimal-packages.nix
          ./common/printer-packages.nix
          ./country/belgium-english.nix
          ./desktopmanager/cinnamon.nix
          # https://flakehub.com/flake/DeterminateSystems/determinate?view=readme
          determinate.nixosModules.default
          # See https://nix-community.github.io/home-manager/index.html#ch-nix-flakes
          home-manager.nixosModules.home-manager
          {
            # Purpose of home-manager.backupFileExtension:
            # On activation move existing files by appending the given 
            # file extension rather than exiting with an error.
            home-manager.backupFileExtension = "backup";
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.gerard = import ./machine/gerard-laptop/home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
          }
          hosts.nixosModule {
             networking.stevenBlackHosts = {
                enable = true;
                blockFakenews = true;
                blockGambling = true;
                blockPorn = true;
                blockSocial = true;
                };
             }
          #./kernel/cachyos.nix # cachyos kernel cannot be used to build iso image
          ./kernel/latest.nix # latest kernel must be used instead of cachyos to build iso image
          # lix-module.nixosModules.default # replaces nix package manager
          # disko.nixosModules.disko # disko cannot be used to build iso image
          # ./machine/gerard-laptop/disko-config.nix # disko cannot be used to build iso image
          # Based on https://github.com/vimjoyer/custom-installer-video :
          # Command to build iso image: 
          # cd tmp;sudo nix build /etc/nixos/#nixosConfigurations.gerard-laptop-iso.config.system.build.isoImage
          # Takes around 1h10 minutes to build iso image on ulysses-laptop
          ./machine/gerard-laptop/gerard-laptop-iso.nix
          ./machine/gerard-laptop/hostname.nix
          nix-index-database.nixosModules.nix-index
          nur.modules.nixos.default
          ./user/gerard.nix
       ];
       extraArgs = let flake = builtins.getFlake (builtins.toString ./.);
        in { inherit (flake) inputs; };
    };

# 2023/10/6: Raspberry Pi 4 configuration based on following info:
# MOST IMPORTANT DOCUMENT: https://mtlynch.io/nixos-pi4/
# NixOS image to use for Raspberry Pi 4: https://hydra.nixos.org/build/231913696/download/1/nixos-sd-image-23.11pre515819.8ecc900b2f69-aarch64-linux.img.zst
# https://nixos.wiki/wiki/NixOS_on_ARM#:~:text=NixOS%20has%20support%20for%20these%20boards%20using%20AArch64,as%20much%20is%20supported%20as%20Mainline%20Linux%20supports.
# http://blog.tiserbox.com/posts/2023-05-12-install-nixos-on-a-raspberry-pi.html
# https://wiki.nixos.org/wiki/NixOS_on_ARM/Raspberry_Pi_4

       nixosConfigurations.rpi = nixpkgs.lib.nixosSystem {
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/nixos-with-flakes-enabled
       # specialArgs = inputs; -> do not use, as it causes an infinite recursion error!
       # Based on https://nixos-and-flakes.thiscute.world/nixos-with-flakes/downgrade-or-upgrade-packages
       # The `specialArgs` parameter passes the
       # non-default nixpkgs instances to other nix modules
       # pkgs-unstable can then be referenced in a Home Manager submodule
        system = "aarch64-linux";
        specialArgs = {inherit inputs;};  # disabled use of lix-module by not inheriting pkgs!
        
       # Raspberry Pi 4 Family is only supported as AArch64. Use as armv7 is community supported:
       modules = [ 
         # home-manager.nixosModules.home-manager
         # {
         # Purpose of home-manager.backupFileExtension:
         # On activation move existing files by appending the given 
         # file extension rather than exiting with an error.
         #   home-manager.backupFileExtension = "backup";
         #   home-manager.useGlobalPkgs = false;
         #   home-manager.useUserPackages = true;
         #   home-manager.users.rpi = import ./machine/rpi/home.nix;
         #
         # Optionally, use home-manager.extraSpecialArgs to pass
         # arguments to home.nix
         # }
          # add your model from this list: https://github.com/NixOS/nixos-hardware/blob/master/flake.nix
          nixos-hardware.nixosModules.raspberry-pi-4
          chaotic.nixosModules.default # Their default module
          nur.modules.nixos.default
          ./bootmethod/generic-extlinux-compatible.nix
          # ./common/common.nix
          ./country/belgium-english.nix
          ./desktopmanager/cinnamon.nix
          # https://flakehub.com/flake/DeterminateSystems/determinate?view=readme
          determinate.nixosModules.default
          ./kernel/rpi.nix
          disko.nixosModules.disko
          # lix-module.nixosModules.default # replaces nix package manager
          ./machine/rpi/disko-config.nix
          ./machine/rpi/hostname.nix
          # ./machine/rpi/image-aarch64.nix
          ./user/rpi.nix
          hosts.nixosModule {
             networking.stevenBlackHosts = {
                enable = true;
                blockFakenews = true;
                blockGambling = true;
                blockPorn = true;
                blockSocial = true;
                };
          }
       ];
       extraArgs = let flake = builtins.getFlake (builtins.toString ./.);
        in { inherit (flake) inputs; };
    };

 };
}

# File creation date: 2023/6/19
# Last modification date: 2024/8/27

# flake.nix file for NixOS 23.11 or newer 
# Author: Mark Rijckenberg
# File based on https://raw.githubusercontent.com/ChrisTitusTech/nixos-titus/main/configuration.nix
# How to run NixOS in any systemd GNU/Linux Distro using systemd-nspawn Containers:
# https://nixcademy.com/2023/08/29/nixos-nspawn/
# https://github.com/tfc/nspawn-nixos
# Find all NixOS packages here: https://nixos.org/
# Find all versions of a NixOS package: https://lazamar.co.uk/nix-versions/?channel=nixpkgs-unstable&package=
# Find all NixOS packages here: https://mynixos.com/
# Find all NUR packages here: https://nur.nix-community.org/
# Find Nix flakes here: https://flakehub.com/search
# Flakestry is an open source project with the goal of making it easier to publish and discover Flakes.
# Find Nix flakes here: : https://flakestry.dev/
# Find full logs of build failures for nix packages here: https://hydra.nixos.org/
# https://determinate.systems/posts/introducing-flakehub
# Best beginner's guide: NixOS & Flakes Book - An unofficial book for beginners: https://nixos-and-flakes.thiscute.world/
# NixOS manual: https://nixos.org/manual/nixos/stable/
# Extra hardware support: https://github.com/NixOS/nixos-hardware
# Distro comparisons: https://repology.org/
# https://discourse.nixos.org/t/hdn-1-0-0-install-a-package-with-hdn-add-pkgs-hello/28469
# https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
# https://nixos.wiki/wiki/NixOS_Generations_Trimmer
# NixOS Anywhere - Install NixOS everywhere via ssh:
# https://github.com/numtide/nixos-anywhere?ref=numtide.com
# Awesome-nix: A curated list of the best resources in the Nix community:
# https://nix-community.github.io/awesome-nix/
# See also https://gti.telent.net/dan/liminix
# Liminix - A Nix-based system for configuring consumer wifi routers 
# or IoT device devices, of the kind that OpenWrt or DD-WRT or 
# Gargoyle or Tomato run on. It's a reboot/restart/rewrite of NixWRT.
# See https://www.jjpdev.com/posts/home-router-nixos/
# See https://github.com/ghostbuster91/blogposts/blob/a2374f0039f8cdf4faddeaaa0347661ffc2ec7cf/router2023-part2/main.md
# See also https://flakehub.com/flake/astro/nix-openwrt-imagebuilder
# See also https://github.com/nix-community/robotnix - build Android OS using nix
# Installing NixOS on Raspberry Pi 4: https://mtlynch.io/nixos-pi4/
# Home manager configuration template:  https://github.com/Misterio77/nix-starter-configs/blob/main/standard/flake.nix
# nix-flatpak can install flatpak packages declaratively via .nix configuration:
# https://github.com/gmodena/nix-flatpak/tree/main
# Discussions about lix package manager: https://app.element.io/#/room/#discuss:lix.systems

# RAM requirements to use this flake: at least 8 GB of RAM required!
# DISKSPACE: This script will initially use minimum 60 GB of diskspace ( 56 GB for root partition and 4.2 GB for home+pcloud data))
# DISKSPACE: Reserve 1 GB for FAT32 /boot partition and set boot flag on this partition
# DISKSPACE: Reserve 0 GB for SSD swap partition and 8 GB diskspace for mechanical HD swap partition
# DISKSPACE: Reserve at least 100 GB of diskspace for / (root) F2FS partition for SSD - F2FS compresses better than ext4 on SSD
# DISKSPACE: Install all games in /tmp to avoid clogging up /home directory with files that should not be backed up
# DISKSPACE: Always keep at least 40 GB of diskspace free on / (root) to support large software updates
# DURATION: install duration to run "date > /tmp/1; sudo time nixos-rebuild switch --upgrade-all ; date > /tmp/2; echo 'start time:' ; cat /tmp/1; echo 'end time:'; cat /tmp/2"
# using the config file below: around 26 minutes on old SSD drive from the year 2016

# PRE-INSTALL STEPS:
# 1) update BIOS
# 2) backup wireless password and home directory, including ~/.config subfolder

# POST-INSTALL STEPS:
# 3) extract config_backup_<timestamp>.zip from usb stick into home directory , then reboot PC
# 4) copy backed up documents/files from usb stick to ~/backup in home directory
# 5) set background wallpaper to /etc/nixos/desktopmanager/wallpaper.jpg
# 6) Improve privacy: Load chrome://flags/#encrypted-client-hello in the Chromium browser's address bar.
# Set the status of the Encrypted Client Hello (ECH) flag to Enabled. 
# https://blog.cloudflare.com/announcing-encrypted-client-hello/

# 7) Can be skipped thanks to step 3: Put following program icons in desktop panel for easy access: 
# warp-terminal (to update packages),boxbuddy (to update packages) ,protonup-qt (to update packages)
# anydesk, bitwarden, brave, chrome (google chrome, also used to import/export bookmarks), chromium, librewolf (on alpine-edge or rhino-linux-latest container),
# gitkraken, heroic launcher, hypnotix, krusader, okular (to digitally sign PDF files), pcloud, printer icon, proton vpn, 
# qbittorrent, spacefm, steam, teamviewer, waveterm (terminal)
# 8) Can be skipped thanks to home-manager: copy bitwarden.desktop and maestral.desktop files from https://gitlab.com/Rijckenberg/shell-scripts to ~/.config/autostart
# 9) Can be skipped thanks to step 3:Under Themes - set Icons to Numix-Circle-Light
# 10) Can be skipped thanks to step 3:force Chromium web browser to open
# https://searx.prvcy.eu/ (SearXNG meta search engine) on each startup (manually configure in Chromium settings)
# 11) Disable "block popup windows" in Firefox,Chromium and Google Chrome browser to allow printing mail.google.com emails via Firefox browser -> Firefox is used by dad


# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

