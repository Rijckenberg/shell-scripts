# Last modification date: 2024/5/30
# Based on https://nmariusp.github.io/install-os.html 
# https://www.youtube.com/watch?v=fSXWlE0w-ow
# https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbmZxLUc2dkdPaFpNVWJvTktwOG50YndHVFNQd3xBQ3Jtc0ttQndDVXBaV0tjcUFxdkpLblNfaE52Yk5IeDU4a3ZDZlk0a2lZYl9HWlVqTnlPOGJqeThCZ3YzTlFWd0FYeDZjU0xSNk5aZk1rd3pxYnJpOVNHbkYyX3dLWVYxTFRSanZBWWVJcnVpaXZIQ1BwYnRQOA&q=https%3A%2F%2Frsadowski.de%2Fposts%2F2024-01-09-openbsd-kde%2F&v=fSXWlE0w-ow
# https://rsadowski.de/posts/2024-01-09-openbsd-kde/
# OpenBSD package search: https://openbsd.app/?search=&current=on

# Install even more packages using nix package manager or pkgsrc package manager:
# pkgsrc is a framework for managing third-party software on UNIX-like systems,
# currently containing over 26,000 packages
# https://www.pkgsrc.org/#index3h2

# Run following commands after running "doas su"
pkg_add -v bash git mc emacs--no_x11
chsh -s /usr/local/bin/bash
pkg_add amd-firmware
pkg_add amdgpu-firmware
pkg_add axel
pkg_add bat
pkg_add btop
# buku - buku is a command-line utility to store, tag, search and organize bookmarks.
pkg_add buku
pkg_add chromium
pkg_add curl
pkg_add dmidecode
pkg_add dos2unix
pkg_add duf
pkg_add exfat-fuse
pkg_add ffmpeg
pkg_add firefox
pkg_add flameshot
pkg_add flashrom
pkg_add ggrep
pkg_add git
pkg_add glances
pkg_add gnumeric
pkg_add hunspell
pkg_add intel-firmware
# KDE Plasma 6.0.5:
pkg_add kde-plasma
pkg_add kde-plasma-extras
pkg_add kf6-kdeplasma-addons
pkg_add kf6-systemsettings
pkg_add kpipewire
pkg_add krusader
pkg_add libplasma
pkg_add libreoffice
pkg_add mpv
pkg_add ncdu
pkg_add neofetch
pkg_add newsboat
# nix - Nix is a powerful package manager for Linux and other Unix systems
# that makes package management reliable and reproducible. It provides atomic
# upgrades and rollbacks, side-by-side installation of multiple versions
# of a package, multi-user package management and easy setup of build
# environments.
pkg_add nix
pkg_add ntfs_3g
pkg_add ocean-sound-theme
pkg_add oxygen-sounds
pkg_add p7zip
pkg_add p7zip-rar
pkg_add pan
pkg_add parallel
pkg_add pdfgrep
pkg_add pipewire
pkg_add plasma-browser-integration
# plasma-pa - Plasma applet for audio volume management using PulseAudio.
pkg_add plasma-pa
pkg_add plasma-workspace-wallpapers
pkg_add polkit-kde-agent
pkg_add qbittorrent
# rbw - unofficial command line client for Bitwarden
pkg_add rbw
pkg_add rclone
pkg_add ripgrep
pkg_add rsync
pkg_add scribus
pkg_add shellcheck
pkg_add simple-mtpfs
pkg_add smplayer
pkg_add tigervnc
# typst - Typst is a new markup-based typesetting system that is
# designed to be as powerful as LaTeX while being much easier to learn and use.
pkg_add typst
pkg_add unrar
pkg_add unzip
pkg_add usbutils
pkg_add vlc
pkg_add web-eid-app
pkg_add web-eid-chrome
pkg_add web-eid-firefox
pkg_add web-eid-native
pkg_add wget
pkg_add wireplumber
pkg_add xlsx2csv
pkg_add yt-dlp
