#!/usr/bin/env bash
# Modification date: 2024/6/14
# Author: Mark Rijckenberg
# Required diskspace for software package install: 4 GB 
# See https://github.com/noplacenoaddress/npna/blob/master/6%20-%20OpenBSD%20up%20on%20the%20Alps%2C%20vmm%20and%20Alpine%20linux.md
# See list of available Alpine packages here: https://pkgs.alpinelinux.org/packages?name=*firefo*&branch=edge&repo=&arch=&maintainer=
distrobox-export --delete -a firefox
distrobox-export --delete -a firefox
distrobox-export --delete -a firefox
distrobox-export --delete -a firefox
distrobox-export --delete -a firefox
distrobox-export --delete -a firefox
distrobox-export --delete -a firefox
distrobox-export --delete -a firefox

sudo cp world /etc/apk/world
sudo cp repositories /etc/apk/repositories
sudo dos2unix /etc/apk/world
sudo dos2unix /etc/apk/repositories
sudo apk update
sudo apk upgrade
sudo apk del xz
# Use firefox web browser in Alpine Linux container for secure web banking
# See https://github.com/noplacenoaddress/npna/blob/master/6%20-%20OpenBSD%20up%20on%20the%20Alps%2C%20vmm%20and%20Alpine%20linux.md
# Enable NextDNS in firefox security settings via about:preferences#privacy
distrobox-export --app firefox