#!/usr/bin/env bash
# Modification date: 2025/3/5
# Author: Mark Rijckenberg
echo "Arch Installer"
echo "https://github.com/archlinux/archinstall"

sudo pacman -S aura
sudo pacman -S base-devel
sudo pacman -S cargo
sudo pacman -S git
sudo pacman -S pkgfile
sudo pkgfile --update
############################################################
# See https://github.com/fosskers/aura
# aura 4.0 or newer is best package manager for Arch Linux
# !!!!! ATTENTION: As of the 4.x series, sudo is no longer necessary when running Aura.
# When escalated privileges are required, Aura will automatically prompt you.
# Aura does not augment or alter pacman's commands in any way.
# -S yields repository packages and only those. In Aura, the -A operation is introduced for
# obtaining AUR packages.
cd /opt
sudo git clone https://aur.archlinux.org/aura.git
sudo chown -R ulysses:users ./aura
cd aura
makepkg -s
makepkg -si
aura check
aura conf --gen > ~/.config/aura/config.toml
############################################################
cd /opt
sudo git clone https://aur.archlinux.org/yay.git
sudo chown -R ulysses:users ./yay
cd yay
makepkg -si
############################################################
# Refresh the system packages and upgrade:
# aura 4.0 or newer is best package manager for Arch Linux
# !!!!! ATTENTION: As of the aura 4.x series, sudo is no longer necessary when running Aura.
# When escalated privileges are required, Aura will automatically prompt you.
# Aura does not augment or alter pacman's commands in any way.
# -S yields repository packages and only those. In Aura, the -A operation is introduced for
# obtaining AUR packages.
aura -Ayu
aura -Syu
# sudo yay -Syu
# https://fosskers.github.io/aura/aur.html#:~:text=Let%27s%20walk%20through%20the%20full%20process%20of%20installing,-Au.%20Remember%2C%20no%20need%20to%20call%20sudo%20yourself.
# https://aur.archlinux.org/packages/archlinux-tweak-tool-git
aura -As archlinux-tweak-tool-git
aura -Ai archlinux-tweak-tool-git
aura -A archlinux-tweak-tool-git
