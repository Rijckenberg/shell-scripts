# Copied from https://git.asdf.cafe/majekla/freebsd/raw/branch/master/freebsd-desktop.sh
# FreeBSD source code search engine: http://bxr.su/
# FreeBSD package search engine: https://ports.freebsd.org/cgi/ports.cgi
#!/bin/sh

# WARNING: first run "sudo dos2unix install-freebsd-desktop-env.sh" on this file to fix formatting errors
# Then run using "sudo sh install-freebsd-desktop-env.sh"
# Only run this script from an already installed GhostBSD running from a harddisk or SSD
# Do not run this script from a LiveUSB session!
# Running this script from LiveUSB will cause out-of-diskspace errors

# Install even more packages using nix package manager or pkgsrc package manager:
# pkgsrc is a framework for managing third-party software on UNIX-like systems,
# currently containing over 26,000 packages
# https://www.pkgsrc.org/#index3h2
# List of all pkgsrc packages: https://cdn.netbsd.org/pub/pkgsrc/current/pkgsrc/index-all.html

clear

# check internet connection

    printf "\n"
    echo "Looking for Internet Connection, please wait..."
    sleep 1

    # Check ping with cloudflare
    destination="1.1.1.1"

    # Number of ping attempts
    num_attempts=3

    # Check internet connection with ping
    if ! ping -c "$num_attempts" -q "$destination" > /dev/null; then

        printf "\n"
        echo "-------------------------------------------"
        echo "Your computer is not connected to internet, I can't go on"
        printf "\n"
        exit 0

    fi


# install pkg

    printf "\n"
    echo "Looking for 'pkg', please wait..."
    sleep 1

    if [ ! -x /usr/local/sbin/pkg ]; then

        # Check if pkg can be installed
        if pkg ins -y 2>&1 | grep -q "error"; then

            printf "\n"
            echo "-------------------------------------------"
            echo "I can't install pkg, please check your internet connection"
            printf "\n"
            exit 0

        fi
    fi


# install bsddialog

    printf "\n"
    echo "Looking for 'bsddialog', please wait..."
    sleep 1

    if [ ! -x "/usr/local/bin/bsddialog" ]; then

        if pkg ins -y bsddialog | grep -q "error"; then

            printf "\n"
            echo "-------------------------------------------"
            echo "I can't install bsddialog, please check your internet connection"
            printf "\n"
            exit 0

        fi

    fi

##############################################################################################

# Install usual programs
TMPFILE=$(mktemp)

bsddialog --backtitle "Select programs" \
        --title "Installing applications" \
        --checklist "Select programs:" 30 70 20 \
        "abiword " "Text editor" on \
        "acsccid " "PC/SC driver for CS CCID smart card readers" on \
        "anydesk " "Fast remote desktop" on \
        "audacity " "Audacity audio editor" on \
        "axel " "Download accelerator" on \
        "bash " "GNU Project's Bourne Again SHell" on \
        "beid " "Belgian eID middleware" on \
        "beidconnect " "Belgian eID digital signature browser extension backend" on \
        "bottom " "Graphical process and system monitor" on \
        "browsers " "Chrome browser for Netflix" on \
        "btop " "Monitor of resources" on \
        "cardpeek " "Tool for reading the contents of ISO 7816 smart cards" on \
        "chkrootkit " "Tool to locally check for signs of a rootkit" on \
        "chromium " "Google web browser based on WebKit" on \
        "cpu-microcode " "Meta-package for CPU microcode updates" on \
        "cpu-x " "Gathers information about CPU, motherboard, and more" on \
        "curl " "Command line tool and library for transferring data with URLs" on \
        "czkawka " "gui: cleaner - A simple, fast and easy to use app to remove unnecessary files from your computer" on \
        "de-hunspell " "German hunspell dictionaries" on \
        "dmidecode " "Tool for dumping DMI (SMBIOS) contents in human-readable format" on \
        "doas " "Simple sudo alternative to run commands as another user" on \
        "dos2unix " "DOS/Mac to Unix and vice versa text file format converter" on \
        "drawio " "Diagramming and whiteboarding desktop app" on \
        "element-web " "Glossy Matrix collaboration client for the web" on \
        "en-hunspell " "English hunspell dictionaries" on \
        "exfat-utils " "Utilities to create, check, label and dump exFAT filesystem" on \
        "fastfetch " "Like neofetch, but much faster because written in C" on \
        "fusefs-exfat " "Full-featured exFAT FS implementation as a FUSE module" on \
        "fusefs-ifuse " "FUSE-based filesystem for mounting iOS devices over USB" on \
        "ffmpeg " "Realtime audio/video encoder/converter and streaming server" on \
        "filezilla " "FileZilla FTP client" on \
        "firefox " "Firefox web browser" on \
        "flac " "Free lossless audio codec" on \
        "flameshot " "Powerful yet simple to use screenshot software" on \
        "foliate " "Simple and modern GTK eBook viewer" on \
        "foremost " "Console program to recover files based on their headers and footers" on \
        "fr-hunspell " "Modern French hunspell dictionaries" on \
        "fzf " "Blazing fast command-line fuzzy finder" on \
        "gcc " "Meta-port for the default version of the GNU Compiler Collection" on \
        "gedit " "Small but powerful text editor for the GNOME Desktop Environment" on \
        "gimp " "GIMP image editor" on \
        "git " "Distributed source code management tool" on \
        "gsmartcontrol " "Graphical user interface for smartmontools" on \
        "gtk-mixer " "Sound controller" on \
        "gzdoom " "GL-enhanced source port for Doom-engine games" on \
        "handbrake " "HandBrake video encoder" on \
        "hexchat " "HexChat IRC client" on \
        "hypnotix " "IPTV streaming application using mpv" on \
        "inxi " "Full featured CLI system information tool" on \
        "kate " "Basic editor framework for the KDE system" on \
        # "kde6-devel " "KDE Plasma Desktop and Applications (current)" on \
        "kiwix-tools " "Command-line Kiwix tools - free offline access to 12 GB of Boundless course material" on \
        "konsole " "KDE terminal emulator" on \
        "krename " "Powerful batch file renamer for KDE" on \
        "krita " "Sketching and painting program" on \
        "krusader " "Twin panel file manager for KDE, like midnight or norton commander" on \
        "libreoffice " "Full integrated office productivity suite" on \
        "librewolf " "Custom version of Firefox, focused on privacy, security and freedom" on \
        "linux-wps-office " "Complete office suite with PDF editor" on \
        "lynis " "Security and system auditing tool" on \
        "mp3gain " "Tool to normalize the gain of MP3 files" on \
        "mplayer " "High performance media player supporting many formats" on \
        "mpv " "Free and open-source general-purpose video player" on \
        "mucommander " "Lightweight file manager featuring a Norton Commander style interface" on \
        "nix " "Purely functional package manager" on \
        "nmap " "Nmap network discovery tool" on \
        "nomacs " "easy image viewer/editor" on \
        "nl-hunspell " "Dutch hunspell dictionaries" on \
        "nss " "Libraries to support development of security-enabled applications" on \
        "octopkg " "Graphical front-end to the FreeBSD pkg-ng package manager" on \
        "pcsc-lite " "Middleware library to access a smart card using SCard API (PC/SC)" on \
        "pcsc-tools " "Tools to test a PCSC driver, card, or reader" on \
        "plasma6-breeze " "Artwork, styles and assets for the Breeze visual style for the Plasma Desktop" on \
        "plasma6-discover " "Plasma package management tools" on \
        "plasma6-kde-cli-tools " "Tools based on KDE Frameworks to better interact with the system" on \
        "plasma6-kde-gtk-config " "Syncs KDE settings to GTK applications" on \
        "plasma6-kdeplasma-addons " "All kind of add-ons to improve your Plasma experience" on \
        "plasma6-kinfocenter " "View information about your computer's hardware" on \
        "plasma6-kpipewire " "Components relating to Flatpak pipewire use in Plasma" on \
        "plasma6-kwayland " "Qt-style Client and Server library wrapper for the Wayland libraries" on \
        "plasma6-kwin " "Easy to use, but flexible, X Window Manager and Wayland Compositor" on \
        "plasma6-layer-shell-qt " "Qt component to allow applications to make use of the Wayland wl-layer-shell protocol" on \
        "plasma6-libksysguard " "Library to retrieve information on the current status of computer hardware" on \
        "plasma6-libplasma " "Shared libraries for Plasma Desktop" on \
        "plasma6-oxygen " "Oxygen Style for Qt/KDE Applications" on \
        "plasma6-plasma " "KDE6 plasma meta port" on \
        "plasma6-plasma-browser-integration " "Components necessary to integrate browsers into the Plasma Desktop" on \
        "plasma6-plasma-desktop " "KDE Plasma Desktop" on \
        "plasma6-plasma-disks " "Monitors S.M.A.R.T. capable devices for imminent failure" on \
        "plasma6-plasma-integration " "Qt Platform Theme integration plugins for the Plasma workspaces" on \
        "plasma6-plasma-pa " "Plasma applet for audio volume management using PulseAudio" on \
        "plasma6-plasma-systemmonitor " "Interface for monitoring system sensors, process information and other system resources" on \
        "plasma6-plasma-workspace " "KDE Plasma Workspace" on \
        "plasma6-plasma-workspace-wallpapers " "Wallpapers for Plasma Workspaces" on \
        "plasma6-systemsettings " "Control center to configure your Plasma Desktop" on \
        "py39-glances " "CLI curses based monitoring tool for GNU/Linux and BSD OS" on \
        "py39-magic-wormhole " "Get things from one computer to another - safely" on \
        "python311 " "Interpreted object-oriented programming language" on \
        "qpdfview " "PDF document viewer" on \
        "rclone " "Rclone file transfer tool" on \
        "rclone-browser " "GUI rclone" on \
        "ripgrep-all " "Command line search tool" on \
        "spacefm " "SpaceFM file manager" on \
        "sudo " "Allow others to run commands as root" on \
        "tor " "Tor decentralized anonymous network" on \
        "tor-browser " "Tor Browser for FreeBSD" on \
        "vlc " "VLC multimedia player" on \
        "vscode " "Code Editor" on \
        "wayland " "Core Wayland window system code and protocol" on \
        "wget " "Retrieve files from the Net via HTTPS and FTP" on \
        "xorg " "XOrg complete distribution metaport" on \
        "youtube-dl " "YouTube video downloader" on \
        "ytdl " "YouTube downloader written in Go" on 2>$TMPFILE

choices=$(sed 's/"//g' < $TMPFILE | tr ' ' '\n')

# Install the selected programs.
for choice in $choices; do

    case $choice in
        "browsers") 
            pkg ins -y wget git
            cd /tmp
            git clone https://github.com/mrclksr/linux-browser-installer.git
            cd linux-browser-installer*
            ./linux-browser-installer install brave
            ./linux-browser-installer install chrome
            ./linux-browser-installer install opera
            ./linux-browser-installer install vivaldi
            # Run /usr/local/bin/linux-chrome, /usr/local/bin/linux-brave, /usr/local/bin/linux-vivaldi, 
            # /usr/local/bin/linux-opera to start your installed browser.
            cd ..
            rm -r linux-browser-installer*
        ;;
        "pidgin")
            pkg ins -y pidgin ; pkg ins -y pidgin-bot-sentry ; pkg ins -y pidgin-encryption ; pkg ins -y pidgin-fetion ; pkg ins -y pidgin-hotkeys ; pkg ins -y pidgin-icyque ; pkg ins -y pidgin-latex ; pkg ins -y pidgin-libnotify ; pkg ins -y pidgin-manualsize ; pkg ins -y pidgin-otr ; pkg ins -y pidgin-sipe ; pkg ins -y pidgin-skypeweb ; pkg ins -y pidgin-twitter ; pkg ins -y pidgin-window_merge
        ;;
        *)
            pkg ins -y "$choice"
        ;;
    esac

done

rm -f "$TMPFILE"

##############################################################################################
# Get Belgium Root CA4 certificate for beid middleware and Mozilla Firefox:
cd
git clone https://gitlab.com/Rijckenberg/shell-scripts
cd shell-scripts
git pull

##############################################################################################
# Performing security hardening of FreeBSD:
cd
git clone https://github.com/wravoc/harden-freebsd
cd harden-freebsd
git pull
python harden-freebsd.py

##############################################################################################
# Configure rest of FreeBSD system:
cd
rm freebsd-desktop*
wget https://git.asdf.cafe/majekla/freebsd/raw/branch/master/freebsd-desktop.sh
chmod +x freebsd-desktop.sh
./freebsd-desktop.sh
##############################################################################################
# Procedure to make Mozilla Firefox work with beid middleware package:
# 1) eid browser extension for Firefox should be uninstalled!
# 2) perform steps in following 2 websites for Firefox to manually load 'libbeidpkcs11.so' into Firefox : 
# https://eid.belgium.be/en/faq/firefox-how-do-i-install-and-activate-eid-add#7504
# and also https://www.aideacces.be/135.php?langue=EN
# 3) import shell-scripts/NixOS/common/belgiumrca4-import-in-firefox.crt into Mozilla Firefox
# That is the Belgium Root CA4 certificate
# 4) import BEID library/security device /run/current-system/sw/lib/libbeidpkcs11.so into Firefox (location of security 
# device was found by running "modutil -list"
# 5) in Firefox, enable DNS over https and choose NextDNS (https://nextdns.io/)
# 6) configure all other settings too in about:preferences#privacy in Mozilla Firefox
##############################################################################################