# Added on 2019/6/20
# Last modified on 2019/8/31
# enable ufw firewall in Ubuntu 18.04 or newer:
# restrict Internet access to certain ports
sudo apt install ufw gufw
sudo ufw status verbose
sudo ufw status > /tmp/ufw-status-old
sudo ufw disable
sudo ufw reset
# inbound rules are not needed
#sudo ufw allow in to any port 53
#sudo ufw allow in to any port 80
#sudo ufw allow in to any port 443
#sudo ufw allow in to any port 853
#sudo ufw allow in to any port 5938
# only outbound rules are required
sudo ufw allow out to any port 53
# http needed in order to get and update packages via apt command:
sudo ufw allow out to any port 80
# NTP port 123 needed to sync time:
sudo ufw allow out to any port 123
sudo ufw allow out to any port 443
# following port needed for DNS-over-TLS:
sudo ufw allow out to any port 853
# following port needed for OpenVPN UDP port:
# sudo ufw allow out to any port 1194
# following port needed so that Chrome Remote Desktop works:
sudo ufw allow out to any port 5222
# following port needed so that gpg can connect to keyserver:
sudo ufw allow out to any port 11371
sudo ufw enable
sudo ufw status verbose
sudo ufw status verbose > /tmp/ufw-status-new
# Check Listening Ports with ss
ss -mape | egrep 'tcp|udp'
# check list of established TCP and UDP connections:
lsof -i

# How to achieve similar result in Windows 10:
# In Windows Defender firewall, block all inbound connections, allow all outbound connections (default)
# Create separate outbound rule for outbound connections, blocking all the following TCP port ranges:
# 0-52, 54-79, 81-122, 124-442, 444-852, 854-1193, 1195-5937, 5939-25339, 25341-65535
# In Windows Services (services.msc):
# Enable the "OpenVPN Interactive Service"
# Disable the "OpenVPN Legacy Service"
# Disable "OpenVPNService"