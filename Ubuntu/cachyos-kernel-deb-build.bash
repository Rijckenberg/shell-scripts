#!/bin/bash
# TYPE: Bash Shell script.
# PURPOSE: This bash shell script allows you to easily install new GNU/Linux kernels in Debian based distro
# REQUIRES: bash, Debian based distro, 64-bit
# MODIFICATION DATE: 2025/2/20
cd
git clone https://github.com/Nitrux/linux-cachyos-deb
cd linux-cachyos-deb
chmod +x cachy-kernel-deb
./cachy-kernel-deb