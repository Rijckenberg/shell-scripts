#!/bin/bash
# https://www.ncsc.gov.uk/guidance/eud-security-guidance-ubuntu-1804-lts
# NCSC security hardening script for Ubuntu 22.04 LTS and 
# Raspbian GNU/Linux 10 (buster)
# Last modification date: 2023/3/4
# Modified by Mark Rijckenberg
# Run this script with root privileges

#######################################################################################################################
# perform backup of critical files
#######################################################################################################################
LogTime=$(date '+%Y-%m-%d_%Hh%Mm%Ss')
sudo cp /etc/systemd/resolved.conf  /etc/systemd/resolved.conf.$LogTime
sudo cp /etc/resolv.conf /etc/resolv.conf.$LogTime
sudo cp /etc/nsswitch.conf /etc/nsswitch.conf.$LogTime
sudo cp /etc/systemd/resolved.conf /etc/systemd/resolved.conf.$LogTime
sudo cp /etc/network/interfaces /etc/network/interfaces.$LogTime
sudo cp /etc/hosts /etc/hosts.$LogTime
sudo cp /etc/fstab /etc/fstab.$LogTime
sudo cp /etc/sysctl.conf /etc/sysctl.conf.$LogTime
#sudo cp /etc/grub.d/40_custom  /etc/grub.d/40_custom.$LogTime
#sudo cp /etc/grub.d/10_linux /etc/grub.d/10_linux.$LogTime
sudo cp /etc/default/grub /etc/default/grub.$LogTime
#sudo cp /etc/apt/apt.conf.d/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades.$LogTime
#sudo cp /etc/apt/apt.conf.d/10periodic  /etc/apt/apt.conf.d/10periodic.$LogTime
sudo cp /etc/adduser.conf  /etc/adduser.conf.$LogTime
sudo cp /etc/login.defs  /etc/login.defs.$LogTime
sudo cp /etc/default/useradd  /etc/default/useradd.$LogTime
sudo cp /etc/adduser.conf  /etc/adduser.conf.$LogTime
#sudo cp /etc/apparmor.d/usr.bin.firefox  /etc/apparmor.d/usr.bin.firefox.$LogTime
#sudo cp /etc/apparmor.d/usr.sbin.avahi-daemon /etc/apparmor.d/usr.sbin.avahi-daemon.$LogTime
#sudo cp /etc/apparmor.d/usr.sbin.dnsmasq /etc/apparmor.d/usr.sbin.dnsmasq.$LogTime
#sudo cp /etc/apparmor.d/bin.ping /etc/apparmor.d/bin.ping.$LogTime
#sudo cp /etc/apparmor.d/usr.sbin.rsyslogd /etc/apparmor.d/usr.sbin.rsyslogd.$LogTime
sudo cp /etc/audit/rules.d/tmp-monitor.rules  /etc/audit/rules.d/tmp-monitor.rules.$LogTime
sudo cp /etc/audit/rules.d/admin-home-watch.rules /etc/audit/rules.d/admin-home-watch.rules.$LogTime
sudo cp /etc/default/apport /etc/default/apport.$LogTime
sudo cp /etc/default/whoopsie /etc/default/whoopsie.$LogTime
sudo cp /etc/fwupd/uefi.conf /etc/fwupd/uefi.conf.$LogTime
sudo cp /etc/host.conf /etc/host.conf.$LogTime
sudo cp /etc/stubby/stubby.yml /etc/stubby/stubby.yml.$LogTime

# enable DNSSEC validation by replacing original /etc/stubby/stubby.yml with new one:
sudo cp stubby.yml /etc/stubby/stubby.yml

#######################################################################################################################
# show filelist before installing
#######################################################################################################################
cd
comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u) > filelist-before-installing.txt
# store start time when running script
start=$(date +%s)
#######################################################################################################################

sudo apt update

sudo dnf update
sudo dnf upgrade
# sudo apt install ansible
# sudo ansible-galaxy -c search harden
# sudo ansible-galaxy install githubixx.harden-linux
# sudo ansible-galaxy install florianutz.Ubuntu1604-CIS

#######################################################################################################################

#######################################################################################################################v
# enable new DNS over TLSv1.2 encrypted communications
# in Ubuntu 18.04 64-bit using a bash shell script
sudo apt purge unbound avahi-daemon

# uninstall insecure services
sudo apt purge xinetd nis yp-tools tftpd atftpd tftpd-hpa telnetd rsh-server rsh-redone-server

sudo dnf remove unbound
LogTime=$(date '+%Y-%m-%d_%Hh%Mm%Ss')

#sudo rm  /etc/resolv.conf
#sudo touch /etc/resolv.conf
sudo chattr -i /etc/resolv.conf
sudo chattr -i /etc/systemd/resolved.conf

cp /etc/resolv.conf $HOME/resolv.conf_$LogTime
cp /etc/nsswitch.conf $HOME/nsswitch.conf_$LogTime
cp /etc/systemd/resolved.conf $HOME/resolved.conf_$LogTime
cp /etc/network/interfaces $HOME/interfaces_$LogTime

#sudo service resolvconf stop
#sudo update-rc.d resolvconf remove

sudo apt install unattended-upgrades
sudo apt install stubby python3-pip
sudo dnf install getdns-stubby python3-pip
sudo apt install getdns-utils
sudo dnf install getdns
sudo systemctl stop stubby.service
sudo systemctl start stubby.service
systemctl status stubby
sudo netstat -lnptu | grep stubby
sudo netstat -lnptu | grep systemd-resolve

cp /etc/resolv.conf /tmp/resolv.conf
grep -v nameserver  /tmp/resolv.conf > /tmp/resolv.conf.1
grep -v domain  /tmp/resolv.conf.1 > /tmp/resolv.conf.2
grep -v options  /tmp/resolv.conf.2 > /tmp/resolv.conf.3
echo 'nameserver 127.0.0.1' >> /tmp/resolv.conf.3
# echo 'nameserver 2620:fe::fe' >> /tmp/resolv.conf.3
echo 'domain dnsknowledge.com' >> /tmp/resolv.conf.3
echo 'options rotate' >> /tmp/resolv.conf.3
sudo cp /tmp/resolv.conf.3  /etc/resolv.conf
sudo service resolvconf start

# security harden the /etc/sysctl.conf file which is used to configure kernel parameters at runtime. 
# Linux reads and applies settings from /etc/sysctl.conf at boot time.
touch /tmp/sysctl.conf
echo '# Last config changes to file on 20191107' >> /tmp/sysctl.conf
echo '# Based on following site: http://bookofzeus.com/harden-ubuntu/hardening/sysctl-conf/' >> /tmp/sysctl.conf
echo '# Controls IP packet forwarding' >> /tmp/sysctl.conf
echo 'net.ipv4.ip_forward = 0' >> /tmp/sysctl.conf
echo '# IP Spoofing protection' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.rp_filter = 1' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.rp_filter = 1' >> /tmp/sysctl.conf
echo '# Ignore ICMP broadcast requests' >> /tmp/sysctl.conf
echo 'net.ipv4.icmp_echo_ignore_broadcasts = 1' >> /tmp/sysctl.conf
echo '# Disable source packet routing' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.accept_source_route = 0' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.all.accept_source_route = 0' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.accept_source_route = 0' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_source_route = 0' >> /tmp/sysctl.conf
echo '# Ignore send redirects' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.send_redirects = 0' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.send_redirects = 0' >> /tmp/sysctl.conf
echo '# Block SYN attacks' >> /tmp/sysctl.conf
echo 'net.ipv4.tcp_syncookies = 1' >> /tmp/sysctl.conf
echo 'net.ipv4.tcp_max_syn_backlog = 2048' >> /tmp/sysctl.conf
echo 'net.ipv4.tcp_synack_retries = 2' >> /tmp/sysctl.conf
echo 'net.ipv4.tcp_syn_retries = 5' >> /tmp/sysctl.conf
echo '# Log Martians' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.log_martians = 1' >> /tmp/sysctl.conf
echo 'net.ipv4.icmp_ignore_bogus_error_responses = 1' >> /tmp/sysctl.conf
echo '# Ignore ICMP redirects' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.accept_redirects = 0' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.all.accept_redirects = 0' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.accept_redirects = 0' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_redirects = 0' >> /tmp/sysctl.conf
echo '# Ignore Directed pings' >> /tmp/sysctl.conf
echo 'net.ipv4.icmp_echo_ignore_all = 1' >> /tmp/sysctl.conf
echo '# Accept Redirects? No, this is not router' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.all.secure_redirects = 0' >> /tmp/sysctl.conf
echo '# Log packets with impossible addresses to kernel log? yes' >> /tmp/sysctl.conf
echo 'net.ipv4.conf.default.secure_redirects = 0' >> /tmp/sysctl.conf
echo '#Enable ExecShield protection' >> /tmp/sysctl.conf
echo 'kernel.exec-shield = 1' >> /tmp/sysctl.conf
echo 'kernel.randomize_va_space = 1' >> /tmp/sysctl.conf
echo '########## IPv6 networking start ##############' >> /tmp/sysctl.conf
echo '# Number of Router Solicitations to send until assuming no routers are present.' >> /tmp/sysctl.conf
echo '# This is host and not router' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.router_solicitations = 0' >> /tmp/sysctl.conf
echo '# Accept Router Preference in RA?' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_ra_rtr_pref = 0' >> /tmp/sysctl.conf
echo '# Learn Prefix Information in Router Advertisement' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_ra_pinfo = 0' >> /tmp/sysctl.conf
echo '# Setting controls whether the system will accept Hop Limit settings from a router advertisement' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.accept_ra_defrtr = 0' >> /tmp/sysctl.conf
echo '#router advertisements can cause the system to assign a global unicast address to an interface' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.autoconf = 0' >> /tmp/sysctl.conf
echo '#how many neighbor solicitations to send out per address?' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.dad_transmits = 0' >> /tmp/sysctl.conf
echo '# How many global unicast IPv6 addresses can be assigned to each interface?' >> /tmp/sysctl.conf
echo 'net.ipv6.conf.default.max_addresses = 1' >> /tmp/sysctl.conf
echo '########## IPv6 networking ends ##############' >> /tmp/sysctl.conf

sudo cp /tmp/sysctl.conf /etc/sysctl.conf
# Apply new settings
sudo sysctl -p

# configure DNS server on Ubuntu 18.04 LTS:
touch /tmp/interfaces
cp /etc/network/interfaces /tmp/interfaces
grep -v dns /tmp/interfaces > /tmp/interfaces.0
grep -v nameservers  /tmp/interfaces.0 > /tmp/interfaces.1
grep -v search  /tmp/interfaces.1 > /tmp/interfaces.2
grep -v options  /tmp/interfaces.2 > /tmp/interfaces.3
#echo 'dns-nameservers 9.9.9.9 2620:fe::fe' >> /tmp/interfaces.3
echo 'dns-nameservers 127.0.0.1' >> /tmp/interfaces.3
echo 'dns-search dnsknowledge.com' >> /tmp/interfaces.3
echo 'dns-options rotate' >> /tmp/interfaces.3
sudo cp /tmp/interfaces.3  /etc/network/interfaces

# enable systemd caching DNS resolver
rm /tmp/nsswitch.conf
rm /tmp/nsswitch.conf.1
cp /etc/nsswitch.conf /tmp/nsswitch.conf
grep -v hosts  /tmp/nsswitch.conf > /tmp/nsswitch.conf.1
# dns must be mentioned in next line, or else wget does not work
echo 'hosts: files mdns4_minimal [NOTFOUND=return] resolv dns myhostname mymachines' >> /tmp/nsswitch.conf.1
sudo cp /tmp/nsswitch.conf.1 /etc/nsswitch.conf

# set DNS server to 127.0.0.1
rm /tmp/resolved.conf
rm /tmp/resolved.conf.1
cp /etc/systemd/resolved.conf /tmp/resolved.conf
grep -v DNS  /tmp/resolved.conf > /tmp/resolved.conf.1
#echo 'DNS=9.9.9.9' >> /tmp/resolved.conf.1
echo 'DNS=127.0.0.1' >> /tmp/resolved.conf.1
echo 'DNSSEC=yes' >> /tmp/resolved.conf.1
sudo cp /tmp/resolved.conf.1 /etc/systemd/resolved.conf
sudo systemd-resolve --flush-caches
sudo systemctl restart systemd-resolved
sudo systemd-resolve --flush-caches
sudo systemd-resolve  --status

sudo chattr +i /etc/resolv.conf
sudo chattr +i /etc/systemd/resolved.conf

# start stubby on boot
sudo systemctl enable stubby

# It is probably also necessary to manually set
# the DNS server to 127.0.0.1 in the router's configuration
# and in the NetworkManager GUI

# Then reboot your PC to enable new DNS over TLSv1.2 encrypted communications
# Use wireshark application and capture encrypted DNS packages on port 853 
# There should be no more DNS handshakes on port 53 and only encrypted DNS handshakes on port 853

# Test DNSSEC validation using delv command-line tool
# See: https://www.cyberciti.biz/faq/unix-linux-test-and-validate-dnssec-using-dig-command-line/
# If DNSSEC validation is successful, the delv command output should show 
# "fully_validated"

delv +yaml pir.org
dig DNSKEY pir.org

#######################################################################################################################
# increase security of ssh keys:
#######################################################################################################################
cd
mkdir ~/.ssh
for keyfile in ~/.ssh/id_*; do ssh-keygen -l -f "${keyfile}"; done | uniq > ~/ssh-key-security-status
# DSA or RSA 1024 bits: red flag. Unsafe.
#RSA 2048: yellow recommended to change
#RSA 3072/4096: great, but Ed25519 has some benefits!
#ECDSA: depends. Recommended to change
#Ed25519: wow cool, but are you brute-force safe?
rm -rf ~/.ssh
ssh-keygen -o -a 100 -t ed25519
for keyfile in ~/.ssh/id_*; do ssh-keygen -l -f "${keyfile}"; done | uniq

# Added on January 16, 2016
# Fix openssh on Linux (vulnerability CVE-2016-0777)
# Source: http://www.cyberciti.biz/faq/howto-openssh-client-security-update-cve-0216-0777-cve-0216-0778/
echo 'UseRoaming no' | sudo tee -a /etc/ssh/ssh_config
#######################################################################################################################

#############################################################################################
# install new secure /etc/hosts file to block malware websites
#############################################################################################
LogDay=`date -I`
cd 
sudo apt install python3-lxml
sudo apt install python3.6-minimal
sudo apt install python3.7-minimal
sudo apt install python3.8-minimal
sudo apt install python3-bs4  python3-pip  dos2unix 
sudo dnf install python3-lxml
sudo dnf install python3.6-minimal
sudo dnf install python3.7-minimal
sudo dnf install python3.8-minimal
sudo dnf install python3-bs4  python3-pip  dos2unix 
sudo pip install BeautifulSoup4
sudo rm -rf hosts
git clone https://github.com/StevenBlack/hosts
cd hosts
sudo cp /etc/hosts /etc/hostsBACKUP
sudo python3 updateHostsFile.py -a -r
sudo cp hosts /etc/hosts
sudo chmod 444 /etc/hosts
cd
sudo rm -rf hosts
# copy new hosts file to Windows partition as well:
# sudo mv /media/windows/Windows/System32/drivers/etc/hosts  /media/windows/Windows/System32/drivers/etc/hosts.$LogDay.backup
# sudo cp  /etc/hosts  /media/windows/Windows/System32/drivers/etc/hosts
# convert line breaks from UNIX to DOS format:
# sudo unix2dos -o /media/windows/Windows/System32/drivers/etc/hosts

#######################################################################################################################
# compile and install newest version of openssl in Ubuntu 18.04 LTS
# cd
# sudo cp /usr/bin/c_rehash /usr/bin/c_rehash.$LogDay.backup
# sudo cp /usr/bin/openssl /usr/bin/openssl.$LogDay.backup
# sudo DEBIAN_FRONTEND=noninteractive apt update
# sudo DEBIAN_FRONTEND=noninteractive apt install  checkinstall build-essential
# sudo DEBIAN_FRONTEND=noninteractive apt build-dep  openssl
# sudo rm -rf ~/openssl
# git clone https://github.com/openssl/openssl.git
# cd openssl
# sudo ./config
# sudo make
# sudo make test
# sudo checkinstall
# sudo rm -rf ~/openssl
# sudo cp /usr/local/bin/c_rehash /usr/local/bin/c_rehash.$LogDay.backup
# sudo cp /usr/local/bin/openssl /usr/local/bin/openssl.$LogDay.backup
# sudo ln -s /usr/local/bin/c_rehash /usr/bin/c_rehash
# sudo ln -s /usr/local/bin/openssl /usr/bin/openssl
# openssl version
# apt-cache show openssl
# #######################################################################################################################


echo "Make sure to enable TPM (trusted platform module) and Secure boot in BIOS/UEFI"
echo "Checking if TPM and Secure boot are both enabled during boot:"
sudo dmesg|egrep 'ecure|tpm|TPM'

# show total installation duration:
end=$(date +%s)
runtime=$(python3 -c "print ('%u:%02u' % ((${end} - ${start})/60, (${end} - ${start})%60))")
echo "Runtime was $runtime (minutes+seconds)"

# show list of extra installed files:
cd
comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u) > filelist-after-installing.txt
echo "Here is the list of extra installed files using this bash script:"
sdiff -s filelist-before-installing.txt  filelist-after-installing.txt
sdiff -s filelist-before-installing.txt  filelist-after-installing.txt > filelist-delta.txt

