#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Last modification date: 2024/9/27

# Copy-paste following Terminal commands one by one into the Terminal:
# procedure to install printer driver for HP Laserjet 1020 without needing access to openprinting.org website:
# URL=http://sourceforge.net/projects/hplip/files/latest/download?source=files

# List of printers supported by foo2zjs printer driver:
# https://foo2zjs.linkevich.net/

sudo apt-get update
sudo apt-get remove hplip cups-filters cups hplip-data 
# temporarily uninstall system-config-printer-udev to avoid conflict with foo2zjs:
sudo apt-get remove system-config-printer-udev
sudo apt-get remove system-config-printer
# https://www.theregister.com/2024/09/26/cups_linux_rce_disclosed/
# do not reinstall cups-filters due to CVE-2024-47076 and CVE-2024-47177
sudo apt-get install build-essential tix groff dc axel cups unp vim
sudo apt-get install printer-driver-foo2zjs
sudo ln -s /usr/bin/vim /usr/bin/ex
sudo rm -rf /usr/share/hplip
cd /tmp
rm foo2zjs*
axel https://foo2zjs.linkevich.net/foo2zjs/foo2zjs.tar.gz
unp foo2zjs.tar.gz
cd foo2zjs/
make
./getweb 1020 # Get HP LaserJet 1020 firmware file
sudo make install
sudo make install-hotplug
sudo apt-get install system-config-printer-udev
sudo apt-get install system-config-printer 
# Unplug and re-plug the USB printer to the PC
# add new HP Laserjet 1020 printer via system-config-printer tool 
# and choose to use foo2zjs foomatic printer driver :
system-config-printer
# check /var/log/syslog for any errors related to foo2zjs driver:
tail /var/log/syslog
# then power cycle both your PC and your HP printer
# then retest printing using the newly installed foo2zjs driver

#HPLIPVERSION=`head download |grep label|cut -f2 -d" "`

#mv download hplip-$HPLIPVERSION.run
# after running the following command, the following question will be asked:
# 'Please choose the installation mode (a=automatic*, c=custom, q=quit) :' 
# press <ENTER> to choose (a)utomatic install
# then proceed with next Terminal commands:
#bash hplip-$HPLIPVERSION.run
#wget http://hplipopensource.com/hplip-web/plugin/hplip-$HPLIPVERSION-plugin.run
#sudo bash hplip-$HPLIPVERSION-plugin.run
#sudo hp-setup
#touch ~/.cups/lpoptions

#Troubleshooting

#If the previous procedure does not allow Ubuntu to detect the printer using
#a USB connection, then I suggest

# turning off the printer
# disconnect the printer's USB cable from the PC
# reconnect the printer's USB cable to a different USB port on the PC
# turn the printer back on
# rerun "sudo hp-setup" (last command in previous procedure)

# also try switching from the hpcups printer driver to the foomatic printer driver

 #This procedure should solve the hplip driver error "Device Communication Error, code 5012"
 # Source: https://answers.launchpad.net/hplip/+question/249391
