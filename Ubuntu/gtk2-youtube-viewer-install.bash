#!/bin/bash
# install lightweight GTK-based Youtube viewer (inspired by XenialDog 64-bit LiveUSB distro)
# Prerequisite: Ubuntu 20.04 or Debian 10
# source: https://github.com/trizen/youtube-viewer
# source:  https://mark911.wordpress.com/2018/05/02/how-to-install-gtk-youtube-viewer-from-github-source-into-ubuntu-18-04-lts-using-a-bash-shell-script/

# Please follow the instructions at https://github.com/trizen/youtube-viewer
# to update your Youtube API key in ~/.config/youtube-viewer/api.json
# in order to be able to use this program
# You will need to go to https://console.developers.google.com/apis/dashboard
# The new Google API project must be called gtk2-youtube-viewer
# The API key must be called gtk2-youtube-viewer and must be restricted to YouTube Data API v3 only.
# The OAuth 2.0 Client ID name must be called gtk2-youtube-viewer and must be defined for Desktop use.
# Make sure to put the API key, OAuth 2.0 Client ID (not gtk2-youtube-viewer, but long string of letters and numbers)
# and OAuth 2.0 Client secret in ~/.config/youtube-viewer/api.json
# For best playback performance, choose mpv as video player backend for gtk2-youtube-viewer program
cd
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv ED75B5A4483DA07C
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9A2FD067A2E3EF7B
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EA8F35793D8809A
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9D6D8F6BC857C906
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 8B48AD6246925553
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7638D0442B90D010
sudo apt update
sudo apt install youtube-dl mplayer mpv cpanminus perl git libncurses5-dev libtinfo-dev libreadline-dev pkg-config libgtk2.0-dev libgtk3-perl
sudo rm -rf youtube-viewer
git clone https://github.com/trizen/youtube-viewer
cd youtube-viewer
cpanm .
cpanm --installdeps .
cpanm --from https://cpan.metacpan.org/ CPAN ExtUtils::PkgConfig Module::Build inc::latest PAR::Dist Term::ReadLine::Gnu::XS Unicode::GCString LWP::Protocol::https Data::Dump JSON Gtk2 Gtk3 File::ShareDir LWP::UserAgent::Cached Term::ReadLine::Gnu JSON::XS Unicode::LineBreak
sudo cpanm --from https://cpan.metacpan.org/ CPAN ExtUtils::PkgConfig Module::Build inc::latest PAR::Dist Term::ReadLine::Gnu::XS Unicode::GCString LWP::Protocol::https Data::Dump JSON Gtk2 Gtk3 File::ShareDir LWP::UserAgent::Cached Term::ReadLine::Gnu JSON::XS Unicode::LineBreak
perl Build.PL --gtk2
sudo ./Build installdeps
sudo ./Build install
# install lightweight GTK-based Youtube viewer (inspired by XenialDog 64-bit LiveUSB distro)
# Please follow the instructions at https://github.com/trizen/youtube-viewer
# to update your Youtube API key in ~/.config/youtube-viewer/api.json
# in order to be able to use this program
# You will need to go to https://console.developers.google.com/apis/dashboard
# The new Google API project must be called gtk2-youtube-viewer
# The API key must be called gtk2-youtube-viewer and must be restricted to YouTube Data API v3 only.
# The OAuth 2.0 Client ID name must be called gtk2-youtube-viewer and must be defined for Desktop use.
# Make sure to put the API key, OAuth 2.0 Client ID (not gtk2-youtube-viewer, but long string of letters and numbers)
# and OAuth 2.0 Client secret in ~/.config/youtube-viewer/api.json
# For best playback performance, choose mpv as video player backend for gtk2-youtube-viewer program
cat README.md | more