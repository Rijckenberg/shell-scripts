##########################################################################################################
# install eid card reader middleware - replace codename (for example: trusty) with right Ubuntu codename
# Supported CCID readers: http://pcsclite.alioth.debian.org/ccid/section.html
##########################################################################################################
# bash shell script
# Prerequisites: Ubuntu 22.04 64-bit or newer (LiveUSB session or installed on HD/SSD),
# newest version of Mozilla Firefox, 
# Ubuntu packages in procedure below
# Not compatible with Debian
# install prerequisites for compiling eid-mw from Github
# Supported CCID readers: http://pcsclite.alioth.debian.org/ccid/section.html

sudo rm /etc/apt/sources.list.d/eid.list
sudo touch /etc/apt/sources.list.d/eid.list
sudo sh -c 'echo "deb http://files.eid.belgium.be/debian jammy main" >> /etc/apt/sources.list.d/eid.list'
sudo sh -c 'echo "deb http://files2.eid.belgium.be/debian jammy main" >> /etc/apt/sources.list.d/eid.list'
cd
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 63F7D4AFF6D61D45 A35743EA6773D225 F9FDA6BED73CDC22 3B4FE6ACC0B21F32 4E940D7FDD7FB8CC A040830F7FAC5991 16126D3A3E5C1192 
# obsolete: sudo DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:gertvdijk/opensc-backports
sudo DEBIAN_FRONTEND=noninteractive apt remove --purge beid*
sudo DEBIAN_FRONTEND=noninteractive apt update
sudo DEBIAN_FRONTEND=noninteractive apt install aptitude
sudo DEBIAN_FRONTEND=noninteractive apt install usbutils pciutils eid-mw eid-viewer apt firefox pcscd default-jre 
sudo DEBIAN_FRONTEND=noninteractive apt install opensc libacsccid1 libccid 
sudo DEBIAN_FRONTEND=noninteractive apt install libpcsclite1 libpcsclite-dev pcsc-tools ca-certificates libtool autoconf 
sudo DEBIAN_FRONTEND=noninteractive apt install automake checkinstall git
sudo DEBIAN_FRONTEND=noninteractive apt install openssl libssl-dev 
sudo DEBIAN_FRONTEND=noninteractive apt install libproxy-dev
sudo DEBIAN_FRONTEND=noninteractive apt install libxml2-dev
sudo DEBIAN_FRONTEND=noninteractive apt install libcurl4-openssl-dev
sudo DEBIAN_FRONTEND=noninteractive apt install libgtk2.0-0 
sudo DEBIAN_FRONTEND=noninteractive apt install libgtk2.0-dev
sudo DEBIAN_FRONTEND=noninteractive apt install libusb-1.0-0
sudo DEBIAN_FRONTEND=noninteractive apt install libxml++2.6-dev 
sudo DEBIAN_FRONTEND=noninteractive apt install libgtk-3-dev 
sudo DEBIAN_FRONTEND=noninteractive apt install libacr38u libacr38ucontrol0
sudo DEBIAN_FRONTEND=noninteractive apt install gcc-11-base gcc-12-base autoconf autoconf-archive autogen 
# added following missing package dependencies on July 23, 2019:
sudo DEBIAN_FRONTEND=noninteractive apt install libassuan-dev libgpg-error-dev
# added following missing package dependencies on August 9, 2021:
sudo DEBIAN_FRONTEND=noninteractive apt install libbsd-dev libmd-dev
sudo update-pciids
sudo update-usbids
# compile and install newest version of eid-mw using Github
cd
sudo rm -rf eid-mw
# uninstall to avoid conflict with gcc-11-base and gcc-12-base packages:
sudo apt purge gcc-7-base
sudo apt purge gcc-8-base 
sudo apt purge gcc-9-base
sudo apt purge gcc-10-base
git clone https://github.com/Fedict/eid-mw.git
cd eid-mw/
autoreconf -i
./configure
make
sudo checkinstall
# press 2 and change eid package name to eid-mw and hit <ENTER> key
# press 3 and change version to ISO 8601 date format (YYYYMMDD) and hit <ENTER> key
# Ensure that there are absolutely NO add-on EXTENSIONS installed in the Mozilla Firefox webbrowser
# The add-on PLUGINS like Citrix Receiver for Linux,OpenH264 and Shockwave Flash plugins can remain active in Mozilla Firefox, as they do not seem to interfere with the eid card reader.
# Close all web browser windows. Restart Mozilla Firefox browser and test eid card reader.
