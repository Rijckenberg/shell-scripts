#!/bin/bash
# TYPE: Bash Shell script.
# PURPOSE: This bash shell script allows you to easily install new GNU/Linux kernels in Ubuntu 22.04
# REQUIRES: bash, Ubuntu 22.04 64-bit or newer
# MODIFICATION DATE: 2023/1/14
# Updated by: Mark Rijckenberg
cd /tmp
sudo wget https://raw.githubusercontent.com/pimlie/ubuntu-mainline-kernel.sh/master/ubuntu-mainline-kernel.sh -O /usr/local/bin/ubuntu-mainline-kernel.sh
sudo chmod a+x /usr/local/bin/ubuntu-mainline-kernel.sh
/usr/local/bin/ubuntu-mainline-kernel.sh -l
sudo /usr/local/bin/ubuntu-mainline-kernel.sh -i
