#!/bin/bash

# Function to detect the package manager based on the Linux distribution
detect_package_manager() {
    if [ -x "$(command -v apt)" ]; then
        echo "apt"
    elif [ -x "$(command -v yum)" ]; then
        echo "yum"
    else
        echo "Package manager not found. Please install apt or yum." >&2
        exit 1
    fi
}

# Update system packages
package_manager=$(detect_package_manager)

if [ "$package_manager" == "apt" ]; then
    sudo apt update
elif [ "$package_manager" == "yum" ]; then
    sudo yum update -y
fi

# Install Python and pip
if [ "$package_manager" == "apt" ]; then
    sudo apt install python3 python3-pip -y
elif [ "$package_manager" == "yum" ]; then
    sudo yum install python3 python3-pip -y
fi

# Install Jupyter Notebook
pip3 install jupyter

# Generate Jupyter Notebook configuration file
jupyter notebook --generate-config

# Prompt for a password
read -s -p "Enter a password for Jupyter Notebook: " password
echo ""

# Set Jupyter Notebook password
hashed_password=$(python3 -c "from notebook.auth import passwd; print(passwd('$password'))")
echo "{\"NotebookApp\":{\"password\":\"$hashed_password\"}}" > ~/.jupyter/jupyter_notebook_config.json

# Configure Jupyter Notebook to listen on all IP addresses
echo "c.NotebookApp.ip = '0.0.0.0'" >> ~/.jupyter/jupyter_notebook_config.py

# Configure Jupyter Notebook to use a custom port (replace '8888' with your desired port number)
echo "c.NotebookApp.port = 8888" >> ~/.jupyter/jupyter_notebook_config.py

# Start Jupyter Notebook as a background process
nohup jupyter notebook &

echo "Jupyter Notebook has been installed and started."
echo "You can access it by navigating to http://<your_vm_public_ip>:8888 in your web browser."
