#!/bin/bash
cd /tmp
rm Spotube*.deb
wget https://github.com/KRTirtho/spotube/releases/latest/download/Spotube-linux-x86_64.deb
sudo dpkg -i Spotube*.deb
# install missing dependencies for spotube executable:
sudo apt install -f
