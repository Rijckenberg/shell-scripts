#!/bin/bash
# This script allows you to permanently enable Face ID/facial recognition instead of
# sudo password when using sudo commands
#  Based on following guide:  https://itsfoss.com/face-unlock-ubuntu/
sudo add-apt-repository ppa:boltgolt/howdy
sudo apt update
sudo apt install howdy python3-pip v4l-utils
sudo pip3 install mvt opencv-python
#######################################################################
# add correct videocamera device into /lib/security/howdy/config.ini 
DEVICENAME=`v4l2-ctl --list-devices |grep dev |head -n 1` 
echo $DEVICENAME

# configure howdy to set device_path to value stored in variable DEVICENAME
# for example: devicename might be /dev/video0
sudo howdy config

# video section of /lib/security/howdy/config.ini file could contain following lines:

# The path of the device to capture frames from 
#device_path = /dev/video0


####################################################################### 
# Use the following command to associate a face to the currently logged in user:
sudo howdy add
# List all the known face models for a user
sudo howdy list
