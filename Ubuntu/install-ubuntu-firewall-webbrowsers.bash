#!/bin/bash
# https://support.google.com/chrome/answer/1649523?hl=en

# Added on 2019/6/20
# Last modified on 2023/6/16
# Authors: Mark Rijckenberg and ChatGPT 3.5 Turbo

# Add package repositories and update package list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A2166B8DE8BDC3367D1901C11EE2FF37CA8DA16B
# sudo add-apt-repository -y ppa:apt-fast/stable
sudo add-apt-repository -y "deb [arch=amd64] https://deb.opera.com/opera-stable/ stable non-free"
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/opera.gpg] https://deb.opera.com/opera-stable/ stable non-free" | sudo tee /etc/apt/sources.list.d/opera.list
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt-get update

# install apt-fast
curl -sL https://git.io/vokNn > /tmp/1
bash /tmp/1

# install axel
sudo apt-fast install axel aria2 dirmngr ca-certificates 
sudo apt-fast install software-properties-common apt-transport-https curl -y

# enable ufw firewall in Ubuntu 22.04 or newer:
# restrict Internet access to certain ports

sudo apt-fast install ufw gufw
sudo ufw status verbose
sudo ufw status > /tmp/ufw-status-old
sudo ufw disable
sudo ufw reset
# inbound rules are not needed
#sudo ufw allow in to any port 53
#sudo ufw allow in to any port 80
#sudo ufw allow in to any port 443
#sudo ufw allow in to any port 853
#sudo ufw allow in to any port 5938
# only outbound rules are required
sudo ufw allow out to any port 53
# http needed in order to get and update packages via apt command:
sudo ufw allow out to any port 80
# NTP port 123 needed to sync time:
sudo ufw allow out to any port 123
sudo ufw allow out to any port 443
# following port needed for DNS-over-TLS:
sudo ufw allow out to any port 853
# following port needed for OpenVPN UDP port:
# sudo ufw allow out to any port 1194
# following port needed so that Citrix Workspace works:
sudo ufw allow out to any port 1494
# following port needed so that Citrix Workspace works:
sudo ufw allow out to any port 2598
# following port needed so that Chrome Remote Desktop works:
sudo ufw allow out to any port 5222
# following port needed so that Citrix Workspace works:
sudo ufw allow out to any port 7102
# following port needed so that gpg can connect to keyserver:
sudo ufw allow out to any port 11371
sudo ufw enable
sudo ufw status verbose
sudo ufw status verbose > /tmp/ufw-status-new
# Check Listening Ports with ss
ss -mape | egrep 'tcp|udp'
# check list of established TCP and UDP connections:
lsof -i
###############################################################################################################################################
wget -N http://dl.google.com/linux/direct/chrome-remote-desktop_current_amd64.deb
sudo apt-fast install ./chrome-remote-desktop_current_amd64.deb
sudo apt-fast install -f
sudo usermod -a -G chrome-remote-desktop $USER
(
echo \#\!/bin/bash
echo export LANG=ja_JP.utf-8
cat /usr/share/xsessions/*.desktop  | fgrep Exec= | cut -d= -f2-
) > $HOME/.chrome-remote-desktop-session 
echo export CHROME_REMOTE_DESKTOP_DEFAULT_DESKTOP_SIZES=1366x768 | sudo tee /etc/profile.d/chromeremote.sh

# install webbrowsers

# Install Google Chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt-fast install ./google-chrome-stable_current_amd64.deb

# Install Mozilla Firefox
sudo apt-fast install -y firefox

# Install Chromium
sudo apt-fast install -y chromium-browser

# Install Opera
curl -fsSL https://deb.opera.com/archive.key | gpg --dearmor | sudo tee /usr/share/keyrings/opera.gpg > /dev/null
sudo apt-fast install -y opera-stable

# Install Brave
sudo apt-fast install software-properties-common apt-transport-https curl ca-certificates -y
curl -s https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg | sudo gpg --dearmor | sudo tee /usr/share/keyrings/brave-browser-archive-keyring.gpg > /dev/null
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
sudo apt-fast install -y brave-browser

# Cleanup
sudo apt-fast autoremove -y
sudo apt-fast clean
sudo apt-fast autoclean

echo "Web browsers installation completed."

