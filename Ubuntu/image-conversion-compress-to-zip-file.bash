#!/bin/bash
sudo apt install imagemagick
sudo apt install inkscape
sudo apt install zip
inkscape --export-type=png *.jpg
inkscape --export-type=pdf *.jpg
inkscape --export-type=svg *.jpg
inkscape --export-type=eps *.jpg
zip -9 etsy-file.zip *
