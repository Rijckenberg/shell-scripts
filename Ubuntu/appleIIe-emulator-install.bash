#!/bin/bash
# Apple IIe emulator
# last modification date: 2025/1/31
# install instructions: https://github.com/linappleii/linapple/blob/master/INSTALL.md
sudo apt update
sudo apt install $(echo \
    build-essential \
    checkinstall \
    freeglut3-dev \
    gawk \
    git \
    graphicsmagick-imagemagick-compat \
    libcurl4-openssl-dev \
    libopenal-dev \
    libsdl-image1.2-dev \
    libsdl1.2-dev \
    libsdl2-image-2.0-0 \
    libzip-dev \
    unp \
    zlib1g-dev )
cd
sudo rm -rf apple2
sudo rm -rf linapple
git clone https://github.com/linappleii/linapple
# git clone https://github.com/mauiaaron/apple2.git
# git clone https://github.com/LasDesu/linapple.git
mkdir ~/linapple/disks
cd ~/linapple
make -j4 -e REGISTRY_WRITEABLE=1
sudo make install
cd ~/linapple
# Get ProDOS User Guide
wget --no-check-certificate  http://www.applelogic.org/files/PRODOSUM.pdf
# Get Apple IIe ROM
wget --no-check-certificate  ftp://ftp.apple.asimov.net/pub/apple_II/emulators/rom_images/apple_iie_rom.zip
wget --no-check-certificate  ftp://ftp.apple.asimov.net/pub/apple_II/emulators/rom_images/077-0018%20Apple%20IIe%20Diagnostic%20Card%20-%20English%20-%20Lower%20ROM%202764.bin
wget --no-check-certificate  ftp://ftp.apple.asimov.net/pub/apple_II/emulators/rom_images/077-0019%20Apple%20IIe%20Diagnostic%20Card%20-%20English%20-%20Upper%20ROM%202764.bin
wget --no-check-certificate  ftp://ftp.apple.asimov.net/pub/apple_II/emulators/rom_images/2764_APPLE-IIe-0341-0162-A_PAL_SWE_FIN.bin
# get newest ProDOS 2.4.1 operating system for Apple IIe (release date: September 2016!)
wget --no-check-certificate https://archive.org/download/ProDOS_2_4_1/ProDOS_2_4_1.dsk
# get newest ProDOS 3.2.1 operating system for Apple IIe (release date: August 2020)
wget --no-check-certificate https://archive.org/download/pro-dos-3-versions/ProDOS_3.21%20%28sys%29.dsk
# get Apple II games
wget --no-check-certificate  http://www.virtualapple.org/apple2/AliBaba.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/AppleInvaders_GalaxyWars_Invasion_StellarInv_SuperInv.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Aztec.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/BATTLEZONE.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Bilestoad,The.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/CastleWolfenstein\(fixed\).zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/ChampionshipLodeRunner.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Choplifter.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/DawnPatrol.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Defender.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Decathalon.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/DinoEggs.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Frogger.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Galaxian.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/HardHatMack.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Hi-ResBreakout.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Karateka.zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Mr.RobotandHisFactory\(Disk1of2\).zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/Mr.RobotandHisFactory\(Disk2of2\).zip
wget --no-check-certificate  http://www.virtualapple.org/apple2/SantaParaviaandFiumaccio.zip
unp *.zip
mv *.dsk ~/linapple/disks/
~/linapple/linapple
# Press F3 in linapple and navigate to ~/linapple/disks and select the game to load
# Then press CTRL-SHIFT-F2 to restart the linapple emulator and load the game
