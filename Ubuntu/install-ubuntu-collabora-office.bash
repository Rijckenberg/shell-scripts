#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2023/9/17
# This bash shell script is compatible with Debian 10 Buster running in Qubes OS 4
# Required free disk space for new install: at least 3.5 GB free disk space in / (root) directory
# Required free disk space for software updates: at least 2.4 GB free disk space in / (root) directory
sudo DEBIAN_FRONTEND=noninteractive apt update
sudo apt dist-upgrade
# install newest version of Collabora Office 
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0135B53B
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  E79CEF780135B53B
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys   0C54D189F4BA284D
cp /etc/apt/sources.list /tmp/sources.list
egrep -v 'ollabora' /tmp/sources.list > /tmp/sources.list.1
echo "deb https://www.collaboraoffice.com/downloads/Collabora-Office-23-Snapshot/Linux/apt ./" >>  /tmp/sources.list.1
sudo cp /tmp/sources.list.1 /etc/apt/sources.list
sudo DEBIAN_FRONTEND=noninteractive apt update
sudo DEBIAN_FRONTEND=noninteractive apt purge collabora*
sudo DEBIAN_FRONTEND=noninteractive apt install default-jre libcairo2 libxinerama1
sudo DEBIAN_FRONTEND=noninteractive apt install wget collaboraoffice-desktop
echo "Run  distrobox-export --app collaboraoffice-writer inside ubuntu-22-04 container in distrobox after installing collaboraoffice"
distrobox-export --app collaboraoffice-calc
distrobox-export --app collaboraoffice-draw
distrobox-export --app collaboraoffice-impress
distrobox-export --app collaboraoffice-math
distrobox-export --app collaboraoffice-writer
