cd  /tmp
sudo rm -rf /tmp/linux
sudo apt update
sudo apt install git build-essential fakeroot libncurses5-dev 
sudo apt install libssl-dev ccache bison flex libelf-dev dwarves pkgconf libelf-dev
git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
cd linux
rm vmlinux-gdb.py
cp /boot/config-$(uname -r) .config
scripts/config --set-str SYSTEM_TRUSTED_KEYS ""
time make oldconfig
time make clean
time make -j $(getconf _NPROCESSORS_ONLN) deb-pkg LOCALVERSION=-custom
cd ..
