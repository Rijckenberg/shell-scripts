#!/usr/bin/env bash
# Author: Mark Rijckenberg
# Latest modification date: 2024/2/15
# This bash shell script is compatible with Debian Bookworm running in Qubes OS 4 or running in podman container
# Required free disk space for new install: at least 3.5 GB free disk space in / (root) directory
# Required free disk space for software updates: at least 2.4 GB free disk space in / (root) directory

############## apt package manager install ##################################################
sudo DEBIAN_FRONTEND=noninteractive apt update
###############################################################################################

# 1) install newest version of Calligra Office suite:
sudo DEBIAN_FRONTEND=noninteractive apt purge calligra*
sudo DEBIAN_FRONTEND=noninteractive apt install calligra

# 2) install newest version of Collabora Office 
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0135B53B
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  E79CEF780135B53B
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys   0C54D189F4BA284D
cp /etc/apt/sources.list /tmp/sources.list
egrep -v 'ollabora' /tmp/sources.list > /tmp/sources.list.1
echo "deb https://www.collaboraoffice.com/downloads/Collabora-Office-23-Snapshot/Linux/apt ./" >>  /tmp/sources.list.1
sudo cp /tmp/sources.list.1 /etc/apt/sources.list
sudo DEBIAN_FRONTEND=noninteractive apt update
sudo DEBIAN_FRONTEND=noninteractive apt purge collabora*
sudo DEBIAN_FRONTEND=noninteractive apt install default-jre libcairo2 libxinerama1
sudo DEBIAN_FRONTEND=noninteractive apt install wget collaboraoffice-desktop
echo "Run  distrobox-export --app collaboraoffice-writer inside ubuntu-22-04 container in distrobox after installing collaboraoffice"
distrobox-export --app collaboraoffice-calc
distrobox-export --app collaboraoffice-draw
distrobox-export --app collaboraoffice-impress
distrobox-export --app collaboraoffice-math
distrobox-export --app collaboraoffice-writer

# 3) install newest version of LibreOffice 
# sudo DEBIAN_FRONTEND=noninteractive apt purge libreoffice*
cd
sudo DEBIAN_FRONTEND=noninteractive apt install libreoffice libreoffice-base libreoffice-core libreoffice-writer libreoffice-calc libreoffice-impress libreoffice-draw libreoffice-math
sudo DEBIAN_FRONTEND=noninteractive apt install gnome-software-plugin-flatpak flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak update
# flatpak install flathub org.libreoffice.LibreOffice

# 4) install newest version of OnlyOffice
# flatpak install flathub org.onlyoffice.desktopeditors
cd /tmp
wget https://download.onlyoffice.com/install/desktop/editors/linux/onlyoffice-desktopeditors_amd64.deb
sudo apt install ./onlyoffice-desktopeditors_amd64.deb

# 5) install newest version of WPS Office
# wget https://wdl1.pcfg.cache.wpscdn.com/wpsdl/wpsoffice/download/linux/11664/wps-office_11.1.0.11664.XA_amd64.deb
# sudo dpkg -i wps-office*
cd
sudo DEBIAN_FRONTEND=noninteractive apt install wget apt-transport-https gnupg2 software-properties-common
flatpak install flathub com.wps.Office
flatpak update
# sudo DEBIAN_FRONTEND=noninteractive apt install snapd
# snap install wps-2019-snap
flatpak remotes
flatpak search office

# 6) install newest version of Starmaker FreeOffice (= trialware)
#mkdir /tmp
#cd /tmp
#rm softmaker*
#sudo DEBIAN_FRONTEND=noninteractive apt purge softmaker-freeoffice*
#wget  https://www.softmaker.net/down/softmaker-freeoffice-2021_1050-01_amd64.deb
#sudo dpkg -i softmaker*
###################################################################################################
