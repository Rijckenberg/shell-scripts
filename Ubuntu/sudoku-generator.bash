#!/bin/bash

cd /tmp
rm *.svg
rm *.pdf
rm -rf /tmp/svg
rm -rf /tmp/pdf
mkdir /tmp/svg
mkdir /tmp/pdf

sudo apt install qqwing npm inkscape poppler-utils

git clone https://github.com/neocogent/inkscape-sudoku

qqwing --generate 1000 --one-line --difficulty any | /tmp/inkscape-sudoku/sudo2svg templates/sudo2a4.svg   page

mv /tmp/*.svg /tmp/svg

sudo npm install -g npm-check-updates
sudo npm install -g svg2pdf

for file in  /tmp/svg/*.svg
    do
        filename=$(basename "$file")
        inkscape "$file" --export-type=pdf --export-filename=/tmp/pdf/"${filename%}.pdf"
    done

pdfunite /tmp/pdf/*.pdf
