#!/bin/bash
# TYPE: Bash Shell script.
# PURPOSE: This bash shell script allows you to easily install pipewire audio service
# in Ubuntu 21.04 to replace PulseAudio and JACK
# REQUIRES: bash, Ubuntu 21.04 64-bit or newer
# MODIFICATION DATE: 2022/02/07
# Updated by: Mark Rijckenberg
sudo apt update
sudo apt install pipewire pipewire-pulse pipewire-audio-client-libraries
sudo touch /etc/pipewire/media-session.d/with-pulseaudio
sudo cp /usr/share/doc/pipewire/examples/systemd/user/pipewire-pulse.* /etc/systemd/user/
systemctl --user daemon-reload
systemctl --user --now disable pulseaudio.service pulseaudio.socket
systemctl --user --now enable pipewire pipewire-pulse
systemctl --user --now enable pipewire-media-session.service
# Then reboot your PC to start using pipewire audio service instead of pulseaudio
# "pactl info |grep ipe" Terminal command should show:
# Server Name: PulseAudio (on PipeWire x.x.xx)
# "ps -aux|grep ulse" Terminal command  should now show that
# pipewire-pulse is running

