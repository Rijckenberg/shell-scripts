#!/bin/bash
# Prerequisites: Raspberry Pi 3 or 4, Raspbian Lite / Raspberry Pi OS Lite, snapd, npm
# Prerequisites: Conbee II Zigbee USB stick
# Recommend using Raspberry Pi to avoid connection issues regarding zigbee2mqtt
# Wired ethernet connection is required during install - wireless connection did not allow 
# performing supervised Home Assistant install
# Last modification date of this script: 2021/4/6
# Author of script: Mark Rijckenberg

# First assign a fixed local IP address to the Raspberry Pi that will be running deCONZ and Home Assistant
# Define the static IP address(es) in /etc/dhcpcd.conf and in the network settings of the (wireless) router

sudo usermod -a -G dialout root
sudo gpasswd -a root dialout
sudo usermod -a -G dialout homeassistant
sudo gpasswd -a homeassistant dialout
sudo usermod -a -G dialout $USER
sudo gpasswd -a $USER dialout

# insert Conbee II Zigbee USB stick into Raspberry Pi
# run following command to determine on which serial port the Conbee II is operating: 
# It could be on /dev/ttyACM0 ...
ls -l /dev/serial/by-id
# detected devicename for Conbee II in Home Assistant:
# /dev/serial/by-id/usb-dresden_elektronik_ingenieurtechnik_GmbH_ConBee_II_DE2421421-if00

# first delete any previous installations of deconz:
sudo apt purge deconz
# Install deCONZ app for Conbee II Zigbee USB stick:
# https://github.com/marthoc/docker-deconz
# only way to get deCONZ Zigbee gateway and Conbee II Zigbee USB stick correctly detected in Home Assistant is
# by installing deCONZ via following docker image:
docker run -d --name=deconz --net=host   --restart=always -v /etc/localtime:/etc/localtime:ro  -v /opt/deconz:/root/.local/share/dresden-elektronik/deCONZ  --device=/dev/serial/by-id/usb-dresden_elektronik_ingenieurtechnik_GmbH_ConBee_II_DE2421421-if00 marthoc/deconz:stable

# deconz service and Home Assistant should all run with root account

# do not run deconz or deconz-gui as a service to avoid issues connecting to deCONZ:
# make sure deCONZ is not running on any other computer than the Raspberry Pi
sudo systemctl stop deconz
sudo systemctl stop deconz-gui
sudo systemctl disable deconz
sudo systemctl disable deconz-gui

# navigate to http://raspberrypi.local/pwa/login.html on Raspberry Pi 4
# after installing deCONZ to configure password on Phoscon-GW
# Phoscon-GW is able to detect the Livarno Lux GU10 Zigbee dimmable lights
# Lights can only get detected by Phoscon-GW during initial blinking phase

# use following instructions to install SUPERVISED version of Home Assistant:
# https://peyanski.com/how-to-install-home-assistant-supervised-official-way/#Install_Docker
# then install Home Assistant which will use deCONZ/Phoscon-GW as Zigbee gateway
# deconz and Home Assistant should all run with the root account
# Then install deCONZ Binding/Integration via Home Assistant::Configuration::Integrations
# If there are ever issues connecting to deCONZ binding via Home Assistant,
# then delete and reinstall the deCONZ integration via Home Assistant::Configuration::Integrations

# Go to Phoscon Web app via http://raspberrypi.local/pwa/login.html on Raspberry Pi 4
# Click on Phoscon app::Settings::Gateway::"Authenticate app" to allow discovery of Phoscon Gateway by
# third party application like Home Assistant
# Then use the deCONZ binding in Home Assistant to start scanning and authenticating with Phoscon Gateway using API key
# Go to Phoscon app::Settings::Gateway to update firmware on Conbee II Zigbee USB stick

# Install and start deCONZ add-on via Home Assistant 
# website::Supervisor:Add-on store  (http://raspberrypi.local:8123/hassio/store)

# Install Home Assistant app on smartphone and connect to Home Assistant gateway using local URL 
# http://raspberrypi.local:8123

# Communication flow is like this: Home Assistant app -> Home Assistant Server (backend) -> DeCONZ/Phoscon 
# Zigbee device gateway -> connected Zigbee devices

# install Mosquitto broker add-on via Home Assistant webinterface - Supervisor  -Add-on store

# install zigbee2mqtt to avoid use of any proprietary zigbee gateways/bridges (like the Aqara gateway)
# Home Assistant + Conbee II + deCONZ + zigbee2mqtt + Mosquitto broker (=Open Source MQTT broker) allows 
# controlling the Aqara Water Sensor (Zigbee) without using Aqara gateway
sudo apt update
sudo apt install snapd npm
# skip this line:sudo snap install --edge janlochi-zigbee2mqtt
# Add the repository URL under Home Assistant webinterface - Supervisor  -Add-on store - Manage add-on repositories:
# Add following URL: https://github.com/zigbee2mqtt/hassio-zigbee2mqtt
# In Home Assistant webinterface - Supervisor - Dashboard - Zigbee2MQTT - Configuration
# make sure following configuration lines exist:
# serial:
#  port: /dev/serial/by-id/usb-dresden_elektronik_ingenieurtechnik_GmbH_ConBee_II_DE2421421-if00
#  adapter: deconz
# more info here:   https://www.zigbee2mqtt.io/information/supported_adapters.html#conbee-ii


# Xiaomi Aqara SJCGQ11LM Smart Home Water Sensor IP67 Waterproof - water sensor pairing process:
# https://www.zigbee2mqtt.io/devices/SJCGQ11LM
# Phoscon-GW::Devices::Sensors(part of deCONZ) is solely responsable for manually 
# detecting the water sensor -> this worked for me

# info on how to erase configuration files if there is an unrecoverable configuration error:
# https://community.home-assistant.io/t/cant-remove-docker-containers/60659/6

# Configure tor server and Tor Hidden Service on Raspberry Pi and configure Tor Browser on client 
# (laptop or desktop pc) using following guide (replaces use of https certificates):  
# https://community.home-assistant.io/t/tor-onion-service-configuration/195171
# This will allow you to perform a secure connection to the Home Assistant webpage from a remote location
# using the Tor Browser (web browser) on a laptop or desktop PC
sudo apt install tor
sudo systemctl enable tor
sudo systemctl start tor

# install zigbee2mqtt-edge addon in Home Assistant via following link:
# https://github.com/zigbee2mqtt/hassio-zigbee2mqtt
# this will provide support for Gas Sensor "Smart Zigbee HS1CG-M Gasmelder 
# met gassensor, compatibel met ZigBee"

# install Google Drive Backup addon in Home Assistant via following link:
# https://github.com/sabeechen/hassio-google-drive-backup
 
