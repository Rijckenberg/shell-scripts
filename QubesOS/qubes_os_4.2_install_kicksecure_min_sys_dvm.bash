#!/bin/bash
# based on: https://github.com/adilTepe/minimal-kicksecure-sys-dvm/blob/main/kicksecure-min-sys-dvm.sh
# and based on: https://www.kicksecure.com/wiki/Qubes
# the kicksecure target template is a lightweight secure sys vm for Qubes OS
# bash script to generate a minimal Kicksecure disposable template for QubesOS. 
# The generated template can also be used for sys qubes (sys-net sys-usb sys-firewall). 
# I've tried to separate and explain each step so you can further customize the script to better serve your needs.
# Release date of this script: October 2023

# Kicksecure is a free and open-source Linux distribution that aims to provide a highly secure computing 
# environment. It has been developed from the ground up according to a formidable -- and time proven -- 
# defense in-depth security design. In the default configuration, Kicksecure provides superior layered 
# defenses of protection from many types of Malware.
# Kicksecure is a complete desktop operating system designed. Numerous applications come pre-installed with 
# safe defaults which can be used immediately upon installation with minimal user input.

# Protection from Targeted Malicious Updates
# Kicksecure update servers know neither the identity nor IP address of the user because all upgrades are 
# downloaded over Tor. 
# Kernel Self Protection Settings
# Kicksecure uses strong Kernel Hardening Settings as recommended by the Kernel Self Protection Project
# (KSPP). 
# Time Attack Protection
# Kicksecure defeats time attacks on its users through Boot Clock Randomization and secure network time 
# synchronization using sdwdate. 
# No Open Ports by Default
# Kicksecure provides a much lower attack surface since there are no open server ports by default unlike
# in some other Linux distributions. 
# CPU Information Leak Protection (TCP ISN)
# Without TCP ISN randomization, sensitive information about a system's CPU activity can be leaked through
# outgoing traffic, leaving it vulnerable to side-channel attacks. tirdad prevents that. 
# Available for many virtualizers
# With support for multiple virtualization options, trying out Kicksecure is easy. VMs also help contain
# and prevent the spread of malware. 

# An existing Debian version 12 (codename: bookworm) installation can be converted into Kicksecure by installing
# a Kicksecure .deb package. This procedure is also called distro-morphing.
# There is no downloadable iso yet but it will be available in the future. In the meantime install Debian on the
# host or inside a VM, then install Kicksecure on top.

SOURCE_TEMPLATE=debian-12-minimal
TARGET_TEMPLATE=kicksecure-17-min
TARGET_TEMPLATE_DISP="$TARGET_TEMPLATE"-dvm""

MEMORY_MIN=512
MEMORY_MAX=MEMORY_MIN*10

# download the source template
qvm-template install $SOURCE_TEMPLATE 

# set Belgian keyboard layout:
qvm-run --pass-io --no-gui --user=root $SOURCE_TEMPLATE "setxkbmap -layout be"
# update repositories and upgrade packages
qvm-run --pass-io --no-gui --user=root $SOURCE_TEMPLATE "apt-get update && apt-get full-upgrade -y"
# install basic packages
qvm-run --pass-io --no-gui --user=root $SOURCE_TEMPLATE "apt-get install -y qubes-core-agent-passwordless-root qubes-app-shutdown-idle qubes-core-agent-nautilus nautilus xfce4-terminal qubes-menus pulseaudio-qubes"
# set Xfce4-terminal as default
qvm-run --pass-io --no-gui --user=root $SOURCE_TEMPLATE "update-alternatives --set x-terminal-emulator /usr/bin/xfce4-terminal.wrapper"
# shutdown
qvm-shutdown --wait $SOURCE_TEMPLATE
sleep 20
# set memory limits
qvm-prefs $SOURCE_TEMPLATE memory $MEMORY_MIN
qvm-prefs $SOURCE_TEMPLATE maxmem $MEMORY_MAX
# clone the updated template
qvm-clone $SOURCE_TEMPLATE $TARGET_TEMPLATE
qvm-prefs $TARGET_TEMPLATE memory $MEMORY_MIN
qvm-prefs $TARGET_TEMPLATE maxmem $MEMORY_MAX
qvm-run --pass-io --no-gui --user=root $TARGET_TEMPLATE 'apt-get update && apt-get dist-upgrade -y'
# Some packages needed for kicksecure to install
qvm-run --pass-io --no-gui --user=root $TARGET_TEMPLATE 'apt-get install -y dkms zenity qubes-core-agent-networking qubes-mgmt-salt-vm-connector qubes-kernel-vm-support'
# install kicksecure (straight from the kicksecure website but in a bash script)
qvm-run --pass-io --no-gui --user=root $TARGET_TEMPLATE 'apt-get install -y --no-install-recommends sudo adduser'
qvm-run --pass-io --no-gui --user=root $TARGET_TEMPLATE 'addgroup --system console && adduser user console && adduser user sudo'
qvm-shutdown --wait $TARGET_TEMPLATE
sleep 20
qvm-run --pass-io --no-gui --user=user $TARGET_TEMPLATE 'sudo apt-get install -y --no-install-recommends extrepo'
# See https://www.kicksecure.com/wiki/Debian#b-kicksecure-sup-sup-for-qubes-template
qvm-run --pass-io --no-gui --user=user $TARGET_TEMPLATE 'sudo http_proxy=http://127.0.0.1:8082 https_proxy=http://127.0.0.1:8082 extrepo enable kicksecure'
qvm-run --pass-io --no-gui --user=user $TARGET_TEMPLATE 'sudo apt-get update'
qvm-run --pass-io --no-gui --user=user $TARGET_TEMPLATE 'sudo apt-get dist-upgrade -y'
qvm-run --pass-io --no-gui --user=user $TARGET_TEMPLATE 'sudo apt-get install -y --no-install-recommends kicksecure-qubes-gui'
# kicksecure post-install
qvm-run --pass-io --no-gui --user=user $TARGET_TEMPLATE 'sudo extrepo disable kicksecure'
qvm-run --pass-io --no-gui --user=user $TARGET_TEMPLATE 'sudo mv /etc/apt/sources.list ~/'
qvm-run --pass-io --no-gui --user=user $TARGET_TEMPLATE 'sudo touch /etc/apt/sources.list'
# enable kicksecure repo to stay up-to-date
qvm-run --pass-io --no-gui --user=user $TARGET_TEMPLATE 'sudo repository-dist --enable --repository stable-proposed-updates'
# A bit more hardenning
qvm-run --pass-io --no-gui --user=root $TARGET_TEMPLATE 'apt-get install -y lkrg-dkms tirdad apparmor-notify apparmor-profile-everything libpam-apparmor notification-daemon python3-notify2'
qvm-prefs -s $TARGET_TEMPLATE kernelopts "apparmor=1 security=apparmor"
# install the packages required to enable the minimal template function as a sys qube
qvm-run --pass-io --no-gui --user=root $TARGET_TEMPLATE "apt-get install -y --no-install-recommends qubes-core-agent-networking wpasupplicant qubes-core-agent-network-manager firmware-iwlwifi qubes-usb-proxy qubes-input-proxy-sender zenity policykit-1 libblockdev-crypto2 ntfs-3g qubes-core-agent-dom0-updates"

qvm-shutdown --wait $TARGET_TEMPLATE
sleep 20

# create a disposable template
qvm-create --template $TARGET_TEMPLATE --label red $TARGET_TEMPLATE_DISP 
qvm-prefs $TARGET_TEMPLATE_DISP template_for_dispvms True
qvm-features $TARGET_TEMPLATE_DISP appmenus-dispvm 1  
qvm-prefs $TARGET_TEMPLATE_DISP memory $MEMORY_MIN
qvm-prefs $TARGET_TEMPLATE_DISP maxmem $MEMORY_MAX
